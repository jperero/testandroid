package Test.Android;

import Auxiliar.*;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RunPagoTCPropias {
    /*Para correr este script se debe colocar la posici�n(empezando en 1) del producto
    Tarjetas de Cr�ditos en el archivo dp_login, tal como se muestra en Mis Productos*/
    @Before
    public void inicio() {
        Inicio inicio = new Inicio("PagoTCPropia");
        inicio.startApp();

        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +  ";  SO:" + Util.getDataDispositivo()[3] + " Versi�n:" + Util.getDataDispositivo()[4]);
        /*LOGIN*/
        TestLogin login = new TestLogin();
        login.loginTest("0");

        /*REGISTRO DE EQUIPO*/
        TestRegistroEquipo registro = new TestRegistroEquipo();
        registro.registroTest();
    }

    @Test
    public void pagoTCPropias() throws InterruptedException {
        Reporte.setNombreReporte("Pago de Tarjetas Propias");
        /*PAGO DE TC PROPIAS*/
        PagoTCPropia tcp = new PagoTCPropia();
        //0sec	1Login	2Password	3Identificacion	4TipoId	5NewPass	6Pos_TC	7Pago	8ResetPssw	9Respuesta
        tcp.iniciaPosiciones(Integer.parseInt(Util.getDataCliente()[6]));
        tcp.contraerSecciones();
        tcp.expandirSeccion();
        tcp.seleccionar_tc();

        tcp.vp_etiqueta_tc();
        double cupo_disp_antes = tcp.getCupoDisponible();
        tcp.click_boton_pagar();
        tcp.vp_etiqueta_montopagar();

        tcp.click_boton_cambiar();
        tcp.buscar_ctaDebito();

        //0sec	1Login	2Password	3Identificacion	4TipoId	5NewPass	6Pos_TC	7Pago	8ResetPssw	9Respuesta
        String montopagar = Util.getDataCliente()[7];
        tcp.ingresarMonto(montopagar);
        double saldo_cta = tcp.getSaldoCta();
        double monto_ing = tcp.getMonto();

        tcp.click_boton_Continuar();

        if (saldo_cta < monto_ing)
        {
            tcp.vp_mensaje_excede();
            tcp.click_boton_OK();
        } else {
            tcp.vp_etiqueta_confirmacion();
            double monto_ingresado = tcp.getMontoIngresado();
            tcp.ingresarConcepto();
            tcp.click_boton_Confirmar();
            tcp.vp_verifica_loader();
            tcp.vp_etiqueta_msg_exito();
            Thread.sleep(12000); //Esperar hasta que se cargue el detalle de TC
            Thread.sleep(6000); //Esperar hasta que se carguen movimientos de TC
            tcp.vp_dato_cupo(cupo_disp_antes, monto_ingresado);
        }
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}
