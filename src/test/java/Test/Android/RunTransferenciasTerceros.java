package Test.Android;

import Auxiliar.*;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.Iterator;
import java.util.List;

public class RunTransferenciasTerceros {
    @Before
    public void inicio() {
        Inicio inicio = new Inicio("TransferenciasTerceros");
        inicio.startApp();

        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +  ";  SO:" + Util.getDataDispositivo()[3] + " Versi�n:" + Util.getDataDispositivo()[4]);

        /*LOGIN*/
        TestLogin login = new TestLogin();
        login.loginTest("0");

        /*REGISTRO DE EQUIPO*/
        TestRegistroEquipo registro = new TestRegistroEquipo();
        registro.registroTest();

        /*Menu  Transferir - Transferir*/
        BarraNavegacion barra = new BarraNavegacion();
        barra.click_opc_transf();

        Menu menu = new Menu();
        //menu.presionaMenu(2); //Transferir
        menu.presionaSubmenu(2,0); //Transferir
    }

    @Test
    public void transferenciasTerceros() {
        Reporte.setNombreReporte("Transferencias a Cuentas de Terceros");

        List<String> ctas = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_ctas_terceros.txt");
        Iterator<String> itr = ctas.iterator();
        String[] campos = null;
        int linea = 0;

        Menu menu = new Menu();

        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0banco	1tipo	2numcuenta	3tipoid	4identificacion	5titular	6alias	7favorito	8descripcion	9pagar	10monto
            if (linea == 1)
                continue;

            if (campos[9].equals("N"))
                continue;

            Boolean fav = false;
            if (campos[7].equals("1"))
                fav = true;

            String alias= null;
            if (campos[6].length()>14)
                alias = campos[6].substring(0,14);
            else
                alias = campos[6];

            String montopagar = campos[10];
            double monto = Double.valueOf(montopagar);
            int ordencta = 0;

            TestTransferenciaTercero tt = new TestTransferenciaTercero();
            if (!campos[0].equals("Bolivariano")) {
                tt.setTipotransf("O");
                menu.presionaOpcionSubmenuTransf(6, true); //Otros Bancos
            }
            else{
                tt.setTipotransf("T");
                menu.presionaOpcionSubmenuTransf(4, true); //Terceros
            }
            tt.setAlias(alias);
            tt.setOrdencta(1);
            tt.setFavorito(fav);
            tt.setMonto(montopagar);
            tt.TransferenciaTerceroTest();
            ordencta = tt.getOrdencta();

            while(ordencta>0){//Hacer transferencias con todas las cuentas de origen
                tt = new TestTransferenciaTercero();
                if (!campos[0].equals("Bolivariano")) {
                    tt.setTipotransf("O");
                    menu.presionaOpcionSubmenuTransf(6, true); //Otros Bancos
                }
                else{
                    tt.setTipotransf("T");
                    menu.presionaOpcionSubmenuTransf(4, true); //Terceros
                }

                tt.setAlias(alias);
                tt.setOrdencta(ordencta);
                tt.setFavorito(fav);
                monto = monto + 5;
                montopagar = String.valueOf(monto);
                tt.setMonto(montopagar);//Va sin decimales
                tt.TransferenciaTerceroTest();
                ordencta = tt.getOrdencta(); // ordencta es 0 cuando ya lleg� a la �ltima cuenta
                System.out.println("ordencta:");
                System.out.println(ordencta);
            }
        }
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}
