/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   TRANSFERENCIAS A TERCEROS (BOLIVARIANO | OTROS BANCOS (SPI | PAGO DIRECTO))
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   AGO 26/2021
    Fecha Modificaci�n  :   N/A
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Registro Dispositivo Activo
        2. Dispositivo Seguridad Activo
        3. Producto Cuentas Activo
        4. Validaci�n de Monto (TERCEROS BOLIVARIANO Y OTROS BANCOS (SPI | PAGO DIRECTO):
            4.1. TERCEROS (BOLIVARIANO | OTROS BANCOS (SPI | PAGO DIRECTO)):
                 4.2.1. Valor monto m�nimo de $5.00  (Solo Pago Directo)
                 4.2.2. Valor monto m�ximo de $10000.00 (Ordenante | Receptor)
                 4.2.3. Valor excede saldo disponible
                 4.2.4. Valor cupo disponible
        5. Validaci�n de bloqueo de cuenta
        6. Validaci�n de error de d�bito (Disminuir el saldo disponible durante la confirmaci�n)

        Subflujos:
        7. Matriculaci�n de Beneficiario (TERCEROS)
 */

package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Iterator;
import java.util.List;

public class RunTransferenciasTercerosREF {
    //List<String> datosMonto = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_Transferencias_-_monto.txt");
    List<String> datosTransferencias = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_transferencias.txt");
    Iterator<String> itrTransferencias = datosTransferencias.iterator();
    String[] campos = null;
    int linea = 0;
    String escenario = "10";     // Secuencia de Datos a Ejecutarse del Datapool:
    // 0 Propias Exitoso
    // 1 Terceros Exitoso
    // 2 Terceros Bloqueo
    // 3 Terceros Monto
    // 4 Otros Bancos SPI Exitoso
    // 5 Otros Bancos SPI Bloqueo
    // 6 Otros Bancos SPI Monto
    // 7 Otros Bancos Pago Directo Exitoso
    // 8 Otros Bancos Pago Directo Bloqueo
    // 9 Otros Bancos Pago Directo Monto
    // 10 Otros Bancos Pago Directo Monto (Valor Menor a 5.00 USD - Requiere ambiente con BANRED)
    TestLogin login = new TestLogin();
    String testName;
    String className;

    @Rule
    public TestWatcher watcher = new TestWatcher() {
        public void starting(Description description) {
            testName = description.getMethodName();
            className = description.getClassName();
        }
    };

    @Before
    public void seleccionMenuTransferencias() {
        Inicio inicio = new Inicio("RunTransferenciasTercerosREF");
        inicio.startApp();

        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +
                ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4]);

        if (testName.equals("transferenciasTercerosValidacionSinRegEquipo")) {
            login.registroEquipo = false; //Presiona "No Gracias"
        }
        if ((!testName.equals("transferenciasTransaccionesExitosas"))
                && (!testName.equals("transferenciasValidacionMonto"))
                && (!testName.equals("transferenciasPropiasValidacionSoloUnaCuenta"))) {
            login.loginTest("50");
            Menu menu = new Menu();
            menu.presionaSubmenu(2, 0); //Transferir
        }
    }

    @Test //0905317251  Stallone.BB99   P@ssw5rd    78206   56
    public void transferenciasPropiasValidacionSoloUnaCuenta() {
        Reporte.setNombreReporte("TRANSFERENCIAS PROPIAS - Validaci�n Solo Una Cuenta Transferir");
        login.loginTest("56");
        Menu menu = new Menu();
        menu.presionaSubmenu(2, 0); //Transferir
        TransferenciaTercerosREF transferTercero = new TransferenciaTercerosREF();
        transferTercero.caso = "TRANSFERENCIAS PROPIAS";
        transferTercero.seleccion_tipo_transferencia("Propias");
        transferTercero.mensaje_error("No tienes cuentas asociadas para realizar esta transferencia");
    }

    @Test //1201184973	Zamba.BB99	P@ssw2rd	37426   37
    public void transferenciasTercerosValidacionSinProdCuentas() {
        Reporte.setNombreReporte("TRANSFERENCIAS TERCEROS -  Validaci�n Producto Cuentas");
        TransferenciaTercerosREF transferTercero = new TransferenciaTercerosREF();
        transferTercero.caso = "TRANSFERENCIA A TERCEROS";
        transferTercero.vp_etiqueta_cabecera("TRANSFERENCIAS");
        transferTercero.mensajeValidacion("Producto Cuentas", "No posees cuenta para realizar transferencias");
    }

    @Test //0915649743	Roboto.78	P@ssW2RD 	277295  42
    public void transferenciasTercerosValidacionSinRegEquipo() {
        Reporte.setNombreReporte("TRANSFERENCIAS TERCEROS - Validaci�n Sin Registro Equipo");
        TransferenciaTercerosREF transferTercero = new TransferenciaTercerosREF();
        transferTercero.caso = "TRANSFERENCIA A TERCEROS";
        transferTercero.vp_etiqueta_cabecera("TRANSFERENCIAS");
        transferTercero.mensajeValidacion("Registro de Equipo", "Para transaccionar debes registrar tu dispositivo m�vil");
    }

    @Test //0909956872	Wess.BB99	P@ssw0rd	19155   50
    public void transferenciasTercerosValidacionSinDispositivoSeguridad() {
        Reporte.setNombreReporte("TRANSFERENCIAS TERCEROS - Validaci�n Sin Dispositivo de Seguridad");
        TransferenciaTercerosREF transferTercero = new TransferenciaTercerosREF();
        transferTercero.caso = "TRANSFERENCIA A TERCEROS";
        transferTercero.vp_etiqueta_cabecera("TRANSFERENCIAS");
        transferTercero.mensajeValidacion("Dispositivo de Seguridad", "Para realizar esta transacci�n debes activar tu Clave Virtual");
    }

    @Test //1200724563	Arce.BB99	P@ssw0rd	19686   53
    public void transferenciasValidacionMonto() {
        //Escenarios: 2,3,5,6,8,9,10
        Reporte.setNombreReporte("TRANSFERENCIAS - Validaci�n Monto");
        login.loginTest("53");
        Menu menu = new Menu();
        menu.presionaSubmenu(2, 0); //Transferir
        TransferenciaTercerosREF transferTercero = new TransferenciaTercerosREF();
        while (itrTransferencias.hasNext()) {
            campos = itrTransferencias.next().split("\t");
            linea = linea + 1;/*0SECUENCIA 1TRANSFERENCIA 2VIA 3ESCENARIO 4FAVORITO 5TRANSFIERE 6LOGIN 7ORIGEN 8DESTINO 9ALIAS 10MONTO 11CONCEPTO 12COMENTARIOS*/
            if (linea == 1) {
                continue;
            }
            transferTercero.caso = (campos[2].equals("No Aplica") ? "TRANSFERENCIAS " + campos[1] : "TRANSFERENCIAS " + campos[1] + " - " + (campos[2].equals("Otros Bancos") ? "SPI" : campos[2])).toUpperCase();
            if (campos[0].equals(escenario)) {
                transferTercero.seleccion_tipo_transferencia(campos[1]);
                transferTercero.vp_etiqueta_cabecera("CUENTA DESTINO");
                if (campos[4].equals("N")) {
                    transferTercero.seccion_plegar();
                }
                if (!campos[9].equals("No Aplica")) {
                    transferTercero.seleccion_beneficiario(campos[9]);
                } else {
                    transferTercero.seleccion_beneficiario(campos[8]);
                }
                if (transferTercero.via_transferencia().equals("TRANSFERIR POR")) {
                    transferTercero.vp_etiqueta_cabecera("TRANSFERIR POR");
                    transferTercero.seleccion_via_transferencia(campos[2]);
                }
                transferTercero.vp_etiqueta_cabecera("MONTO A TRANSFERIR");
                transferTercero.click_boton_Cambiar();
                transferTercero.vp_etiqueta_cabecera("CUENTA ORIGEN");
                transferTercero.seleccion_ordenante(campos[7]);
                transferTercero.vp_etiqueta_cabecera("MONTO A TRANSFERIR");
                transferTercero.ingreso_monto(campos[10], campos[12]);
                transferTercero.click_boton_Continuar();
                if (campos[12].equals("Valor excede al saldo disponible")) {
                    transferTercero.mensaje_error(campos[12]);
                    transferTercero.click_boton_Cancelar();
                    menu.presionaSubmenu(2, 0);
                } else {
                    transferTercero.vp_etiqueta_cabecera("CONFIRMACI�N");
                    transferTercero.validar_Datos(campos[11]);
                    transferTercero.click_boton_Transferir();
                    transferTercero.mensaje_error(campos[12]);
                }
            }
        }
    }

    @Test//1200724563	Arce.BB99	P@ssw0rd	19686   53
    public void transferenciasTransaccionesExitosas() {
        //Escenarios: 0,1,4,7
        Reporte.setNombreReporte("TRANSFERENCIAS - Exitoso");
        login.loginTest("53");
        Menu menu = new Menu();
        menu.presionaSubmenu(2, 0); //Transferir
        TransferenciaTercerosREF transferTercero = new TransferenciaTercerosREF();
        OTP otp = new OTP();

        while (itrTransferencias.hasNext()) {
            campos = itrTransferencias.next().split("\t");
            linea = linea + 1;/*0SECUENCIA 1TRANSFERENCIA 2VIA 3ESCENARIO 4FAVORITO 5TRANSFIERE 6LOGIN 7ORIGEN 8DESTINO 9ALIAS 10MONTO 11CONCEPTO 12COMENTARIOS*/
            if (linea == 1) {
                continue;
            }
            transferTercero.caso = (campos[2].equals("No Aplica") ? "TRANSFERENCIAS " + campos[1] : "TRANSFERENCIAS " + campos[1] + " - " + (campos[2].equals("Otros Bancos") ? "SPI" : campos[2])).toUpperCase();
            if (campos[0].equals(escenario)) {
                transferTercero.seleccion_tipo_transferencia(campos[1]);
                transferTercero.vp_etiqueta_cabecera("CUENTA DESTINO");
                if (campos[4].equals("N")) {
                    transferTercero.seccion_plegar();
                }
                if (!campos[9].equals("No Aplica")) {
                    transferTercero.seleccion_beneficiario(campos[9]);
                } else {
                    transferTercero.seleccion_beneficiario(campos[8]);
                }
                if (transferTercero.via_transferencia().equals("TRANSFERIR POR")) {
                    transferTercero.vp_etiqueta_cabecera("TRANSFERIR POR");
                    transferTercero.seleccion_via_transferencia(campos[2]);
                }
                transferTercero.vp_etiqueta_cabecera("MONTO A TRANSFERIR");
                transferTercero.click_boton_Cambiar();
                transferTercero.vp_etiqueta_cabecera("CUENTA ORIGEN");
                transferTercero.seleccion_ordenante(campos[7]);
                transferTercero.vp_etiqueta_cabecera("MONTO A TRANSFERIR");
                transferTercero.ingreso_monto(campos[10]);
                transferTercero.click_boton_Continuar();
                transferTercero.vp_etiqueta_cabecera("CONFIRMACI�N");
                transferTercero.validar_Datos(campos[11]);
                transferTercero.click_boton_Transferir();
                if (campos[4].equals("N")) {
                    otp.genera_valida_Otp();
                }
                transferTercero.vp_etiqueta_cabecera("CONFIRMACI�N");
                transferTercero.click_boton_Continuar();
                menu.presionaSubmenu(2, 0);
            }
        }
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}
