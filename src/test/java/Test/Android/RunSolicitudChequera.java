package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RunSolicitudChequera {
	@Before
	public void Iniciar()
	{
		Inicio inicio = new Inicio("RunSolicitudChequera");
		inicio.startApp();

		Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +  ";  SO:" + Util.getDataDispositivo()[3] + " Versi�n:" + Util.getDataDispositivo()[4]);
		/*LOGIN*/
		TestLogin login = new TestLogin();
		login.registroEquipo = true;
		login.loginTest("0");

		Menu opcMenu = new Menu();
		opcMenu.presionaMenu(6);
	}

	@Test
	public void AlguienMasIra()  {
		Reporte.setNombreReporte("Solicitud de Chequera Exitoso");

		Boolean flagContinua;

		/*SOLICITAR CHEQUERA*/
		SolicitarChequera chequera = new SolicitarChequera();
		//chequera.solicitarChequera();
		chequera.validaciones();
		chequera.vp_etiqueta_tipochequera();
		chequera.seleccionarTipoChequera(); //Pantalla de Selecci�n Tipo Chequera
		chequera.vp_etiqueta_cuentacte();
		chequera.seleccionarCuentaCTE();    //Pantalla de Selecci�n Cuenta Corriente
		chequera.vp_etiqueta_oficina();
		chequera.seleccionarOficina("CENTENARIO");      //Pantalla de Selecci�n Oficina a Retirar

		chequera.vp_etiqueta_quienretira();
		chequera.seleccionar_alguienmasira();

		flagContinua = chequera.validar_mensaje_cupo();  //Pantalla de Selecci�n Persona Autorizada a Retirar

		if (flagContinua) {
			chequera.vp_etiqueta_personautoriza();
			chequera.cargaDatosPersonaAutorizada();
			chequera.click_boton_continuar();
			chequera.validar_mensaje_cedula();
			chequera.vp_etiqueta_confirmacion();       //Pantalla de Confirmaci�n
			chequera.click_boton_solicitar();
			OTP otp = new OTP();       //Pantalla de Selecci�n Medios de Envio e Ingreso de OTP
			otp.genera_valida_Otp();
			chequera.validar_mensaje_transaccion();    //Pantalla de Mensaje Exitoso
		}
	}

	@After
	public void tearDown() {
		Reporte.finReporte();
	}
}
