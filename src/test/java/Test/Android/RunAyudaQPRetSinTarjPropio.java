/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   AYUDA - QUICKPAY RETIRO SIN TARJETA (PROPIO)
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   SEPT 29/2021
    Fecha Modificaci�n  :   N/A
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Registro Dispositivo Activo
        2. Dispositivo Seguridad Activo
        3. Producto Cuentas Activo
        4. Servicio de QuickPay Activo
        5. Validaci�n de Monto (PROPIO):
            5.1. PROPIAS:
                 5.1.1. Valor m�ltiplo de $10.00
                 5.1.2. Valor monto m�nimo de $10.00
                 5.1.3. Valor monto m�ximo de $1000.00
                 5.1.4. Valor excede saldo disponible
 */
package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Iterator;
import java.util.List;

public class RunAyudaQPRetSinTarjPropio {
    List<String> datosMonto = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_RetiroSinTarjetaQP_-_montoQP.txt");
    Iterator<String> itrMonto = datosMonto.iterator();
    String[] campos = null;
    int linea = 0;

    TestLogin login = new TestLogin();

    String testName;
    String className;
    String usuario = "";

    @Rule
    public TestWatcher watcher = new TestWatcher() {
        public void starting(Description description) {
            testName = description.getMethodName();
            className = description.getClassName();
        }
    };

    public String usuarioLoginTest(String testName) {
        switch (testName) {
            case "ayudaRetiroSinTarjetaValidacionSinServicioActivo":
                usuario = "48";
                break;
            case "ayudaRetiroSinTarjetaValidacionSinDispositivoSeguridad":
                usuario = "50";
                break;
            case "ayudaRetiroSinTarjetaValidacionSinRegEquipo":
                login.registroEquipo = false;
                usuario = "31";
                break;
            case "ayudaRetiroSinTarjetaValidacionSinProdCuentas":
                usuario = "37";
                break;
            case "ayudaRetiroSinTarjetaValidacionMontos":
                usuario = "42";
                break;
            default:
                usuario = "0";
                break;
        }
        return usuario;
    }

    @Before
    public void seleccionMenuAyudaQPRetSinTarjPropio() {
        Inicio inicio = new Inicio("Ayuda_QPRetSinTarjPropio");
        inicio.startApp();
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] + ";  SO:"
                + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4]);
        login.activoClave = false;

        login.loginTest(usuarioLoginTest(testName));
        Menu menu = new Menu();
        menu.presionaMenuNuevo(13);//Otros > Ayuda
        Ayuda ayuda = new Ayuda();
        ayuda.caso = "AYUDA";
        ayuda.vp_etiqueta_cabecera("AYUDA");
        ayuda.click_boton_Opcion(3,"Necesito efectivo y no tengo mi tarjeta"); //Necesito efectivo y no tengo mi tarjeta
    }

    @Test //0401320072	Bank.BB99	P@ssw0rd	480051  48
    public void ayudaRetiroSinTarjetaValidacionSinServicioActivo() {
        Reporte.setNombreReporte("AYUDA - Validaci�n Sin Servicio Activo: <br>" +
                "<ul style='font-size: 20px'>" +
                "<li>QUICKPAY RETIRO SIN TARJETA (PROPIO) - Necesito efectivo y no tengo mi tarjeta</li>" +
                "</ul>");
        RetiroSinTarjetaQP retiroSinTarjetaQP = new RetiroSinTarjetaQP();
        retiroSinTarjetaQP.mensajeValidacion("Servicio Activo QuickPay - Retiro Sin Tarjeta", "No tienes activo el servicio de QuickPay");
        retiroSinTarjetaQP.click_boton_Cancelar();
    }

    @Test //0909956872	Wess.BB99	P@ssw0rd	19155   50
    public void ayudaRetiroSinTarjetaValidacionSinDispositivoSeguridad() {
        Reporte.setNombreReporte("AYUDA - Validaci�n Sin Dispositivo de Seguridad: <br>" +
                "<ul style='font-size: 20px'>" +
                "<li>QUICKPAY RETIRO SIN TARJETA (PROPIO) - Necesito efectivo y no tengo mi tarjeta</li>" +
                "</ul>");
        RetiroSinTarjetaQP retiroSinTarjetaQP = new RetiroSinTarjetaQP();
        retiroSinTarjetaQP.vp_etiqueta_cabecera();
        retiroSinTarjetaQP.mensajeValidacion("Dispositivo de Seguridad", "Para realizar esta transacci�n debes activar tu Clave Virtual");
    }

    @Test //1306669001	Big.BB99 	P@ssw2rd	609941  31
    public void ayudaRetiroSinTarjetaValidacionSinRegEquipo() {
        Reporte.setNombreReporte("AYUDA - Validaci�n Sin Registro Equipo: <br>" +
                "<ul style='font-size: 20px'>" +
                "<li>QUICKPAY RETIRO SIN TARJETA (PROPIO) - Necesito efectivo y no tengo mi tarjeta</li>" +
                "</ul>");
        RetiroSinTarjetaQP retiroSinTarjetaQP = new RetiroSinTarjetaQP();
        retiroSinTarjetaQP.vp_etiqueta_cabecera();
        retiroSinTarjetaQP.mensajeValidacion("Registro de Equipo", "Para transaccionar debes registrar tu dispositivo m�vil");
    }

    @Test //1201184973	Zamba.BB99	P@ssw2rd	37426   37
    public void ayudaRetiroSinTarjetaValidacionSinProdCuentas() {
        Reporte.setNombreReporte("AYUDA - Validaci�n Producto Cuentas: <br>" +
                "<ul style='font-size: 20px'>" +
                "<li>QUICKPAY RETIRO SIN TARJETA (PROPIO) - Necesito efectivo y no tengo mi tarjeta</li>" +
                "</ul>");
        RetiroSinTarjetaQP retiroSinTarjetaQP = new RetiroSinTarjetaQP();
        retiroSinTarjetaQP.vp_etiqueta_cabecera();
        retiroSinTarjetaQP.mensajeValidacion("Producto Cuentas", "No posees cuenta para realizar esta transacci�n");
    }

    @Test //0915649743	Roboto.78	P@ssW2RD 	277295  42
    public void ayudaRetiroSinTarjetaValidacionMontos() {
        Reporte.setNombreReporte("AYUDA - Validaciones de Monto: <br>" +
                "<ul style='font-size: 20px'>" +
                "<li>QUICKPAY RETIRO SIN TARJETA (PROPIO) - Necesito efectivo y no tengo mi tarjeta</li>" +
                "<li>Valor m�ltiplo de $10.00</li>" +
                "<li>Valor monto m�nimo de $10.00</li>" +
                "<li>Valor monto m�ximo de $1500.00</li>" +
                "<li>Valor excede saldo disponible</li>" +
                "<li>Valor v�lido</li>" +
                "</ul>");

        RetiroSinTarjetaQP retiroSinTarjetaQP = new RetiroSinTarjetaQP();

        while (itrMonto.hasNext()) {
            campos = itrMonto.next().split("\t");
            linea = linea + 1; //0Sec	1Escenario	2Valor	3Validacion
            if (linea == 1)
                continue;
            retiroSinTarjetaQP.vp_etiqueta_cabecera_monto();
            retiroSinTarjetaQP.ingreso_monto(campos[2], campos[3]);
            retiroSinTarjetaQP.click_boton_Continuar();
            if (!campos[3].equals("Valor v�lido")) {
                retiroSinTarjetaQP.mensaje_error(campos[1], campos[3]);
                retiroSinTarjetaQP.borra_monto(campos[2], campos[3]);
            } else {
                retiroSinTarjetaQP.vp_etiqueta_cabecera_confirmacion();
                retiroSinTarjetaQP.click_boton_Retirar_Monto();
                retiroSinTarjetaQP.vp_etiqueta_cabecera_clave();
                retiroSinTarjetaQP.click_boton_Aceptar();
                Ayuda ayuda = new Ayuda();
                ayuda.vp_etiqueta_cabecera("AYUDA");
            }
        }
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}
