/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   CLAVE VIRTUAL - DESBLOQUEO
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   AGO 11/2021
    Fecha Modificaci�n  :   N/A
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Validaci�n Clave Virtual No Bloqueada
        2. Validaci�n Clave Virtual Desbloqueo
        3. Validaci�n Base Conocimiento (Preguntas / Respuestas) - Fallido (3 intentos)
 */
package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.ClaveVirtualOpciones;
import Pom.Android.Inicio;
import Pom.Android.Menu;
import Pom.Android.TestLogin;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RunClaveVirtualDesbloqueoMetodoIngreso {
    @Before
    public void seleccionMenuClaveVirtual() {
        Inicio inicio = new Inicio("ClaveVirtualOpciones");
        inicio.startApp();
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] + ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4]);
        TestLogin login = new TestLogin();
        login.activoClave = false;
        login.loginTest("53");
        Menu menu = new Menu();
        menu.presionaMenuNuevo(7);
    }

    @Test
    public void desbloqueoClaveVirtualNoBloqMetIngreso() {
        Reporte.setNombreReporte("CLAVE VIRTUAL - Validaci�n de M�todo de Ingreso No Bloqueado");
        ClaveVirtualOpciones claveVirtualDesbloqueoMetIngreso = new ClaveVirtualOpciones();
        claveVirtualDesbloqueoMetIngreso.textoFlujo = "CLAVE VIRTUAL - DESBLOQUEAR INGRESO";
        claveVirtualDesbloqueoMetIngreso.click_boton_Opcion(9);
        claveVirtualDesbloqueoMetIngreso.mensajeValidacion("Tienes activo el ingreso para generar la clave virtual");
    }

    @Test
    public void desbloqueoClaveVirtualDesbloquearIngreso() {
        Reporte.setNombreReporte("CLAVE VIRTUAL - Validaci�n de Desbloqueo M�todo de Ingreso Exitoso");
        ClaveVirtualOpciones claveVirtualDesbloqueoMetIngreso = new ClaveVirtualOpciones();
        claveVirtualDesbloqueoMetIngreso.textoFlujo = "CLAVE VIRTUAL - DESBLOQUEAR INGRESO";
        claveVirtualDesbloqueoMetIngreso.click_boton_Opcion(9);
        claveVirtualDesbloqueoMetIngreso.vp_etiqueta_cabecera("PREGUNTA DE SEGURIDAD");
        claveVirtualDesbloqueoMetIngreso.pregunta_transaccional();
        claveVirtualDesbloqueoMetIngreso.mensajeValidacion("�Desbloqueo Exitoso!");
        claveVirtualDesbloqueoMetIngreso.vp_etiqueta_cabecera("CLAVE VIRTUAL");
    }

    @Test
    public void desbloqueoClaveVirtualBaseConocimientoFallida() {
        Reporte.setNombreReporte("CLAVE VIRTUAL - Validaci�n de Base Conocimiento (Superar Intentos Fallidos)");
        ClaveVirtualOpciones claveVirtualDesbloqueoMetIngreso = new ClaveVirtualOpciones();
        claveVirtualDesbloqueoMetIngreso.textoFlujo = "CLAVE VIRTUAL - DESBLOQUEAR INGRESO";
        claveVirtualDesbloqueoMetIngreso.click_boton_Opcion(9);
        claveVirtualDesbloqueoMetIngreso.vp_etiqueta_cabecera("PREGUNTA DE SEGURIDAD");
        for (int i = 0; i < 3; i++) {
            claveVirtualDesbloqueoMetIngreso.ingreso_respuesta(("ERROR" + i));
            claveVirtualDesbloqueoMetIngreso.mensaje_popup_Error(i);
            if (i != 2) {
                claveVirtualDesbloqueoMetIngreso.click_boton_Cambio_Pregunta();
            }
        }
        claveVirtualDesbloqueoMetIngreso.vp_etiqueta_cabecera("CLAVE VIRTUAL");
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}

