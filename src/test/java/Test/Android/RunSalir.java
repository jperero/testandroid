/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   QUICKPAY - RETIRO SIN TARJETA (PROPIO | TERCEROS)
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   ENE 25/2021
    Fecha Modificaci�n  :   FEB 03/2021 (REFACTORING - CARGA DE DATOS TERCEROS)
                            JUN 21/2021 (REFACTORING - CAMBIO EN METODOLOGIA DE QA)
                            JUL 09/2021 (REFACTORING - QUICKPAY PROPIO)
                            JUL 13/2021 (REFACTORING - QUICKPAY PROPIO VALIDACION MONTO DATAPOOL)
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Registro Dispositivo Activo
        2. Dispositivo Seguridad Activo
        3. Producto Cuentas Activo
        4. Servicio de QuickPay Activo
        5. Validaci�n de Monto (PROPIO | TERCEROS):
            5.1. PROPIAS:
                 5.1.1. Valor m�ltiplo de $10.00
                 5.1.2. Valor monto m�nimo de $10.00
                 5.1.3. Valor monto m�ximo de $1000.00
                 5.1.4. Valor excede saldo disponible
        6. Validaci�n de error de d�bito (Disminuir el saldo disponible durante la confirmaci�n)
 */
package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.BarraNavegacion;
import Pom.Android.Inicio;
import Pom.Android.Salir;
import Pom.Android.TestLogin;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

public class RunSalir {
    List<String> datosMonto = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_RetiroSinTarjetaQP_-_montoQP.txt");
    Iterator<String> itrMonto = datosMonto.iterator();
    String[] campos = null;
    int linea = 0;

    TestLogin login = new TestLogin();

    @Before
    public void inicioSalir() {
        Inicio inicio = new Inicio("RunSalir.class");
        inicio.startApp();
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +
                ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4]);
        login.loginTest("53");
    }
    @Test
    public void menuOtrosSalir(){
        BarraNavegacion barra = new BarraNavegacion();
        Salir salirAPP = new Salir();
        barra.click_opc_otros();
        salirAPP.otrosSalirAPP();
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}
