/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   AYUDA - BLOQUEO TARJETA D�BITO / CR�DITO
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   SEPT 29/2021
    Fecha Modificaci�n  :   N/A
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Registro Dispositivo Activo
        2. Dispositivo Seguridad Activo
        3. Producto Tarjetas (D�bito / Cr�dito) Activo
        5. Validaci�n de Bloqueo de Tarjeta:
            5.1. DEBITO:
                 5.1.1. P�rdida
                 5.1.2. Robo
            5.2. CR�DITO:
                 5.1.1. P�rdida
                 5.1.2. Robo
 */
package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;


public class RunAyudaBloqueoTarjeta {
    TestLogin login = new TestLogin();

    String testName;
    String className;
    String usuario = "";

    @Rule
    public TestWatcher watcher = new TestWatcher() {
        public void starting(Description description) {
            testName = description.getMethodName();
            className = description.getClassName();
        }
    };

    public String usuarioLoginTest(String testName) {
        switch (testName) {
            case "ayudaBlqTarjValidacionSinDispositivoSeguridad":
                usuario = "50";
                break;
            case "ayudaBlqTarjValidacionSinRegEquipo":
                login.registroEquipo = false;
                usuario = "31";
                break;
            case "ayudaBlqTarjValidacionSinProdTarjetas":
                usuario = "38";
                break;
            case "ayudaBlqTarjDebito":
                usuario = "55";
                break;
            case "ayudaBlqTarjCredito":
                usuario = "57";
                break;
            default:
                usuario = "0";
                break;
        }
        return usuario;
    }

    @Before
    public void seleccionMenuAyudaBloqueoTarjeta() {
        Inicio inicio = new Inicio("Ayuda_BloqueoTarjeta");
        inicio.startApp();
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] + ";  SO:"
                + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4]);
        login.activoClave = false;

        login.loginTest(usuarioLoginTest(testName));
        Menu menu = new Menu();
        menu.presionaMenuNuevo(13);//Otros > Ayuda
        Ayuda ayuda = new Ayuda();
        ayuda.caso = "AYUDA";
        ayuda.vp_etiqueta_cabecera("AYUDA");
        ayuda.click_boton_Opcion(2,"Me han robado o no encuentro mi tarjeta"); //Me han robado o no encuentro mi tarjeta
    }

    @Test //0909956872	Wess.BB99	P@ssw0rd	19155   50
    public void ayudaBlqTarjValidacionSinDispositivoSeguridad() {
        Reporte.setNombreReporte("AYUDA - Validaci�n Sin Dispositivo de Seguridad: <br>" +
                "<ul style='font-size: 20px'>" +
                "<li>ADMINISTRAR TARJETAS - Me han robado o no encuentro mi tarjeta</li>" +
                "</ul>");
    }

    @Test //1306669001	Big.BB99 	P@ssw2rd	609941  31
    public void ayudaBlqTarjValidacionSinRegEquipo() {
    }

    @Test //0930864111	Thor.BB99 	F4zzio78@	1345006 38
    public void ayudaBlqTarjValidacionSinProdTarjetas() {
    }

    @Test //1203064702	Venom.BB99	P@ssw0rd	37657   55
    public void ayudaBlqTarjDebito() {
        Reporte.setNombreReporte("AYUDA - Bloqueo de Tarjeta de D�bito");
        Ayuda ayuda = new Ayuda();
        ayuda.caso = "AYUDA";
        OTP otp = new OTP();
        ayuda.vp_etiqueta_cabecera("ADMINISTRAR TARJETAS");
        ayuda.click_boton_Opcion("Reportar como Perdida/Robada.");
        ayuda.vp_etiqueta_cabecera("REPORTAR");
        ayuda.seleccionar_motivo_bloqueo(4);  //4 Reportar como Perdida 5 Reportar como Robada
        ayuda.click_boton_Bloquear_Tarjeta();
        otp.genera_valida_Otp();
        ayuda.mensaje_exito("Bloqueo de tarjeta exitosa");
    }

    @Test //1709070054	Maritzawer.BB99	P@ssw0rd    479983  55
    public void ayudaBlqTarjCredito() {
        Reporte.setNombreReporte("AYUDA - Bloqueo de Tarjeta de Cr�dito");
        Ayuda ayuda = new Ayuda();
        ayuda.caso = "AYUDA";
        OTP otp = new OTP();
        ayuda.vp_etiqueta_cabecera("ADMINISTRAR TARJETAS");
        ayuda.click_boton_Opcion("Reportar como Perdida/Robada.");
        ayuda.vp_etiqueta_cabecera("REPORTAR");
        ayuda.seleccionar_motivo_bloqueo(4);  //4 Reportar como Perdida 5 Reportar como Robada
        ayuda.click_boton_Bloquear_Tarjeta();
        otp.genera_valida_Otp();
        ayuda.mensaje_exito("Bloqueo de tarjeta exitosa");
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}
