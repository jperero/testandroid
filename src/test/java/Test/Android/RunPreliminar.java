package Test.Android;
import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RunPreliminar {
	@Before
	public void inicio()
	{
		Inicio inicio = new Inicio("PantallaPreliminar");
		inicio.startApp();
		Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +  ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4] );
	}

	@Test
	public void MostrarPantallaPreliminar() {
		Reporte.setNombreReporte("Mostrar pantalla Preliminar - Saludo");

		ModoDebug modo = new ModoDebug();
		modo.modoDebug();

		ActualizacionApp actualiza = new ActualizacionApp();
		actualiza.loginActApp();

		ScreenPreliminar preliminar = new ScreenPreliminar();
		preliminar.vp_etiqueta_saludo();
	}

	@After
	public void tearDown() {
		Reporte.finReporte();
	}
}
