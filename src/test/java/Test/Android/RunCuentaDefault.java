package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.Inicio;
import Pom.Android.ConfigurarCuentaDefault;
import Pom.Android.TestLogin;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RunCuentaDefault {
	@Before
	public void inicio()
	{
		Inicio inicio = new Inicio("CuentaDefault");
		inicio.startApp();
		Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +  ";  SO:" + Util.getDataDispositivo()[3] + " Versi�n:" + Util.getDataDispositivo()[4]);

		/*LOGIN*/
		TestLogin login = new TestLogin();
		login.registroEquipo = false;
		login.loginTest("0");
	}
	@Test
	public void configurarCuentaDefaultTest() {
		Reporte.setNombreReporte("Activaci�n de Cuenta Default");
		/*CUENTA DEFAULT*/
		ConfigurarCuentaDefault cuentaDefault = new ConfigurarCuentaDefault();
		cuentaDefault.ingresarAjustes();
	}
	@After
	public void tearDown() {
		Reporte.finReporte();
	}
}
