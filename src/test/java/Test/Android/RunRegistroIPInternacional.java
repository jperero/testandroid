/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   REGISTRO DE IP INTERNACIONAL
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   FEB 22/2021
    Fecha Modificaci�n  :
                            MAR 01/2021: Seteo de Usuario de Prueba a trav�s de archivo dp_login_RegIpInternacional
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Registro IP Internacional - ACEPTAR
           1.1 Con Registro Equipo Activado
           1.2 Sin Registro Equipo Activado
        2. Registro IP Internacional - NO GRACIAS
 */
package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RunRegistroIPInternacional {
    @Before //IP INTERNACIONAL - Ejecuta Launch de la app 24m�vil
    public void registrarIPInternacional() {
        Inicio inicio = new Inicio("RegistroIPInternacional");
        inicio.startApp();

        Reporte.setEntorno("Ambiente: Desarrollo2" +
                "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +
                ";  SO:" + Util.getDataDispositivo()[3] +
                " Version:" + Util.getDataDispositivo()[4]);

        TestLogin login = new TestLogin();
        login.previoLogin("0");

        LoginIngresar ingreso = new LoginIngresar();
        ingreso.loginIngresar();
    }

    @Test //IP INTERNACIONAL - Aceptar con Registro Equipo
    public void registrarIPInternacionalSIconRegEquipo(){
        Reporte.setNombreReporte("IP INTERNACIONAL - Aceptar con Registro Equipo");

        TestLogin login = new TestLogin();
        login.continuaLogin();

        if (login.flag_ipinternacional) {
            /*INICIO REGISTRO DE IP INTERNACIONAL*/
            RegistroIPInternacional registroIP = new RegistroIPInternacional();

            registroIP.vp_cabecera_registroIP();
            registroIP.vp_etiqueta_viaje();
            registroIP.click_boton_Aceptar();

            PreguntaSeguridad pregunta = new PreguntaSeguridad();
            pregunta.vp_etiqueta_pregunta();
            pregunta.contestar_pregunta();
            pregunta.click_boton_aceptar_ip();
            if (pregunta.error_respuesta())
            {
                pregunta.click_boton_cambiarpregunta();
                pregunta.contestar_pregunta(Util.getDataCliente()[9]);
                pregunta.click_boton_aceptar_ip();
            }
        }
        else
            System.out.println("***Login sin Registro de IP internacional");

        //Registrar Equipo
        login.registroEquipo = true;
        login.continuaLogin();
    }

    @Test //IP INTERNACIONAL - Aceptar sin Registro Equipo
    public void registrarIPInternacionalSIsinRegEquipo(){
        Reporte.setNombreReporte("IP INTERNACIONAL - Aceptar sin Registro Equipo");

        MisProductos misProductos = new MisProductos();
        misProductos.vp_etiqueta_msg();
        misProductos.vp_etiqueta_misproductos();

        if ( misProductos.flag_ipinternacional) {
            /*INICIO REGISTRO DE IP INTERNACIONAL*/
            RegistroIPInternacional registroIP = new RegistroIPInternacional();

            registroIP.vp_cabecera_registroIP();
            registroIP.vp_etiqueta_viaje();
            registroIP.click_boton_Aceptar();

            PreguntaSeguridad pregunta = new PreguntaSeguridad();
            pregunta.vp_etiqueta_pregunta();
            pregunta.contestar_pregunta();
            pregunta.click_boton_aceptar_ip();
            if (pregunta.error_respuesta())
            {
                pregunta.click_boton_cambiarpregunta();
                pregunta.contestar_pregunta(Util.getDataCliente()[9]);
                pregunta.click_boton_aceptar_ip();
            }
        }
        else
            System.out.println("***Login sin Registro de IP internacional");

        //Registrar Equipo
        TestLogin login = new TestLogin();
        login.registroEquipo = false;
        login.continuaLogin();
    }

    @Test //IP INTERNACIONAL - No Aceptar
    public void registrarIPInternacionalNO(){
        Reporte.setNombreReporte("IP INTERNACIONAL - No Aceptar");

        /*INICIO REGISTRO DE IP INTERNACIONAL*/
        RegistroIPInternacional registroIP = new RegistroIPInternacional();
        PreguntaSeguridad preguntasIP=new PreguntaSeguridad();

        registroIP.vp_cabecera_registroIP();
        registroIP.vp_etiqueta_viaje();
        registroIP.click_boton_NoGracias();

        LoginIngresar ingreso = new LoginIngresar();
        ingreso.vp_etiqueta_problemas();
    }

    @After
    public void tearDown() {
        Reporte.finReporte();}
}
