/*
    Institución         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   CLAVE VIRTUAL - DESBLOQUEO
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creación      :   AGO 11/2021
    Fecha Modificación  :   N/A
    Aplicación          :   24móvil

    Criterios de Aceptación:
        1. Validación Clave Virtual No Bloqueada
        2. Validación Clave Virtual Desbloqueo
        3. Validación Base Conocimiento (Preguntas / Respuestas) - Fallido (3 intentos)
 */
package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.ClaveVirtualOpciones;
import Pom.Android.Inicio;
import Pom.Android.Menu;
import Pom.Android.TestLogin;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RunClaveVirtualDesbloqueo {
    @Before
    public void seleccionMenuClaveVirtual() {
        Inicio inicio = new Inicio("ClaveVirtualOpciones");
        inicio.startApp();
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] + ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4]);
        TestLogin login = new TestLogin();
        login.activoClave = false;
        login.loginTest("53");
        Menu menu = new Menu();
        menu.presionaMenuNuevo(7);
    }
    @Test
    public void desbloqueoClaveVirtualNoBloqueado() {
        Reporte.setNombreReporte("CLAVE VIRTUAL - Validación de Clave Virtual No Bloqueada");
        ClaveVirtualOpciones claveVirtualDesbloqueo = new ClaveVirtualOpciones();
        claveVirtualDesbloqueo.textoFlujo = "CLAVE VIRTUAL - DESBLOQUEO DE TU CLAVE VIRTUAL";
        claveVirtualDesbloqueo.click_boton_Opcion(10);
        claveVirtualDesbloqueo.mensajeValidacion("Tu Clave Virtual no se encuentra bloqueada");
    }
    @Test
    public void desbloqueoClaveVirtualDesbloquear() {
        Reporte.setNombreReporte("CLAVE VIRTUAL - Validación de Desbloqueo Exitoso");
        ClaveVirtualOpciones claveVirtualDesbloqueo = new ClaveVirtualOpciones();
        claveVirtualDesbloqueo.textoFlujo = "CLAVE VIRTUAL - DESBLOQUEO DE TU CLAVE VIRTUAL";
        claveVirtualDesbloqueo.click_boton_Opcion(10);
        claveVirtualDesbloqueo.vp_etiqueta_cabecera("PREGUNTA DE SEGURIDAD");
        claveVirtualDesbloqueo.pregunta_transaccional();
        claveVirtualDesbloqueo.mensajeValidacion("Tu Clave Virtual se ha desbloqueado");
        claveVirtualDesbloqueo.vp_etiqueta_cabecera("CLAVE VIRTUAL");
    }
    @Test
    public void desbloqueoClaveVirtualBaseConocimientoFallida() {
        Reporte.setNombreReporte("CLAVE VIRTUAL - Validación de Base Conocimiento (Superar Intentos Fallidos)");
        ClaveVirtualOpciones claveVirtualDesbloqueo = new ClaveVirtualOpciones();
        claveVirtualDesbloqueo.textoFlujo = "CLAVE VIRTUAL - DESBLOQUEO DE TU CLAVE VIRTUAL";
        claveVirtualDesbloqueo.click_boton_Opcion(10);
        claveVirtualDesbloqueo.vp_etiqueta_cabecera("PREGUNTA DE SEGURIDAD");
        for (int i = 0; i < 3; i++) {
            claveVirtualDesbloqueo.ingreso_respuesta(("ERROR" + i));
            claveVirtualDesbloqueo.mensaje_popup_Error(i);
            if (i != 2) {
                claveVirtualDesbloqueo.click_boton_Cambio_Pregunta();
            }
        }
        claveVirtualDesbloqueo.vp_etiqueta_cabecera("CLAVE VIRTUAL");
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}

