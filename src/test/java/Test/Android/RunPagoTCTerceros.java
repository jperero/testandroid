package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.Iterator;
import java.util.List;

public class RunPagoTCTerceros {
    @Before
    public void inicio() {
        Inicio inicio = new Inicio("PagoTCTerceros");
        inicio.startApp();

        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +  ";  SO:" + Util.getDataDispositivo()[3] + " Versi�n:" + Util.getDataDispositivo()[4]);
        /*LOGIN*/
        TestLogin login = new TestLogin();
        login.loginTest("0");

        /*REGISTRO DE EQUIPO*/
        TestRegistroEquipo registro = new TestRegistroEquipo();
        registro.registroTest();

        /*Menu  Pagar - Pagar Tarjeta*/
        BarraNavegacion barra = new BarraNavegacion();
        barra.click_opc_pagar();

        Menu menu = new Menu();
        //menu.presionaMenu(3); //Pagar
        menu.presionaSubmenu(3, 1); //Pagar Tarjeta
    }

    @Test
    public void pagoTCTercerosTest() {
        Reporte.setNombreReporte("Pago de Tarjetas Terceros");

        List<String> tarjetas = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_tarjetas_terceros.txt");
        Iterator<String> itr = tarjetas.iterator();
        String[] campos = null;
        int linea = 0;

        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0banco	1tipo	2numtc  3tipoid 4identificacion	5titular	6alias	7favorito	8descripcion    9pagar  10monto
            if (linea == 1)
                continue;

            if (campos[9].equals("N"))
                continue;

            Boolean fav = false;
            if (campos[7].equals("1"))
                fav = true;

            String alias= null;
            if (campos[6].length()>14)
                alias = campos[6].substring(0,14).trim();
            else
                alias = campos[6];

            PagoTcTerceros pago = new PagoTcTerceros();
            pago.buscaTarjeta(alias);
            pago.seleccionaTC(fav);

            pago.vp_etiqueta_monto();
            TecladoNumerico tc = new TecladoNumerico();
            tc.IngresarValor(2, campos[10], true);
            pago.vp_dato_monto(campos[10]);

            pago.click_boton_Continuar();

            //**DATOS DE ENTRADA VS DATOS DE CONFIRMACION
            pago.vp_etiqueta_confirmacion();
            pago.vp_dato_ConfirmaMonto(campos[10]);
            pago.vp_dato_ConfirmaTarjeta(alias);
            pago.click_boton_Confirmar();

            if (!fav)
            {
                OTP otp = new OTP();
                otp.genera_valida_Otp();
            }

            //pago.vp_etiqueta_msg();
            pago.vp_verifica_loader("Estamos realizando tu pago...");

            //COMPROBANTE
            Comprobante comp = new Comprobante();
            comp.vp_etiqueta_Confirmacion();
            comp.vp_etiqueta_Exitosa();
            comp.vp_dato_Monto(campos[10]);
            comp.vp_dato_tcDestino(alias);
            //comp.vp_dato_ctaOrigen(sel_ctaorig);
            comp.click_boton_MasOpciones();
            comp.click_boton_NuevoPago();
        }
    }


    @After
    public void tearDown() {
        Reporte.finReporte();
    }

}
