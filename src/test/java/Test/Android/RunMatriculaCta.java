package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class RunMatriculaCta {
	@Before
	public void inicio() {
		Inicio inicio = new Inicio("MatriculacionCuentas");
		inicio.startApp();

		Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +  ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4] );

		/*LOGIN*/
		TestLogin login = new TestLogin();
		login.loginTest("0");

		/*REGISTRO DE EQUIPO*/
		TestRegistroEquipo registro = new TestRegistroEquipo();
		registro.registroTest();

		BarraNavegacion barra = new BarraNavegacion();
		barra.click_opc_transf();

		Menu menu = new Menu();
		//menu.presionaMenu(2); //Transferir
		menu.presionaSubmenu(2, 2); //Administrar
	}

	@Test
	public void matricularCtaTest()  {
		Reporte.setNombreReporte("Matriculación de Cuentas");

		List<String> cuentas = Util.getCamposDataPool(System.getProperty("user.dir")+"/archivos/dp_ctas_terceros.txt");
		Iterator<String> itr = cuentas.iterator();
		String[] campos = null;
		int linea = 0;

		Menu menu = new Menu();

		while (itr.hasNext()) {
			campos = itr.next().split("\t");
			linea = linea + 1;
			//0banco	1tipo	2numcuenta	3tipoid	4identificacion	5titular	6alias	7favorito	8descripcion	9accion	10monto
			if (linea == 1)
				continue;

			if (campos[9].equals("N"))
				continue;

			MatriculaCta matricular = new MatriculaCta();
			if (!campos[0].equals("Bolivariano")) {
				menu.presionaOpcionSubmenuTransf(4, true); //Otros Bancos

			}
			else{
				menu.presionaOpcionSubmenuTransf(2, true); //Terceros
			}

			System.out.println(linea);

			Administrar administrar = new Administrar();
			administrar.vp_etiqueta_titulo();
			administrar.click_boton_Matricular("MATRICULAR CUENTA");

			if (!campos[0].equals("Bolivariano")) {
				matricular.seleccionaBanco(campos[0]);
			}

			matricular.vp_etiqueta_numCta(campos[0]);
			matricular.vp_etiqueta_numCtaBenef();
			matricular.vp_etiqueta_tipoCta();

			matricular.seleccionaTipoCtaDestino(campos[0], campos[1]);

			TecladoNumerico teclado = new TecladoNumerico();
			teclado.IngresarValor(1, campos[2],false);

			matricular.click_boton_Continuar();

			if (!campos[0].equals("Bolivariano")) {
				matricular.vp_etiqueta_titular();
				matricular.vp_etiqueta_nombTitular();
				matricular.ingresoTitular(campos[5]);
				matricular.click_boton_Continuar();

				matricular.seleccionaIdentificacion(campos[3]);
				matricular.vp_etiqueta_numIdentif();

				teclado = new TecladoNumerico();
				teclado.IngresarValor(1, campos[4], false);
				matricular.vp_dato_identif(campos[4]);
				matricular.click_boton_Continuar();
			}

			matricular.vp_etiqueta_detalle(campos[0]);
			//Detalle de cuenta destino
			matricular.vp_etiqueta_detNumCta();
			matricular.vp_dato_detnumCta(campos[2]);
			matricular.vp_etiqueta_detTipoCta();

			if (!campos[0].equals("Bolivariano")) {
				matricular.vp_dato_detTipoCta(campos[1]);
				matricular.vp_etiqueta_detTitular();
				matricular.vp_dato_detTitular(campos[5]);
			}
			else {
				matricular.vp_dato_detTipoCta(matricular.getTipo_cta());
				matricular.vp_etiqueta_detTitular();
			}

			//matricular.vp_etiqueta_favorito();
			matricular.click_img_Favorito(campos[7]);

			//matricular.vp_etiqueta_alias();
			matricular.ingresoAlias(0, campos[6]); // solo 15 caracteres
			matricular.click_boton_Confirmar();

			OTP otp = new OTP();
			otp.genera_valida_Otp();

			matricular.vp_etiqueta_msg();
            administrar.vp_etiqueta_titulo();

			Util.driver.pressKey(new KeyEvent(AndroidKey.BACK));
		}
	}

	//Expected: is "ˇMATRICULACIÓN EXITOSA!"
	//got: "ALIAS YA SE ENCUENTRA MATRICULADO"

	@After
	public void tearDown() {
		Reporte.finReporte();
	}
}