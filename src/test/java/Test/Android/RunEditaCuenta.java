/***************************************************************/
/*  Instituci�n     :   BANCO BOLIVARIANO, C.A.                */
/*  Aplicaci�n      :   24m�vil                                */
/*  Funcionalidad   :   Edicion Cuentas Terceros - Otros Bancos*/
/*                      Modificar Alias / Marca Favorito       */
/*  Tester          :   Fernando Rodriguez Mu�oz               */
/*  Fecha Creaci�n  :   JUL 14/2021                            */
/***************************************************************/
/*                    MODIFICACIONES                           */
/*  Fecha        Tester                Descripcion             */
/***************************************************************/


package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

public class RunEditaCuenta {

    @Before
    public void inicio() {
        Inicio inicio = new Inicio("EdicionCuentas");
        inicio.startApp();

        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +  ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4] );

        /*LOGIN*/
        TestLogin login = new TestLogin();
        login.loginTest("0");

        /*REGISTRO DE EQUIPO*/
        TestRegistroEquipo registro = new TestRegistroEquipo();
        registro.registroTest();

        BarraNavegacion barra = new BarraNavegacion();
        barra.click_opc_transf();

        Menu menu = new Menu();
        //menu.presionaMenu(2); //Transferir
        menu.presionaSubmenu(2, 2); //Administrar
    }

    @Test
    public void editaAliasCTA()  {
        Reporte.setNombreReporte("Editar Alias de Cuentas");

        List<String> cuentas = Util.getCamposDataPool(System.getProperty("user.dir")+"/archivos/dp_ctas_terceros.txt");
        Iterator<String> itr = cuentas.iterator();
        String[] campos = null;
        int linea = 0;

        Menu menu = new Menu();

        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0banco	1tipo	2numcuenta	3tipoid	4identificacion	5titular	6alias	7favorito	8descripcion	9accion	10monto
            if (linea == 1)
                continue;

            if (campos[9].equals("N"))
                continue;

            if (!campos[0].equals("Bolivariano")) {
                menu.presionaOpcionSubmenuTransf(4, true); //Otros Bancos
            }
            else{
                menu.presionaOpcionSubmenuTransf(2, true); //Terceros
            }

            Administrar administrar = new Administrar();

            String alias = null;
            if (campos[6].length()>14)
                alias = campos[6].substring(0,14);
            else
                alias = campos[6];

            administrar.vp_etiqueta_titulo();
            administrar.buscaCuenta(alias, true);
            administrar.seleccionaCuenta(campos[7], true);
            administrar.click_boton_Editar("CUENTA",true,2);
            administrar.vp_popup_editar("CUENTA");

            String alias_mod = alias.substring(0,4) + "ED";

            administrar.selec_popup_editar("CUENTA","Alias",true);
            administrar.editar_alias("CUENTA",alias_mod,true);
            administrar.click_boton_confirmar_editar(true, "CUENTA");
            administrar.vp_mensaje_edicion(true,"CUENTA");

            try {
                Thread.sleep(3000);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }

            administrar.click_boton_Atras();

            //Restablecer Alias
            if (!campos[0].equals("Bolivariano")) {
                menu.presionaOpcionSubmenuTransf(4, false); //Otros Bancos
            }
            else{
                menu.presionaOpcionSubmenuTransf(2, false); //Terceros
            }

            administrar.buscaCuenta(alias_mod, false);
            administrar.seleccionaCuenta(campos[7], false);
            administrar.click_boton_Editar("CUENTA",false,2);
            administrar.selec_popup_editar("CUENTA","Alias",false);
            administrar.editar_alias("",alias,false);
            administrar.click_boton_confirmar_editar(false, "");
            administrar.vp_mensaje_edicion(false,"");

            try {
                Thread.sleep(3000);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }

            administrar.click_boton_Atras();

        }
    }

    @Test
    public void EditaMarcaFavoritoCTA() {
        Reporte.setNombreReporte("Editar Marca Favorito de Cuentas");

        List<String> cuentas = Util.getCamposDataPool(System.getProperty("user.dir")+"/archivos/dp_ctas_terceros.txt");
        Iterator<String> itr = cuentas.iterator();
        String[] campos = null;
        int linea = 0;

        Menu menu = new Menu();

        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0banco	1tipo	2numcuenta	3tipoid	4identificacion	5titular	6alias	7favorito	8descripcion	9accion	10monto
            if (linea == 1)
                continue;

            if (campos[9].equals("N"))
                continue;

            if (!campos[0].equals("Bolivariano")) {
                menu.presionaOpcionSubmenuTransf(4, true); //Otros Bancos
            }
            else{
                menu.presionaOpcionSubmenuTransf(2, true); //Terceros
            }

            Administrar administrar = new Administrar();

            String alias = null;
            if (campos[6].length()>14)
                alias = campos[6].substring(0,14);
            else
                alias = campos[6];

            administrar.vp_etiqueta_titulo();
            administrar.buscaCuenta(alias, true);
            administrar.seleccionaCuenta(campos[7], true);
            administrar.click_boton_Editar("CUENTA",true,2);
            administrar.vp_popup_editar("CUENTA");

            administrar.selec_popup_editar("CUENTA","Marca Favorito",true);

            OTP otp = new OTP();
            otp.genera_valida_Otp();

            administrar.vp_mensaje_edicion(true,"CUENTA");

            try {
                Thread.sleep(3000);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }

            administrar.click_boton_Atras();

        }

    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}
