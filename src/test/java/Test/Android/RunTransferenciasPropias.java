package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RunTransferenciasPropias {
    @Before
    public void inicio() {
        Inicio inicio = new Inicio("TransferenciasPropias");
        inicio.startApp();

        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +  ";  SO:" + Util.getDataDispositivo()[3] + " Versi?n:" + Util.getDataDispositivo()[4]);

        /*LOGIN*/
        TestLogin login = new TestLogin();
        login.loginTest("0");

        /*REGISTRO DE EQUIPO*/
        TestRegistroEquipo registro = new TestRegistroEquipo();
        registro.registroTest();

        /*Menu  Transferir - Transferir*/
        BarraNavegacion barra = new BarraNavegacion();
        barra.click_opc_transf();

        Menu menu = new Menu();
        //menu.presionaMenu(2); //Transferir
        menu.presionaSubmenu(2,0); //Transferir
        menu.presionaOpcionSubmenuTransf(2, true); //Propias
    }
    @Test
    public void transferenciaPropia()  {
        Reporte.setNombreReporte("Transferencias a Cuentas Propias");
        /*Transferencias PROPIAS*/
        //El indice de cuentas empieza en 2
        Transferencia transf = new Transferencia();
        //*Escojo Cuenta Destino//
        transf.vp_etiqueta_CtaDestino();
        int numctas = transf.numCtasDestino();
        //0sec	1Login	2Password	3Identificacion	4NewPass	5Pos_TC	6CTE	7Pago
        String montopagar = Util.getDataCliente()[7];
        double monto = Double.valueOf(montopagar);
        System.out.println(montopagar);
        Menu menu = new Menu();
        //Rcorre todas las cuentas destino
        for (int orden=1; orden<=numctas; orden++)
        {
            TestTransferenciaPropia tp = new TestTransferenciaPropia();
            tp.TransferenciaPropiaTest(orden, montopagar);
            monto = monto + 5;
            montopagar = String.valueOf(monto);
            menu.presionaOpcionSubmenuTransf(2, true); //Propias
        }
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}
