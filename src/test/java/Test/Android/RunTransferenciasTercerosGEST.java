/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   TRANSFERENCIAS A TERCEROS (BOLIVARIANO | OTROS BANCOS (SPI | PAGO DIRECTO))
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   AGO 26/2021
    Fecha Modificaci�n  :   N/A
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Registro Dispositivo Activo
        2. Dispositivo Seguridad Activo
        3. Producto Cuentas Activo
        4. Validaci�n de Monto (TERCEROS BOLIVARIANO Y OTROS BANCOS (SPI | PAGO DIRECTO):
            4.1. TERCEROS (BOLIVARIANO | OTROS BANCOS (SPI | PAGO DIRECTO)):
                 4.2.1. Valor monto m�nimo de $5.00  (Solo Pago Directo)
                 4.2.2. Valor monto m�ximo de $10000.00 (Ordenante | Receptor)
                 4.2.3. Valor excede saldo disponible
                 4.2.4. Valor cupo disponible
        5. Validaci�n de bloqueo de cuenta
        6. Validaci�n de error de d�bito (Disminuir el saldo disponible durante la confirmaci�n)

        Subflujos:
        7. Matriculaci�n de Beneficiario (TERCEROS)
 */

package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Iterator;
import java.util.List;

public class RunTransferenciasTercerosGEST {
    //List<String> datosMonto = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_Transferencias_-_monto.txt");
    List<String> datosTransferencias = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_transferencias.txt");
    Iterator<String> itrTransferencias = datosTransferencias.iterator();
    String[] campos = null;
    int linea = 0;
    String escenario = "11";     // Secuencia de Datos a Ejecutarse del Datapool:
    // 0 Propias Exitoso
    // 1 Terceros Exitoso
    // 2 Terceros Bloqueo
    // 3 Terceros Monto
    // 4 Otros Bancos SPI Exitoso
    // 5 Otros Bancos SPI Bloqueo
    // 6 Otros Bancos SPI Monto
    // 7 Otros Bancos Pago Directo Exitoso
    // 8 Otros Bancos Pago Directo Bloqueo
    // 9 Otros Bancos Pago Directo Monto
    // 10 Otros Bancos Pago Directo Monto (Valor Menor a 5.00 USD - Requiere ambiente con BANRED)
    TestLogin login = new TestLogin();
    String testName;
    String className;

    @Rule
    public TestWatcher watcher = new TestWatcher() {
        public void starting(Description description) {
            testName = description.getMethodName();
            className = description.getClassName();
        }
    };

    @Before
    public void seleccionMenuTransferencias() {
        Inicio inicio = new Inicio("RunTransferenciasTercerosREF");
        inicio.startApp();

        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +
                ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4]);

        if (testName.equals("transferenciasTercerosValidacionSinRegEquipo")) {
            login.registroEquipo = false; //Presiona "No Gracias"
        }
        if ((!testName.equals("transferenciasTransaccionesExitosas"))
                && (!testName.equals("transferenciasValidacionMonto"))
                && (!testName.equals("transferenciasPropiasValidacionSoloUnaCuenta"))) {
            login.loginTest("50");
            Menu menu = new Menu();
            menu.presionaSubmenu(2, 0); //Transferir
        }
    }

    @Test //1200724563	Arce.BB99	P@ssw0rd	19686   53
    public void transferenciasTransaccionesExitosas() {
        //Escenarios: 0,1,4,7
        Reporte.setNombreReporte("TRANSFERENCIAS - Exitoso");
        login.loginTest("53");
        Menu menu = new Menu();
        menu.presionaSubmenu(2); //Transferir
        TransferenciaTercerosGEST transferTercero = new TransferenciaTercerosGEST();
        OTPSwiping otp = new OTPSwiping();

        while (itrTransferencias.hasNext()) {
            campos = itrTransferencias.next().split("\t");
            linea = linea + 1;//0SECUENCIA 1TRANSFERENCIA 2VIA 3ESCENARIO 4FAVORITO 5TRANSFIERE 6LOGIN 7ORIGEN 8DESTINO 9ALIAS 10MONTO 11CONCEPTO 12COMENTARIOS
            if (linea == 1) {
                continue;
            }
            transferTercero.caso = (campos[2].equals("No Aplica") ? "TRANSFERENCIAS " + campos[1] : "TRANSFERENCIAS " + campos[1] + " - " + (campos[2].equals("Otros Bancos") ? "SPI" : campos[2])).toUpperCase();
            if (campos[0].equals(escenario)) {
                transferTercero.vp_popover_cabecera("�A qui�n deseas transferir?");
                transferTercero.seleccion_tipo_transferencia(campos[1]);
                transferTercero.io_validar_datos(campos[8]);
                transferTercero.seleccion_beneficiario(campos[8]);
                transferTercero.buscar_ordenante();
                transferTercero.vp_popover_cabecera("Elige la cuenta a debitar");
                transferTercero.seleccion_ordenante(campos[7]);
                transferTercero.ingreso_monto(campos[10]);
                transferTercero.io_validar_datos(campos[11]);
                transferTercero.click_boton_Continuar();
                //transferTercero.click_boton("CONTINUAR");
                transferTercero.vp_popover_cabecera("Confirma tu transferencia");
                transferTercero.espera_transfiriendo(5000);
                transferTercero.click_boton_Transferir();
                //transferTercero.click_boton("TRANSFERIR");
                if (campos[4].equals("N")) {
                    otp.generarOTPSwiping();
                }
                transferTercero.vp_popover_mensaje("Estamos transfiriendo tu dinero");
                transferTercero.espera_transfiriendo(2000);
                transferTercero.vp_popover_mensaje("�Listo! transferencia enviada");
                transferTercero.click_boton_comprobante("Ir a posici�n consolidada");
                menu.presionaSubmenu(2);
            }
        }
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}
