package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RunLoginCambioContrasena {
    @Before
    public void inicio()
    {
        Inicio inicio = new Inicio("LoginCambioContrasena");
        inicio.startApp();

        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +  ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4] );

        TestLogin login = new TestLogin();
        login.previoLogin("6");
    }

    @Test
    public void cambioContrasenaObligatorioPreaviso_Exitoso() {
        LoginIngresar ingreso = new LoginIngresar();
        ingreso.loginIngresar();

        if ((ingreso.flag_pssw_obligatorio))
            Reporte.setNombreReporte("Cambio de Contraseņa Obligatorio");

        if (ingreso.flag_pssw_preaviso)
            Reporte.setNombreReporte("Cambio de Contraseņa Preaviso");


        if ((ingreso.flag_pssw_obligatorio)||(ingreso.flag_pssw_preaviso)) {
            ingreso.vp_popup_msgcambio();
            ingreso.click_popup_aceptar();

            OTP otp = new OTP();
            otp.genera_valida_Otp();

            CambiarContrasena contrasena = new CambiarContrasena();
            contrasena.vp_etiqueta_cambiar();
            contrasena.vp_etiqueta_nueva();
            contrasena.ingreso_nueva(Util.getDataCliente()[5]);
            contrasena.click_boton_ojo();
            contrasena.vp_etiqueta_repite();
            contrasena.ingreso_repite(Util.getDataCliente()[5]);
            contrasena.click_boton_cambiar();
            contrasena.vp_mensaje();
        }
        else
            System.out.println("***Login sin cambio de contraseņa");
    }

    @Test
    public void cambioContrasenaxBloqueo() {
        Reporte.setNombreReporte("Cambio de Contraseņa por Bloqueo de intentos fallidos");

        LoginIngresar ingreso = new LoginIngresar();
        ingreso.loginIngresar();

        if (ingreso.flag_pssw_bloqueo) {
            ingreso.vp_popup_msgbloqueo();
            ingreso.click_popup_continuar();

            VerificarUsuario usuario = new VerificarUsuario();
            usuario.vp_etiqueta_verificar();
            usuario.seleccionaIdentificacion(Util.getDataCliente()[4]);
            usuario.ingresaIdentificacion(Util.getDataCliente()[4], Util.getDataCliente()[3]);
            usuario.click_boton_continuar();

            OTP otp = new OTP();
            otp.genera_valida_Otp();

            usuario.vp_etiqueta_bienvenido();
            usuario.click_boton_cambiarcontra();

            PreguntaSeguridad pregunta = new PreguntaSeguridad();
            pregunta.vp_etiqueta_pregunta();
            pregunta.contestar_pregunta();
            pregunta.click_boton_aceptar();

            if (pregunta.error_respuesta())
            {
                pregunta.click_boton_cambiarpregunta();
                pregunta.contestar_pregunta(Util.getDataCliente()[9]);
                pregunta.click_boton_aceptar();
            }

            CambiarContrasena contrasena = new CambiarContrasena();
            contrasena.vp_etiqueta_cambiar();
            contrasena.vp_etiqueta_nueva();
            contrasena.ingreso_nueva(Util.getDataCliente()[5]);
            contrasena.click_boton_ojo();
            contrasena.vp_etiqueta_repite();
            contrasena.ingreso_repite(Util.getDataCliente()[5]);
            contrasena.click_boton_cambiar();
            contrasena.vp_mensaje();
        }
        else
            System.out.println("***Login sin cambio de contraseņa");
    }

    @Test
    public void cambioContrasenaObligatorioPreaviso_Validaciones() {
        LoginIngresar ingreso = new LoginIngresar();
        ingreso.loginIngresar();

        if ((ingreso.flag_pssw_obligatorio))
            Reporte.setNombreReporte("Cambio de Contraseņa Obligatorio - Validaciones");

        if (ingreso.flag_pssw_preaviso)
            Reporte.setNombreReporte("Cambio de Contraseņa Preaviso - Validaciones");


        if ((ingreso.flag_pssw_obligatorio)||(ingreso.flag_pssw_preaviso)) {
            ingreso.vp_popup_msgcambio();
            ingreso.click_popup_aceptar();

            OTP otp = new OTP();
            otp.genera_valida_Otp();

            CambiarContrasena contrasena = new CambiarContrasena();
            contrasena.vp_etiqueta_cambiar();
            contrasena.vp_etiqueta_nueva();
            contrasena.ingreso_nueva(Util.getDataCliente()[5]);
            contrasena.click_boton_ojo();
            contrasena.vp_etiqueta_repite();
            contrasena.ingreso_repite(Util.getDataCliente()[5]);
            contrasena.click_boton_cambiar();
            contrasena.vp_mensaje_error();
        }
        else
            System.out.println("***Login sin cambio de contraseņa");
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}
