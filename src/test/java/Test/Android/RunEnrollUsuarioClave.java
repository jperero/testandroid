/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   ENROLL (Creaci�n Usuario / Clave Alfanum�rica)
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   FEB 22/2021
    Fecha Modificaci�n  :   MAR 16/2021 --> DATAPOOL DE MENSAJES DE ERROR
                            ABR 13/2021 --> BUCLE CON DATOS
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Creaci�n Usuario - C�dula
           Creaci�n Usuario - Pasaporte
        2. Validaci�n de N�mero de Identificaci�n - C�dula
        3. Validaci�n de C�digo Dactilar - C�dula
        4. Validaci�n de Usuario Creado en Plataforma (Enrolado)
        5. Validaci�n de Login Existente (Coincidencia)
        6. Validaci�n de servicio Registro Civil / Municipal
 */
package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;


public class RunEnrollUsuarioClave {
    List<String> personaEnroll = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_enroll_-_datos2.txt");
    Iterator<String> itr = personaEnroll.iterator();
    String[] campos = null;
    int linea = 0;
    /*String[] datoEnroll;*/

    @Before
    public void inicio() {
        //ENROLL - ACCESO A LA APP
        Inicio inicio = new Inicio("RunEnrollUsuarioClave.class");
        inicio.startApp();
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1]
                + ";  SO:" + Util.getDataDispositivo()[3]
                + " Version:" + Util.getDataDispositivo()[4]);

        ModoDebug modo = new ModoDebug();
        modo.modoDebug();

        ActualizacionApp actualiza = new ActualizacionApp();
        actualiza.loginActApp();
    }

    // ***************************************************************************************
    // CREAR MI USUARIO - ACCESO DESDE PANTALLA LOGIN > LINK REGISTRATE
    // ***************************************************************************************
    @Test // CREAR MI USUARIO (CEDULA / PASAPORTE)
    public void registrateLinkRegistrate() {
        //REGISTRAR ENROLL - Ingresar > Link Registrate > Crear Mi Usuario + Clave + Preguntas de Seguridad + Imagen con C�dula / Pasaporte
        Reporte.setNombreReporte("Reg�strate Con CEDULA / PASAPORTE desde Pantalla Login > Link Registrate"
                + "<h3>Crea: Usuario | Contrase�a | Base Conocimiento | Imagen</h3>");

        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0num_login	1num_test	2tipo_doc	3num_doc_ident	4cod_dactilar	5num_doc_ident	6usuario	7clave	8msj_error
            if (linea == 1)
                continue;
            if (campos[1].equals("1")) {
                EnrollUsuarioClave enrollUsuarioClave = new EnrollUsuarioClave();
                enrollUsuarioClave.acceso_preliminar_login();
                enrollUsuarioClave.vp_etiqueta_no_tienes_usuario();
                enrollUsuarioClave.click_no_tienes_usuario();
                enrollUsuarioClave.vp_etiqueta_creacion_usuario();
                enrollUsuarioClave.click_check();
                enrollUsuarioClave.click_boton_continuar();
                //enrollUsuarioClave.setSeleccionPersonaEnroll(2);
                enrollUsuarioClave.vp_etiqueta_ingresar_identificacion();
                if (campos[2].equals("C")) {
                    enrollUsuarioClave.click_boton_cedula();
                } else if (campos[2].equals("P")) {
                    enrollUsuarioClave.click_boton_pasaporte();
                }
                enrollUsuarioClave.vp_ingreso_tipo_documento_identidad();
                enrollUsuarioClave.ingreso_dato_documento_identidad(campos[3]);
                /* Comentado debido a que no se esta solicitando Cod.Huella Dactilar
                if (campos[2].equals("C")){
                    enrollUsuarioClave.vp_ingreso_codigo_dactilar();
                    enrollUsuarioClave.ingreso_codigo_dactilar(campos[4]);
                }
                 */
                enrollUsuarioClave.click_boton_continuar();
                OTP otp = new OTP();
                otp.genera_valida_Otp();
                enrollUsuarioClave.click_boton_continuar();
                enrollUsuarioClave.vp_etiqueta_crea_usuario();
                enrollUsuarioClave.vp_ingreso_dato_usuario();
                enrollUsuarioClave.ingreso_dato_usuario(campos[6]);
                enrollUsuarioClave.vp_ingreso_dato_contrasenia();
                enrollUsuarioClave.ingreso_dato_contrasenia(campos[7]);
                enrollUsuarioClave.vp_ingreso_dato_repite_contrasenia();
                enrollUsuarioClave.ingreso_dato_repite_contrasenia(campos[7]);
                enrollUsuarioClave.click_boton_continuar();
                enrollUsuarioClave.mensaje_exito(campos[6]);
                enrollUsuarioClave.click_boton_ingresar();
                enrollUsuarioClave.vp_etiqueta_no_tienes_usuario();
                EnrollBaseConocimientoImagen enrollBaseConocimientoImagen = new EnrollBaseConocimientoImagen();
                //enrollBaseConocimientoImagen.setSeleccionPersonaEnroll(2);
                enrollBaseConocimientoImagen.ingreso_usuario(campos[6]);
                enrollBaseConocimientoImagen.ingreso_clave(campos[7]);
                enrollBaseConocimientoImagen.vp_boton_ingresar();
                enrollBaseConocimientoImagen.vp_etiqueta_completa_perfil();
                enrollBaseConocimientoImagen.click_boton_aceptar();
                enrollBaseConocimientoImagen.vp_etiqueta_pregunta_seguridad();
                enrollBaseConocimientoImagen.seleccionar_preguntas_respuestas();
                enrollBaseConocimientoImagen.click_boton_continuar();
                enrollBaseConocimientoImagen.vp_etiqueta_imagen_seguridad();
                enrollBaseConocimientoImagen.ingreso_alias();
                enrollBaseConocimientoImagen.seleccionar_avatar();
                enrollBaseConocimientoImagen.click_boton_continuar();
                inicio();
            }
        }
    }

    @Test // CREAR USUARIO (CEDULA) - VALIDACION N�MERO IDENTIFICACI�N
    public void registrateLinkRegistrateValidacionNumIdent() {
        //REGISTRAR ENROLL - Validaci�n N�mero Identificaci�n
        Reporte.setNombreReporte("Reg�strate Con CEDULA - Validaci�n N�mero Identificaci�n");

        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0num_login	1num_test	2tipo_doc	3num_doc_ident	4cod_dactilar	5num_doc_ident	6usuario	7clave	8msj_error
            if (linea == 1)
                continue;
            if (campos[1].equals("2")) {
                EnrollUsuarioClave enrollUsuarioClave = new EnrollUsuarioClave();
                enrollUsuarioClave.acceso_preliminar_login();
                enrollUsuarioClave.vp_etiqueta_no_tienes_usuario();
                enrollUsuarioClave.click_no_tienes_usuario();
                enrollUsuarioClave.vp_etiqueta_creacion_usuario();
                enrollUsuarioClave.click_check();
                enrollUsuarioClave.click_boton_continuar();
                //enrollUsuarioClave.setSeleccionPersonaEnroll(1);
                enrollUsuarioClave.vp_etiqueta_ingresar_identificacion();
                if (campos[2].equals("C")) {
                    enrollUsuarioClave.click_boton_cedula();
                } else if (campos[2].equals("P")) {
                    enrollUsuarioClave.click_boton_pasaporte();
                }
                enrollUsuarioClave.vp_ingreso_tipo_documento_identidad();
                enrollUsuarioClave.ingreso_dato_documento_identidad(campos[3]);
                /* Comentado debido a que no se esta solicitando Cod.Huella Dactilar
                if (campos[2].equals("C")){
                    enrollUsuarioClave.vp_ingreso_codigo_dactilar();
                    enrollUsuarioClave.ingreso_codigo_dactilar(campos[4]);
                }*/
                enrollUsuarioClave.click_boton_continuar();
                enrollUsuarioClave.setSeleccionMensajeError(1);
                enrollUsuarioClave.mensaje_error();
            }
        }
    }

    @Test // CREAR USUARIO (CEDULA) - VALIDACION CODIGO DACTILAR
    public void registrateLinkRegistrateValidacionCodigoDactilar() {
        //REGISTRAR ENROLL - Validaci�n C�digo Dactilar
        Reporte.setNombreReporte("Reg�strate Con CEDULA - Validaci�n C�digo Dactilar");

        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0num_login	1num_test	2tipo_doc	3num_doc_ident	4cod_dactilar	5num_doc_ident	6usuario	7clave	8msj_error
            if (linea == 1)
                continue;
            if (campos[1].equals("3")) {
                EnrollUsuarioClave enrollUsuarioClave = new EnrollUsuarioClave();
                enrollUsuarioClave.acceso_preliminar_login();
                enrollUsuarioClave.vp_etiqueta_no_tienes_usuario();
                enrollUsuarioClave.click_no_tienes_usuario();
                enrollUsuarioClave.vp_etiqueta_creacion_usuario();
                enrollUsuarioClave.click_check();
                enrollUsuarioClave.click_boton_continuar();
                //enrollUsuarioClave.setSeleccionPersonaEnroll(2);
                enrollUsuarioClave.vp_etiqueta_ingresar_identificacion();
                if (campos[2].equals("C")) {
                    enrollUsuarioClave.click_boton_cedula();
                } else if (campos[2].equals("P")) {
                    enrollUsuarioClave.click_boton_pasaporte();
                }
                enrollUsuarioClave.vp_ingreso_tipo_documento_identidad();
                enrollUsuarioClave.ingreso_dato_documento_identidad(campos[3]);
                /* Comentado debido a que no se esta solicitando Cod.Huella Dactilar
                if (campos[2].equals("C")){
                    enrollUsuarioClave.vp_ingreso_codigo_dactilar();
                    enrollUsuarioClave.ingreso_codigo_dactilar(campos[4]);
                }*/
                enrollUsuarioClave.click_boton_continuar();
                enrollUsuarioClave.setSeleccionMensajeError(2);
                enrollUsuarioClave.mensaje_error();
            }
        }
    }

    @Test // CREAR MI USUARIO - VALIDACION USUARIO CREADO EN PLATAFORMA
    public void registrateLinkRegistrateValidacionUsuarioRegistrado() {
        //REGISTRAR ENROLL - Ingresar > Link Registrate > Crear Mi Usuario - Validaci�n Usuario Enrolado en Plataforma
        Reporte.setNombreReporte("Reg�strate Con PASAPORTE - Validaci�n Usuario Enrolado en Plataforma");

        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0num_login	1num_test	2tipo_doc	3num_doc_ident	4cod_dactilar	5num_doc_ident	6usuario	7clave	8msj_error
            if (linea == 1)
                continue;
            if (campos[1].equals("4")) {
                EnrollUsuarioClave enrollUsuarioClave = new EnrollUsuarioClave();
                enrollUsuarioClave.acceso_preliminar_login();
                enrollUsuarioClave.vp_etiqueta_no_tienes_usuario();
                enrollUsuarioClave.click_no_tienes_usuario();
                enrollUsuarioClave.vp_etiqueta_creacion_usuario();
                enrollUsuarioClave.click_check();
                enrollUsuarioClave.click_boton_continuar();
                //enrollUsuarioClave.setSeleccionPersonaEnroll(4);
                enrollUsuarioClave.vp_etiqueta_ingresar_identificacion();
                if (campos[2].equals("C")) {
                    enrollUsuarioClave.click_boton_cedula();
                } else if (campos[2].equals("P")) {
                    enrollUsuarioClave.click_boton_pasaporte();
                }
                enrollUsuarioClave.vp_ingreso_tipo_documento_identidad();
                enrollUsuarioClave.ingreso_dato_documento_identidad(campos[3]);
                /* Comentado debido a que no se esta solicitando Cod.Huella Dactilar
                if (campos[2].equals("C")){
                    enrollUsuarioClave.vp_ingreso_codigo_dactilar();
                    enrollUsuarioClave.ingreso_codigo_dactilar(campos[4]);
                }*/
                enrollUsuarioClave.click_boton_continuar();
                enrollUsuarioClave.setSeleccionMensajeError(3);
                enrollUsuarioClave.mensaje_error();
            }
        }
    }

    @Test // CREAR MI USUARIO - VALIDACION LOGIN EXISTENTE
    public void registrateLinkRegistrateValidacionUsuarioLoginRegistrado() {
        //REGISTRAR ENROLL - Ingresar > Link Registrate > Crear Mi Usuario - Validaci�n Login Existente
        Reporte.setNombreReporte("Reg�strate Con PASAPORTE - Validaci�n Login Existente");

        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0num_login	1num_test	2tipo_doc	3num_doc_ident	4cod_dactilar	5num_doc_ident	6usuario	7clave	8msj_error
            if (linea == 1)
                continue;
            if (campos[1].equals("5")) {
                EnrollUsuarioClave enrollUsuarioClave = new EnrollUsuarioClave();
                enrollUsuarioClave.acceso_preliminar_login();
                enrollUsuarioClave.vp_etiqueta_no_tienes_usuario();
                enrollUsuarioClave.click_no_tienes_usuario();
                enrollUsuarioClave.vp_etiqueta_creacion_usuario();
                enrollUsuarioClave.click_check();
                enrollUsuarioClave.click_boton_continuar();
                //enrollUsuarioClave.setSeleccionPersonaEnroll(6);
                enrollUsuarioClave.vp_etiqueta_ingresar_identificacion();
                if (campos[2].equals("C")) {
                    enrollUsuarioClave.click_boton_cedula();
                } else if (campos[2].equals("P")) {
                    enrollUsuarioClave.click_boton_pasaporte();
                }
                enrollUsuarioClave.vp_ingreso_tipo_documento_identidad();
                enrollUsuarioClave.ingreso_dato_documento_identidad(campos[3]);
                /* Comentado debido a que no se esta solicitando Cod.Huella Dactilar
                if (campos[2].equals("C")){
                    enrollUsuarioClave.vp_ingreso_codigo_dactilar();
                    enrollUsuarioClave.ingreso_codigo_dactilar(campos[4]);
                }*/
                enrollUsuarioClave.click_boton_continuar();
                OTP otp = new OTP();
                otp.genera_valida_Otp();
                enrollUsuarioClave.click_boton_continuar();
                enrollUsuarioClave.vp_etiqueta_crea_usuario();
                enrollUsuarioClave.vp_ingreso_dato_usuario();
                enrollUsuarioClave.ingreso_dato_usuario(campos[6]);
                enrollUsuarioClave.vp_ingreso_dato_contrasenia();
                enrollUsuarioClave.ingreso_dato_contrasenia(campos[7]);
                enrollUsuarioClave.vp_ingreso_dato_repite_contrasenia();
                enrollUsuarioClave.ingreso_dato_repite_contrasenia(campos[7]);
                enrollUsuarioClave.click_boton_continuar();
                enrollUsuarioClave.setSeleccionMensajeError(6);
                enrollUsuarioClave.mensaje_error();
            }
        }
    }

    @Test // CREAR MI USUARIO - VALIDACION SIN SERVICIO REGISTRO CIVIL / MUNICIPAL
    public void registrateLinkRegistrateSinServicio() {
        //REGISTRAR ENROLL - Ingresar > Link Registrate > Crear Mi Usuario - Validaci�n Sin Servicio Registro Civil / Municipal
        Reporte.setNombreReporte("Reg�strate - Validaci�n Sin Servicio Registro Civil / Municipal");

        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0num_login	1num_test	2tipo_doc	3num_doc_ident	4cod_dactilar	5num_doc_ident	6usuario	7clave	8msj_error
            if (linea == 1)
                continue;
            if (campos[1].equals("0")) {
                EnrollUsuarioClave enrollUsuarioClave = new EnrollUsuarioClave();
                enrollUsuarioClave.acceso_preliminar_login();
                enrollUsuarioClave.vp_etiqueta_no_tienes_usuario();
                enrollUsuarioClave.click_no_tienes_usuario();
                enrollUsuarioClave.vp_etiqueta_creacion_usuario();
                enrollUsuarioClave.click_check();
                enrollUsuarioClave.click_boton_continuar();
                //enrollUsuarioClave.setSeleccionPersonaEnroll(5);
                enrollUsuarioClave.vp_etiqueta_ingresar_identificacion();
                if (campos[2].equals("C")) {
                    enrollUsuarioClave.click_boton_cedula();
                } else if (campos[2].equals("P")) {
                    enrollUsuarioClave.click_boton_pasaporte();
                }
                enrollUsuarioClave.vp_ingreso_tipo_documento_identidad();
                enrollUsuarioClave.ingreso_dato_documento_identidad(campos[3]);
                /* Comentado debido a que no se esta solicitando Cod.Huella Dactilar
                if (campos[2].equals("C")){
                    enrollUsuarioClave.vp_ingreso_codigo_dactilar();
                    enrollUsuarioClave.ingreso_codigo_dactilar(campos[4]);
                }*/
                enrollUsuarioClave.click_boton_continuar();
                enrollUsuarioClave.setSeleccionMensajeError(4);
                enrollUsuarioClave.mensaje_error();
            }
        }
    }

    // ***************************************************************************************
    // CREAR MI USUARIO - ACCESO DESDE PANTALLA PRELIMINAR > M�S OPCIONES > CREAR MI USUARIO
    // ***************************************************************************************
    @Test // CREAR MI USUARIO (CEDULA / PASAPORTE)
    public void registrateMasOpcionesCEDULA() {
        //REGISTRAR ENROLL - Ingresar > Link Registrate > Crear Mi Usuario con C�dula
        Reporte.setNombreReporte("Reg�strate Con CEDULA - Ingresar > Link Registrate > Crear Mi Usuario con C�dula");
        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0num_login	1num_test	2tipo_doc	3num_doc_ident	4cod_dactilar	5num_doc_ident	6usuario	7clave	8msj_error
            if (linea == 1)
                continue;
            if (campos[1].equals("1")) {
                EnrollUsuarioClave enrollUsuarioClave = new EnrollUsuarioClave();
                enrollUsuarioClave.acceso_preliminar_mas_opciones();
                enrollUsuarioClave.vp_etiqueta_creacion_usuario();
                enrollUsuarioClave.click_check();
                enrollUsuarioClave.click_boton_continuar();
                //enrollUsuarioClave.setSeleccionPersonaEnroll(2);
                enrollUsuarioClave.vp_etiqueta_ingresar_identificacion();
                if (campos[2].equals("C")) {
                    enrollUsuarioClave.click_boton_cedula();
                } else if (campos[2].equals("P")) {
                    enrollUsuarioClave.click_boton_pasaporte();
                }
                enrollUsuarioClave.vp_ingreso_tipo_documento_identidad();
                enrollUsuarioClave.ingreso_dato_documento_identidad(campos[3]);
                /* Comentado debido a que no se esta solicitando Cod.Huella Dactilar
                if (campos[2].equals("C")){
                    enrollUsuarioClave.vp_ingreso_codigo_dactilar();
                    enrollUsuarioClave.ingreso_codigo_dactilar(campos[4]);
                }
                 */
                enrollUsuarioClave.click_boton_continuar();
                OTP otp = new OTP();
                otp.genera_valida_Otp();
                enrollUsuarioClave.click_boton_continuar();
                enrollUsuarioClave.vp_etiqueta_crea_usuario();
                enrollUsuarioClave.vp_ingreso_dato_usuario();
                enrollUsuarioClave.ingreso_dato_usuario(campos[6]);
                enrollUsuarioClave.vp_ingreso_dato_contrasenia();
                enrollUsuarioClave.ingreso_dato_contrasenia(campos[7]);
                enrollUsuarioClave.vp_ingreso_dato_repite_contrasenia();
                enrollUsuarioClave.ingreso_dato_repite_contrasenia(campos[7]);
                enrollUsuarioClave.click_boton_continuar();
                enrollUsuarioClave.mensaje_exito(campos[6]);
                enrollUsuarioClave.click_boton_ingresar();
                enrollUsuarioClave.vp_etiqueta_no_tienes_usuario();
                EnrollBaseConocimientoImagen enrollBaseConocimientoImagen = new EnrollBaseConocimientoImagen();
                //enrollBaseConocimientoImagen.setSeleccionPersonaEnroll(2);
                enrollBaseConocimientoImagen.ingreso_usuario(campos[6]);
                enrollBaseConocimientoImagen.ingreso_clave(campos[7]);
                enrollBaseConocimientoImagen.vp_boton_ingresar();
                enrollBaseConocimientoImagen.vp_etiqueta_completa_perfil();
                enrollBaseConocimientoImagen.click_boton_aceptar();
                enrollBaseConocimientoImagen.vp_etiqueta_pregunta_seguridad();
                enrollBaseConocimientoImagen.seleccionar_preguntas_respuestas();
                enrollBaseConocimientoImagen.click_boton_continuar();
                enrollBaseConocimientoImagen.vp_etiqueta_imagen_seguridad();
                enrollBaseConocimientoImagen.ingreso_alias();
                enrollBaseConocimientoImagen.seleccionar_avatar();
                enrollBaseConocimientoImagen.click_boton_continuar();
                inicio();
            }
        }
    }

    @Test // CREAR USUARIO (CEDULA) - VALIDACION N�MERO IDENTIFICACI�N
    public void registrateMasOpcionesValidacionNumIdent() {
        //REGISTRAR ENROLL - - Validaci�n N�mero Identificaci�n
        Reporte.setNombreReporte("Reg�strate Con CEDULA - Validaci�n N�mero Identificaci�n");

        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0num_login	1num_test	2tipo_doc	3num_doc_ident	4cod_dactilar	5num_doc_ident	6usuario	7clave	8msj_error
            if (linea == 1)
                continue;
            if (campos[1].equals("2")) {
                EnrollUsuarioClave enrollUsuarioClave = new EnrollUsuarioClave();
                enrollUsuarioClave.acceso_preliminar_mas_opciones();
                enrollUsuarioClave.vp_etiqueta_creacion_usuario();
                enrollUsuarioClave.click_check();
                enrollUsuarioClave.click_boton_continuar();
                //enrollUsuarioClave.setSeleccionPersonaEnroll(1);
                enrollUsuarioClave.vp_etiqueta_ingresar_identificacion();
                if (campos[2].equals("C")) {
                    enrollUsuarioClave.click_boton_cedula();
                } else if (campos[2].equals("P")) {
                    enrollUsuarioClave.click_boton_pasaporte();
                }
                enrollUsuarioClave.vp_ingreso_tipo_documento_identidad();
                enrollUsuarioClave.ingreso_dato_documento_identidad(campos[3]);
                /* Comentado debido a que no se esta solicitando Cod.Huella Dactilar
                if (campos[2].equals("C")){
                    enrollUsuarioClave.vp_ingreso_codigo_dactilar();
                    enrollUsuarioClave.ingreso_codigo_dactilar(campos[4]);
                }*/
                enrollUsuarioClave.click_boton_continuar();
                enrollUsuarioClave.setSeleccionMensajeError(1);
                enrollUsuarioClave.mensaje_error();
            }
        }
    }

    @Test // CREAR USUARIO (CEDULA) - VALIDACION CODIGO DACTILAR
    public void registrateMasOpcionesValidacionCodigoDactilar() {
        //REGISTRAR ENROLL - Validaci�n C�digo Dactilar
        Reporte.setNombreReporte("Reg�strate Con CEDULA - Validaci�n C�digo Dactilar");

        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0num_login	1num_test	2tipo_doc	3num_doc_ident	4cod_dactilar	5num_doc_ident	6usuario	7clave	8msj_error
            if (linea == 1)
                continue;
            if (campos[1].equals("3")) {
                EnrollUsuarioClave enrollUsuarioClave = new EnrollUsuarioClave();
                enrollUsuarioClave.acceso_preliminar_mas_opciones();
                enrollUsuarioClave.vp_etiqueta_creacion_usuario();
                enrollUsuarioClave.click_check();
                enrollUsuarioClave.click_boton_continuar();
                //enrollUsuarioClave.setSeleccionPersonaEnroll(2);
                enrollUsuarioClave.vp_etiqueta_ingresar_identificacion();
                if (campos[2].equals("C")) {
                    enrollUsuarioClave.click_boton_cedula();
                } else if (campos[2].equals("P")) {
                    enrollUsuarioClave.click_boton_pasaporte();
                }
                enrollUsuarioClave.vp_ingreso_tipo_documento_identidad();
                enrollUsuarioClave.ingreso_dato_documento_identidad(campos[3]);
                /* Comentado debido a que no se esta solicitando Cod.Huella Dactilar
                if (campos[2].equals("C")){
                    enrollUsuarioClave.vp_ingreso_codigo_dactilar();
                    enrollUsuarioClave.ingreso_codigo_dactilar(campos[4]);
                }*/
                enrollUsuarioClave.click_boton_continuar();
                enrollUsuarioClave.setSeleccionMensajeError(2);
                enrollUsuarioClave.mensaje_error();
            }
        }
    }

    @Test // CREAR MI USUARIO - VALIDACION USUARIO CREADO EN PLATAFORMA
    public void registrateMasOpcionesValidacionUsuarioRegistrado() {
        //REGISTRAR ENROLL - Ingresar > Link Registrate > Crear Mi Usuario - Validaci�n Usuario Enrolado en Plataforma
        Reporte.setNombreReporte("Reg�strate - Validaci�n Usuario Enrolado en Plataforma");

        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0num_login	1num_test	2tipo_doc	3num_doc_ident	4cod_dactilar	5num_doc_ident	6usuario	7clave	8msj_error
            if (linea == 1)
                continue;
            if (campos[1].equals("4")) {
                EnrollUsuarioClave enrollUsuarioClave = new EnrollUsuarioClave();
                enrollUsuarioClave.acceso_preliminar_mas_opciones();
                enrollUsuarioClave.vp_etiqueta_creacion_usuario();
                enrollUsuarioClave.click_check();
                enrollUsuarioClave.click_boton_continuar();
                //enrollUsuarioClave.setSeleccionPersonaEnroll(4);
                enrollUsuarioClave.vp_etiqueta_ingresar_identificacion();
                if (campos[2].equals("C")) {
                    enrollUsuarioClave.click_boton_cedula();
                } else if (campos[2].equals("P")) {
                    enrollUsuarioClave.click_boton_pasaporte();
                }
                enrollUsuarioClave.vp_ingreso_tipo_documento_identidad();
                enrollUsuarioClave.ingreso_dato_documento_identidad(campos[3]);
                /* Comentado debido a que no se esta solicitando Cod.Huella Dactilar
                if (campos[2].equals("C")){
                    enrollUsuarioClave.vp_ingreso_codigo_dactilar();
                    enrollUsuarioClave.ingreso_codigo_dactilar(campos[4]);
                }*/
                enrollUsuarioClave.click_boton_continuar();
                enrollUsuarioClave.setSeleccionMensajeError(3);
                enrollUsuarioClave.mensaje_error();
            }
        }
    }

    @Test // CREAR MI USUARIO - VALIDACION LOGIN EXISTENTE
    public void registrateMasOpcionesValidacionUsuarioLoginRegistrado() {
        //REGISTRAR ENROLL - Ingresar > Link Registrate > Crear Mi Usuario - Validaci�n Login Existente
        Reporte.setNombreReporte("Reg�strate - Validaci�n Login Existente");

        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0num_login	1num_test	2tipo_doc	3num_doc_ident	4cod_dactilar	5num_doc_ident	6usuario	7clave	8msj_error
            if (linea == 1)
                continue;
            if (campos[1].equals("5")) {
                EnrollUsuarioClave enrollUsuarioClave = new EnrollUsuarioClave();
                enrollUsuarioClave.acceso_preliminar_mas_opciones();
                enrollUsuarioClave.vp_etiqueta_creacion_usuario();
                enrollUsuarioClave.click_check();
                enrollUsuarioClave.click_boton_continuar();
                //enrollUsuarioClave.setSeleccionPersonaEnroll(6);
                enrollUsuarioClave.vp_etiqueta_ingresar_identificacion();
                if (campos[2].equals("C")) {
                    enrollUsuarioClave.click_boton_cedula();
                } else if (campos[2].equals("P")) {
                    enrollUsuarioClave.click_boton_pasaporte();
                }
                enrollUsuarioClave.vp_ingreso_tipo_documento_identidad();
                enrollUsuarioClave.ingreso_dato_documento_identidad(campos[3]);
                /* Comentado debido a que no se esta solicitando Cod.Huella Dactilar
                if (campos[2].equals("C")){
                    enrollUsuarioClave.vp_ingreso_codigo_dactilar();
                    enrollUsuarioClave.ingreso_codigo_dactilar(campos[4]);
                }*/
                enrollUsuarioClave.click_boton_continuar();
                OTP otp = new OTP();
                otp.genera_valida_Otp();
                enrollUsuarioClave.click_boton_continuar();
                enrollUsuarioClave.vp_etiqueta_crea_usuario();
                enrollUsuarioClave.vp_ingreso_dato_usuario();
                enrollUsuarioClave.ingreso_dato_usuario(campos[6]);
                enrollUsuarioClave.vp_ingreso_dato_contrasenia();
                enrollUsuarioClave.ingreso_dato_contrasenia(campos[7]);
                enrollUsuarioClave.vp_ingreso_dato_repite_contrasenia();
                enrollUsuarioClave.ingreso_dato_repite_contrasenia(campos[7]);
                enrollUsuarioClave.click_boton_continuar();
                enrollUsuarioClave.setSeleccionMensajeError(6);
                enrollUsuarioClave.mensaje_error();
            }
        }
    }

    @Test // CREAR MI USUARIO (CEDULA) - VALIDACION SIN SERVICIO REGISTRO CIVIL / MUNICIPAL
    public void registrateMasOpcionesSinServicio() {
        //REGISTRAR ENROLL - Ingresar > Link Registrate > Crear Mi Usuario - Validaci�n Sin Servicio Registro Civil / Municipal
        Reporte.setNombreReporte("Reg�strate - Validaci�n Sin Servicio Registro Civil / Municipal");

        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0num_login	1num_test	2tipo_doc	3num_doc_ident	4cod_dactilar	5num_doc_ident	6usuario	7clave	8msj_error
            if (linea == 1)
                continue;
            if (campos[1].equals("0")) {
                EnrollUsuarioClave enrollUsuarioClave = new EnrollUsuarioClave();
                enrollUsuarioClave.acceso_preliminar_mas_opciones();
                enrollUsuarioClave.vp_etiqueta_creacion_usuario();
                enrollUsuarioClave.click_check();
                enrollUsuarioClave.click_boton_continuar();
                //enrollUsuarioClave.setSeleccionPersonaEnroll(5);
                enrollUsuarioClave.vp_etiqueta_ingresar_identificacion();
                if (campos[2].equals("C")) {
                    enrollUsuarioClave.click_boton_cedula();
                } else if (campos[2].equals("P")) {
                    enrollUsuarioClave.click_boton_pasaporte();
                }
                enrollUsuarioClave.vp_ingreso_tipo_documento_identidad();
                enrollUsuarioClave.ingreso_dato_documento_identidad(campos[3]);
                /* Comentado debido a que no se esta solicitando Cod.Huella Dactilar
                if (campos[2].equals("C")){
                    enrollUsuarioClave.vp_ingreso_codigo_dactilar();
                    enrollUsuarioClave.ingreso_codigo_dactilar(campos[4]);
                }*/
                enrollUsuarioClave.click_boton_continuar();
                enrollUsuarioClave.setSeleccionMensajeError(4);
                enrollUsuarioClave.mensaje_error();
            }
        }
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}