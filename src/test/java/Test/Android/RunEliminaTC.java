/************************************************************/
/*  Instituci�n     :   BANCO BOLIVARIANO, C.A.             */
/*  Aplicaci�n      :   24m�vil                             */
/*  Funcionalidad   :   Eliminacion Tarjetas de Credito     */
/*  Tester          :   Fernando Rodriguez Mu�oz            */
/*  Fecha Creaci�n  :   JUL 09/2021                         */
/************************************************************/
/*                    MODIFICACIONES                        */
/*  Fecha        Tester                Descripcion          */
/************************************************************/

package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

public class RunEliminaTC {
    @Before
    public void inicio() {
        Inicio inicio = new Inicio("EliminacionTarjetas");
        inicio.startApp();

        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +  ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4] );

        /*LOGIN*/
        TestLogin login = new TestLogin();
        login.loginTest("0");

        /*REGISTRO DE EQUIPO*/
        TestRegistroEquipo registro = new TestRegistroEquipo();
        registro.registroTest();

        BarraNavegacion barra = new BarraNavegacion();
        barra.click_opc_pagar();

        Menu menu = new Menu();
        //menu.presionaMenu(2); //Transferir
        menu.presionaSubmenu(2, 2); //Administrar
    }

    @Test
    public void eliminacionTCTest() {
        Reporte.setNombreReporte("Eliminaci�n de Tarjetas");

        List<String> tarjetas = Util.getCamposDataPool(System.getProperty("user.dir")+"/archivos/dp_tarjetas_terceros.txt");
        Iterator<String> itr = tarjetas.iterator();
        String[] campos = null;
        int linea = 0;

        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0banco	1tipo	2numtc	3tipoid	4identificacion	5titular	6alias	7favorito	8descripcion	9pagar	10monto
            if (linea == 1)
                continue;

            if (campos[9].equals("N"))
                continue;

            //System.out.println(linea);

            Administrar administrar = new Administrar();

            String alias = null;
            if (campos[6].length()>14)
                alias = campos[6].substring(0,14);
            else
                alias = campos[6];

            administrar.buscaTarjeta(alias,true);
            administrar.click_boton_EliminarTC();
            administrar.vp_etiqueta_popup_eliminaTC();
            administrar.click_popup_boton_eliminarTC();
            administrar.vp_mensaje_eliminarTC();

            try {
                Thread.sleep(2000);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
            // administrar.click_boton_Atras();
        }
    }

    @After
    public void tearDown() { Reporte.finReporte(); }
}
