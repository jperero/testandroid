/*
    Institución         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   AJUSTES - DESBLOQUEO DE TU: CLAVE VIRTUAL | CLAVE24 | CLAVE DIGITAL
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creación      :   JUL 31/2021
    Fecha Modificación  :   N/A
    Aplicación          :   24móvil

    Criterios de Aceptación:
        1. Validación 2FA Desbloqueo
        2. Validación No Posee 2FA
        3. Validación 2FA No Bloqueada
        4. Validación Base Conocimiento (Preguntas / Respuestas) - Fallido (3 intentos)
 */
package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.ClaveVirtualOpciones;
import Pom.Android.Inicio;
import Pom.Android.Menu;
import Pom.Android.TestLogin;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RunDesbloqueoDispositivoSeguridadREF {
    @Before
    public void seleccionMenuAjustes() {
        Inicio inicio = new Inicio("DesbloqueoDispositivoSeguridad");
        inicio.startApp();
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] + ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4]);
        TestLogin login = new TestLogin();
        login.activoClave = false;
        login.loginTest("40");
        /*      0909956872	Wess.BB99       P@ssw0rd	19155   50  TARJ.COORD.:  (Clave24)
                1203575095	Alarcon.BB99	P@ssw0rd	74720   40  TOKEN: 0844834892 (Clave Digital)
                1200724563	Arce.BB99	    P@ssw0rd	19686   53  SOFTOKEN: 78746-36447 (Clave Virtual)*/
        Menu menu = new Menu();
        menu.presionaMenuNuevo(11);
    }
    @Test
    public void desbloqueoDispositivoSeguridadValidacionNo2FA(){
        Reporte.setNombreReporte("DESBLOQUE DE DISPOSITIVO SEGURIDAD - Validación No Posee 2FA");
        ClaveVirtualOpciones desbloqueoDispSeg = new ClaveVirtualOpciones();
        desbloqueoDispSeg.textoFlujo = "DESBLOQUEO DISPOSITIVO SEGURIDAD (AJUSTES)";
        desbloqueoDispSeg.vp_etiqueta_cabecera("AJUSTES");
        desbloqueoDispSeg.vp_buscar_opciondesbloqueo();
    }
    @Test
    public void desbloqueoDispositivoSeguridadValidacionNoBloqueado() {
        Reporte.setNombreReporte("DESBLOQUE DE DISPOSITIVO SEGURIDAD - Validación de Dispositivo de Seguridad No Bloqueado");
        ClaveVirtualOpciones desbloqueoDispSeg = new ClaveVirtualOpciones();
        desbloqueoDispSeg.textoFlujo = "DESBLOQUEO DISPOSITIVO SEGURIDAD (AJUSTES)";
        desbloqueoDispSeg.vp_etiqueta_cabecera("AJUSTES");
        desbloqueoDispSeg.click_boton_Opcion(12); //Desbloqueo de tu (CLAVE VIRTUAL | CLAVE24 | CLAVE DIGITAL) desde Ajustes
        desbloqueoDispSeg.mensajeValidacion();
    }
    @Test
    public void desbloqueoDispositivoSeguridadDesbloquear() {
        Reporte.setNombreReporte("DESBLOQUE DE DISPOSITIVO SEGURIDAD - Validación de Desbloqueo Exitoso");
        ClaveVirtualOpciones desbloqueoDispSeg = new ClaveVirtualOpciones();
        desbloqueoDispSeg.textoFlujo = "DESBLOQUEO DISPOSITIVO SEGURIDAD (AJUSTES)";
        desbloqueoDispSeg.vp_etiqueta_cabecera("AJUSTES");
        desbloqueoDispSeg.click_boton_Opcion(12); //Desbloqueo de tu (CLAVE VIRTUAL | CLAVE24 | CLAVE DIGITAL) desde Ajustes
        desbloqueoDispSeg.vp_etiqueta_cabecera("PREGUNTA DE SEGURIDAD");
        desbloqueoDispSeg.pregunta_transaccional();
        desbloqueoDispSeg.mensajeValidacion("Dispositivo Seguridad");//Desbloqueo de tu (CLAVE VIRTUAL | CLAVE24 | CLAVE DIGITAL) desde Ajustes
        desbloqueoDispSeg.vp_etiqueta_cabecera("AJUSTES");
    }
    @Test
    public void eliminacionDispositivoSeguridadBaseConocimientoFallida() {
        Reporte.setNombreReporte("DESBLOQUE DE DISPOSITIVO SEGURIDAD - Validación de Base Conocimiento (Superar Intentos Fallidos)");
        ClaveVirtualOpciones desbloqueoDispSeg = new ClaveVirtualOpciones();
        desbloqueoDispSeg.textoFlujo = "DESBLOQUEO DISPOSITIVO SEGURIDAD (AJUSTES)";
        desbloqueoDispSeg.vp_etiqueta_cabecera("AJUSTES");
        desbloqueoDispSeg.click_boton_Opcion(12); //Desbloqueo de tu (CLAVE VIRTUAL | CLAVE24 | CLAVE DIGITAL) desde Ajustes
        desbloqueoDispSeg.vp_etiqueta_cabecera("PREGUNTA DE SEGURIDAD");
        for (int i = 0; i < 3; i++) {
            desbloqueoDispSeg.ingreso_respuesta(("ERROR" + i));
            desbloqueoDispSeg.mensaje_popup_Error(i);
            if (i != 2) {
                desbloqueoDispSeg.click_boton_Cambio_Pregunta();
            }
        }
        desbloqueoDispSeg.vp_etiqueta_cabecera("AJUSTES");
    }
    @After
    public void tearDown(){
        Reporte.finReporte();
    }
}
