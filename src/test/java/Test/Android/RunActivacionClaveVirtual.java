package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RunActivacionClaveVirtual {
    @Before
    public void inicio() {
        Inicio inicio = new Inicio("ActivacionClaveVirtual");
        inicio.startApp();

        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +  ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4] );

        /*LOGIN*/
        TestLogin login = new TestLogin();
        login.activoClave = false;
        login.loginTest("0");

        /*Menu  Clave Virtual*/
        Menu menu = new Menu();
        menu.presionaMenu(8); //Clave Virtual
    }

    @Test
    public void activacionClaveVirtualNoCamp() {
        Reporte.setNombreReporte("Activación de Clave Virtual");
        //menu.clave_activa
        ClaveVirtual clave  = new ClaveVirtual();
        clave.vp_etiqueta_activacion();
        clave.click_check();
        clave.click_boton_activar();
        clave.vp_etiqueta_popupDispositivo();
        clave.click_boton_continuar();
        clave.vp_etiqueta_cta_cobro();
        clave.selecciona_cta_cobro();
        //Pregunta
        PreguntaSeguridad pregunta = new PreguntaSeguridad();
        pregunta.vp_etiqueta_pregunta();
        pregunta.contestar_pregunta();
        pregunta.click_boton_aceptar();

        clave.vp_etiqueta_activarServicio();
        clave.vp_etiqueta_metodoUsar();
        clave.selecciona_metodo();
        clave.vp_etiqueta_activarServicio();
        clave.click_boton_activarYseleccionar();
        clave.popup_activacion();
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}
