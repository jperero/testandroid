package Test.Android;

import Auxiliar.Reporte;
import Pom.Android.Inicio;
import Pom.Android.TestLogin;
import Pom.Android.RetiroSinTarjetaQP;
import org.junit.After;
import org.junit.Test;

import java.io.IOException;

public class RunRetiroSinTarjeta {
	@Test
	public void retirarSinTarjetaTest() throws InterruptedException, IOException {
		Inicio inicio = new Inicio("RunRetiroSinTarjeta.class");
		inicio.startApp();


		/*LOGIN*/
		TestLogin login = new TestLogin();
		login.registroEquipo = false;
		login.loginTest("0");

		/*
		*/
		/*REGISTRO DE EQUIPO*//*

		TestRegistroEquipo registro = new TestRegistroEquipo();
		registro.registroTestCancelar();
		*/

		/*RETIRO SIN TARJETA (PROPIO | TERCEROS)*/
		RetiroSinTarjetaQP retiroSinTarjetaQP = new RetiroSinTarjetaQP();
		retiroSinTarjetaQP.ingresarRetiro();
	}
	@After
	public void tearDown() {
		Reporte.finReporte();
	}
}