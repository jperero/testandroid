/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   QUICKPAY - RETIRO SIN TARJETA (PROPIO | TERCEROS)
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   ENE 25/2021
    Fecha Modificaci�n  :   FEB 03/2021 (REFACTORING - CARGA DE DATOS TERCEROS)
                            JUN 21/2021 (REFACTORING - CAMBIO EN METODOLOGIA DE QA)
                            JUL 14/2021 (REFACTORING - QUICKPAY TERCERO VALIDACION MONTO DATAPOOL)
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Registro Dispositivo Activo
        2. Dispositivo Seguridad Activo
        3. Producto Cuentas Activo
        4. Servicio de QuickPay Activo
        5. Validaci�n de Monto (TERCEROS):
            5.1. TERCEROS:
                 5.2.1. Valor monto m�nimo de $1.00
                 5.2.2. Valor monto m�ximo de $1000.00
                 5.2.3. Valor excede saldo disponible
        6. Validaci�n de error de d�bito (Disminuir el saldo disponible durante la confirmaci�n)

        Subflujos:
        7. Matriculaci�n de Beneficiario (TERCEROS)
        8. Buscar d�nde retirar

 */
package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Iterator;
import java.util.List;

public class RunRetiroSinTarjetaQP_Tercero {
    List<String> datosMonto = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_RetiroSinTarjetaQP_-_montoQP.txt");
    Iterator<String> itrMonto = datosMonto.iterator();
    String[] campos = null;
    int linea = 0;

    TestLogin login = new TestLogin();

    String testName;
    String className;

    @Rule
    public TestWatcher watcher = new TestWatcher() {
        public void starting(Description description) {
            testName = description.getMethodName();
            className = description.getClassName();
        }
    };

    @Before
    public void seleccionMenuRetiroSinTarjetaQP() {
        Inicio inicio = new Inicio("RunRetiroSinTarjetaQP_Tercero.class");
        inicio.startApp();

        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +
                ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4]);

        if (testName.equals("retiroSinTarjetaValidacionSinRegEquipo")) {
            login.registroEquipo = false;
        }
        if (!testName.equals("retiroSinTarjetaValidacionMontosFavorito")
                &&(!testName.equals("retiroSinTarjetaValidacionMontosNoFavorito"))
                &&(!testName.equals("retiroSinTarjetaValidacionBeneficiario"))){
            login.loginTest("48");
            Menu menu = new Menu();
            menu.presionaMenuNuevo(4);
        }
    }

    @Test //0401320072	Bank.BB99	P@ssw0rd	480051  48
    public void retiroSinTarjetaValidacionSinServicioActivo() {
        Reporte.setNombreReporte("QUICKPAY RETIRO SIN TARJETA - Validaci�n Sin Servicio Activo");
        RetiroSinTarjetaQP retiroSinTarjetaQP = new RetiroSinTarjetaQP();
        retiroSinTarjetaQP.mensajeValidacion("Servicio Activo QuickPay - Retiro Sin Tarjeta", "No tienes activo el servicio de QuickPay");
        retiroSinTarjetaQP.click_boton_Cancelar();
    }

    @Test //0909956872	Wess.BB99	P@ssw0rd	19155   50
    public void retiroSinTarjetaValidacionSinDispositivoSeguridad() {
        Reporte.setNombreReporte("QUICKPAY RETIRO SIN TARJETA - Validaci�n Sin Dispositivo de Seguridad");
        RetiroSinTarjetaQP retiroSinTarjetaQP = new RetiroSinTarjetaQP();
        retiroSinTarjetaQP.vp_etiqueta_cabecera();
        retiroSinTarjetaQP.mensajeValidacion("Dispositivo de Seguridad", "Para realizar esta transacci�n debes activar tu Clave Virtual");
    }

    @Test //1306669001	Big.BB99 	P@ssw2rd	609941  31
    public void retiroSinTarjetaValidacionSinRegEquipo() {
        Reporte.setNombreReporte("QUICKPAY RETIRO SIN TARJETA - Validaci�n Sin Registro Equipo");
        RetiroSinTarjetaQP retiroSinTarjetaQP = new RetiroSinTarjetaQP();
        retiroSinTarjetaQP.vp_etiqueta_cabecera();
        retiroSinTarjetaQP.mensajeValidacion("Registro de Equipo", "Para transaccionar debes registrar tu dispositivo m�vil");
    }

    @Test //1201184973	Zamba.BB99	P@ssw2rd	37426   37
    public void retiroSinTarjetaValidacionSinProdCuentas() {
        Reporte.setNombreReporte("QUICKPAY RETIRO SIN TARJETA - Validaci�n Producto Cuentas");
        RetiroSinTarjetaQP retiroSinTarjetaQP = new RetiroSinTarjetaQP();
        retiroSinTarjetaQP.vp_etiqueta_cabecera();
        retiroSinTarjetaQP.mensajeValidacion("Producto Cuentas", "No posees cuenta para realizar esta transacci�n");
    }

    @Test //0914378005	Carl.BB9	P@ssw3rd	98725   43
    public void retiroSinTarjetaValidacionBeneficiario() {
        Reporte.setNombreReporte("QUICKPAY RETIRO SIN TARJETA TERCERO - Validaci�n Beneficiario");

        RetiroSinTarjetaQP retiroSinTarjetaQP = new RetiroSinTarjetaQP();

        login.loginTest("43");
        Menu menu = new Menu();
        menu.presionaMenuNuevo(4);

        retiroSinTarjetaQP.vp_etiqueta_cabecera();
        retiroSinTarjetaQP.click_boton_Retirar();
        retiroSinTarjetaQP.vp_etiqueta_cabecera_retiro_de_efectivo();
        retiroSinTarjetaQP.click_boton_Alguien_mas_hara_el_retiro();
        retiroSinTarjetaQP.vp_etiqueta_cabecera_beneficiario();
        retiroSinTarjetaQP.mensajeValidacion("Beneficiario","No tienes beneficiarios matriculados");
    }

    @Test //0915649743	Roboto.78	P@ssW2RD 	277295  42
    public void retiroSinTarjetaValidacionMontosFavorito() {
        Reporte.setNombreReporte("QUICKPAY RETIRO SIN TARJETA TERCERO - Validaciones de Monto: <br>" +
                "<ul style='font-size: 20px'>" +
                "<li>Valor monto m�nimo de $1.00</li>" +
                "<li>Valor monto m�ximo de $1500.00</li>" +
                "<li>Valor excede saldo disponible</li>" +
                "<li>Valor v�lido</li>" +
                "</ul>");

        RetiroSinTarjetaQP retiroSinTarjetaQP = new RetiroSinTarjetaQP();

        login.loginTest("42");
        Menu menu = new Menu();
        menu.presionaMenuNuevo(4);

        retiroSinTarjetaQP.vp_etiqueta_cabecera();
        retiroSinTarjetaQP.click_boton_Retirar();
        retiroSinTarjetaQP.vp_etiqueta_cabecera_retiro_de_efectivo();
        retiroSinTarjetaQP.click_boton_Alguien_mas_hara_el_retiro();
        retiroSinTarjetaQP.vp_etiqueta_cabecera_beneficiario();
        retiroSinTarjetaQP.seleccionar_beneficiario_favorito();

        while (itrMonto.hasNext()) {
            campos = itrMonto.next().split("\t");
            linea = linea + 1; //0Sec	1Escenario	2Valor	3Validacion
            if (linea <= 2)
                continue;
            retiroSinTarjetaQP.vp_etiqueta_cabecera_monto();
            retiroSinTarjetaQP.ingreso_monto(campos[2], campos[3]);
            retiroSinTarjetaQP.click_boton_Continuar();
            if (!campos[3].equals("Valor v�lido")) {
                retiroSinTarjetaQP.mensaje_error(campos[1], campos[3]);
                retiroSinTarjetaQP.borra_monto(campos[2], campos[3]);
            } else {
                retiroSinTarjetaQP.vp_etiqueta_cabecera_confirmacion();
                retiroSinTarjetaQP.click_boton_Retirar_Monto();
                retiroSinTarjetaQP.vp_etiqueta_cabecera_clave();
                retiroSinTarjetaQP.click_boton_Aceptar();
                retiroSinTarjetaQP.mensaje_exito();
            }
        }
    }

    @Test //0915649743	Roboto.78	P@ssW2RD 	277295  42
    public void retiroSinTarjetaValidacionMontosNoFavorito() {
        Reporte.setNombreReporte("QUICKPAY RETIRO SIN TARJETA TERCERO - Validaciones de Monto: <br>" +
                "<ul style='font-size: 20px'>" +
                "<li>Valor monto m�nimo de $1.00</li>" +
                "<li>Valor monto m�ximo de $1500.00</li>" +
                "<li>Valor excede saldo disponible</li>" +
                "<li>Valor v�lido</li>" +
                "</ul>");

        RetiroSinTarjetaQP retiroSinTarjetaQP = new RetiroSinTarjetaQP();
        OTP otp = new OTP();

        login.loginTest("42");
        Menu menu = new Menu();
        menu.presionaMenuNuevo(4);

        retiroSinTarjetaQP.vp_etiqueta_cabecera();
        retiroSinTarjetaQP.click_boton_Retirar();
        retiroSinTarjetaQP.vp_etiqueta_cabecera_retiro_de_efectivo();
        retiroSinTarjetaQP.click_boton_Alguien_mas_hara_el_retiro();
        retiroSinTarjetaQP.vp_etiqueta_cabecera_beneficiario();
        retiroSinTarjetaQP.seleccionar_beneficiario_no_favorito();

        while (itrMonto.hasNext()) {
            campos = itrMonto.next().split("\t");
            linea = linea + 1; //0Sec	1Escenario	2Valor	3Validacion
            if (linea <= 2)
                continue;
            retiroSinTarjetaQP.vp_etiqueta_cabecera_monto();
            retiroSinTarjetaQP.ingreso_monto(campos[2], campos[3]);
            retiroSinTarjetaQP.click_boton_Continuar();
            if (!campos[3].equals("Valor v�lido")) {
                retiroSinTarjetaQP.mensaje_error(campos[1], campos[3]);
                retiroSinTarjetaQP.borra_monto(campos[2], campos[3]);
            } else {
                retiroSinTarjetaQP.vp_etiqueta_cabecera_confirmacion();
                retiroSinTarjetaQP.click_boton_Retirar_Monto();
                otp.genera_valida_Otp();
                retiroSinTarjetaQP.vp_etiqueta_cabecera_clave();
                retiroSinTarjetaQP.click_boton_Aceptar();
                retiroSinTarjetaQP.mensaje_exito();
            }
        }
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}
