/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   PERFIL
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   AGO 20/2021
    Fecha Modificaci�n  :   N/A
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Validaci�n Servicio Activo en flujos: OK
           1.1. Clave Virtual
           1.2. Dep�sito Express
           1.3. QuickPay
           1.4. QuickView
           1.5. Acceso Biom�trico
           1.6. Registro de Equipo
        2. Validaci�n Producto Cuentas en flujos: OK
           2.1. Clave Virtual
           2.2. Dep�sito Express
           2.3. QuickPay
        3. Validaci�n Registro Equipo en flujos: OK
           3.1. Clave Virtual
           3.2. Dep�sito Express
           3.3. QuickPay
           3.4. QuickView
           3.5. Acceso Biom�trico
           3.6. Registro de Equipo
 */
package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.Inicio;
import Pom.Android.Menu;
import Pom.Android.Perfil;
import Pom.Android.TestLogin;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import java.util.Iterator;
import java.util.List;

public class RunPerfil {
    List<String> datosPerfilOpciones = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_AjustesPerfil_-_opciones.txt");
    Iterator<String> itrPerfilOpciones = datosPerfilOpciones.iterator();
    String[] campos = null;

    int linea = 0;
    int idx = 6;
    String testName;
    String className;
    TestLogin login = new TestLogin();

    @Rule
    public TestWatcher watcher = new TestWatcher() {
        public void starting(Description description) {
            testName = description.getMethodName();
            className = description.getClassName();
        }
    };

    @Before
    public void seleccionMenuAjustesPerfil() {
        Inicio inicio = new Inicio("Ajustes - Perfil");
        inicio.startApp();
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] + ";  SO:"
                + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4]);

        if ((testName.equals("ajustesPerfilValidacionRegistroEquipo")) || (testName.equals("ajustesPerfilValidacionSinProdCuentas"))) {
            login.registroEquipo = false;
        }
        login.activoClave = false;
        login.loginTest("37");
        Menu menu = new Menu();
        menu.presionaMenuNuevo(11);//Otros > Ajustes
    }

    @Test //0913905089	Zion.BB99	12BB**7p	101601  34
    public void ajustesPerfilOpcionesSI() {
        Reporte.setNombreReporte("PERFIL - Validaci�n Servicios Activos (SI)");
        Perfil perfil = new Perfil();
        perfil.caso = "PERFIL";
        perfil.vp_etiqueta_cabecera("AJUSTES");
        perfil.click_boton_Opcion(10); //Perfil
        while (itrPerfilOpciones.hasNext()) {
            campos = itrPerfilOpciones.next().split("\t");
            linea = linea + 1; //0Sec 1Escenario 2Valor 3Validacion
            if (linea == 1)
                continue;
            if (campos[1].equals("Activo")) {
                perfil.validarOpcionSiNo(idx, "S�", campos[1], campos[2], campos[3]);
                idx += 2;
            }
        }
    }

    @Test //1200098315	Space.BB99	P@ssw0rd	118702  47
    public void ajustesPerfilOpcionesNO() {
        Reporte.setNombreReporte("PERFIL - Validaci�n Servicios Activos (NO)");
        Perfil perfil = new Perfil();
        perfil.caso = "PERFIL";
        perfil.vp_etiqueta_cabecera("AJUSTES");
        perfil.click_boton_Opcion(10); //Perfil
        while (itrPerfilOpciones.hasNext()) {
            campos = itrPerfilOpciones.next().split("\t");
            linea = linea + 1; ////0Sec 1Escenario 2Valor 3Validacion
            if (linea == 1)
                continue;
            switch (campos[1]) {
                case "Registro Default":
                    perfil.validarOpcionSiNo(idx, "No", campos[1], campos[2], "Para transaccionar debes registrar tu dispositivo m�vil");
                    idx += 2;
                    break;
                case "Registro":
                    perfil.validarOpcionSiNo(idx, "No", campos[1], campos[2], campos[3]);
                    idx += 2;
                    break;
            }
        }
    }

    @Test //1200098315	Space.BB99	P@ssw0rd	118702  47
    public void ajustesPerfilValidacionRegistroEquipo() {
        Reporte.setNombreReporte("PERFIL - Validaci�n Registro Equipo");
        Perfil perfil = new Perfil();
        perfil.caso = "PERFIL";
        perfil.vp_etiqueta_cabecera("AJUSTES");
        perfil.click_boton_Opcion(10); //Perfil
        while (itrPerfilOpciones.hasNext()) {
            campos = itrPerfilOpciones.next().split("\t");
            linea = linea + 1; ////0Sec 1Escenario 2Valor 3Validacion
            if (linea == 1)
                continue;
            switch (campos[1]) {
                case "Registro Default":
                    perfil.validarOpcionSiNo(idx, "No", campos[1], campos[2], "Para transaccionar debes registrar tu dispositivo m�vil");
                    idx += 2;
                    break;
                case "Registro":
                    perfil.validarOpcionSiNo(idx, "No", campos[1], campos[2], campos[3]);
                    idx += 2;
                    break;
            }
        }
    }

    @Test //1201184973	Zamba.BB99	P@ssw2rd	37426   37
    public void ajustesPerfilValidacionSinProdCuentas() {
        Reporte.setNombreReporte("PERFIL - Validaci�n Producto Cuentas");
        Perfil perfil = new Perfil();
        perfil.caso = "PERFIL";
        perfil.vp_etiqueta_cabecera("AJUSTES");
        perfil.click_boton_Opcion(10); //Perfil
        while (itrPerfilOpciones.hasNext()) {
            campos = itrPerfilOpciones.next().split("\t");
            linea = linea + 1; ////0Sec 1Escenario 2Valor 3Validacion
            if (linea == 1)
                continue;
            switch (campos[1]) {
                case "Cuenta":
                    perfil.validarOpcionSiNo(idx, "No", campos[1], campos[2], campos[3]);
                    idx += 2;
                    break;
                case "Cuenta Default":
                    perfil.validarOpcionSiNo(idx, "No", campos[1], campos[2], "No posees cuenta para realizar esta transacci�n");
                    idx += 2;
                    break;
            }
        }
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}
