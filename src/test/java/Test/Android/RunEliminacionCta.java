package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

public class RunEliminacionCta {
    @Before
    public void inicio() {
        Inicio inicio = new Inicio("EliminacionCuentas");
        inicio.startApp();

        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +  ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4] );

        /*LOGIN*/
        TestLogin login = new TestLogin();
        login.loginTest("0");

        /*REGISTRO DE EQUIPO*/
        TestRegistroEquipo registro = new TestRegistroEquipo();
        registro.registroTest();

        BarraNavegacion barra = new BarraNavegacion();
        barra.click_opc_transf();

        Menu menu = new Menu();
        //menu.presionaMenu(2); //Transferir
        menu.presionaSubmenu(2, 2); //Administrar
    }

    @Test
    public void eliminacionCtaTest()  {
        Reporte.setNombreReporte("Eliminación de Cuentas");

        List<String> cuentas = Util.getCamposDataPool(System.getProperty("user.dir")+"/archivos/dp_ctas_terceros.txt");
        Iterator<String> itr = cuentas.iterator();
        String[] campos = null;
        int linea = 0;

        Menu menu = new Menu();

        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0banco	1tipo	2numcuenta	3tipoid	4identificacion	5titular	6alias	7favorito	8descripcion	9accion	10monto
            if (linea == 1)
                continue;

            if (campos[9].equals("N"))
                continue;

            if (!campos[0].equals("Bolivariano")) {
                menu.presionaOpcionSubmenuTransf(4, true); //Otros Bancos
            }
            else{
                menu.presionaOpcionSubmenuTransf(2, true); //Terceros
            }

            System.out.println(linea);

            Administrar administrar = new Administrar();
            administrar.vp_etiqueta_titulo();
            administrar.buscaCuenta(campos[6],true);
            administrar.seleccionaCuenta(campos[7], true);
            administrar.click_boton_Eliminar();
            administrar.vp_etiqueta_popup_elimina();
            administrar.click_popup_boton_eliminar();
            administrar.vp_mensaje();
            try {
                Thread.sleep(2000);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
            administrar.click_boton_Atras();
        }
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}
