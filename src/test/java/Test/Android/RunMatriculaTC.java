/************************************************************/
/*  Instituci�n     :   BANCO BOLIVARIANO, C.A.             */
/*  Aplicaci�n      :   24m�vil                             */
/*  Funcionalidad   :   Matriculaci�n Tarjetas de Credito   */
/*  Tester          :   Jazm�n Perero Villon                */
/*  Fecha Creaci�n  :   AGO 19/2020                         */
/************************************************************/
/*                    MODIFICACIONES                        */
/*   Fecha       Tester            Descripcion              */
/* JUL 09/2021  FRODRIGM  Adiciona Thread.sleep al verificar*/
/*        mensaje de matriculacion exitosa (vp_etiqueta_msg)*/
/************************************************************/

package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import io.appium.java_client.android.Activity;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Dimension;

public class RunMatriculaTC {
    @Before
    public void inicio() {
        Inicio inicio = new Inicio("MatriculacionTarjetas");
        inicio.startApp();

        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +  ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4] );

        /*LOGIN*/
        TestLogin login = new TestLogin();
        login.loginTest("0");

        /*REGISTRO DE EQUIPO*/
        TestRegistroEquipo registro = new TestRegistroEquipo();
        registro.registroTest();

        /*Menu  Pagar - Administrar*/
        BarraNavegacion barra = new BarraNavegacion();
        barra.click_opc_pagar();

        Menu menu = new Menu();
        //menu.presionaMenu(3); //Pagar
        menu.presionaSubmenu(3,2); //Administrar
    }

    @Test
    public void matricularTCTest() {
        Reporte.setNombreReporte("Matriculaci�n de Tarjetas de Cr�dito");

        List<String> tarjetas = Util.getCamposDataPool(System.getProperty("user.dir")+"/archivos/dp_tarjetas_terceros.txt");
        Iterator<String> itr = tarjetas.iterator();
        String[] campos = null;
        int linea = 0;

        while (itr.hasNext())
        {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0banco	1tipo	2numtc	3tipoid	4identificacion	5titular	6alias	7favorito	8descripcion	9pagar	10monto
            if (linea == 1)
                continue;

            if (campos[9].equals("N"))
                continue;

            Administrar administrar = new Administrar();
            administrar.click_boton_Matricular("MATRICULAR TARJETA");

            MatriculaTC matricular = new MatriculaTC();
            matricular.seleccionaTipoTC(campos[1]);
            matricular.seleccionaBanco(campos[0]);

            matricular.vp_etiqueta_numTC();
            TecladoNumerico tc = new TecladoNumerico();
            tc.IngresarValor(1, campos[2], false);
            matricular.vp_dato_numTC(campos[2]);
            matricular.click_boton_Continuar();

            if (!campos[0].equals("Bolivariano"))
            {
                matricular.seleccionaIdentificacion(campos[3]);
                matricular.vp_etiqueta_numIdentif();

                tc = new TecladoNumerico();
                tc.IngresarValor(1, campos[4], false);
                matricular.vp_dato_identif(campos[4]);
                matricular.click_boton_Continuar();
            }

            matricular.click_img_Favorito(campos[7]);

            if (!campos[0].equals("Bolivariano")) {
                matricular.ingresoTitular(campos[5]);
                matricular.ingresoAlias(1, campos[6]); // solo 15 caracteres //idx 0 para BB 1 para otros bancos
            }
            else{
                matricular.ingresoAlias(0, campos[6]); // solo 15 caracteres //idx 0 para BB 1 para otros bancos
            }

            Dimension ws = Util.driver.manage().window().getSize();
            Util.TocarPantalla(ws.getWidth()/2,ws.getHeight()/2, ws.getWidth()/2,(ws.getHeight()/2)-200);

            matricular.click_boton_Continuar();

            OTP otp = new OTP();
            otp.genera_valida_Otp();

            matricular.vp_etiqueta_msg();

            try {
                Thread.sleep(2000);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
        }
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}
