/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   CLAVE VIRTUAL - ELIMINACI�N
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   JUL 22/2021
    Fecha Modificaci�n  :   N/A
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Validaci�n PopUp Eliminaci�n acci�n "CANCELAR"
        2. Validaci�n PopUp Eliminaci�n acci�n "ELIMINAR"
           2.1  Validaci�n Base Conocimiento (Preguntas / Respuestas) - Exitoso
        3. Validaci�n Base Conocimiento (Preguntas / Respuestas) - Fallido (3 intentos)
 */
package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.ClaveVirtualOpciones;
import Pom.Android.Inicio;
import Pom.Android.Menu;
import Pom.Android.TestLogin;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RunClaveVirtualEliminacion {
    @Before
    public void seleccionMenuClaveVirtual() {
        Inicio inicio = new Inicio("ClaveVirtualEliminacion");
        inicio.startApp();
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] + ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4]);
        TestLogin login = new TestLogin();
        login.activoClave = false;
        login.loginTest("53");
        Menu menu = new Menu();
        menu.presionaMenuNuevo(7);
    }

    @Test
    public void eliminacionClaveVirtualCancelar() {
        Reporte.setNombreReporte("CLAVE VIRTUAL - Validaci�n de Clave Virtual Eliminaci�n - Bot�n Cancelar PopUp");
        ClaveVirtualOpciones claveVirtualEliminacion = new ClaveVirtualOpciones();
        claveVirtualEliminacion.textoFlujo="CLAVE VIRTUAL - ELIMINACION";
        claveVirtualEliminacion.click_boton_Opcion(11);
        claveVirtualEliminacion.mensaje_popup_Eliminar(1);//1: Cancelar 2: Eliminar
        claveVirtualEliminacion.vp_etiqueta_cabecera("CLAVE VIRTUAL");
    }

    @Test
    public void eliminacionClaveVirtualEliminar() {
        Reporte.setNombreReporte("CLAVE VIRTUAL - Validaci�n de Clave Virtual Eliminaci�n - Bot�n Eliminar PopUp");
        ClaveVirtualOpciones claveVirtualEliminacion = new ClaveVirtualOpciones();
        claveVirtualEliminacion.textoFlujo="CLAVE VIRTUAL - ELIMINACION";
        claveVirtualEliminacion.click_boton_Opcion(11);
        claveVirtualEliminacion.mensaje_popup_Eliminar(2);//1: Cancelar 2: Eliminar
        claveVirtualEliminacion.vp_etiqueta_cabecera("PREGUNTA DE SEGURIDAD");
        claveVirtualEliminacion.pregunta_transaccional();
        claveVirtualEliminacion.mensaje_popup_Aviso();
    }

    @Test
    public void eliminacionClaveVirtualBaseConocimientoFallida() {
        Reporte.setNombreReporte("CLAVE VIRTUAL - Validaci�n de Base Conocimiento (Superar Intentos Fallidos");
        ClaveVirtualOpciones claveVirtualEliminacion = new ClaveVirtualOpciones();
        claveVirtualEliminacion.textoFlujo="CLAVE VIRTUAL - ELIMINACION";
        claveVirtualEliminacion.click_boton_Opcion(11);
        claveVirtualEliminacion.mensaje_popup_Eliminar(2);//1: Cancelar 2: Eliminar
        claveVirtualEliminacion.vp_etiqueta_cabecera("PREGUNTA DE SEGURIDAD");
        for (int i = 0; i < 3; i++) {
            claveVirtualEliminacion.ingreso_respuesta(("ERROR" + i));
            claveVirtualEliminacion.mensaje_popup_Error(i);
            if (i != 2) {
                claveVirtualEliminacion.click_boton_Cambio_Pregunta();
            }
        }
        claveVirtualEliminacion.vp_etiqueta_cabecera("CLAVE VIRTUAL");
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}

