package Test.Android;

import Pom.Android.*;
import Auxiliar.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class RunLoginExitoso {
	@Before
	public void inicio()
	{
		Inicio inicio = new Inicio("Login");
		inicio.startApp();

		Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +  ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4] );
	}

    @Test
	public void loginSIRegistroTest() {
		Reporte.setNombreReporte("Login Exitoso con Registro de Equipo");
		/*LOGIN*/
		TestLogin login = new TestLogin();
		login.loginTest("0");
	}

	@Test
	public void loginNORegistroTest() {
		Reporte.setNombreReporte("Login Exitoso sin Registro de Equipo");
		/*LOGIN*/
		TestLogin login = new TestLogin();
		login.registroEquipo = false;
		login.loginTest("0");
	}

	@After
	public void tearDown() {
    	Reporte.finReporte();
	}

}
