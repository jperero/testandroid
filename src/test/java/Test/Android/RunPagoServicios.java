package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

public class RunPagoServicios {

    @Before
    public void inicio() {
        Inicio inicio = new Inicio("PagodeServicios");
        inicio.startApp();

        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +  ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4] );

        /*LOGIN*/
        TestLogin login = new TestLogin();
        login.loginTest("0");

        /*REGISTRO DE EQUIPO*/
        TestRegistroEquipo registro = new TestRegistroEquipo();
        registro.registroTest();

        BarraNavegacion barra = new BarraNavegacion();
        barra.click_opc_pagar();

        Menu menu = new Menu();
        //menu.presionaMenu(2); //Transferir
        //menu.presionaSubmenu(2, 2); //Administrar
        menu.presionaSubmenu(3, 0); //Pagar Servicio
    }

    @Test
    public void PagoServicios() {
        //Reporte.setNombreReporte("Editar Alias de Tarjetas");
        Reporte.setNombreReporte("Pago de Servicios");
/*
        List<String> tarjetas = Util.getCamposDataPool(System.getProperty("user.dir")+"/archivos/dp_tarjetas_terceros.txt");
        Iterator<String> itr = tarjetas.iterator();
        String[] campos = null;
        int linea = 0;

        //Menu menu = new Menu();

        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            //0banco	1tipo	2numtc	3tipoid	4identificacion	5titular	6alias	7favorito	8descripcion	9pagar	10monto
            if (linea == 1)
                continue;

            if (campos[9].equals("N"))
                continue;

            //System.out.println(linea);

            Administrar administrar = new Administrar();

            //System.out.println(campos[6]);
            String alias = null;
            if (campos[6].length()>14)
                alias = campos[6].substring(0,14);
            else
                alias = campos[6];

            administrar.buscaTarjeta(alias);

            administrar.click_boton_EliminarTC();
            //administrar.vp_etiqueta_popup_eliminaTC();
            //administrar.click_popup_boton_eliminarTC();
            //administrar.vp_mensaje_eliminarTC();

            try {
                Thread.sleep(2000);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
            // administrar.click_boton_Atras();
        }
*/
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }

}
