/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   QUICKPAY - RETIRO SIN TARJETA TERCEROS MATRICULACION BENEFICIARIO
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   JUL 16/2021
    Fecha Modificaci�n  :   N/A
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Matriculaci�n de Beneficiario Favorito
        2. Matriculaci�n de Beneficiario No Favorito
        3. Validaci�n Agregar Beneficiario:
            3.1. Celular:
                 3.1.1. El n�mero de celular debe empezar en 09
                 3.1.2. El n�mero debe de ser de 10 d�gitos
            3.2. C�dula:
                 3.2.1. El n�mero de c�dula no es v�lido
                 3.2.2. El c�digo de la provincia (dos primeros d�gito) es inv�lido
 */
package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

public class RunRetiroSinTarjetaQP_AdmBeneficiario {
    List<String> datosTercero = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_retiro_sin_tarjeta_-_persona_autorizada.txt");
    Iterator<String> itrTercero = datosTercero.iterator();
    String[] campos = null;
    int linea = 0;

    TestLogin login = new TestLogin();

    @Before
    public void seleccionMenuRetiroSinTarjetaQP() {
        Inicio inicio = new Inicio("RunRetiroSinTarjetaQP_AdmBeneficiario.class");
        inicio.startApp();

        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +
                ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4]);

        login.loginTest("43");
        Menu menu = new Menu();
        menu.presionaMenuNuevo(4);

    }

    @Test //0914378005	Carl.BB9	P@ssw3rd	98725   43
    public void retiroSinTarjetaValidacionesAgregarBeneficiario() {
        Reporte.setNombreReporte("QUICKPAY RETIRO SIN TARJETA TERCERO - Validaciones Agregar Beneficiario");

        RetiroSinTarjetaQP retiroSinTarjetaQP = new RetiroSinTarjetaQP();
        retiroSinTarjetaQP.vp_etiqueta_cabecera();
        retiroSinTarjetaQP.click_boton_Retirar();
        retiroSinTarjetaQP.vp_etiqueta_cabecera_retiro_de_efectivo();
        retiroSinTarjetaQP.click_boton_Alguien_mas_hara_el_retiro();
        retiroSinTarjetaQP.vp_etiqueta_cabecera_beneficiario();
        retiroSinTarjetaQP.click_boton_Matricular_beneficiario();
        retiroSinTarjetaQP.vp_etiqueta_cabecera_contacto();

        while (itrTercero.hasNext()) {
            campos = itrTercero.next().split("\t");
            linea = linea + 1; //0Sec	1Escenario	2NumDocumento   3NumCelular 4Favorito   5Alias  6Validacion
            if (linea == 1)
                continue;
            if (!campos[1].equals("Tercero")) {
                retiroSinTarjetaQP.ingreso_dato(campos[2], campos[3]);
                retiroSinTarjetaQP.click_boton_Continuar();
                retiroSinTarjetaQP.mensajeValidacion(campos[1], campos[6]);
            }
        }
    }

    @Test //0914378005	Carl.BB9	P@ssw3rd	98725   43
    public void retiroSinTarjetaAgregarBeneficiarioFavorito() {
        Reporte.setNombreReporte("QUICKPAY RETIRO SIN TARJETA TERCERO - Agregar Beneficiario Favorito");
        OTP otp = new OTP();

        RetiroSinTarjetaQP retiroSinTarjetaQP = new RetiroSinTarjetaQP();
        retiroSinTarjetaQP.vp_etiqueta_cabecera();
        retiroSinTarjetaQP.click_boton_Retirar();
        retiroSinTarjetaQP.vp_etiqueta_cabecera_retiro_de_efectivo();
        retiroSinTarjetaQP.click_boton_Alguien_mas_hara_el_retiro();
        retiroSinTarjetaQP.vp_etiqueta_cabecera_beneficiario();
        retiroSinTarjetaQP.click_boton_Matricular_beneficiario();
        retiroSinTarjetaQP.vp_etiqueta_cabecera_contacto();
        while (itrTercero.hasNext()) {
            campos = itrTercero.next().split("\t");
            linea = linea + 1; //0Sec	1Escenario	2NumDocumento   3NumCelular 4Favorito   5Alias  6Validacion
            if (linea == 1)
                continue;
            if ((campos[1].equals("Tercero")) && (campos[4].equals("S"))) {
                retiroSinTarjetaQP.ingreso_dato(campos[2], campos[3]);
                retiroSinTarjetaQP.click_boton_Continuar();
                retiroSinTarjetaQP.vp_etiqueta_cabecera_detalles();
                retiroSinTarjetaQP.selecciona_favorito();
                retiroSinTarjetaQP.ingreso_alias(campos[5]);
                retiroSinTarjetaQP.click_boton_Confirmar();
                otp.genera_valida_Otp();
                retiroSinTarjetaQP.mensaje_exito(campos[6]);
            }
        }
    }

    @Test //0914378005	Carl.BB9	P@ssw3rd	98725   43
    public void retiroSinTarjetaAgregarBeneficiarioNoFavorito() {
        Reporte.setNombreReporte("QUICKPAY RETIRO SIN TARJETA TERCERO - Agregar Beneficiario No Favorito");
        OTP otp = new OTP();

        RetiroSinTarjetaQP retiroSinTarjetaQP = new RetiroSinTarjetaQP();
        retiroSinTarjetaQP.vp_etiqueta_cabecera();
        retiroSinTarjetaQP.click_boton_Retirar();
        retiroSinTarjetaQP.vp_etiqueta_cabecera_retiro_de_efectivo();
        retiroSinTarjetaQP.click_boton_Alguien_mas_hara_el_retiro();
        retiroSinTarjetaQP.vp_etiqueta_cabecera_beneficiario();
        retiroSinTarjetaQP.click_boton_Matricular_beneficiario();
        retiroSinTarjetaQP.vp_etiqueta_cabecera_contacto();
        while (itrTercero.hasNext()) {
            campos = itrTercero.next().split("\t");
            linea = linea + 1; //0Sec	1Escenario	2NumDocumento   3NumCelular 4Favorito   5Alias  6Validacion
            if (linea == 1)
                continue;
            if ((campos[1].equals("Tercero")) && (campos[4].equals("N"))) {
                retiroSinTarjetaQP.ingreso_dato(campos[2], campos[3]);
                retiroSinTarjetaQP.click_boton_Continuar();
                retiroSinTarjetaQP.vp_etiqueta_cabecera_detalles();
                retiroSinTarjetaQP.ingreso_alias(campos[5]);
                retiroSinTarjetaQP.click_boton_Confirmar();
                otp.genera_valida_Otp();
                retiroSinTarjetaQP.mensaje_exito(campos[6]);
            }
        }
    }

    @After
    public void tearDown() {
        Reporte.finReporte();
    }
}
