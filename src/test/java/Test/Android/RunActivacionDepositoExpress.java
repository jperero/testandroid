/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   ACTIVACION DE DEPOSITO EXPRESS
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   FEB 10/2021
    Fecha Modificaci�n  :   N/A
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Registro Dispositivo Activo
        2. Dispositivo Seguridad Activo
        3. Producto Cuentas Activo
        4. Medios de Env�o Activo (Correo Electr�nico)
        5. Lista Negra
        6. Segmento / Subsegmento (bv_deposito_movil)
 */
package Test.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class RunActivacionDepositoExpress {
    @Before
    public void seleccionMenuDepositoExpress() throws InterruptedException {
        //DEPOSITO EXPRESS - Ejecuta Launch de la app 24m�vil
        Inicio inicio = new Inicio("RunActivacionDepositoExpress.class");
        inicio.startApp();
        Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] + ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4]);
        //DEPOSITO EXPRESS - Realiza Login (Usuario / Contrase�a)
        TestLogin login = new TestLogin();
        login.loginTest("26");
        //DEPOSITO EXPRESS - Selecci�n en Men�: OTROS > DEPOSITO EXPRESS
        login.loginTest("51");
        //DEPOSITO EXPRESS - Selecci�n en Men�: MAS > DEPOSITO EXPRESS
        Menu menu = new Menu();
        menu.presionaMenuNuevo(3);
    }

    @Test // ACTIVACION DE DEPOSITO EXPRESS - SI
    public void activacionDepositoExpressSI() {
        //DEPOSITO EXPRESS - Activar
        Reporte.setNombreReporte("DEPOSITO EXPRESS - Activar");
        DepositoExpress depositoExpress = new DepositoExpress();
        depositoExpress.vp_etiqueta_activacion();
        depositoExpress.click_check();
        depositoExpress.click_boton_Activar();
        OTP otp = new OTP();
        otp.genera_valida_Otp();
        depositoExpress.mensaje_exitoso();
        depositoExpress.vp_etiqueta_deposito_express();
    }

    @Test // ACTIVACION DE DEPOSITO EXPRESS - NO
    public void activacionDepositoExpressNO() {
        //DEPOSITO EXPRESS - No Gracias
        Reporte.setNombreReporte("DEPOSITO EXPRESS - No Gracias");
        DepositoExpress depositoExpress = new DepositoExpress();
        depositoExpress.vp_etiqueta_activacion();
        depositoExpress.click_boton_NoGracias();
        depositoExpress.return_posicion_consolidada();
    }

    @Ignore("No iniciliaza con Before, ya que setea registro.Equipo como false")
    @Test // ACTIVACION DE DEPOSITO EXPRESS - VALIDACI�N REGISTRO DE EQUIPO
    public void activacionDepositoExpressValidacionSinRegEquipo() throws InterruptedException {
        //DEPOSITO EXPRESS - Ejecuta Launch de la app 24m�vil
        Reporte.setNombreReporte("DEPOSITO EXPRESS - Validaci�n Sin Registro Equipo");
        Inicio inicio = new Inicio("RunActivacionDepositoExpress.class");
        inicio.startApp();
        //DEPOSITO EXPRESS - Realiza Login (Usuario / Contrase�a)
        TestLogin login = new TestLogin();
        login.registroEquipo = false;
        login.loginTest("26");
        //DEPOSITO EXPRESS - Selecci�n en Men�: MAS > DEPOSITO EXPRESS
        Menu menu = new Menu();
        menu.presionaMenuNuevo(3);
        //DEPOSITO EXPRESS - Validaci�n Registro de Equipo
        DepositoExpress depositoExpress = new DepositoExpress();
        depositoExpress.vp_mensaje_registro_equipo();
        depositoExpress.click_boton_Registrar();
        TestRegistroEquipo registrarEquipo = new TestRegistroEquipo();
        registrarEquipo.registroTest();
        //DEPOSITO EXPRESS - Selecci�n en Men�: MAS > DEPOSITO EXPRESS
        menu.presionaMenuNuevo(3);
        //DEPOSITO EXPRESS - Activar
        depositoExpress.vp_etiqueta_activacion();
        depositoExpress.click_check();
        depositoExpress.click_boton_Activar();
        OTP otp = new OTP();
        otp.genera_valida_Otp();
        depositoExpress.mensaje_exitoso();
        depositoExpress.vp_etiqueta_deposito_express();
/*        Salir salir = new Salir();
        salir.massalirAPP();*/
    }

    @Test // ACTIVACION DE DEPOSITO EXPRESS - VALIDACI�N DISPOSITIVO DE SEGURIDAD
    public void activacionDepositoExpressValidacionSinDispSeguridad(){
        //DEPOSITO EXPRESS - Validaci�n Dispositivo Seguridad  //0909956872	Wess.BB99	P@ssw0rd	19155   50
        Reporte.setNombreReporte("DEPOSITO EXPRESS - Validaci�n Dispositivo Seguridad");
        DepositoExpress depositoExpress = new DepositoExpress();
        depositoExpress.vp_mensaje_dispositivo_seguridad();
    }

    @Test // ACTIVACION DE DEPOSITO EXPRESS - VALIDACI�N SIN PRODUCTO CUENTAS
    public void activacionDepositoExpressValidacionSinProdCuentas(){
        //DEPOSITO EXPRESS - Validaci�n Producto Cuentas //1201184973	Zamba.BB99	P@ssw2rd	37426   37
        Reporte.setNombreReporte("DEPOSITO EXPRESS - Validaci�n Producto Cuentas");
        DepositoExpress depositoExpress = new DepositoExpress();
        depositoExpress.vp_mensaje_sin_producto_cuentas();
    }

    @Test // ACTIVACION DE DEPOSITO EXPRESS - VALIDACI�N SIN MEDIOS ENVIO
    public void activacionDepositoExpressValidacionMediosEnvio(){
        //DEPOSITO EXPRESS - Validaci�n Sin Medios Envio  //1301984074	Ozzy.BB99	P@ssw0rd	17231   51
        Reporte.setNombreReporte("DEPOSITO EXPRESS - Validaci�n Sin Medios Envio");
        DepositoExpress depositoExpress = new DepositoExpress();
        depositoExpress.vp_etiqueta_activacion();
        depositoExpress.click_check();
        depositoExpress.click_boton_Activar();
        depositoExpress.vp_mensaje_sin_medios_envio();
        depositoExpress.click_boton_Cancelar();
        depositoExpress.click_boton_NoGracias();
        depositoExpress.return_posicion_consolidada();
    }

    @Test // ACTIVACION DE DEPOSITO EXPRESS - VALIDACI�N LISTA NEGRA
    public void activacionDepositoExpressValidacionListaNegra() throws InterruptedException {
        //DEPOSITO EXPRESS - Validaci�n Lista Negra
        Reporte.setNombreReporte("DEPOSITO EXPRESS - Validaci�n Lista Negra");
        DepositoExpress depositoExpress = new DepositoExpress();
        depositoExpress.mensaje_error();
        Thread.sleep(5000);
        depositoExpress.return_posicion_consolidada();
    }

    @Test // ACTIVACION DE DEPOSITO EXPRESS - VALIDACI�N SEGMENTO / SUBSEGMENTO
    public void activacionDepositoExpressValidacionSegmentoSubsegmento() throws InterruptedException {
        //DEPOSITO EXPRESS - Validaci�n Segmento / Subsegmento
        Reporte.setNombreReporte("DEPOSITO EXPRESS - Validaci�n Segmento / Subsegmento");
        DepositoExpress depositoExpress = new DepositoExpress();
        depositoExpress.mensaje_error();
        Thread.sleep(2000);
        depositoExpress.return_posicion_consolidada();
    }

/*    @After
        public void salirApp(){
            Salir salir = new Salir();
            salir.massalirAPP();
        }*/
    @After
    public void tearDown() {
        Reporte.finReporte();
    }

/*    @After
    public void salirApp(){
        Salir salir = new Salir();
        salir.massalirAPP();
    }*/
    @After
    public void tearDown() {Reporte.finReporte();}

}
