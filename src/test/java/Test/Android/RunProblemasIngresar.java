package Test.Android;

import Auxiliar.*;
import Pom.Android.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;

public class RunProblemasIngresar
{
	@Before
	public void inicio() {
		Inicio inicio = new Inicio("ProblemasIngresar_Contrasena");
		inicio.startApp();
		//inicio.startBrowser();

		Reporte.setEntorno("Ambiente: Desarrollo2" + "</b><br>" + "Dispositivo:" + Util.getDataDispositivo()[1] +  ";  SO:" + Util.getDataDispositivo()[3] + " Version:" + Util.getDataDispositivo()[4] );

		/*LOGIN*/
		TestLogin login = new TestLogin();
		login.irAProblemasIngresarTest();

	}

	@Test
	public void cambioContrasenaTest()  {
		Reporte.setNombreReporte("Cambio de Contrase�a desde problemas para ingresar");

		List<String> ctas = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_login.txt");
		Iterator<String> itr = ctas.iterator();
		String[] campos = null;
		int linea = 0;

		while (itr.hasNext()) {
			campos = itr.next().split("\t");
			linea = linea + 1;
			//0sec	1Login	2Password	3Identificacion	4TipoId	5NewPass	6Pos_TC	7Pago	8ResetPssw	9Respuesta
			if (linea == 1)
				continue;

			if (campos[8].equals("N"))
				continue;

			LoginIngresar pantalla = new LoginIngresar();
			pantalla.click_link_problemas();

			VerificarUsuario usuario = new VerificarUsuario();
			usuario.vp_etiqueta_verificar();
			usuario.seleccionaIdentificacion(campos[4]);
			usuario.ingresaIdentificacion(campos[4], campos[3]);
			usuario.click_boton_continuar();

			OTP otp = new OTP();
			otp.genera_valida_Otp();

			usuario.vp_etiqueta_bienvenido();
			usuario.click_boton_cambiarcontra();

			PreguntaSeguridad pregunta = new PreguntaSeguridad();
			pregunta.vp_etiqueta_pregunta();
			pregunta.contestar_pregunta();
			pregunta.click_boton_aceptar();

			if (pregunta.error_respuesta())
			{
				pregunta.click_boton_cambiarpregunta();
				pregunta.contestar_pregunta(campos[9]);
				pregunta.click_boton_aceptar();
			}

			CambiarContrasena contrasena = new CambiarContrasena();
			contrasena.vp_etiqueta_cambiar();
			contrasena.vp_etiqueta_nueva();
			contrasena.ingreso_nueva(campos[5]);
			contrasena.click_boton_ojo();
			contrasena.vp_etiqueta_repite();
			contrasena.ingreso_repite(campos[5]);
			contrasena.click_boton_cambiar();
			contrasena.vp_mensaje();
		}

	}

	@Test
	public void ObtenerUsuario(){
		Reporte.setNombreReporte("Obtener Usuario desde problemas para ingresar");

		List<String> ctas = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_login.txt");
		Iterator<String> itr = ctas.iterator();
		String[] campos = null;
		int linea = 0;

		while (itr.hasNext()) {
			campos = itr.next().split("\t");
			linea = linea + 1;
			//0sec	1Login	2Password	3Identificacion	4TipoId	5NewPass	6Pos_TC	7Pago	8ResetPssw	9Respuesta
			if (linea == 1)
				continue;

			if (campos[8].equals("N"))
				continue;

			LoginIngresar pantalla = new LoginIngresar();
			pantalla.click_link_problemas();

			VerificarUsuario usuario = new VerificarUsuario();
			usuario.vp_etiqueta_verificar();
			usuario.seleccionaIdentificacion(campos[4]);
			usuario.ingresaIdentificacion(campos[4], campos[3]);
			usuario.click_boton_continuar();

			OTP otp = new OTP();
			otp.genera_valida_Otp();

			usuario.vp_etiqueta_bienvenido();
			usuario.click_boton_ingresarcomo();

			PreguntaSeguridad pregunta = new PreguntaSeguridad();
			pregunta.vp_etiqueta_pregunta();
			pregunta.contestar_pregunta();
			pregunta.click_boton_aceptar();

			if (pregunta.error_respuesta())
			{
				pregunta.click_boton_cambiarpregunta();
				pregunta.contestar_pregunta(campos[9]);
				pregunta.click_boton_aceptar();
			}

			//repeated = new String(new char[n]).replace("\0", s);
			//Donde n es el n�mero de veces que desea repetir la cadena y s es la cadena a repetir
			int longusu = campos[1].length() - 4;
			String caracter = new String(new char[longusu]).replace("\0", "*");
			String usurecup = campos[1].substring(0,2)+caracter+campos[1].substring(longusu+2);
			//System.out.println(usurecup);

			usuario.vp_usuario_recuperado(usurecup);

		}

	}

	@After
	public void tearDown() {
		Reporte.finReporte();
	}
}
