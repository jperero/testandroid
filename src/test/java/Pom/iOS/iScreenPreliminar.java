package Pom.iOS;

import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.*;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;

public class iScreenPreliminar {
    public iScreenPreliminar() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
    }

    @WithTimeout(time=15, chronoUnit = ChronoUnit.SECONDS)
    @iOSXCUITFindBy(accessibility = "lblClaveVirtual")
    MobileElement opcClaveVirtual = null;

    public void click_boton_ClaveVirtual()
    {
        // PANTALLA PRELIMINAR - VERIFICA CLAVE VIRTUAL
        String actual = opcClaveVirtual.getText();
        Util.assert_igual("PANTALLA PRELIMINAR", "Verificación de Opción de Clave Virtual", actual, "Clave Virtual", true , "N");
        opcClaveVirtual.click();
    }
}
