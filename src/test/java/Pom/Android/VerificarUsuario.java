package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class VerificarUsuario {
    public VerificarUsuario() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> edit = null;

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> imagen = null;

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    public void vp_etiqueta_verificar() {
        String actual = texto.get(0).getText();
        Util.assert_igual("VERIFICAR USUARIO", "Verificación de etiqueta", actual,"VERIFICAR USUARIO", true , "N");
    }

    public void seleccionaIdentificacion(String tipoid) {
        String actual = texto.get(3).getText();
        Util.assert_igual("VERIFICAR USUARIO", "Verificación de etiqueta", actual, "Tipo de Identificación", true , "N");

        imagen.get(0).click(); //combo
        String identifica = null;
        identifica = tipoid;
        int idx = 0;
        switch (tipoid) {
            case "C": identifica = "Cédula"; idx=1;  break;
            case "P": identifica = "Pasaporte"; idx=2;  break;
        }

        actual = texto.get(idx).getText();
        Util.assert_igual("MATRICULACIÓN DE CUENTA", "Selecciona Tipo de Identificación", actual, identifica, true , "N");
        texto.get(idx).click();
    }

    public void ingresaIdentificacion(String tipo, String identificacion) {
        if (tipo.equals("C"))
        {
            TecladoNumerico ingreso = new TecladoNumerico();
            ingreso.IngresarValor(0,identificacion,false);
        }
        else{
            edit.get(0).sendKeys(identificacion);
        }
        Reporte.agregarPaso("VERIFICAR USUARIO", "Ingresa identificación", identificacion,"", true , "N");
    }

    public void click_boton_continuar() {
        String actual = boton.get(0).getText();
        Util.assert_igual("VERIFICAR USUARIO", "Presiona botón Continuar", actual,"CONTINUAR", false , "N");
        boton.get(0).click();
    }

    public void vp_etiqueta_bienvenido()
    {
        String actual = texto.get(2).getText();
        Util.assert_igual("VERIFICAR USUARIO", "Verificación de etiqueta", actual,"Bienvenido", true , "N");
    }

    public void click_boton_cambiarcontra()
    {
        String actual = texto.get(6).getText();
        Util.assert_contiene("VERIFICAR USUARIO", "Verificación de etiqueta", actual,"Cambiar Contrase", false , "N");
        texto.get(6).click();
    }

    public void click_boton_ingresarcomo()
    {
        String actual = texto.get(4).getText();
        Util.assert_contiene("VERIFICAR USUARIO", "Verificación de etiqueta", actual,"Ingresar como", false , "N");
        texto.get(4).click();
    }

    public void vp_usuario_recuperado(String usuario) {
        String actual = edit.get(0).getText();
        Util.assert_igual("VERIFICAR USUARIO", "Verificación de Usuario Recuperado", actual,usuario, true , "N");
    }

}
