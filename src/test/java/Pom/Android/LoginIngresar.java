package Pom.Android;

import Auxiliar.*;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.support.PageFactory;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class LoginIngresar {
    public LoginIngresar() {
        if (Util.plataforma.equals("iOS"))
            PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
        else
            PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time=3, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> txtedit = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @iOSXCUITFindBy(accessibility = "tbxUsername")
    MobileElement txtuser = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @iOSXCUITFindBy(accessibility = "tbxPassword")
    MobileElement txtpssw = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @iOSXCUITFindBy(accessibility = "flxHide")
    MobileElement img_mostrar = null;

    @WithTimeout(time=3, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @iOSXCUITFindBy(accessibility = "btnLogIn")
    MobileElement btn_ingresar = null;

    @WithTimeout(time=2, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> imagen = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
    @iOSXCUITFindBy(accessibility = "lblPopup")
    MobileElement mensaje_error = null;

    @WithTimeout(time=2, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView")
    MobileElement mensaje_loader = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time=3, chronoUnit = ChronoUnit.SECONDS)
    @iOSXCUITFindBy(accessibility = "CopylblDesc0ccb1b6c88ed94e")
    MobileElement popup_msg = null;

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[2]")
    @iOSXCUITFindBy(accessibility = "CopylblDesc0dc80597723da4a")
    MobileElement popup_contrasena = null;

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.TextView")
    MobileElement popup_aceptar = null;

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup[3]/android.widget.TextView")
    MobileElement popup_aceptar2 = null;

    public Boolean flag_pssw_obligatorio = false;
    public Boolean flag_pssw_preaviso = false;
    public Boolean flag_pssw_bloqueo = false;

    void ingreso_usuario()
    {
        //Usuario
        if (Util.plataforma.equals("iOS")) {
            txtuser.clear();
            txtuser.sendKeys(Util.getDataCliente()[1]);
        }
        else {
            txtedit.get(0).clear();
            txtedit.get(0).sendKeys(Util.getDataCliente()[1]);
        }
        //Util.driver.pressKey(new KeyEvent(AndroidKey.ENTER));
        Reporte.agregarPaso("LOGIN","Ingresa Usuario:", Util.getDataCliente()[1],"", true, "N");
    }

    void ingreso_contrasena()
    {
        //Contrase�a
        if (Util.plataforma.equals("iOS")) {
            txtpssw.clear();
            txtpssw.sendKeys(Util.getDataCliente()[2]);
            img_mostrar.click();
        }
        else {
            txtedit.get(1).clear();
            txtedit.get(1).sendKeys(Util.getDataCliente()[2]);
        }
        //Util.driver.pressKey(new KeyEvent(AndroidKey.ENTER));
        Reporte.agregarPaso("LOGIN","Ingresa Contrase�a:", Util.getDataCliente()[2], "",true, "N");
    }

    public void click_boton_Ingresar()
    {
        //Ingresar
        String actual = null;
        if (Util.plataforma.equals("iOS"))
            actual = btn_ingresar.getText();
        else
            actual = boton.get(0).getText();
        Util.assert_igual("LOGIN", "Presiona bot�n Ingresar", actual, "INGRESAR",false, "N");

        if (Util.plataforma.equals("iOS"))
            btn_ingresar.click();
        else
            boton.get(0).click();
    }

    void vp_etiqueta_msg()
    {
        String actual = null;
        try {
            actual = mensaje_error.getText();
            this.validacion_msg(actual);
        }
        catch (Exception e) {
            System.out.println("LoginIngresar-Sin alerta en rojo \n");
        }

        try {
            if ((Util.plataforma.equals("iOS")) && (actual == null)) {
                actual = popup_msg.getText();
                this.validacion_msg(actual);
            }
        }
        catch (Exception e) {
            System.out.println("LoginIngresar-Sin popup \n");
        }
    }

    void validacion_msg(String actual)
    {
        System.out.println("LoginIngresar->"+actual);
        if(actual.equals("Cambiar Contrase�a"))
            flag_pssw_obligatorio =  true;
        else if(actual.contains("Deseas cambiar tu Contrase�a ahora"))
            flag_pssw_preaviso =  true;
        else if(actual.contains("Por tu seguridad tu usuario se encuentra bloqueado."))
            flag_pssw_bloqueo =  true;
        else
            Util.assert_igual("LOGIN", "Verificaci�n de acceso al canal", actual, "MIS PRODUCTOS", true, "N");
    }

    public void click_boton_IngresarCon()
    {
        //Ingresar
        try {
            String actual = boton.get(1).getText();
            Util.assert_contiene("LOGIN", "Presiona bot�n Ingresar Con", actual, "INGRESAR CON",false, "N");
            boton.get(1).click();
        }
        catch (Exception e) {
            //Util.logbm.info("LOGIN SIN MENSAJE DE ERROR \n" );
        }
    }

    void vp_verifica_loader(String msgesperado)
    {
        Boolean flag = true;
        int cont = 0;
        while (flag) {
            try {
                String actual = mensaje_loader.getText();
                System.out.println("-1");
                cont++;
                if (cont == 1) {
                    Reporte.agregarPaso("LOGIN", "Presentaci�n de Loader Din�mico", msgesperado, "",true, "N");
                }
                if (!actual.equals(msgesperado)) {
                    flag = false;
                    System.out.println("0");
                }
            } catch (Exception e) {
                flag = false;
                System.out.println("1");
            }
        }
    }

    public void loginIngresar(){
        this.ingreso_usuario();
        this.ingreso_contrasena();
        this.click_boton_Ingresar();
        this.vp_verifica_loader("Estamos cargando tu informaci�n...");
        this.vp_etiqueta_msg();
    }

    public void vp_etiqueta_problemas(){
        String actual = texto.get(2).getText();
        Util.assert_contiene("PANTALLA LOGIN", "Verificaci�n de etiqueta", actual, "Problemas para ingresar",true, "N");
    }

    public void click_link_problemas(){
        String actual = texto.get(2).getText();
        Util.assert_contiene("PANTALLA LOGIN", "Presiona link Problemas para ingresar", actual, "Problemas para ingresar",true, "N");
        texto.get(2).click();
    }

    public void vp_popup_msgcambio()
    {
        String actual = popup_contrasena.getText();
        Util.assert_contiene("POPUP CAMBIO CONTRASE�A", "Verificaci�n de etiqueta", actual, "Por motivos de seguridad, debes cambiar tu contrase�a",true, "N");
    }

    public void click_popup_aceptar(){
        String actual = null;
        if (this.flag_pssw_preaviso)
            actual = popup_aceptar2.getText();
        else
            actual = popup_aceptar.getText();

        Util.assert_contiene("POPUP CAMBIO CONTRASE�A", "Click en bot�n Aceptar", actual, "Aceptar",false, "N");

        if (this.flag_pssw_preaviso)
            popup_aceptar2.click();
        else
            popup_aceptar.click();
    }

    public void vp_popup_msgbloqueo()
    {
        String actual = popup_contrasena.getText();
        Util.assert_contiene("POPUP CAMBIO CONTRASE�A POR BLOQUEO", "Verificaci�n de etiqueta", actual, "Para continuar necesitas cambiar tu contrase�a.",true, "N");
    }

    public void click_popup_continuar(){
        String actual = boton.get(2).getText();
        Util.assert_contiene("POPUP CAMBIO CONTRASE�A POR BLOQUEO", "Click en bot�n Continuar", actual, "Continuar",false, "N");
        boton.get(2).click();
    }
}


