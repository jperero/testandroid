package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class TransferenciaTercerosREF {
    public TransferenciaTercerosREF() {
        if (Util.plataforma.equals("iOS"))
            PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
        else
            PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> imagen = null;

    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> editor = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.Button[@text='CONTINUAR']")
    AndroidElement boton_Continuar = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.Button[@text='CANCELAR']")
    AndroidElement boton_Cancelar = null;


    public String caso = "";
    int saldoDisponible = 0;
    TecladoNumerico tecladoDatos = new TecladoNumerico();

    public void display() {
        int idx = 0;
        System.out.println("/////INICIO PANTALLA " + texto.get(0).getText());
        System.out.println("TEXTO:-------------------------------------->");
        for (AndroidElement x : texto) {
            System.out.println("IDX: " + idx + " VALOR: " + x.getText());
            idx += 1;
        }
        idx = 0;
        System.out.println("BOTON:-------------------------------------->");
        for (AndroidElement y : boton) {
            System.out.println("IDX: " + idx + " VALOR: " + y.getText());
            idx += 1;
        }
        System.out.println("/////FIN PANTALLA");
    }

    public void validar_Datos(String datoDescripcion) {
        editor.get(0).setValue(datoDescripcion);
        Reporte.agregarPaso(caso, "Dato de Descripci�n", datoDescripcion, "", true, "N");
    }

    public void vp_etiqueta_cabecera(String esperado) {
        String actual = texto.get(0).getText();
        Util.assert_igual(caso, "Verificaci�n Cabecera", actual, esperado, false, "N");
    }

    public String via_transferencia() {
        return texto.get(0).getText();
    }

    public void seccion_plegar() {
        imagen.get(1).click();
    }

    public void seleccion_tipo_transferencia(String opcion) {
        int idx = 0;
        switch (opcion) {
            case "Propias":
                idx = 2;
                break;
            case "Terceros":
                idx = 4;
                break;
            case "Otros Bancos":
                idx = 6;
                break;
        }
        String actual = texto.get(idx).getText();
        Util.assert_igual(caso, "Presiona bot�n " + opcion, actual, opcion, true, "N");
        texto.get(idx).click();
    }

    public void seleccion_ordenante(String ordenante) {
        ordenante = "-" + ordenante;
        try {
            Util.assert_contiene(caso, "Selecci�n de ordenante", Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + ordenante + "')]")).getText(), ordenante, true, "N");
            Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + ordenante + "')]")).click();
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + ordenante);
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void seleccion_beneficiario(String beneficiario) {
        try {
            Util.assert_contiene(caso, "Selecci�n de beneficiario", Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + beneficiario + "')]")).getText(), beneficiario, true, "N");
            System.out.println("BENEFICIARIO: "+Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + beneficiario + "')]")).getText());
            Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + beneficiario + "')]")).click();
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + beneficiario);
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void seleccion_via_transferencia(String via_transferencia) {
        try {
            Util.assert_igual(caso, "Selecci�n de Via de Transferencia", Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + via_transferencia + "']")).getText(), via_transferencia, true, "N");
            Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + via_transferencia + "']")).click();
        } catch (Exception e) {
            System.out.println("No se encontr� el elemento buscado: " + via_transferencia);
        }
    }

    public void ingreso_monto(String monto) {
        TecladoNumerico tecladoNumerico = new TecladoNumerico();
        tecladoNumerico.IngresarValor(2, monto, true);
    }

    public void ingreso_monto(String monto, String escenario) {
        if (escenario.equals("Valor excede al saldo disponible")) {
            saldoDisponible = (int) ((Double.parseDouble(texto.get(4).getText().substring(0, (texto.get(4).getText().length() - 4)).replaceAll(",", "")) + Double.parseDouble(monto)) * 100);
            monto = (String.valueOf(saldoDisponible).replaceAll(",", ""));
        }
        TecladoNumerico tecladoNumerico = new TecladoNumerico();
        tecladoNumerico.IngresarValor(2, monto, true);
    }

    public void borra_monto(String monto, String escenario) {
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (escenario.equals("Valor excede al saldo disponible")) {
            saldoDisponible = (int) ((Double.parseDouble(texto.get(4).getText().substring(0, (texto.get(4).getText().length() - 4)).replaceAll(",", "")) + Double.parseDouble(monto)) * 100);
            monto = (String.valueOf(saldoDisponible).replaceAll(",", ""));
        }
        for (int i = 0; i < monto.length(); i++) {
            imagen.get(2).click();
        }
    }

    public void mensajeValidacion(String validacion, String esperado) {
        try {
            Util.assert_igual(caso, "Validaci�n " + validacion, Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
    }

    public void mensaje_error(String esperado) {
        try {
            if (esperado.equals("Valor excede al saldo disponible")) {
                Util.assert_contiene(caso, "Validaci�n " + esperado, Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + esperado + "')]")).getText(), esperado, true, "N");
            } else {
                Thread.sleep(5000);
                Util.assert_igual(caso, "Validaci�n " + esperado, Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
            }
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void click_boton_Transferir() {
        String esperado = "TRANSFERIR";
        try {
            Util.assert_contiene(caso, "Presiona bot�n Transferir", Util.driver.findElement(By.xpath("//android.widget.Button[contains(@text,'" + esperado + "')]")).getText(), esperado, true, "N");
            boton.get(1).click();
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
    }

    public void click_boton_Cambiar() {
        Util.assert_igual(caso, "Presiona bot�n Cambiar", boton.get(1).getText(), "CAMBIAR", true, "N");
        boton.get(1).click();//Bot�n Cambiar
    }

    public void click_boton_Continuar() {
        Util.assert_igual(caso, "Presiona bot�n Continuar", boton_Continuar.getText(), "CONTINUAR", true, "N");
        boton_Continuar.click();
    }

    public void click_boton_Cancelar() {
        Util.assert_igual(caso, "Presiona bot�n Cancelar", boton_Cancelar.getText(), "CANCELAR", true, "N");
        boton_Cancelar.click();
    }
}
