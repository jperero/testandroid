package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Administrar {
    public Administrar() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> edit = null;

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> imagen = null;

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
    MobileElement mensaje = null;

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    MobileElement alias_tc = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='�Eliminaci�n exitosa!']")
    static AndroidElement msj_eliminaTC_exitosa = null;

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView")
    MobileElement popup_edit_tc = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    //@AndroidFindBy(xpath = "//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[2]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView")
    MobileElement edita_opcion_tc = null;

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView")
    MobileElement popup_edit_cta = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[2]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView")
    MobileElement edita_alias_cta = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[2]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[2]/android.widget.TextView")
    MobileElement edita_marcafav_cta = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='�Edici�n exitosa!']")
    static AndroidElement msj_edicion_exitosa = null;

    //-----CUENTAS TERCEROS - OTROS BANCOS

    public void vp_etiqueta_titulo() {
        String txt_expected1 = "TERCEROS";
        String txt_expected2 = "OTROS BANCOS";

        String actual = texto.get(0).getText();
        Util.assert_contiene("ADMINISTRAR", "Verificaci�n de etiqueta", actual, txt_expected1, txt_expected2, false, "N");
    }

    public void buscaCuenta(String cuenta, Boolean reporte)
    {
        String cuenta_= null;
        if (cuenta.length()>14)
            cuenta_ = cuenta.substring(0,14);
        else
            cuenta_ = cuenta;

        edit.get(0).sendKeys(cuenta_);
        if (reporte) {
            Reporte.agregarPaso("ADMINISTRAR", "Busca cuenta matriculada",cuenta_,"",true,"N");
        }
    }

    public void seleccionaCuenta(String favorito, Boolean reporte)
    {
        int idx = 0;
        if (favorito.equals("S")) 
            idx=2;
        else
            idx=3;

        if (texto.size()>2)
        {
            String actual = texto.get(idx).getText();
            if (reporte) {
                Reporte.agregarPaso("ADMINISTRAR", "Selecciona cuenta matriculada", actual, "", true, "N");
            }
            texto.get(idx).click();
        }
        else {
            if (reporte) {
                String actual = texto.get(1).getText();
                Reporte.agregarPaso("ADMINISTRAR", "Selecciona cuenta matriculada", actual, "", true,1, "N");
            }
        }
    }

    public void click_boton_Matricular(String etiqueta) {
        //"MATRICULAR TARJETA"  "MATRICULAR CUENTA"
        String actual = boton.get(0).getText();
        Util.assert_igual("ADMINISTRAR", "Presiona bot�n Matricular", actual, etiqueta, true, "N");
                          boton.get(0).click();
    }

    public void click_boton_Eliminar() {
        String actual = boton.get(1).getText();
        Util.assert_igual("ADMINISTRAR - ELIMINAR CUENTA", "Presiona bot�n Eliminar", actual, "ELIMINAR", true, "N");
        boton.get(1).click();
    }

    public void vp_etiqueta_popup_elimina() {
        String actual = texto.get(11).getText();
        Util.assert_contiene("ADMINISTRAR - ELIMINAR CUENTA", "Verificaci�n de etiqueta", actual, "Deseas eliminar por completo",  true, "N");
    }

    public void click_popup_boton_eliminar() {
        String actual = texto.get(13).getText();
        Util.assert_igual("ADMINISTRAR - ELIMINAR CUENTA", "Presiona bot�n Eliminar de popup", actual, "Eliminar", false, "N");
        texto.get(13).click();
    }

    public void vp_mensaje() {
        String actual = mensaje.getText();
        //String actual = texto.get(0).getText();
        Util.assert_contiene("ADMINISTRAR - ELIMINAR CUENTA", "Verificaci�n de mensaje de transancci�n", actual,"�Eliminaci�n exitosa!", true , "N");
    }

    public void click_boton_Atras() {
        imagen.get(0).click();
    }

    //EDITAR CUENTAS (ALIAS / MARCA FAVORITO)

    public void click_boton_Editar(String tipo, Boolean reporte, int idx) {
        //idx 0 = TC ; idx 2 = CTA
        if (reporte) {
            String actual = boton.get(idx).getText();
            Util.assert_igual("ADMINISTRAR - EDITAR " + tipo, "Presiona bot�n Editar", actual, "EDITAR", true, "N");
        }
        boton.get(idx).click();
    }

    public void selec_popup_editar(String tipo, String opcion, Boolean reporte) {
        if (tipo.equals("TARJETA")) {
            edita_opcion_tc = Util.driver.findElementByXPath("//android.widget.TextView[@text='" + opcion + "']");
            if (reporte) {
                String actual = edita_opcion_tc.getText();
                Util.assert_contiene("ADMINISTRAR - EDITAR " + tipo, "Selecciona Opci�n a Editar", actual, opcion, false, "N");
            }
            edita_opcion_tc.click();
        }
        else {
            if (opcion.equals("Alias")) {
                if (reporte) {
                    String actual = edita_alias_cta.getText();
                    Util.assert_contiene("ADMINISTRAR - EDITAR " + tipo, "Selecciona Opci�n a Editar", actual, opcion, false, "N");
                }
                edita_alias_cta.click();
            }
            else {
                if (reporte) {
                    String actual = edita_marcafav_cta.getText();
                    Util.assert_contiene("ADMINISTRAR - EDITAR " + tipo, "Selecciona Opci�n a Editar", actual, opcion, false, "N");
                }
                edita_marcafav_cta.click();
            }
        }
    }

    public void editar_alias(String tipo, String aliasmod, Boolean reporte) {
        if (reporte) {
            String alias_act = edit.get(0).getText();
            Reporte.agregarPaso("ADMINISTRAR - EDITAR " + tipo, "Alias Actual", alias_act, "", true, "N");
        }

        //Borra el alias anterior e ingresa el nuevo
        edit.get(0).clear();
        edit.get(0).sendKeys(aliasmod);
        if (reporte) {
            Reporte.agregarPaso("ADMINISTRAR - EDITAR " + tipo, "Nuevo Alias", aliasmod, "", true, "N");
        }
    }

    //-----TARJETAS DE CREDITO

    public void buscaTarjeta(String tarjeta_alias, Boolean reporte)
    {
        alias_tc=Util.driver.findElementByXPath("//android.widget.TextView[contains(@text,'"+ tarjeta_alias + "')]");
        alias_tc.click();

        if (reporte) {
            Reporte.agregarPaso("ADMINISTRAR", "Busca tarjeta matriculada", tarjeta_alias, "", true, "N");
        }
    }

    //ELIMINAR TARJETAS DE CREDITO

    public void click_boton_EliminarTC() {
        String actual = boton.get(1).getText();
        Util.assert_igual("ADMINISTRAR - ELIMINAR TARJETA", "Presiona bot�n Eliminar", actual, "ELIMINAR", true, "N");
        boton.get(1).click();
    }

    public void vp_etiqueta_popup_eliminaTC() {
        String actual = texto.get(0).getText();
        Util.assert_contiene("ADMINISTRAR - ELIMINAR TARJETA", "Verificaci�n de etiqueta", actual, "Deseas eliminar por completo",  true, "N");
    }

    public void click_popup_boton_eliminarTC() {
        String actual = texto.get(2).getText();
        Util.assert_igual("ADMINISTRAR - ELIMINAR TARJETA", "Presiona bot�n Eliminar de popup", actual, "Eliminar", false, "N");
        texto.get(2).click();
    }

    public void vp_mensaje_eliminarTC() {
        Util.assert_igual("ADMINISTRAR - ELIMINAR TARJETA", "Verificaci�n de mensaje", msj_eliminaTC_exitosa.findElement(By.xpath("//*[@text]")).getText(), "�Eliminaci�n exitosa!", true, "N");
    }

    //EDITAR TARJETAS DE CREDITO

    public void click_boton_EditarTC(Boolean reporte) {
        if (reporte) {
            String actual = boton.get(0).getText();
            Util.assert_igual("ADMINISTRAR - EDITAR TARJETA", "Presiona bot�n Editar", actual, "EDITAR", true, "N");
        }
        boton.get(0).click();
    }

    public void vp_popup_editar(String tipo) {
        String actual = null;
        if (tipo.equals("CUENTA")) {
            actual = popup_edit_cta.getText();
        }
        else {
            actual = popup_edit_tc.getText();
        }

        Util.assert_contiene("ADMINISTRAR - EDITAR " + tipo, "Muestra Popup Editar Matriculacion", actual,"Editar Matriculaci�n", true , "N");
    }

    public void click_boton_confirmar_editar(Boolean reporte, String tipo) {
        if (reporte) {
            String actual = boton.get(1).getText();
            Util.assert_igual("ADMINISTRAR - EDITAR " + tipo, "Presiona bot�n Confirmar", actual, "CONFIRMAR", true, "N");
        }
        boton.get(1).click();
    }

    public void vp_mensaje_edicion(Boolean reporte, String tipo) {
        if (reporte) {
            Util.assert_igual("ADMINISTRAR - EDITAR " + tipo, "Verificaci�n de mensaje", msj_edicion_exitosa.findElement(By.xpath("//*[@text]")).getText(), "�Edici�n exitosa!", true, "N");
        }
        else {
            String actual = msj_edicion_exitosa.findElement(By.xpath("//*[@text]")).getText();

            try {
                assertThat(actual, is("�Edici�n exitosa!"));
            }
            catch (AssertionError e) {
                System.out.println(e.getMessage());
                Assert.fail();
            }
        }
    }
}
