package Pom.Android;

import Auxiliar.BD;
import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class OTP {

    public OTP() {
        if (Util.plataforma.equals("iOS"))
            PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
        else
            PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> imagen = null;

    @WithTimeout(time=6, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> txt_otp = null;

    @WithTimeout(time=6, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
    MobileElement mensaje_error = null;

    @WithTimeout(time=3, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView")
    MobileElement mensaje_loader = null;

    void vp_etiqueta_recibirotp()
    {
        try {
            Thread.sleep(8000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        String actual = texto.get(2).getText();
        Util.assert_igual("OTP", "Verificaci�n de t�tulo", actual, "�D�nde deseas recibir el c�digo temporal (OTP)?", true, "N");
    }

    void selecciona_medio() {
        /*try {
            Thread.sleep(2500);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }*/
        String actual = texto.get(3).getText();
        Reporte.agregarPaso("OTP", "Selecciona Medio de env�o", actual, "", false, "N");
        imagen.get(3).click(); //Primer medio //2 icono de celular  3 icono de flecha
        //Segundo medio //4 icono de celular  5 icono de flecha
    }

    void selecciona_medio_swipe() {
        String actual = texto.get(3).getText();
        Reporte.agregarPaso("OTP", "Selecciona Medio de env�o", actual, "", false, "N");
        imagen.get(3).click(); //Primer medio //2 icono de celular  3 icono de flecha
        //Segundo medio //4 icono de celular  5 icono de flecha
    }

    void vp_etiqueta_ingresacodigo()
    {
        this.vp_etiqueta_msg();
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String actual = texto.get(2).getText();
        Util.assert_igual("OTP", "Verificaci�n de t�tulo", actual, "Ingresa C�digo Temporal", true, "N");
    }

    String get_CodigoOtp() {
        String codotp = null;

        if (Util.prop.getProperty("otp_base").equals("S")) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
            //BD connbd = new BD();

            //try {
                //codotp = connbd.ConsultarOTP(Util.getDataCliente()[3]);
                codotp = "11111111";
            //}
            //catch (InterruptedException e) {
                //e.printStackTrace();
            //}
        }
        else {
            OTPBrowser otpb = new OTPBrowser();
            otpb.cambiarAbrowser();

            otpb.IdentificacionIngresar(Util.getDataCliente()[3]);
            otpb.click_boton_Continuar();
            codotp = otpb.obtieneOtp();

            otpb.cambiarApp();
        }
        return codotp;
    }

    void click_boton_Continuar() {
        String actual = boton.get(1).getText();
        Reporte.agregarPaso("OTP", "Presiona bot�n Continuar", actual, "", false, "N");
        boton.get(1).click();
    }

    public void vp_etiqueta_msg()
    {
        try {
            String actual = mensaje_error.getText();
            Util.assert_igual("OTP", "Verificaci�n de mensaje de error", actual, "", true, "N");
        }
        catch (Exception e) {
            System.out.println("OTP-Sin alerta en rojo \n");
        }
    }

    public void vp_verifica_loader(String msgesperado)
    {
        Boolean flag = true;
        while (flag) {
            try {
                String actual = mensaje_loader.getText();
                if (!actual.equals(msgesperado)) {
                    flag = false;
                    System.out.println("0");
                }
            } catch (Exception e) {
                flag = false;
                System.out.println("1");
                this.vp_etiqueta_msg();
            }
        }
    }

    public void genera_valida_Otp() {
        this.vp_etiqueta_recibirotp();
        this.selecciona_medio();
        this.vp_etiqueta_ingresacodigo();

        String codotp = this.get_CodigoOtp();
        this.txt_otp.get(0).sendKeys(codotp);
        Reporte.agregarPaso("OTP", "Ingresa c�digo Otp", codotp, "", true, "N");

        this.imagen.get(1).click();
        this.click_boton_Continuar();
    }
}
