package Pom.Android;

import Auxiliar.Util;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;

public class TestPreliminarOpciones {
	public TestPreliminarOpciones() {
		PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
	}

	@Test
	public void loginTest()
	{
		ModoDebug modo = new ModoDebug();
		modo.modoDebug();

		ScreenPreliminar screenPreliminar = new ScreenPreliminar();
		screenPreliminar.opcionesPreliminar();
	}

	@After
	public void tearDown() throws IOException {
		//Util.driver.quit();
	}
}