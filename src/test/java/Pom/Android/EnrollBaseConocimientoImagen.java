package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class EnrollBaseConocimientoImagen {
    public EnrollBaseConocimientoImagen() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> editor = null;

    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> imagen = null;

    List<String> personaEnroll = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_enroll_-_datos.txt");
    String[] datoEnroll;

    List<String> preguntasEnroll = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_preguntas_seguridad.txt");
    String[] preguntaEnroll;

    //METODOS GET / SET - SELECCIONAR USUARIO A REALIZAR ENROLL
    public void setSeleccionPersonaEnroll(int seleccionPersonaEnroll) {
        datoEnroll = personaEnroll.get(seleccionPersonaEnroll).split("\t");
    }

    public String[] getSeleccionPersonaEnroll() {
        return datoEnroll;
    }

    //METODOS GET / SET - SELECCIONAR PREGUNTAS-RESPUESTA DE SEGURIDAD
    public void setSeleccionRespuestaSeguridad(int seleccionRespuestaSeguridad) {
        preguntaEnroll = preguntasEnroll.get(seleccionRespuestaSeguridad).split("\t");
    }

    public String[] getSeleccionRespuestaSeguridad() {
        return preguntaEnroll;
    }

    public void ingreso_usuario(String Usuario) {
        editor.get(0).clear();
        //editor.get(0).setValue(getSeleccionPersonaEnroll()[5]);
        //Reporte.agregarPaso("BASE CONOCIMIENTO E IMAGEN (ENROLL)", "Ingreso de Usuario", getSeleccionPersonaEnroll()[5], "", true, "N");
        editor.get(0).setValue(Usuario);
        Reporte.agregarPaso("BASE CONOCIMIENTO E IMAGEN (ENROLL)", "Ingreso de Usuario", Usuario, "", true, "N");
    }

    public void ingreso_clave(String Contrasenia) {
        editor.get(1).clear();
        //editor.get(1).setValue(getSeleccionPersonaEnroll()[6]);
        //Reporte.agregarPaso("BASE CONOCIMIENTO E IMAGEN (ENROLL)", "Ingreso de Clave", getSeleccionPersonaEnroll()[6], "", true, "N");
        editor.get(1).setValue(Contrasenia);
        Reporte.agregarPaso("BASE CONOCIMIENTO E IMAGEN (ENROLL)", "Ingreso de Clave", Contrasenia, "", true, "N");
    }

    public void vp_boton_ingresar() {
        String actual = boton.get(0).getText();
        Util.assert_igual("BASE CONOCIMIENTO E IMAGEN (ENROLL)", "Verificación Etiqueta - Botón Ingresar", actual, "INGRESAR", false, "N");
        boton.get(0).click();
    }

    public void vp_etiqueta_completa_perfil() {
        String actual = texto.get(6).getText();
        Util.assert_igual("BASE CONOCIMIENTO E IMAGEN (ENROLL)", "Verificación PopUp - Completa Perfil", actual, "Estás a pocos pasos de completar tu perfil", true, "N");
    }

    public void click_boton_aceptar() {
        String actual = texto.get(9).getText();
        Util.assert_igual("BASE CONOCIMIENTO E IMAGEN (ENROLL)", "Presiona botón Aceptar", actual, "Aceptar", false, "N");
        texto.get(9).click();
    }

    public void click_boton_aceptar_respuesta() {
        String actual = boton.get(1).getText();
        Util.assert_igual("BASE CONOCIMIENTO E IMAGEN (ENROLL)", "Presiona botón Aceptar", actual, "ACEPTAR", false, "N");
        boton.get(1).click();//0: ACEPTAR
    }

    public void vp_etiqueta_pregunta_seguridad() {
        String actual = texto.get(1).getText();
        Util.assert_igual("BASE CONOCIMIENTO E IMAGEN (ENROLL)", "Verificación Etiqueta - Pregunta de Seguridad", actual, "PREGUNTA DE SEGURIDAD", false, "N");
    }

    public void seleccionar_preguntas_respuestas() {
        int idxPregunta = 3; //Posición 3 empiezan las preguntas de la Base de Conocimiento
        int idxRespuesta = 0;
        String pregunta, respuesta;
        String actual;
        for (int i = 0; i < 5; i++) {
            actual = texto.get(idxPregunta).getText();
            idxRespuesta = i + 1;
            setSeleccionRespuestaSeguridad(idxRespuesta);
            pregunta = getSeleccionRespuestaSeguridad()[1];
            respuesta = getSeleccionRespuestaSeguridad()[2];
            Util.assert_igual("BASE CONOCIMIENTO E IMAGEN (ENROLL)", "Verificación Etiqueta - Pregunta de Seguridad", actual, pregunta, true, "N");
            texto.get(idxPregunta).click();
            Util.assert_igual("BASE CONOCIMIENTO E IMAGEN (ENROLL)", "Verificación Etiqueta - Memoriza tu respuesta para usos futuros", texto.get(2).getText(), "Memoriza tu respuesta para usos futuros", false, "N");
            editor.get(0).setValue(respuesta);
            Reporte.agregarPaso("BASE CONOCIMIENTO E IMAGEN (ENROLL)", "Verificación Etiqueta - Respuesta de Seguridad", respuesta, "", true, "N");
            click_boton_aceptar_respuesta();
            if (idxPregunta == 9) {
                idxPregunta = idxPregunta + 4;
            } else {
                idxPregunta = idxPregunta + 2;
            }
        }
    }

    public void click_boton_continuar() {
        int idx = Math.toIntExact(boton.stream().count());
        idx = (idx == 1) ? 0 : (idx - 1);
        String actual = boton.get(idx).getText();
        Util.assert_igual("BASE CONOCIMIENTO E IMAGEN (ENROLL)", "Presiona botón Continuar", actual, "CONTINUAR", true, "N");
        boton.get(idx).click();//0: CANCELAR  1: CONTINUAR
    }

    public void vp_etiqueta_imagen_seguridad() {
        String actual = texto.get(1).getText();
        Util.assert_igual("BASE CONOCIMIENTO E IMAGEN (ENROLL)", "Verificación Etiqueta - Imagen de Seguridad", actual, "Imagen de Seguridad", false, "N");
    }

    public void ingreso_alias() {
        editor.get(0).clear();
        editor.get(0).setValue("AVATAR");
        Reporte.agregarPaso("BASE CONOCIMIENTO E IMAGEN (ENROLL)", "Ingreso de Alias", "AVATAR", "", false, "N");
    }

    public void seleccionar_avatar() {
        imagen.get(1).click();
        Reporte.agregarPaso("BASE CONOCIMIENTO E IMAGEN (ENROLL)", "Seleccionar Avatar", "AVATAR", "", false, "N");
    }
}