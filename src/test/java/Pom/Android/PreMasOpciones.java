package Pom.Android;

import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class PreMasOpciones {
    public PreMasOpciones() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }
    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> opcMasOpciones=null;

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.LinearLayout")
    List<AndroidElement> opcAppOpciones=null;

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> txtURL=null;

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> txtRegistro=null;

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(id="com.android.permissioncontroller:id/permission_allow_always_button")
    MobileElement btnGPS=null;

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> txtUbicaciones=null;

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> btnATRAS=null;

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> opcOtros=null;

    String[] txt_opcMasOpciones = { "M�S OPCIONES",
                                    "Abre una cuenta",
                                    "Crear mi usuario",
                                    "Encontrar un cajero u oficina",
                                    "Chatear con AVI",
                                    "M�s Informaci�n"};

    String[] txt_opcOtros = { "OTROS",
                              "Preguntas Frecuentes",
                              "T�rminos y Condiciones",
                              "Ingreso QuickPay No Clientes",
                              "Banco Bolivariano C.A.",
                              "Junin 200 y Panama",
                              "Guayaquil, Ecuador",
                              "Contact Center:",
                              "1700 50 50 50",
                              " 2.2.4"};

    @Test
    public void ingresarGeneral(){
        ingresarAbrirCuenta();
        ingresarRegistrate();
        ingresarUbicaciones();
        ingresarChat();
        ingresarOtros();
    }
    @Test
    public void ingresarAbrirCuenta(){
        //Util.logbm.info("M�S OPCIONES - ABRE UNA CUENTA");
        assertThat(opcMasOpciones.get(0).getText(), is(txt_opcMasOpciones[0]));

        //Util.logbm.info(txt_opcMasOpciones[0]);
        assertThat(opcMasOpciones.get(1).getText(), is(txt_opcMasOpciones[1]));
        opcMasOpciones.get(1).click();

        String txtURLCuentas="cuentas.cuentafuturo.com/inicio/WAP";
        opcAppOpciones.get(0).click();
        assertThat(txtURL.get(2).getText(), is(txtURLCuentas));
        //Util.logbm.info("==>" + txtURLCuentas);

        Util.driver.pressKey(new KeyEvent(AndroidKey.BACK));
    }
    @Test
    public void ingresarRegistrate() {
        //Util.logbm.info("M�S OPCIONES - REGISTRATE");
        assertThat(opcMasOpciones.get(0).getText(), is(txt_opcMasOpciones[0]));
        //Util.logbm.info(txt_opcMasOpciones[0]);
        assertThat(opcMasOpciones.get(4).getText(), is(txt_opcMasOpciones[2]));
        opcMasOpciones.get(4).click();
        /*
        String txtPantallaRegistro="Con�ctate con nosotros a cualquier hora donde est�s";
        assertThat(txtRegistro.get(1).getText(), is(txtPantallaRegistro));
        //Util.logbm.info("==>" + txtPantallaRegistro);

        Util.driver.pressKey(new KeyEvent(AndroidKey.BACK));

         */
    }
    @Test
    public void ingresarUbicaciones(){
        //Util.logbm.info("M�S OPCIONES - UBICACIONES");
        assertThat(opcMasOpciones.get(0).getText(), is(txt_opcMasOpciones[0]));
        //Util.logbm.info(txt_opcMasOpciones[0]);
        assertThat(opcMasOpciones.get(3).getText(), is(txt_opcMasOpciones[3]));
        opcMasOpciones.get(3).click();

        try{
            btnGPS.click();
        }catch (Exception e){
            //Util.logbm.info("PERMISOS DE GEOLOCALIZACION HABILITADOS");
        }

        String txtPantallaUbicaciones = "UBICACIONES";
        assertThat(txtUbicaciones.get(0).getText(), is(txtPantallaUbicaciones));
        //Util.logbm.info("==>" + txtPantallaUbicaciones);
        btnATRAS.get(0).click();
        btnATRAS.get(0).click();
    }
    @Test
    public void ingresarChat(){
        //Util.logbm.info("M�S OPCIONES - CHAT");
        assertThat(opcMasOpciones.get(0).getText(), is(txt_opcMasOpciones[0]));
        //Util.logbm.info(txt_opcMasOpciones[0]);
        assertThat(opcMasOpciones.get(8).getText(), is(txt_opcMasOpciones[4]));
        opcMasOpciones.get(8).click();

        String txtURLChatbot="api.whatsapp.com/send?phone=593992505050&text=Hola,%20Avi!";
        opcAppOpciones.get(0).click();
        assertThat(txtURL.get(0).getText(), is(txtURLChatbot));
        //Util.logbm.info("==>" + txtURLChatbot);

        Util.driver.navigate().back();
    }
    @Test
    public void ingresarOtros(){
        //Util.logbm.info("M�S OPCIONES - OTROS");
        assertThat(opcMasOpciones.get(0).getText(), is(txt_opcMasOpciones[0]));
        //Util.logbm.info(txt_opcMasOpciones[0]);
        assertThat(opcMasOpciones.get(5).getText(), is(txt_opcMasOpciones[5]));
        opcMasOpciones.get(5).click();

        for (int i=0; i < txt_opcOtros.length; i++){
            assertThat(opcOtros.get(i).getText(), is(txt_opcOtros[i]));
            //Util.logbm.info(txt_opcOtros[i]);
        }
        Util.driver.navigate().back();
        Util.driver.pressKey(new KeyEvent(AndroidKey.ESCAPE));
    }
}
