package Pom.Android;

import Auxiliar.*;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import java.io.IOException;
import java.time.temporal.ChronoUnit;

public class ModoDebug {
	public ModoDebug() {
		if (Util.plataforma.equals("iOS"))
			PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
		else
			PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
	}

	@WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(id = "android:id/alertTitle")
	MobileElement popup_debbuger = null;

	@WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(id = "android:id/button2")
	MobileElement btn_cancel = null;

	@Test
	public void modoDebug()
	{
		try {
			//Util.assert_igual("MODO DE EJECUCIÓN","Verificación del Modo", popup_debbuger.getText(), "Kony JavaScript Debugger Configuration",false);
			String modo= Util.prop.getProperty("verifica_modo");

			if (Util.plataforma.equals("iOS"))
				return;
			else
				if (modo.equals("N"))
					return;

			String mensaje = popup_debbuger.getText();
			if (mensaje.equals("Kony JavaScript Debugger Configuration"))
			{
				String actual = btn_cancel.getText();
				Util.assert_igual("MODO DE EJECUCIÓN", "Presiona botón Cancelar", actual, "CANCEL", true, "N");
				btn_cancel.click();
				Thread.sleep(4000);
			}
		}
		catch (Exception e) {
			Reporte.agregarPaso("MODO DE EJECUCIÓN", "Verificación del Modo", "El modo de este entregable es Release", "", false, "N");
		}
	}

	@After
	public void tearDown() {
		//Util.driver.quit();
	}
}