package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class PagoTCPropia {
	public PagoTCPropia() {
		PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
	}

	@WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(className = "android.widget.Button")
	List<AndroidElement> boton = null;

	@WithTimeout(time=2, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(className = "android.widget.ImageView")
	List<AndroidElement> imagen = null;

	@WithTimeout(time=30, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(className = "android.widget.TextView")
	List<AndroidElement> texto = null;

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> edit = null;

    @WithTimeout(time = 45, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]")
    MobileElement seccion1 = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
    MobileElement mensaje_error = null;

    @WithTimeout(time = 4, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
    MobileElement mensaje_exitoso = null;

    @WithTimeout(time=2, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView")
    MobileElement mensaje_loader = null;

    int pos_seccion = 0;
    public int getPos_seccion() {return pos_seccion;}
    public void setPos_seccion(int pos_seccion) {
        this.pos_seccion = pos_seccion;
    }

    int pos_ini_x = 0;
    public int getPos_ini_x() {return pos_ini_x;}
    public void setPos_ini_x(int pos_ini_x) {
        this.pos_ini_x = pos_ini_x;
    }

    int pos_ini_y = 0;
    public int getPos_ini_y() {return pos_ini_y;}
    public void setPos_ini_y(int pos_ini_y) {
        this.pos_ini_y = pos_ini_y;
    }

    int alto_seccion = 0;
    public int getAlto_seccion() {return alto_seccion;}
    public void setAlto_seccion(int alto_seccion) {
        this.alto_seccion = alto_seccion;
    }

    public void iniciaPosiciones(int posicion) {
        this.setPos_seccion(posicion);
        this.setPos_ini_x(seccion1.getRect().x + seccion1.getRect().width - 100);
        this.setPos_ini_y(seccion1.getRect().y + (seccion1.getRect().height / 2));
        this.setAlto_seccion(seccion1.getRect().height);
        System.out.println("Posici�n de la Secci�n:" + this.getPos_seccion());
    }

    public void contraerSecciones() {
        //Contraer cada secci�n hasta llegar a la seccion de Tarjetas
        //pos_seccion empieza en 1 y se lo obtiene del archivo dp_login.txt
        //1 Cuentas  2 Cuentas en Euros  3 Tarjetas de Cr�dito  4 Pr�stamos 5 Inversiones
        //1 Cuentas  2 Tarjetas de Cr�dito  3 Pr�stamos     4 Inversiones
        //1 Tarjetas de Cr�dito
        for(int i=1; i<this.getPos_seccion(); i++)
        {
            Util.TocarPantalla(this.getPos_ini_x(), this.getPos_ini_y());//Tocar cada secci�n hasta que llegue a la posici�n de dp_login_txt Pos_TC
            Reporte.agregarPaso("PAGO TC PROPIA", "Contrae secci�n", "v", "", true, "N");
            this.setPos_ini_y(this.getPos_ini_y() + this.getAlto_seccion());
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }

    public void expandirSeccion()
    {
        Util.TocarPantalla(this.getPos_ini_x(), this.getPos_ini_y());//Tocar primera tarjeta listada
        Reporte.agregarPaso("PAGO TC PROPIA", "Expande secci�n", "Tarjetas", "", true, "N");
        this.setPos_ini_y(this.getPos_ini_y() + this.getAlto_seccion());
        try {
            Thread.sleep(6000); //Esperar hasta que se cargue la pantalla de detalle y movimientos de TC
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }

    public void seleccionar_tc()
    {
        int idx = this.getPos_seccion()+1;
        String xpath_tc = "//android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup["+ idx +"]";
        MobileElement numeroTC =  Util.Buscar_xpath(xpath_tc + "/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView");
        String actual = numeroTC.getText();
        Reporte.agregarPaso("PAGO TC PROPIA", "Ingresa N�mero de TC", actual, "", false, "N");

        MobileElement marcaTC =  Util.Buscar_xpath(xpath_tc + "/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.TextView");
        actual = marcaTC.getText();
        Reporte.agregarPaso("PAGO TC PROPIA", "Selecciona Marca de TC", actual, "", false, "N");

        MobileElement tipoTC =  Util.Buscar_xpath(xpath_tc + "/android.view.ViewGroup/android.widget.TextView");
        actual = tipoTC.getText();
        Reporte.agregarPaso("PAGO TC PROPIA", "Selecciona Tipo de TC", actual, "", false, "N");

        numeroTC.click();
    }

    public void vp_etiqueta_tc()
    {
        String actual = texto.get(0).getText();
        Util.assert_igual("PAGO TC PROPIA", "Verificaci�n de etiqueta", actual,"TARJETA DE CR�DITO",  true, "N");
    }

    public double getCupoDisponible()
    {
        String cupo = texto.get(3).getText().replace(",","");
        return Double.valueOf(cupo.substring(0, cupo.length()-3));
    }

    public void click_boton_pagar()
    {
        String actual = texto.get(8).getText().toUpperCase();
        Util.assert_igual("PAGO TC PROPIA", "Presiona bot�n Pagar", actual,"PAGAR",  false, "N");
        texto.get(8).click();
    }

    public void vp_etiqueta_montopagar()
    {
        String actual = texto.get(0).getText();
        Util.assert_igual("PAGO TC PROPIA", "Verificaci�n de etiqueta", actual,"MONTO A PAGAR",  true, "N");
    }

    public void click_boton_cambiar() {
        //CAMBIO DE CUENTA ORIGEN;
        try {
            Thread.sleep(2500);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        String actual = boton.get(1).getText();
        Util.assert_igual("PAGO TC PROPIA", "Presiona bot�n Cambiar", actual,"CAMBIAR",  false, "N");
        boton.get(1).click();
    }

    public void buscar_ctaDebito()
    {
        try {
            Thread.sleep(1500);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
       /* edit.get(0).sendKeys("Perero");
        Util.driver.pressKey(new KeyEvent(AndroidKey.ENTER));
        Util.CapturarImagen();*/
        String actual = texto.get(0).getText();
        Util.assert_igual("PAGO TC PROPIA", "Verificaci�n de etiqueta", actual,"CUENTA D�BITO",  false, "N");

        actual = texto.get(1).getText();
        Reporte.agregarPaso("PAGO TC PROPIA", "Selecciona Cuenta", actual, "", true, "N");
        texto.get(1).click(); //Primera cuenta, la segunda est� en el indice 4 (2 usd 3 Saldo Disponible)
        try {
            Thread.sleep(2500);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }

    public void ingresarMonto(String montopagar)
    {
        String actual = texto.get(0).getText();
        Util.assert_igual("PAGO TC PROPIA", "Verificaci�n de etiqueta", actual,"MONTO A PAGAR",  false, "N");

        if (texto.get(11).getText().equals("0.00 USD"))
        {
            TecladoNumerico teclado = new TecladoNumerico();
            teclado.IngresarValor(2, montopagar, true);
        }
    }

    public double getSaldoCta()
    {
        //10 SALDO cuenta d�bito
        String saldo = texto.get(4).getText().substring(0, texto.get(4).getText().length()-4).replace(",","");
        Reporte.agregarPaso("PAGO TC PROPIA", "Verificaci�n de Saldo de la Cuenta",  saldo, "", false, "N");
        return Double.valueOf( saldo);
    }

    public double getMonto()
    {
        //14 MONTO a pagar
        String monto = texto.get(14).getText().replace(",","");
        Reporte.agregarPaso("PAGO TC PROPIA", "Ingresa Monto",  monto, "", false, "N");
        return Double.valueOf(monto);
    }

    public void click_boton_Continuar()
    {
        String actual = boton.get(2).getText();
        Util.assert_igual("PAGO TC PROPIA", "Presiona bot�n Continuar", actual,"CONTINUAR",  true, "N");
        boton.get(2).click();
    }

    public void vp_mensaje_excede()
    {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        String actual = texto.get(0).getText();
        Util.assert_igual("PAGO TC PROPIA", "Verificaci�n de saldo Disponible", actual,"Valor excede el saldo disponible",  true, "N");
    }

    public void click_boton_OK()
    {
        String actual = boton.get(0).getText();
        Util.assert_igual("PAGO TC PROPIA", "Presiona bot�n OK", actual,"OK",  true, "N");
        boton.get(0).click();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }

    public void vp_etiqueta_confirmacion()
    {
        String actual = texto.get(0).getText();
        Util.assert_igual("PAGO TC PROPIA", "Verificaci�n de etiqueta", actual,"CONFIRMACI�N",  true, "N");
    }

    public double getMontoIngresado()
    {
        String monto_ingre = texto.get(2).getText().replace(",", "");
        return Double.valueOf(monto_ingre.substring(0, monto_ingre.length() - 3));
    }

    public void ingresarConcepto()
    {
        edit.get(0).sendKeys("PagoTCpropia");
        Util.driver.pressKey(new KeyEvent(AndroidKey.ENTER));
    }

    public void click_boton_Confirmar()
    {
        String actual = boton.get(1).getText();
        Util.assert_contiene("PAGO TC PROPIA", "Presiona bot�n Pagar", actual,"PAGAR",  true, "N");
        boton.get(1).click();
    }

    public void vp_dato_cupo(double cupo_disp_antes, double monto_ingresado)
    {
        Reporte.agregarPaso("PAGO TC PROPIA", "Verificaci�n de CUPO DISPONIBLE antes de pago", String.valueOf(cupo_disp_antes),"",  false, "N");
        Reporte.agregarPaso("PAGO TC PROPIA", "Verificaci�n de MONTO pagado", String.valueOf(monto_ingresado),"",  false, "N");

        String cupo = texto.get(3).getText().replace(",", "");
        double cupo_disp_desp = Double.valueOf(cupo.substring(0, cupo.length() - 3));
        BigDecimal bd = new BigDecimal(cupo_disp_desp).setScale(2, RoundingMode.HALF_UP);
        double cupo_disp_despues = bd.doubleValue();
        Reporte.agregarPaso("PAGO TC PROPIA", "Verificaci�n de CUPO DISPONIBLE despu�s de pago", String.valueOf(cupo_disp_despues),"",  false, "N");

        double cupo_disp_calc = Double.valueOf(cupo_disp_antes + monto_ingresado);
        bd = new BigDecimal(cupo_disp_calc).setScale(2, RoundingMode.HALF_UP);
        double cupo_disp_calculado = bd.doubleValue();
        Reporte.agregarPaso("PAGO TC PROPIA", "Verificaci�n de CUPO DISPONIBLE calculado", String.valueOf(cupo_disp_calculado),"",  false, "N");

        Util.assert_igual("PAGO TC PROPIA", "Verificaci�n de CUPO DISPONIBLE despu�s de pago", String.valueOf(cupo_disp_despues),String.valueOf(cupo_disp_calculado),  true, "N");
    }

    void vp_etiqueta_msg()
    {
        try {
            String actual = mensaje_error.getText();
            Util.assert_contiene("PAGO TC PROPIA", "Verificaci�n de mensaje de error", actual, "Transacci�n exitosa", true, "N");
        }
        catch (Exception e) {
            System.out.println("PAGO TC PROPIA SIN MENSAJE DE ERROR \n" );
        }
    }

    public void vp_verifica_loader()
    {
        Boolean flag = true;
        while (flag) {
            try {
                //"Estamos realizando tu pago..."
                if (imagen.size() != 5) {
                    flag = false;
                    System.out.println("0");
                    this.vp_etiqueta_msg();
                }
            } catch (Exception e) {
                flag = false;
                System.out.println("1");
                this.vp_etiqueta_msg();
            }
        }
    }

    public void vp_etiqueta_msg_exito()
    {
        try {
            String actual = mensaje_exitoso.getText();
            Util.assert_contiene("PAGO TC PROPIA", "Verificaci�n de mensaje de confirmaci�n", actual, "Transacci�n exitosa", true, "N");
        }
        catch (Exception e) {
            System.out.println("PAGO TC PROPIA SIN MENSAJE DE ERROR \n" );
        }
    }
}
