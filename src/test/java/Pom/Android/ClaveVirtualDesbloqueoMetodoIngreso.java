/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   CLAVE VIRTUAL - DESBLOQUEO
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   AGO 11/2021
    Fecha Modificaci�n  :   N/A
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Validaci�n Clave Virtual No Bloqueada
        2. Validaci�n Clave Virtual Desbloqueo
        3. Validaci�n Base Conocimiento (Preguntas / Respuestas) - Fallido (3 intentos)
 */
package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.Iterator;
import java.util.List;

public class ClaveVirtualDesbloqueoMetodoIngreso {
    public ClaveVirtualDesbloqueoMetodoIngreso() {
        if (Util.plataforma.equals("iOS"))
            PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
        else
            PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    AndroidElement edita = null;

    List<String> datosBaseConocimiento = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_preguntas_transaccional.txt");
    Iterator<String> itrBaseConocimiento = datosBaseConocimiento.iterator();
    String[] campos = null;
    int linea = 0;

    String respuestaBaseConocimiento = "";

    public String getBaseConocimiento() {
        return respuestaBaseConocimiento;
    }

    public void setBaseConocimiento(String pregunta) {
        while (itrBaseConocimiento.hasNext()) {
            campos = itrBaseConocimiento.next().split("\t");
            linea = linea + 1; //0Sec 1Pregunta_transaccional 2Respuesta_transaccional
            if (linea == 1)
                continue;
            //System.out.println("PREGUNTA DE ARCHIVO: "+ campos[1]);
            if (pregunta.equals(campos[1])) {
                System.out.println("RESPUESTA: " + campos[2]);
                respuestaBaseConocimiento = campos[2];
                break;
            }
        }
    }

    public void vp_etiqueta_cabecera(String esperado) {
        String actual = texto.get(0).getText();
        Util.assert_igual("CLAVE VIRTUAL DESBLOQUEO", "Verificaci�n Cabecera", actual, esperado, true, "N");
    }

    public void click_boton_Desbloquear_Ingreso() {
        String actual = texto.get(9).getText();
        Util.assert_igual("CLAVE VIRTUAL DESBLOQUEAR INGRESO", "Presiona bot�n Desbloquear Ingreso", actual, "Desbloquear Ingreso", true, "N");
        texto.get(9).click();//9: DESBLOQUEAR INGRESO
    }

    public void mensajeValidacion(String esperado) {
        try {
            if (esperado.equals("Tienes activo el ingreso para generar la clave virtual")) {
                Util.assert_igual("CLAVE VIRTUAL DESBLOQUEO", "Mensaje Validaci�n No Bloqueado", Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
                Thread.sleep(5000);
            } else {
                Thread.sleep(2000);
                Util.assert_contiene("CLAVE VIRTUAL DESBLOQUEAR INGRESO", "Mensaje Validaci�n" + esperado, Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + esperado + "')]")).getText(), esperado, true, "N");
                Thread.sleep(6000);
            }
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
    }
    public void mensaje_popup_Error(int idx) {
        if (idx < 2) {
            Util.assert_igual("CLAVE VIRTUAL DESBLOQUEO", "Repuesta Err�nea", texto.get(0).getText(), "Has ingresado una respuesta err�nea. Por favor, intenta nuevamente", true, "N");
            texto.get(1).click();
        } else {
            Util.assert_igual("CLAVE VIRTUAL DESBLOQUEO", "Supera N�mero de Repuesta Err�nea", texto.get(0).getText(), "Superaste los intentos en responder la pregunta.", true, "N");
            texto.get(2).click();
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void pregunta_transaccional() {
        String actual = texto.get(2).getText();
        setBaseConocimiento(actual.replaceAll("[�?\"]", ""));
        Reporte.agregarPaso("CLAVE VIRTUAL DESBLOQUEAR INGRESO", "Valida Base Conocimiento - Pregunta", actual, "", true, "N");
        edita.click();
        edita.clear();
        edita.sendKeys(getBaseConocimiento());
        Reporte.agregarPaso("CLAVE VIRTUAL DESBLOQUEAR INGRESO", "Valida Base Conocimiento - Respuesta", getBaseConocimiento(), "", true, "N");
        texto.get(5).click();
    }

    public void ingreso_respuesta(String respuesta) {
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        edita.click();
        edita.clear();
        edita.sendKeys(respuesta);
        texto.get(5).click(); //5 Bot�n Aceptar
    }

    public void click_boton_Cambio_Pregunta() {
        String actual = boton.get(0).getText();
        Util.assert_igual("CLAVE VIRTUAL DESBLOQUEO", "Presiona bot�n Cambiar de Pregunta", actual, "CAMBIAR DE PREGUNTA", true, "N");
        boton.get(0).click();//0: CAMBIAR DE PREGUNTA
    }
}
