package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class Menu {
    public Menu() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }
    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    private List<AndroidElement> menu_mas = null;

    @WithTimeout(time=15, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    private List<AndroidElement> menu = null;

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    private List<AndroidElement> lbl_titulo = null;

    @WithTimeout(time=15, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    private List<AndroidElement> opc_transferir = null;

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    private List<AndroidElement> msj_noctacte = null;

    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    //@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[5]/android.view.ViewGroup/android.widget.TextView[2]")
    //MobileElement clave_activa_sino = null;
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> clave_activa_sino = null;

    public String clave_activa = "No";

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> item_menu = null;

    public void presionaMenuNuevo (int idx_menu){
        BarraNavegacion barra = new BarraNavegacion();
        barra.click_opc_otros();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //System.out.println("ITEM DE MENU: "+item_menu.get(idx_menu).getText());
        Reporte.agregarPaso("MENU", "Ingresa a Men�", item_menu.get(idx_menu).getText(), "", true, "N");
        /*try {
            Reporte.agregarPaso("MENU", "Ingresa a Men�", item_menu.get(idx_menu).getText(), "", true, "N");
        }catch (Exception IndexOutOfBoundsException){
            System.out.println("No se encontr� el elemento buscado: " + item_menu.get(idx_menu).getText());
        }*/

        if (idx_menu == 7){
            clave_activa=clave_activa_sino.get(8).getText();
        }
        item_menu.get(idx_menu).click();

        String txt_expected = null;

        switch (idx_menu){
            case 3:
                if (!lbl_titulo.get(0).getText().equals("Lo sentimos, servicio no disponible por el momento. Int�ntalo mas tarde")) {
                    if (lbl_titulo.get(0).getText().equals("ACTIVAR SERVICIO")) {
                        txt_expected = "ACTIVAR SERVICIO";
                    } else {
                        txt_expected = "DEP�SITO EXPRESS";
                    }
                }else{
                    txt_expected="Lo sentimos, servicio no disponible por el momento. Int�ntalo mas tarde";
                }
                break;
            case 4:
                if (lbl_titulo.get(0).getText().equals("RETIRO SIN TARJETA")){
                    txt_expected="RETIRO SIN TARJETA";
                }else{
                    txt_expected=lbl_titulo.get(0).getText();
                }
                break;
            case 7: //Clave Virtual
                if (clave_activa.equals("No")){
                    txt_expected = "ACTIVAR SERVICIO";
                }
                else{
                    txt_expected = "CLAVE VIRTUAL";
                }
                break;
            case 11: //Ajustes
                txt_expected = "AJUSTES";
                break;
            case 13: //Ayuda
                txt_expected = "AYUDA";
                break;
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        String actual = lbl_titulo.get(0).getText();
        Util.assert_igual("MENU", "Accede a la pantalla del item seleccionado", actual, txt_expected, false, "N");
    }

    public void presionaMenu(int opcion){
        //menu_mas.get(0).click(); //Menu =
        //Reporte.agregarPaso("MENU", "Presiona Men� Hamburguesa", "Men� Hamburguesa", "",true, "N");
        BarraNavegacion barra = new BarraNavegacion();
        barra.click_opc_otros();

        //clave_activa = clave_activa_sino.getText().trim();

        menu.get(opcion).click(); //Opcion 2 Transferir

        String txt_expected = null;
        String mensaje = null;
        ValidacionesCascada validarMensajesDeError = new ValidacionesCascada();

	   switch (opcion) {
		   case 1:
               if (!lbl_titulo.get(0).getText().equals("Lo sentimos, servicio no disponible por el momento. Int�ntalo mas tarde")) {
                   if (lbl_titulo.get(0).getText().equals("ACTIVAR SERVICIO")) {
                       txt_expected = "ACTIVAR SERVICIO";
                   } else {
                       txt_expected = "DEP�SITO EXPRESS";
                   }
               }else{
                   txt_expected="Lo sentimos, servicio no disponible por el momento. Int�ntalo mas tarde";
               }
               break;
           case 2:
               mensaje = msj_noctacte.get(0).getText();
               txt_expected = validarMensajesDeError.validarMensajeAdvertencia(mensaje,"RETIRO SIN TARJETA");
               break;
           case 5: //Clave Virtual
               if (clave_activa.equals("No")){
                   txt_expected = "ACTIVAR SERVICIO";
               }
               else{
                   txt_expected = "CLAVE VIRTUAL";
               }
               break;
           case 6:
               mensaje = msj_noctacte.get(1).getText();
                txt_expected = validarMensajesDeError.validarMensajeAdvertencia(mensaje,"SOLICITAR CHEQUERA");
            break;
           case 8:
               txt_expected = "AJUSTES";
               break;
       }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        String actual = lbl_titulo.get(0).getText();
        Util.assert_igual("MENU", "Ingresa a Men�", actual, txt_expected, true, "N");
    }

    public void presionaSubmenu(int opcionmenu, int opcion)
    {
        //Indice de las imagenes de las opciones de Transferencias opcionmenu 2: TRANSFERIR (2) / QUICKPAY / BIMO (3) /ADMINISTRAR (4)
        //Indice de las imagenes de las opciones de Pagos opcionmenu 3: PAGAR SERVICIO (2) / PAGAR TARJETA (3) /ADMINISTRAR (4)
        BarraNavegacion barra = new BarraNavegacion();

        switch (opcionmenu){
            case 2:
                barra.click_opc_transf();
                break;
            case 3:
                barra.click_opc_pagar();
                break;
        }
        opc_transferir.get(opcion).click();

        String txt_expected = null;

        switch (opcionmenu) {
            case 2:  //Transferir
                switch (opcion) {
                    case 0:  //Primer Boton
                        //txt_expected = "TRANSFERIR";
                        txt_expected=lbl_titulo.get(0).getText().equals("TRANSFERIR")?"TRANSFERIR":"TRANSFERENCIAS";
                        break;
                    case 1:  //Segundo Boton
                        txt_expected = "BIMO";
                        break;
                    case 2:   //Tercer Boton
                        txt_expected = "ADMINISTRAR";
                        break;
                }
                break;
            case 3:   //Pagar
                switch (opcion) {
                    case 0:  //Primer Boton
                        txt_expected = "PAGAR SERVICIO";
                        break;
                    case 1:  //Segundo Boton
                        txt_expected = "TARJETA DESTINO";
                        break;
                    case 2:   //Tercer Boton
                        txt_expected = "ADMINISTRAR";
                        break;
                }
                break;
            case 11:
                switch (opcion){
                    case 12: //Desbloqueo de tu clave virtual
                        txt_expected = "XXXX";
                        break;
                }
        }

        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        String actual = lbl_titulo.get(0).getText();
        Util.assert_igual("MENU", "Accediendo a Submen�", actual, txt_expected, true, "N");
    }

    //25-NOV-2021 Nuevo flujo de transferencias
    public void presionaSubmenu(int opcionmenu){
        BarraNavegacion barra = new BarraNavegacion();
        switch (opcionmenu){
            case 2:
                barra.click_opc_transf();
                break;
        }
    }
    public void presionaOpcionSubmenuTransf(int opcion, Boolean reporte)
    {
        //Indice de las opciones de Administrar: Terceros (2) / Otros Bancos (3?)
        //Indice de las opciones de Transferir: Propias (2) / Terceros (4) / Otros Bancos (6)

        if (reporte) {
            String actual = lbl_titulo.get(opcion).getText();
            Reporte.agregarPaso("MENU", "Accediendo a Submen� Transferencias", actual, "", true, "N");
        }
        lbl_titulo.get(opcion).click();
    }
}
