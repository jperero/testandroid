/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   PERFIL
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   AGO 20/2021
    Fecha Modificaci�n  :   N/A
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Validaci�n Servicio Activo en flujos: OK
           1.1. Clave Virtual
           1.2. Dep�sito Express
           1.3. QuickPay
           1.4. QuickView
           1.5. Acceso Biom�trico
           1.6. Registro de Equipo
        2. Validaci�n Producto Cuentas en flujos: OK
           2.1. Clave Virtual
           2.2. Dep�sito Express
           2.3. QuickPay
        3. Validaci�n Registro Equipo en flujos: OK
           3.1. Clave Virtual
           3.2. Dep�sito Express
           3.3. QuickPay
           3.4. QuickView
           3.5. Acceso Biom�trico
           3.6. Registro de Equipo
 */

package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class Perfil {
    public Perfil() {
        if (Util.plataforma.equals("iOS"))
            PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
        else
            PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    public String caso = "";

    public void vp_etiqueta_cabecera(String esperado) {
        String actual = texto.get(0).getText();
        Util.assert_igual(caso, "Verificaci�n Cabecera", actual, esperado, true, "N");
    }

    public void click_boton_Opcion(int idx) {
        String actual = texto.get(idx).getText();
        Util.assert_igual(caso, "Presiona bot�n " + actual, actual, "Perfil", true, "N");
        texto.get(idx).click();
    }

    public void click_boton_Opcion(int idxOpc, int idxResp, String esperado) {
        String actual = texto.get(idxOpc).getText();
        Util.assert_igual(caso, "Presiona bot�n " + actual + " " + texto.get(idxResp).getText(), actual, esperado, true, "N");
        texto.get(idxOpc).click();
    }

    public void validarOpcionSiNo(int idx, String SiNo, String escenario, String valor, String mensaje) {
        if (texto.get(idx + 1).getText().equals(SiNo)) {
            click_boton_Opcion(idx, idx + 1, valor);
            switch (escenario) {
                case "Activo":
                    mensaje_popup_Aviso("Aviso Servicio Activo - " + valor, mensaje);
                    break;
                case "Inactivo":
                    mensaje_popup_Aviso("Aviso Servicio Inactivo - " + valor, mensaje);
                    break;
                case "Cuenta":
                    mensaje_popup_Aviso("Producto Cuentas - " + valor, mensaje);
                    break;
                case "Registro":
                    mensaje_Aviso(valor, mensaje);
                    break;
                default:
                    try {
                        Thread.sleep(3000);
                        escenario = escenario.equals("Cuenta Default") ? "Producto Cuentas - " : "Registro Equipo - ";
                        Util.assert_igual(caso, escenario + valor, Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + mensaje + "']")).getText(), mensaje, true, "N");
                        Util.driver.navigate().back();
                    } catch (Exception NoSuchElementException) {
                        System.out.println("No se encontr� el elemento buscado: " + mensaje);
                    }
                    break;
            }
        }
    }

    public void mensaje_popup_Aviso(String escenario, String esperado) {
        String actual = texto.get(1).getText();
        Util.assert_igual(caso, escenario, actual, esperado, true, "N");
        texto.get(2).click();
    }

    public void mensaje_Aviso(String escenario, String esperado) {
        try {
            Thread.sleep(3000);
            String actual = texto.get(1).getText();
            escenario = escenario.equals("Registro de Equipo") ? escenario : "Registro Equipo - " + escenario;
            Util.assert_igual(caso, escenario, actual, esperado, true, "N");
            if (escenario.equals("Registro de Equipo")) {
                Reporte.agregarPaso(caso, "Presiona bot�n No Gracias", boton.get(1).getText(), "", true, "N");
                boton.get(1).click();
            }else{
                Util.driver.navigate().back();
            }
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }

    }
}
