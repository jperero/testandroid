/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   CLAVE VIRTUAL - ELIMINACI�N
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   JUL 22/2021
    Fecha Modificaci�n  :   N/A
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Validaci�n PopUp Eliminaci�n acci�n "CANCELAR"
        2. Validaci�n PopUp Eliminaci�n acci�n "ELIMINAR"
           2.1  Validaci�n Base Conocimiento (Preguntas / Respuestas) - Exitoso
        3. Validaci�n Base Conocimiento (Preguntas / Respuestas) - Fallido (3 intentos)
 */
package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.Iterator;
import java.util.List;

public class ClaveVirtualEliminacion {
    public ClaveVirtualEliminacion() {
        if (Util.plataforma.equals("iOS"))
            PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
        else
            PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    AndroidElement edita = null;

    List<String> datosBaseConocimiento = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_preguntas_transaccional.txt");
    Iterator<String> itrBaseConocimiento = datosBaseConocimiento.iterator();
    String[] campos = null;
    int linea = 0;

    String mensajePopUp= "�Deseas eliminar por completo este Dipositivo?\n\nAl realizar esta acci�n eliminar�s definitivamente tu Clave Virtual en este equipo.\n\n";
    String respuestaBaseConocimiento = "";
    //private String preguntaBaseConocimiento = "";

    public String getBaseConocimiento(){
        return respuestaBaseConocimiento;
    }

    public void setBaseConocimiento(String pregunta){
        while (itrBaseConocimiento.hasNext()) {
            campos = itrBaseConocimiento.next().split("\t");
            linea = linea + 1; //0Sec 1Pregunta_transaccional 2Respuesta_transaccional
            if (linea == 1)
                continue;
            //System.out.println("PREGUNTA DE ARCHIVO: "+ campos[1]);
            if (pregunta.equals(campos[1])){
                System.out.println("RESPUESTA: "+campos[2]);
                respuestaBaseConocimiento=campos[2];
                break;
            }
        }
    }
    public void vp_etiqueta_cabecera(String esperado) {
        String actual = texto.get(0).getText();
        Util.assert_igual("CLAVE VIRTUAL ELIMINAR", "Verificaci�n Cabecera", actual, esperado, true, "N");
    }
    public void click_boton_Eliminar(){
        String actual = texto.get(11).getText();
        Util.assert_igual("CLAVE VIRTUAL ELIMINAR", "Presiona bot�n Eliminar", actual, "Eliminar", true, "N");
        texto.get(11).click();//11: ELIMINAR
    }
    public void mensaje_popup_Error(int idx){
        if (idx<2) {
            Util.assert_igual("CLAVE VIRTUAL ELIMINAR", "Repuesta Err�nea", texto.get(0).getText(), "Has ingresado una respuesta err�nea. Por favor, intenta nuevamente", true, "N");
            texto.get(1).click();
        }else{
            Util.assert_igual("CLAVE VIRTUAL ELIMINAR", "Supera N�mero de Repuesta Err�nea", texto.get(0).getText(), "Superaste los intentos en responder la pregunta.", true, "N");
            texto.get(2).click();
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void mensaje_popup_Eliminar(int idx) {
        String actual = texto.get(0).getText();
        Util.assert_igual("CLAVE VIRTUAL ELIMINAR", "Mensaje Confirmaci�n Eliminaci�n", actual, mensajePopUp, false, "N");
        if (idx == 1) {
            Util.assert_igual("CLAVE VIRTUAL ELIMINAR", "Confirmaci�n Eliminaci�n", texto.get(idx).getText(), "Cancelar", true, "N");
        } else {
            Util.assert_igual("CLAVE VIRTUAL ELIMINAR", "Confirmaci�n Eliminaci�n", texto.get(idx).getText(), "Eliminar", true, "N");
        }
        texto.get(idx).click();
    }

    public void mensaje_popup_Aviso() {
        String actual = texto.get(0).getText();
        Util.assert_igual("CLAVE VIRTUAL ELIMINAR", "Aviso Confirmaci�n Eliminaci�n", actual, "Has eliminado correctamente tu Clave Virtual. Por favor, vuelve a iniciar sesi�n", true, "N");
        texto.get(1).click();
    }

    public void pregunta_transaccional(){
        String actual = texto.get(2).getText();
        setBaseConocimiento(actual.replaceAll("[�?\"]",""));
        Reporte.agregarPaso("CLAVE VIRTUAL ELIMINAR","Valida Base Conocimiento - Pregunta",actual,"",true, "N");
        edita.click();
        edita.clear();
        edita.sendKeys(getBaseConocimiento());
        Reporte.agregarPaso("CLAVE VIRTUAL ELIMINAR","Valida Base Conocimiento - Respuesta",getBaseConocimiento(),"",true, "N");
        texto.get(5).click();
    }
    public void ingreso_respuesta(String respuesta){
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        edita.click();
        edita.clear();
        edita.sendKeys(respuesta);
        texto.get(5).click();
    }
    public void click_boton_Cambio_Pregunta(){
        String actual = boton.get(0).getText();
        Util.assert_igual("CLAVE VIRTUAL ELIMINAR", "Presiona bot�n Cambiar de Pregunta", actual, "CAMBIAR DE PREGUNTA", true, "N");
        boton.get(0).click();//0: CAMBIAR DE PREGUNTA
    }

}
