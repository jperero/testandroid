package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class PreguntaSeguridad {
    public PreguntaSeguridad() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> editText = null;

    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time = 30, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
    AndroidElement mensaje = null;

    public void vp_etiqueta_pregunta() {
        String actual = texto.get(0).getText();
        Util.assert_contiene("PREGUNTA SEGURIDAD", "Verificación de etiqueta", actual, "PREGUNTA DE SEGURIDAD", true, "N");
    }

    public void contestar_pregunta() {
        String pregunta = texto.get(2).getText();
        String respuesta = null;
        String[] datos = null;

        datos = Util.getCamposDataPool(System.getProperty("user.dir")+"/archivos/dp_preguntas_seguridad.txt", pregunta, 1);
        if (datos!= null)
            respuesta = datos [2];
        else
            respuesta = "No hay respuesta";
        editText.get(0).sendKeys(respuesta);
        Reporte.agregarPaso("PREGUNTA SEGURIDAD", "Verificación de Pregunta", pregunta, "", false, "N");
        Reporte.agregarPaso("PREGUNTA SEGURIDAD", "Ingresa Respuesta", respuesta, "", true, "N");
    }

    public void contestar_pregunta(String respuesta) {
        editText.get(0).sendKeys(respuesta);
        Reporte.agregarPaso("PREGUNTA SEGURIDAD", "Ingresa Respuesta", respuesta, "", true, "N");
    }

    public void contestar_pregunta_RegIpInter() {
        String pregunta = texto.get(2).getText();
        String respuesta = Util.getDataCliente()[5];//<--"internacional"
        editText.get(0).sendKeys(respuesta);
        Reporte.agregarPaso("PREGUNTA SEGURIDAD", "Verificación de Pregunta", pregunta, "", false, "N");
        Reporte.agregarPaso("PREGUNTA SEGURIDAD", "Ingresa Respuesta", respuesta, "", true, "N");
    }

    public void click_boton_aceptar() {
        String actual = texto.get(5).getText();
        Util.assert_igual("PREGUNTA SEGURIDAD", "Presiona botón Aceptar", actual, "ACEPTAR", false, "N");
        texto.get(5).click();
    }

    public void click_boton_aceptar_ip() {
        String actual = texto.get(4).getText();
        Util.assert_igual("PREGUNTA SEGURIDAD", "Presiona botón Aceptar", actual, "ACEPTAR", false, "N");
        texto.get(4).click();
    }

    public Boolean error_respuesta(){
        try {
            String msj = mensaje.getText();//texto.get(0).getText();
            if (msj.equals("Has ingresado una respuesta errónea, por favor intenta nuevamente"))
                return true;
            else
                return false;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public void click_boton_cambiarpregunta() {
        String actual = boton.get(0).getText();
        Util.assert_igual("PREGUNTA SEGURIDAD", "Presiona botón Cambiar", actual, "CAMBIAR DE PREGUNTA", false, "N");
        boton.get(0).click();
    }

}
