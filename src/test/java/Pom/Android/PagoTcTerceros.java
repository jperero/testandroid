package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class PagoTcTerceros {
    public PagoTcTerceros() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    private MobileElement tc = null;
    private MobileElement tc_titular = null;
    private MobileElement tc_banco = null;

    public MobileElement getTc() {
        return tc;
    }
    public void setTc(MobileElement tc) {
        this.tc = tc;
    }

    public MobileElement getTc_titular() {
        return tc_titular;
    }
    public void setTc_titular(MobileElement tc_titular) {
        this.tc_titular = tc_titular;
    }

    public MobileElement getTc_banco() {
        return tc_banco;
    }
    public void setTc_banco(MobileElement tc_banco) {
        this.tc_banco = tc_banco;
    }

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> txtedit = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView[1]")
    MobileElement monto = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[2]")
    MobileElement confirma_monto = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.widget.TextView[2]")
    MobileElement confirma_tc = null;

    @WithTimeout(time=6, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
    MobileElement mensaje_error = null;

    @WithTimeout(time=3, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView")
    MobileElement mensaje_loader = null;

    //++++
    public void buscaTarjeta(String alias) {
        String actual = texto.get(0).getText();
        Util.assert_igual("PAGO DE TC TERCEROS", "Verificaci�n de t�tulo", actual, "TARJETA DESTINO", false, "N");

        txtedit.get(0).sendKeys(alias); // son 14 caracteres de alias
        Util.driver.pressKey(new KeyEvent(AndroidKey.ENTER));
        Reporte.agregarPaso("PAGO DE TC TERCEROS", "B�squeda de Tarjeta", alias, "", true, "N");
    }
    //++++
    public void seleccionaTC(Boolean fav) {
        String ini_path = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.ScrollView/android.view.ViewGroup/androidx.recyclerview.widget.RecyclerView";
        if (fav)
        {
            this.setTc(Util.Buscar_xpath(ini_path + "/android.view.ViewGroup[2]/android.view.ViewGroup"));
            this.setTc_titular(Util.Buscar_xpath(ini_path + "/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView[1]"));
            this.setTc_banco(Util.Buscar_xpath(ini_path + "/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView[2]"));
        }
        else
        {
            this.setTc(Util.Buscar_xpath(ini_path + "/android.view.ViewGroup[3]/android.view.ViewGroup"));
            this.setTc_titular(Util.Buscar_xpath(ini_path + "/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.TextView[1]"));
            this.setTc_banco(Util.Buscar_xpath(ini_path + "/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.TextView[2]"));
        }

        String actual = this.getTc_titular().getText();
        Reporte.agregarPaso("PAGO DE TC TERCEROS", "Tarjeta Seleccionada:", actual, "", true, "N");
        this.getTc().click();

    }

    public void vp_etiqueta_monto() {
        String actual = texto.get(0).getText();
        Util.assert_igual("PAGO DE TC TERCEROS", "Verificaci�n de t�tulo", actual, "MONTO A PAGAR", true, "N");
    }

    public void vp_dato_monto(String montopagar) {
        String actual = monto.getText().replace(",","");
        Util.assert_igual("PAGO DE TC TERCEROS", "Monto ingresado:", actual, montopagar, true, "N");
    }

    public void click_boton_Continuar() {
        String actual = boton.get(2).getText();
        Util.assert_igual("PAGO DE TC TERCEROS", "Monto ingresado:", actual, "CONTINUAR", false, "N");
        boton.get(2).click();
    }

    public void vp_etiqueta_confirmacion() {
        String actual = texto.get(0).getText();
        Util.assert_igual("PAGO DE TC TERCEROS", "Verificaci�n de t�tulo", actual, "CONFIRMACI�N", true, "N");
    }

    public void vp_dato_ConfirmaMonto(String monto_ingresado) {
        //String[] conf_monto = confirma_monto.getText().split(" ");//Separar USD del monto
        //String actual = conf_monto[0].replace(",","");
        String actual = texto.get(2).getText().replace(",","");
        Util.assert_igual("PAGO DE TC TERCEROS", "Verificaci�n de Monto ingresado", actual, monto_ingresado, false, "N");
    }

    public void vp_dato_ConfirmaTarjeta(String alias) {
        //String[] conf_ctadest = confirma_tc.getText().split("-");
        String[] conf_ctadest = texto.get(9).getText().split("-");
        String actual = conf_ctadest[0].trim();
        Util.assert_igual("PAGO DE TC TERCEROS", "Verificaci�n de Tarjeta a pagar", actual, alias, false, "N");
    }

    public void click_boton_Confirmar() {
        String actual = boton.get(1).getText();
        Util.assert_contiene("PAGO DE TC TERCEROS", "Presiona bot�n Pagar", actual, "PAGAR", false, "N");
        boton.get(1).click();
    }

    /*public void vp_etiqueta_msg() throws InterruptedException {
        String actual = msg_confirmacion.getText();
        Util.assert_contiene("PAGO DE TC TERCEROS", "Verificaci�n de Mensaje", actual, "�Transacci�n Exitosa!", true);
        Thread.sleep(3000);
    }*/

    public void vp_etiqueta_msg()
    {
        try {
            String actual = mensaje_error.getText();
            Util.assert_igual("PAGO DE TC TERCEROS", "Verificaci�n de Mensaje", actual, "CONFIRMACI�N", true, "N");
        }
        catch (Exception e) {
            //Util.logbm.info("SIN MENSAJE \n" );
        }
    }

    public void vp_verifica_loader(String msgesperado) {
        Boolean flag = true;
        int cont = 0;
        while (flag) {
            try {
                String actual = mensaje_loader.getText();
                cont++;
                if (cont == 1) {
                    Reporte.agregarPaso("PAGO DE TC TERCEROS", "Loader", msgesperado, "",true, "N");
                }
                if (!actual.equals(msgesperado)) {
                    flag = false;
                    System.out.println("0");
                }
            } catch (Exception e) {
                flag = false;
                System.out.println("1");
                this.vp_etiqueta_msg();
            }
        }
    }
}
