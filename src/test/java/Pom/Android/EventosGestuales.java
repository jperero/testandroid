package Pom.Android;

import Auxiliar.Util;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;
import java.awt.*;
import java.time.Duration;

import static io.appium.java_client.touch.WaitOptions.waitOptions;

public class EventosGestuales {
    public void gestualTouchScreenScrollHorizontal() { //De swipe de derecha a izquierda
        TouchAction swipeH = new TouchAction(Util.driver)
                .press(PointOption.point(1000, 1900))
                .waitAction(waitOptions(Duration.ofMillis(800)))
                .moveTo(PointOption.point(250, 1900))
                .release()
                .perform();

        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        System.out.println("Tu resolución es de " + screenSize.width + "x" + screenSize.height);
    }
    public void gestualTouchScreenScrollVertical() { //De swipe de abajo hacia arriba
        TouchAction swipeV = new TouchAction(Util.driver)
                .press(PointOption.point(540, 2147))
                .waitAction(waitOptions(Duration.ofMillis(10000)))
                .moveTo(PointOption.point(540, 752))
                .release()
                .perform();
    }
}
