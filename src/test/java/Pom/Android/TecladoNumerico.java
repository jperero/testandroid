package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class TecladoNumerico {
    public TecladoNumerico() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    private List<AndroidElement> lst_botones = null;

    public void IngresarValor(int cont, String codigo, Boolean trx )
    {
        //Listado de botones de numeros
        //cont 0 OTP / Problemas al ingresar
        //cont 1 Matricula Ctas, TC VALOR porque se tiene estos botones: 0CANCELAR  1CONTINUAR
        //cont 2 Pago de TC propia MONTO porque se tiene estos botones: 0CANCELAR  1CAMBIAR  2CONTINUAR
        System.out.println("CODIGO: "+codigo);
        for (char ch: codigo.toCharArray())
        {
            if (ch == '.') {
                if (lst_botones.get(0).getText().equals("SALIR")) {
                    Reporte.agregarPaso("TECLADO", "Ingreso de Valor:", lst_botones.get(10).getText(), "", false, "N");
                    System.out.println("valor ch:" + ch);
                    lst_botones.get(10).click();
                }
                continue;
            }
            int numero = Character.getNumericValue(ch);
            if (numero == 0){
                if (trx)
                    numero = 11;
                else
                    numero = 10;
            }
            if(codigo.contains(".")){
                Reporte.agregarPaso("TECLADO", "Ingreso de Valor:", lst_botones.get(numero+cont).getText(),"", false, "N");
                lst_botones.get(numero+cont).click();
            }else {
                Reporte.agregarPaso("TECLADO", "Ingreso de Valor:", lst_botones.get(cont).getText(), "", false, "N");
                lst_botones.get(cont).click();
            }
        }
    }
}
