package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;

public class BarraNavegacion {
    public BarraNavegacion() {
        if (Util.plataforma.equals("iOS"))
            PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
        else
            PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    //@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.widget.ImageView")
    //MobileElement opc_inicio = null;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Inicio']")
    AndroidElement opc_inicio = null;

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    //@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.widget.ImageView")
    //MobileElement opc_transf = null;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Transferir']")
    AndroidElement opc_transf = null;

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[3]/android.view.ViewGroup")
    MobileElement opc_mas = null;
/*    @AndroidFindBy(xpath = "//android.view.ViewGroup[@text='']")
    AndroidElement opc_mas = null;*/

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    //@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[4]/android.widget.ImageView")
    //MobileElement opc_pagar = null;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Pagar']")
    AndroidElement opc_pagar = null;

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    //@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[5]/android.widget.ImageView")
    //MobileElement opc_otros = null;
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Otros']")
    AndroidElement opc_otros = null;

    public void click_opc_inicio()
    {
        Reporte.agregarPaso("BARRA DE NAVEGACION", "Presiona opci�n Inicio", "Inicio", "", true, "N");
        opc_inicio.click();
    }

    public void click_opc_transf()
    {
        try {
            Reporte.agregarPaso("BARRA DE NAVEGACION", "Presiona opci�n Transferir", "Transferir", "", true, "N");
            opc_transf.click();
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void click_opc_mas()
    {
        Reporte.agregarPaso("BARRA DE NAVEGACION", "Presiona opci�n Mas Productos", "Mas", "", true, "N");
        opc_mas.click();
    }

    public void click_opc_pagar()
    {
        Reporte.agregarPaso("BARRA DE NAVEGACION", "Presiona opci�n Pagar", "Pagar", "", true, "N");
        opc_pagar.click();
    }

    public void click_opc_otros()
    {
        Reporte.agregarPaso("BARRA DE NAVEGACION", "Presiona opci�n Otros", "Otros", "", true, "N");
        opc_otros.click();
    }
}


