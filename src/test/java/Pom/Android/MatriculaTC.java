package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class MatriculaTC {
    public MatriculaTC() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView")
    MobileElement num_tc = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView")
    MobileElement identif = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> txtedit = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> imagen = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='¡Matriculación exitosa!']")
    static AndroidElement msj_matriculaTC_exitosa = null;

    private MobileElement tc_tipo_desc = null;
    private MobileElement tc_tipo = null;

    private MobileElement getTC_tipo_desc() {
        return tc_tipo_desc;
    }
    private void setTC_tipo_desc(MobileElement tc_tipodesc) {
        this.tc_tipo_desc = tc_tipodesc;
    }

    private MobileElement getTC_tipo() {
        return this.tc_tipo;
    }
    private void setTC_tipo(MobileElement tipo) {
        this.tc_tipo = tipo;
    }

    private MobileElement tc_banco_desc = null;
    private MobileElement tc_banco = null;

    private MobileElement getTC_banco_desc() {
        return tc_banco_desc;
    }
    private void setTC_banco_desc(MobileElement tc_bcodesc) {
        this.tc_banco_desc = tc_bcodesc;
    }

    private MobileElement getTC_banco() {
        return this.tc_banco;
    }
    private void setTC_banco(MobileElement bco) {
        this.tc_banco = bco;
    }

    String txt_expected = null;
    String ini_path = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[3]/androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup";

    private void setSelectedTipoTC(int idx) //Indice del tipo de tarjeta
    {
        String ini_path_tc = ini_path + "[";
        this.setTC_tipo_desc(null);
        this.setTC_tipo(null);

        this.setTC_tipo_desc(Util.Buscar_xpath(ini_path_tc + idx + "]/android.widget.TextView"));
        this.setTC_tipo(Util.Buscar_xpath(ini_path_tc + idx + "]"));
    }

    public void seleccionaTipoTC(String tipotc) {
        //Recorre el catalogo de tipo de tc hasta encontrar el que corresponde al datapool
        try {
            Thread.sleep(2000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        String sec_tctipo = texto.get(1).getText().trim();
        int idx1 = sec_tctipo.indexOf("(");
        int idx2 = sec_tctipo.indexOf(")");
        //Util.setNum_ctasmat(sec_tctipo.substring(idx1+1, idx2));
        int numtctipo = Integer.valueOf(sec_tctipo.substring(idx1+1, idx2));

        for (int idx=2; idx<numtctipo+2; idx++)
        {
            this.setSelectedTipoTC(idx); //imagen   tipo
            //Indice del listado de cuentas empieza en 2 y el ultimo es 7
            if (this.getTC_tipo_desc().getText().equals(tipotc))
            {
                Reporte.agregarPaso("MATRICULACIÓN DE TC ", "Selecciona de Tipo de Tarjeta", this.getTC_tipo_desc().getText(), "", true, "N");
                this.getTC_tipo().click();
                break;
            }
        }
    }
    //++++
    public void buscaBanco(String banco) {
        Util.assert_igual("MATRICULACIÓN DE TC ", "Verificación de etiqueta:", texto.get(0).getText(), "BUSCAR BANCO", true, "N");

        txtedit.get(0).sendKeys(banco);
        Util.driver.pressKey(new KeyEvent(AndroidKey.ENTER));
        Reporte.agregarPaso("MATRICULACIÓN DE TC ", "Busca Banco:", banco, "", true, "N");
    }
    //++++
    public void seleccionaBanco(String banco) {
        this.buscaBanco(banco);

        this.setTC_banco_desc(null);
        this.setTC_banco(null);

        this.setTC_banco_desc(Util.Buscar_xpath(this.ini_path + "/android.widget.TextView"));
        this.setTC_banco(Util.Buscar_xpath(this.ini_path));

        Reporte.agregarPaso("MATRICULACIÓN DE TC ", "Selecciona Banco", this.getTC_banco_desc().getText(), "", false, "N");
        this.getTC_banco().click();
    }

    public void vp_etiqueta_numTC() {
        String actual = texto.get(0).getText();
        Util.assert_igual("MATRICULACIÓN DE TC ", "Verificación de etiqueta:", actual, "NÚMERO DE TARJETA", true, "N");
    }

    public void vp_dato_numTC(String numerotc) {
        String actual = num_tc.getText();
        Util.assert_igual("MATRICULACIÓN DE TC ", "Ingresa Número de Tarjeta", actual, numerotc, true, "N");
    }

    public void click_boton_Continuar() {
        String actual = boton.get(1).getText();
        Util.assert_igual("MATRICULACIÓN DE TC ", "Presiona botón Continuar", actual, "CONTINUAR", false, "N");
        boton.get(1).click();
    }

    public void click_img_Favorito(String Fav) {
        Boolean fav = false;
        if (Fav.equals("1"))
            fav = true;

        if (fav)
        {
            imagen.get(2).click(); //2 Favorito 1 Informacion
            Reporte.agregarPaso("MATRICULACIÓN DE TC ", "Ingresa marca Favorito", "*", "", true, "N");
        }
    }

    public void ingresoAlias(int idx, String alias) {
        String alias_= null;
        if (alias.length()>14)
            alias_ = alias.substring(0,14);
        else
            alias_ = alias;

        try {
            Thread.sleep(2000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }

        txtedit.get(idx).sendKeys(alias_); //idx 0 para BB 1 para otros bancos
        //Util.driver.pressKey(new KeyEvent(AndroidKey.ENTER));
        Reporte.agregarPaso("MATRICULACIÓN DE TC ", "Ingresa Alias", alias_, "", true, "N");
    }

    public void ingresoTitular(String titular) {
        txtedit.get(0).sendKeys(titular);
       //Util.driver.pressKey(new KeyEvent(AndroidKey.ENTER));
        Reporte.agregarPaso("MATRICULACIÓN DE TC ", "Ingresa Titular", titular, "", true, "N");
    }

    public void seleccionaIdentificacion(String tipoid) {
        String actual = texto.get(2).getText();
        Util.assert_igual("MATRICULACIÓN DE TC ", "Verificación de etiqueta", actual, "Tipo de Identificación", true, "N");

        imagen.get(1).click(); //combo

        txt_expected = tipoid;
        int idx = 0;
        switch (tipoid) {
            case "C": txt_expected = "Cédula"; idx=1;  break;
            case "P": txt_expected = "Pasaporte"; idx=2;  break;
            case "R": txt_expected = "RUC"; idx=3;  break;
        }

        actual = texto.get(idx).getText();
        Util.assert_igual("MATRICULACIÓN DE TC ", "Verificación de Tipo de Identificación seleccionada", actual, txt_expected, true, "N");
        texto.get(idx).click();
    }

    public void vp_etiqueta_numIdentif() {
        String actual = texto.get(4).getText();
        Util.assert_igual("MATRICULACIÓN DE TC ", "Verificación de etiqueta", actual, "Número de Identificación", true, "N");
    }

    public void vp_dato_identif(String txt_expected) {
        String actual = identif.getText();
        Util.assert_igual("MATRICULACIÓN DE TC ", "Verificación de Identificación ingresada", actual, txt_expected, true, "N");
    }

    public void vp_etiqueta_msg() {
        Util.assert_igual("MATRICULACIÓN DE TC", "Verificación de Mensaje", msj_matriculaTC_exitosa.findElement(By.xpath("//*[@text]")).getText(), "¡Matriculación exitosa!", true, "N");
    }
}
