package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.support.PageFactory;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class RegistroEquipo {
    public RegistroEquipo() {
        if (Util.plataforma.equals("iOS"))
            PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
        else
            PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> imagen = null;

    @WithTimeout(time=3, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> txtedit = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
    MobileElement mensaje_exitoso = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
    MobileElement mensaje_error = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @iOSXCUITFindBy(accessibility = "btnNoThanks")
    MobileElement btn_nogracias = null;

    String get_etiquetaRegistrar() {
        String etiqueta = "";
        etiqueta = texto.get(0).getText();

        if (!etiqueta.equals("REGISTRAR DISPOSITIVO")) {
            etiqueta = "NO REQUIERE REGISTRO de DISPOSITIVO";
        }
        Reporte.agregarPaso("REGISTRO de DISPOSITIVO","Verificaci�n de Registro de Dispositivo", etiqueta,"", false, "N");
        return etiqueta;
    }

    void vp_etiqueta_deseas()
    {
        String actual = texto.get(1).getText();
        Util.assert_igual("REGISTRO de DISPOSITIVO", "Verificaci�n de etiqueta Deseas Registrar", actual, "�Deseas registrar este dispositivo?", true, "N");
    }

    void click_check()
    {
        imagen.get(1).click();
    }

    void click_boton_registrar()
    {
        String actual = boton.get(0).getText();
        Util.assert_igual("REGISTRO de DISPOSITIVO", "Presiona bot�n Registrar", actual, "REGISTRAR", true, "N");
        boton.get(0).click();//0 Registrar  1 No gracias
    }

    void click_boton_ContinuarEliminar()
    {
        String actual = boton.get(1).getText();
        Util.assert_igual("REGISTRO de DISPOSITIVO", "Presiona bot�n Continuar", actual, "Continuar", false, "N");
        boton.get(1).click();
    }

    String get_etiquetaPopupEliminar()
    {
        String etiqueta = null;
        //ELIMINACION DE DISPOSITIVOS
        try{
            boton.get(3).getText();
            etiqueta = texto.get(0).getText();
            Util.assert_igual("REGISTRO de DISPOSITIVO", "Verificaci�n de Popup Eliminar", etiqueta, "Para registrar este dispositivo debes eliminar uno de la lista", true, "N");
        }
        catch (Exception e)
        {
            //Util.logbm.info("No requiere eliminar dispositivos registrados");
        }
        return etiqueta;
    }

    void vp_etiqueta_equipos()
    {
        String actual = texto.get(0).getText();
        Util.assert_igual("REGISTRO de DISPOSITIVO", "Verificaci�n de Equipos Registrados", actual, "EQUIPOS REGISTRADOS", true, "N");
    }

    void selecciona_equipo()
    {
        //Listado de dispositivos registrados  2 primero 4 segundo 6 tercero
        imagen.get(2).click();
    }

    void vp_etiqueta_popupeliminar()
    {
        String actual = texto.get(1).getText();
        Util.assert_igual("REGISTRO de DISPOSITIVO", "Verificaci�n de Deseas eliminar", actual, "�Deseas eliminar por completo este dispositivo?", true, "N");
    }

    void click_boton_popupEliminar()
    {
        String actual = boton.get(1).getText();
        Util.assert_igual("REGISTRO de DISPOSITIVO", "Presiona bot�n Eliminar", actual, "Eliminar", false, "N");
        boton.get(1).click();
    }

    void vp_mensaje_eliminado() {
        try {
            Thread.sleep(4000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        String actual = mensaje_exitoso.getText();
        Util.assert_igual("REGISTRO de DISPOSITIVO", "Verificaci�n de Eliminaci�n de Dispositivo", actual, "Has eliminado tu dispositivo con �xito", true, "N");
    }

    void click_boton_ContinuarELiminado()
    {
        String actual = boton.get(0).getText();
        Util.assert_igual("REGISTRO de DISPOSITIVO", "Presiona bot�n Continuar", actual, "CONTINUAR", false, "N");
        boton.get(0).click();
    }

    void vp_etiqueta_alias()
    {
        String actual = texto.get(1).getText();
        Util.assert_igual("REGISTRO de DISPOSITIVO", "Verificaci�n de Ingreso de alias", actual, "Ingresa el alias del dispositivo", true, "N");
    }

    void ingresar_alias()
    {
        LocalTime time = LocalTime.now();
        DateTimeFormatter formattime = DateTimeFormatter.ofPattern("HHmm");

        String alias = time.format(formattime) ;
        txtedit.get(0).sendKeys(alias);
        Util.driver.pressKey(new KeyEvent(AndroidKey.ENTER));
        Reporte.agregarPaso("REGISTRO de DISPOSITIVO", "Ingresa Alias", alias, "", true, "N");
    }

    void click_boton_ContinuarAlias()
    {
        String actual = texto.get(3).getText();
        Util.assert_igual("REGISTRO de DISPOSITIVO", "Presiona bot�n Continuar", actual, "CONTINUAR", false, "N");
        texto.get(3).click();
    }

    void vp_mensaje_registro() {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        String actual = mensaje_exitoso.getText();
        Util.assert_igual("REGISTRO de DISPOSITIVO", "Verificaci�n de Registro de Alias", actual, "Has registrado tu dispositivo con �xito", true, "N");
    }

    void selecciona_metodo()
    {
		/*Thread.sleep(6000);
		txt_expected = "Selecciona tu m�todo de ingreso";
		assertThat(texto.get(1).getText(), is(txt_expected));
		Util.logbm.info("==>" + txt_expected);*/
        imagen.get(3).click();
        //0 img usuario y contrase�a  1 visto de seleccion 2 img touch
    }

    void vp_etiqueta_Touch()
    {
        try {
            Thread.sleep(2500);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        String actual = texto.get(1).getText().toUpperCase();
        Util.assert_igual("REGISTRO de DISPOSITIVO", "Verificaci�n de M�todo de Acceso", actual, "TOUCH ID", false, "N");
    }

    void click_boton_Activar() {
        String actual = boton.get(1).getText().toUpperCase();
        Util.assert_igual("REGISTRO de DISPOSITIVO", "Presiona bot�n Activar y Seleccionar", actual, "ACTIVAR Y SELECCIONAR", true, "N");
        boton.get(1).click();
    }

    void vp_mensaje_metodo() {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        String actual = mensaje_exitoso.getText();
        Util.assert_igual("REGISTRO de DISPOSITIVO", "Verificaci�n de Registro de M�todo", actual, "Has configurado tu m�todo de ingreso para 24m�vil", true, "N");
    }

    void click_boton_ContinuarMetodo()  {
        String actual = boton.get(1).getText();
        Util.assert_igual("REGISTRO de DISPOSITIVO", "Presiona bot�n Continuar", actual, "CONTINUAR", false, "N");
        boton.get(1).click();
    }

    void vp_mensaje_error()
    {
        //Si aparece un mensaje de error al intentar hacer login, termina el script y se inserta en el log el mensaje que apareci�
        try {
            mensaje_error.getText();
            Util.assert_igual("REGISTRO de DISPOSITIVO", "Verificaci�n de Login", mensaje_error.getText(), "Mis productos", true, "N");
        }
        catch (Exception e) {
            //Util.logbm.info("LOGIN SIN MENSAJES DE ERROR" );
        }
    }

    public void click_boton_NoGracias()
    {
        try{
            String actual = null;
            if (Util.plataforma.equals("iOS"))
                actual = btn_nogracias.getText();
            else
                actual = boton.get(1).getText();
            Util.assert_igual("REGISTRO de DISPOSITIVO - NO GRACIAS", "Presiona bot�n No Gracias", actual, "NO GRACIAS", true, "N");
            boton.get(1).click();
        }catch (Exception e) {
            //Util.logbm.info("No se registr� el dispositivo");
        }
        this.vp_mensaje_error();
    }
}
