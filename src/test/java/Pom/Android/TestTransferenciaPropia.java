package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class TestTransferenciaPropia {
    public TestTransferenciaPropia() {PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);}

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView[1]")
    MobileElement monto = null;

    public void TransferenciaPropiaTest(int orden, String montopagar) {
        String sel_ctaorig = null;
        String sel_tipoctaorig = null;
        String sel_ctadest = null;
        String sel_tipoctadest = null;

        Transferencia transf = new Transferencia();

        //*Escojo Cuenta Destino//
        transf.vp_etiqueta_CtaDestino();
        transf.seleccionaCta("P", orden);
        sel_ctadest = transf.getSelectedNumCta("P");
        sel_tipoctadest = transf.getSelectedTipoCta();
        Reporte.agregarPaso("TRANSFERENCIA", "Selecciona N�mero de Cuenta", sel_ctadest, "", false, "N");
        Reporte.agregarPaso("TRANSFERENCIA", "Selecciona Tipo de Cuenta", sel_tipoctadest, "", false, "N");
        transf.getSelectedCta().click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }

        //*Escojo Cuenta Origen//
        if (texto.get(0).getText().equals("CUENTA ORIGEN")) {
            //*Escojo  Cuenta Origen//
            transf.vp_etiqueta_CtaOrigen();
            transf.seleccionaCta("P",  1);
            sel_ctaorig = transf.getSelectedNumCta("P");
            sel_tipoctaorig = transf.getSelectedTipoCta();
            Reporte.agregarPaso("TRANSFERENCIA", "Selecciona N�mero de Cuenta", sel_ctaorig, "", false, "N");
            Reporte.agregarPaso("TRANSFERENCIA", "Selecciona Tipo de Cuenta", sel_tipoctaorig, "", false, "N");
            transf.getSelectedCta().click();
        }
        //Ingreso Monto
        transf.vp_etiqueta_MontoTransferir();

        transf.click_boton_Cambiar();

        transf.seleccionaCta("P",  1);
        sel_ctaorig = transf.getSelectedNumCta("P");
        sel_tipoctaorig = transf.getSelectedTipoCta();
        Reporte.agregarPaso("TRANSFERENCIA", "Selecciona N�mero de Cuenta", sel_ctaorig, "", false, "N");
        Reporte.agregarPaso("TRANSFERENCIA", "Selecciona Tipo de Cuenta", sel_tipoctaorig, "", false, "N");
        transf.getSelectedCta().click();

        TecladoNumerico teclado = new TecladoNumerico();
        teclado.IngresarValor(2, montopagar, true);
        String monto_ingresado = monto.getText();
        Reporte.agregarPaso("TRANSFERENCIA", "Ingresa Monto", monto_ingresado, "", false, "N");

        transf.click_boton_Continuar();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }

        //**DATOS DE ENTRADA VS DATOS DE CONFIRMACION
        transf.vp_etiqueta_Confirmacion();

        //Monto a Transferir
        transf.vp_dato_ConfirmaMonto(monto_ingresado);
        //Cta origen
        transf.vp_dato_ConfirmaCtaOrigen(sel_ctaorig);
        transf.vp_dato_ConfirmaTipoCtaOrigen(sel_tipoctaorig);
        //Cta destino
        transf.vp_dato_ConfirmaCtaDestino(sel_ctadest);
        transf.vp_dato_ConfirmaTipoCtaDestino(sel_tipoctadest);
        //Confirmar
        transf.click_boton_Confirmar();

        transf.vp_verifica_loader("Estamos transfiriendo tu dinero");

        //COMPROBANTE
        Comprobante comp = new Comprobante();
        comp.vp_etiqueta_Confirmacion();
        comp.vp_etiqueta_Exitosa();
        comp.vp_dato_Monto(monto_ingresado);
        comp.vp_dato_ctaDestino(sel_ctadest);
        comp.vp_dato_ctaOrigen(sel_ctaorig);
        Util.CapturarImagen();
        comp.click_boton_MasOpciones();
        Util.CapturarImagen();
        comp.click_boton_NuevaTransf();

        //Tap sobre el primer registro de ultimas transferencias
       /* Util.RefrescarBusq();
        lst_transfer.click();*/

        /*PUNTOS DE VERIFICACION DE LA TRANSFERENCIA REALIZADA*/
        /*DATOS DE PANTALLA DETALLE DE LA TRANSFERENCIA VS DATOS DE PANTALLA DE CONFIRMACION*/
        /*transf.vp_etiqueta_Detalle();
        transf.vp_dato_DetalleMonto(monto_ingresado);
        transf.vp_dato_DetalleCtaOrigen(sel_ctaorig);
        transf.vp_dato_DetalleCtaDestino(sel_ctadest);
        */

    }
}
