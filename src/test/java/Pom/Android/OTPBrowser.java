package Pom.Android;

import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class OTPBrowser {
    public OTPBrowser() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);

    }

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.view.View")
    List<AndroidElement> lst_apps = null;

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> txtedit = null;

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[1]/android.widget.FrameLayout[2]/android.webkit.WebView/android.view.View/android.view.View/android.widget.GridView/android.view.View[2]/android.view.View")
    MobileElement otp = null;

    String txt_expected = null;

    public void cambiarAbrowser() {
        Activity activity = new Activity(Util.appPackageBrowser, Util.appActivityBrowser);
        Util.driver.startActivity(activity);
        //activity.setStopApp(true);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }

    public void IdentificacionIngresar(String id) {
        txtedit.get(0).sendKeys(id);
        Util.CapturarImagen();
        //Util.logbm.info("Usuario==>" + id);
    }

    public void click_boton_Continuar() {
        txt_expected = "Ingresar";
        assertThat(boton.get(0).getText(), is(txt_expected));
        boton.get(0).click();
        //Util.logbm.info("==>" + txt_expected);
    }

    public void cambiarApp() {
        //Cambia a la app anterior
        Util.driver.pressKey(new KeyEvent(AndroidKey.APP_SWITCH));
        try {
            Thread.sleep(3500);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        Util.driver.pressKey(new KeyEvent(AndroidKey.APP_SWITCH));
    }

    public String obtieneOtp () {
        try {
            Thread.sleep(8000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        //Util.logbm.info("==>" + otp.getText());
        String info[] = otp.getText().split(">>");
        String codigo[] = info[1].split(":");
        //Util.logbm.info("OTP ==> "+codigo[1]);
        return codigo[1].trim();
    }

    }
