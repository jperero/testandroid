package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

import static io.appium.java_client.touch.WaitOptions.waitOptions;
import static java.time.Duration.ofMillis;

public class Salir {
    public Salir() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> cab_titulo = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    //@AndroidFindBy(className = "android.widget.Button")
    //List<AndroidElement> boton = null;
    @AndroidFindBy(xpath = "//android.widget.Button[@text='Cerrar sesi�n']")
    MobileElement boton = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView")
    MobileElement mensaje_popup = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup[3]/android.widget.TextView")
    MobileElement btn_salir_popup = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='�Est�s seguro que deseas salir?']")
    MobileElement mensaje_popup2 = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Salir']")
    MobileElement btn_salir_popup2 = null;

    @WithTimeout(time = 15, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> btn_salir = null;

    @WithTimeout(time = 15, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> btn_menuBurguer = null;

    String tituloCabecera = null;
    String mensajePopUp = "�Est�s seguro que deseas salir?";

    public void salirAPP() {
        //Situado en pantalla MIS PRODUCTOS (Posici�n Consolidada) - Pulsa Bot�n Salir
        tituloCabecera = "MIS PRODUCTOS";
        //assertThat(cab_titulo.get(0).getText(), is(tituloCabecera));
        Util.assert_igual("MIS PRODUCTOS", "Salir APP", cab_titulo.get(0).getText(), tituloCabecera, true, "N");
        System.out.println("==>" + tituloCabecera);
        btn_salir.get(2).click();

        //Confirma la acci�n de Salir de la APP
        mensajePopUp = "�Est�s seguro que deseas salir?";
        //assertThat(mensaje_popup.getText(), is(mensajePopUp));
        Util.assert_igual("MIS PRODUCTOS", "Confirma Salir APP", mensaje_popup.getText(), mensajePopUp, true, "N");
        System.out.println("==>" + mensajePopUp);
        Reporte.agregarPaso("MIS PRODUCTOS", "Confirma Salir APP", btn_salir_popup.getText(), "", false, "N");
        System.out.println("==>" + btn_salir_popup.getText());
        btn_salir_popup.click();
    }

    public void massalirAPP() {
        btn_menuBurguer.get(0).click();//Imagen - Men� Burguer
        try {
            Thread.sleep(1000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        btn_menuBurguer.get(0).click();//Imagen - Men� MAS > Bot�n Salir
        //Confirma la acci�n de Salir de la APP
        mensajePopUp = "�Est�s seguro que deseas salir?";
        Util.assert_igual("MIS PRODUCTOS", "Confirma Salir APP", mensaje_popup2.getText(), mensajePopUp, true, "N");
        System.out.println("==>" + mensajePopUp);
        Reporte.agregarPaso("MIS PRODUCTOS", "Confirma Salir APP", btn_salir_popup2.getText(), "", false, "N");
        System.out.println("==>" + btn_salir_popup2.getText());
        btn_salir_popup2.click();

    }

    public void otrosSalirAPP() {
        TouchAction acciones = new TouchAction(Util.driver)
                .press(PointOption.point(540, 2010))
                .waitAction(waitOptions(ofMillis(10000)))
                .moveTo(PointOption.point(540, 740))
                .release()
                .perform();
        System.out.println(boton.getText());
        if (boton.isDisplayed()){
            System.out.println(boton.getText());
            Reporte.agregarPaso("MIS PRODUCTOS", "Bot�n Cerrar Sesi�n", boton.getText(), "", true, "N");
            boton.click();

            Util.assert_igual("MIS PRODUCTOS", "PopUp - Mensaje Confirmaci�n", mensaje_popup2.getText(), mensajePopUp, true, "N");
            Reporte.agregarPaso("MIS PRODUCTOS", "PopUp - Bot�n Salir", btn_salir_popup2.getText(), "", false, "N");
            btn_salir_popup2.click();
        }

    }
}
