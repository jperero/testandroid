package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class ConfigurarCuentaDefault {
    public ConfigurarCuentaDefault() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    static List<AndroidElement> seccion_titulo = null;

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    static List<AndroidElement> msj_popup = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    static List<AndroidElement> cab_titulo = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    static List<AndroidElement> btn_Radiobutton = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    static List<AndroidElement> btn_Continuar = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
    static AndroidElement msj_exito_verde = null;

    static ValidacionesCascada validarMensajesDeError;
    static Salir saliendoAPP;

    public void ingresarAjustes() {
        String txt_expected = "Cuenta d�bito predeterminada";
        int i, idxTransfer, idxPayCard;
        Boolean flagContinua=true;

        Menu opcMenu = new Menu();
        opcMenu.presionaMenu(11); //Selecci�n Opci�n M�S > Ajustes (Item: 11)
        String actual = seccion_titulo.get(13).getText();
        Util.assert_igual("CUENTA DEFAULT", "Secci�n Cuenta Default", actual, txt_expected, true, "N");

        i = 0;
        idxTransfer=0;
        idxPayCard=0;
        //Recorre los elementos de la pantalla de AJUSTES
        for (AndroidElement ElementoY : seccion_titulo) {
            switch (ElementoY.getText()) {
                case "Transferencias":
                    idxTransfer = i;
                    break;
                case "Pago de tarjetas":
                    idxPayCard = i;
                    break;
            }
            i++;
        }
        //Opcion Cuenta Default > Transferencias
        String opcSeleccionada = seccion_titulo.get(idxTransfer).getText();
        seccion_titulo.get(idxTransfer).click();//Selecciona Opci�n Cuenta D�bito Predeterminada>(idx=12 Transferencias | idx=14 Pago de Tarjetas)
        flagContinua=validarCuentaDefault(opcSeleccionada,flagContinua);

        //Opcion Cuenta Default > Pago de Tarjetas
        if (flagContinua){
            try {
                Thread.sleep(7000);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
            opcSeleccionada=seccion_titulo.get(idxPayCard).getText();
            seccion_titulo.get(idxPayCard).click();//Selecciona Opci�n Cuenta D�bito Predeterminada>(idx=12 Transferencias | idx=14 Pago de Tarjetas)
            flagContinua=validarCuentaDefault(opcSeleccionada,flagContinua);
        }
    }
    public Boolean validarCuentaDefault(String opcionSeleccionada,Boolean flagContinua)  {
        validarMensajesDeError = new ValidacionesCascada();
        validarMensajesDeError.validarMensajeAdvertencia(msj_popup.get(1).getText());
        if (cab_titulo.get(0).getText().equals("AJUSTES")) {
          saliendoAPP = new Salir();
          saliendoAPP.massalirAPP();
          flagContinua=false;
        } else {
          seleccionarCuentaDefault(opcionSeleccionada);//Pantalla de Selecci�n Cuenta Default
          pantallaMensajeFinal();    //Pantalla de Mensaje Exitoso
        }
        return flagContinua;
    }
    public void seleccionarCuentaDefault (String opcionSeleccionada){
        int k=2;
        Util.assert_igual("CUENTA DEFAULT","Selecci�n de Cuenta<br>"+opcionSeleccionada,cab_titulo.get(0).getText(),"CUENTA PREDETERMINADA",true, "N");
        for (AndroidElement ElementoX : cab_titulo){
           System.out.println("Elemento: "+k+" "+ElementoX.getText());
           if (ElementoX.getText().contains("Cuenta")){
               k++;
           }
        }
        btn_Radiobutton.get(k-1).click();
        Reporte.agregarPaso("CUENTA DEFAULT","Confirmar","CONFIRMAR","CONFIRMAR",false, "N");
        //Util.logbm.info("INFORMACI�N: ==>" + "CONFIRMAR");
        btn_Continuar.get(0).click();
    }
    public static void pantallaMensajeFinal(){
        String mensaje = msj_exito_verde.findElement(By.xpath("//*[@text]")).getText();
        ValidacionesCascada mensajeEmergente = new ValidacionesCascada();
        String txt_expected=mensajeEmergente.validarMensajeEmergente(mensaje);
        Util.assert_igual("CUENTA DEFAULT","Mensaje Resultado",mensaje,txt_expected,true, "N");
        //Util.logbm.info("INFORMACI�N: ==>" + txt_expected);
    }
}