package Pom.Android;

import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class PreQuickView {
    public PreQuickView() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }
    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> msjQuickView=null;

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.view.ViewGroup")
    MobileElement desplegableQuickView=null;

    String txt_msjQuickView= "Activa Quick View en los Ajustes de 24m�vil";
    String txt_saldoaGirar= "Saldo a Girar";

    @Test
    public void verificarActivacionQW(){
        try {
            //Util.logbm.info("QUICKVIEW");
            if (desplegableQuickView.isDisplayed() && msjQuickView.get(4).getText().equals(txt_msjQuickView) ) {
                //Util.logbm.info(txt_msjQuickView);
                assertThat(msjQuickView.get(4).getText(), is(txt_msjQuickView));
            }else{
                //Util.logbm.info(txt_msjQuickView);
                assertThat(msjQuickView.get(7).getText(), is(txt_saldoaGirar));
                //Util.logbm.info("QUICKVIEW ACTIVADO - PRESENTA CUENTAS");
            }
        }catch (Exception e){
            //Util.logbm.info("NO CARGA QUICKVIEW");
        }
        msjQuickView.get(1).click();
    }
}
