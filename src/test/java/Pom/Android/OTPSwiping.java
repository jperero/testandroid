package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.awt.*;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static io.appium.java_client.touch.WaitOptions.waitOptions;

public class OTPSwiping {
    public OTPSwiping() {
        if (Util.plataforma.equals("iOS"))
            PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
        else
            PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time = 6, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> imagen = null;

    @WithTimeout(time = 6, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> txt_otp = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.HorizontalScrollView")
    AndroidElement scroll= null;


    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.Button[@text='CONFIRMAR']")
    AndroidElement boton_Confirmar = null;



    String caso = "OTP";

    void vp_popover_cabecera(String esperado) {
        try {
            System.out.println("VALOR ESPERADO: "+ esperado);
            System.out.println("VALOR ENCONTRADO: "+ Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + esperado + "')]")).getText());

            ////android.view.ViewGroup/

            Util.assert_igual(caso, "Verificaci�n Cabecera PopOver", Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, false, "N");
        } catch (Exception e) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
    }

    void click_boton_Confirmar() {
        Util.assert_igual(caso, "Presiona bot�n Continuar", boton_Confirmar.getText(), "CONFIRMAR", true, "N");
        boton_Confirmar.click();
    }

    public void generarOTPSwiping() {
        String medio = "3507";
        this.vp_popover_cabecera("�D�nde deseas recibir el c�digo de seguridad?");
        this.seleccionar_medios_swipe(medio);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        this.vp_popover_cabecera("Introduce el c�digo que enviamos a tu n�mero de celular terminado en " + medio);

        String codotp = this.get_CodigoOtp();
        this.ingreso_codigo_OTP(codotp);
        Reporte.agregarPaso(caso, "Ingresa c�digo Otp", codotp, "", true, "N");

        this.click_boton_Confirmar();
    }

    String get_CodigoOtp() {
        String codotp = null;

        if (Util.prop.getProperty("otp_base").equals("S")) {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException exception) {
                exception.printStackTrace();
            }
            codotp = "111111";
        } else {
            OTPBrowser otpb = new OTPBrowser();
            otpb.cambiarAbrowser();

            otpb.IdentificacionIngresar(Util.getDataCliente()[3]);
            otpb.click_boton_Continuar();
            codotp = otpb.obtieneOtp();

            otpb.cambiarApp();
        }
        return codotp;
    }

    void ingreso_codigo_OTP(String otp) {
        TecladoNumerico tecladoNumerico = new TecladoNumerico();
        tecladoNumerico.IngresarValor(0, otp, true);
    }

    void gestualTouchScreenScrollHorizontal() {
        Toolkit t = Toolkit.getDefaultToolkit();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int xCoord1 = (int) (screenSize.height*0.10);
        int xCoord2 = (int) (screenSize.height*0.90);
        String boundsString[]=((((scroll.getAttribute("bounds").replaceAll("\\]\\[",",").replaceAll("\\]\\[",",")).replaceAll("\\[","")).replaceAll("\\]","")).split(","));
        //String boundsCoord=(((scroll.getAttribute("bounds").replaceAll("\\]\\[",",").replaceAll("\\]\\[",",")).replaceAll("\\[","")).replaceAll("\\]",""));
        //String boundsString[] = boundsCoord.split(",");
        int yCoord = Integer.parseInt(boundsString[1])+((Integer.parseInt(boundsString[3])- Integer.parseInt(boundsString[1])) / 2);

        TouchAction swipeH = new TouchAction(Util.driver)
                .press(PointOption.point(xCoord2, yCoord))//x=1000 y=1900
                .waitAction(waitOptions(Duration.ofMillis(800)))
                .moveTo(PointOption.point(xCoord1, yCoord))//x=250  y=1900
                .release()
                .perform();
    }

    void seleccionar_medios_swipe(String medio) {
        medio = "*" + medio;
        boolean flag = true;
        while (flag) {
            try {
                if (Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + medio + "')]")).isDisplayed()) {
                    Util.assert_contiene(caso, "Selecci�n de medio", Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + medio + "')]")).getText(), medio, true, "N");
                    flag = false;
                }
            } catch (Exception NoSuchElementException) {
                gestualTouchScreenScrollHorizontal();
            }
        }
        Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + medio + "')]")).click();
    }
}
