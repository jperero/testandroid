package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class ClaveVirtualActivacion {
    public ClaveVirtualActivacion()  {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> imagen = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    Boolean reactivacion = false;
    Boolean popup = false;

    public void vp_etiqueta_activacion() {
        String actual = texto.get(1).getText();
        String esperado1 = "Deseas activar el servicio Clave Virtual";
        String esperado2 = "Deseas reactivar el servicio Clave Virtual";

        Util.assert_contiene("ACTIVAR CLAVE VIRTUAL", "Verificaci�n de etiqueta de Activaci�n", actual,esperado1, esperado2, true, "N");

        if (actual.contains("reactivar")) {
            System.out.println("****->>>>"+actual);
            reactivacion = true;
        }
    }

    public void click_check()
    {
        if (!reactivacion) {
            Reporte.agregarPaso("ACTIVAR CLAVE VIRTUAL", "Presiona check de T�rminos y Condiciones", "", "ACTIVAR", true, "N");
            imagen.get(2).click();
        }
    }

    public void click_boton_activar()
    {
            String actual = boton.get(1).getText();
            Util.assert_igual("ACTIVAR CLAVE VIRTUAL", "Presiona bot�n Activar", actual, "ACTIVAR", false, "N");
            boton.get(1).click();//0 Registrar  1 No gracias
    }

    public void click_boton_noGracias()
    {
        String actual = boton.get(2).getText();
        Util.assert_igual("ACTIVAR CLAVE VIRTUAL", "Presiona bot�n No Gracias", actual, "NO GRACIAS", true, "N");
        boton.get(1).click();//0 Registrar  1 No gracias
    }

    public void vp_etiqueta_popupDispositivo() {
        try{
            String actual = texto.get(0).getText();
            //quitar el assert y poner un if
            if (actual.equals("Al activar tu Clave Virtual"))
            {
                Reporte.agregarPaso("ACTIVAR CLAVE VIRTUAL", "Verificaci�n de popup para cliente con un dispositivo asignado", actual, "Al activar tu Clave Virtual", true, "N");
                popup = true;
            }
        }
        catch (Exception e)
        {
            popup = false;
        }
    }

    public void click_boton_cancelar()
    {
        String actual = boton.get(0).getText();
        Util.assert_igual("ACTIVAR CLAVE VIRTUAL", "Presiona bot�n Cancelar de popup", actual, "Cancelar", false, "N");
        boton.get(0).click();//0 Registrar  1 No gracias
    }

    public void click_boton_continuar()
    {
        if (popup) {
            String actual = boton.get(1).getText();
            Util.assert_igual("ACTIVAR CLAVE VIRTUAL", "Presiona bot�n Continuar de popup", actual, "Continuar", false, "N");
            boton.get(1).click();//0 Registrar  1 No gracias
        }
    }

    public void vp_etiqueta_cta_cobro() {
        if (!reactivacion) {
            String actual = texto.get(1).getText();
            Util.assert_contiene("ACTIVAR CLAVE VIRTUAL", "Verificaci�n de t�tulo", actual, "Elige una cuenta para realizar el cobro", true, "N");
        }
    }

    public void selecciona_cta_cobro()
    {
        if (!reactivacion) {
            String cuenta = texto.get(2).getText();
            String tipo_cuenta = texto.get(4).getText();
            String saldo = texto.get(3).getText();
            texto.get(2).click();
        }
    }

    public void vp_etiqueta_activarServicio() {
        String actual = texto.get(0).getText();
        Util.assert_contiene("ACTIVAR CLAVE VIRTUAL", "Verificaci�n de t�tulo", actual,"ACTIVAR SERVICIO", true, "N");
    }

    public void vp_etiqueta_metodoUsar() {
        String actual = texto.get(1).getText();
        Util.assert_contiene("ACTIVAR CLAVE VIRTUAL", "Verificaci�n de etiqueta", actual,"Selecciona tu m�todo para usar Clave Virtual", false, "N");
    }

    public void selecciona_metodo()
    {
        String touch = texto.get(3).getText();
        String pin = texto.get(5).getText();
        String directa = texto.get(7).getText();
        texto.get(7).click();
    }

    public void click_boton_activarYseleccionar()
    {
        String actual = boton.get(0).getText();
        Util.assert_igual("ACTIVAR CLAVE VIRTUAL", "Presiona bot�n Activar y Seleccionar", actual, "ACTIVAR Y SELECCIONAR", false, "N");
        boton.get(0).click();//0 Registrar  1 No gracias
    }

    public void popup_activacion()
    {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        String actual = texto.get(0).getText();
        Util.assert_igual("ACTIVAR CLAVE VIRTUAL", "Verificaci�n de popup de activaci�n", actual, "Has activado correctamente tu Clave Virtual. Por favor vuelve a iniciar sesi�n", true, "N");
        boton.get(0).click();//0 Registrar  1 No gracias
    }

}
