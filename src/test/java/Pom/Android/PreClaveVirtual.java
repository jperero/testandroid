package Pom.Android;
import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class PreClaveVirtual {
    public PreClaveVirtual() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }
    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> msjClaveVirtual=null;

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    MobileElement btnAtras=null;

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> opcPreliminar=null;

    @WithTimeout(time=30, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    MobileElement btnTouchIDSamsung = null;

    String txtTituloClaveVirtual ="CLAVE VIRTUAL";
    String msjSinClaveVirtual ="Activa la Clave Virtual desde los Ajustes de 24m�vil";

    public void verificarActivacionCV(){
        //assertThat(msjClaveVirtual.get(0).getText(), is(txtTituloClaveVirtual));

        PreClaveVirtualMetodo preCVMetodo = new PreClaveVirtualMetodo();

        try{
            /*Util.logbm.info(txtTituloClaveVirtual);
            assertThat(msjClaveVirtual.get(1).getText(), is(msjSinClaveVirtual));
            Util.logbm.info("==>" + msjSinClaveVirtual);*/
            if (msjClaveVirtual.get(1).getText().equals(msjSinClaveVirtual)){

                assertThat(msjClaveVirtual.get(1).getText(), is(msjSinClaveVirtual));

            }else{

                switch (opcPreliminar.get(0).getText()) { //Encuentra el texto del popup del m�todo
                    case "Ingrese Pin":

                        preCVMetodo.ingresarMetodoPIN();
                        btnAtras.click();
                        break;
                    case "Verificar su identidad":

                        if (btnTouchIDSamsung.isDisplayed()){
                            btnTouchIDSamsung.click();
                            opcPreliminar.get(2).click();
                        }else{
                            preCVMetodo.ingresarMetodoTOUCHID();
                        }
                        break;
                    case "Touch ID":

                        preCVMetodo.ingresarMetodoTOUCHID();
                        break;
                    default:
                        //Util.logbm.info("==>" + "M�todo Verificaci�n Directa");
                }
            }
        }
        catch (Exception e){
            //Util.logbm.info("NO CARGA CLAVE VIRTUAL");
        }
    btnAtras.click();
    }
}
