package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class CambiarContrasena
{
	public CambiarContrasena() 	{
		PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
	}

	@WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(className = "android.widget.TextView")
	List<AndroidElement> texto = null;

	@WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(className = "android.widget.EditText")
	List<AndroidElement> edit = null;

	@WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(className = "android.widget.Button")
	List<AndroidElement> boton = null;

	@WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(className = "android.widget.ImageView")
	List<AndroidElement> imagen = null;

	@WithTimeout(time = 30, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
	AndroidElement mensaje = null;

	@WithTimeout(time = 30, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
	AndroidElement mensaje_error = null;

	public void vp_etiqueta_cambiar() {
		String actual = texto.get(0).getText();
		Util.assert_igual("CAMBIAR CONTRASEÑA", "Verificación de etiqueta", actual,"CAMBIAR CONTRASEÑA", true , "N");
	}

	public void vp_etiqueta_nueva() {
		String actual = texto.get(3).getText();
		Util.assert_igual("CAMBIAR CONTRASEÑA", "Verificación de etiqueta", actual,"Nueva Contraseña", true , "N");
	}

	public void ingreso_nueva(String nueva)
	{
		edit.get(0).sendKeys(nueva);
		Reporte.agregarPaso("CAMBIAR CONTRASEÑA", "Ingresa nueva contraseña", nueva,"", true , "N");
	}

	public void click_boton_ojo(){
		imagen.get(1).click();
		Reporte.agregarPaso("CAMBIAR CONTRASEÑA", "Presiona botón en claro", "En claro","", true , "N");
	}

	public void vp_etiqueta_repite() {
		String actual = texto.get(4).getText();
		Util.assert_igual("CAMBIAR CONTRASEÑA", "Verificación de etiqueta", actual,"Re-Ingresa Nueva Contraseña", true , "N");
	}

	public void ingreso_repite(String nueva)
	{
		edit.get(1).sendKeys(nueva);
		Reporte.agregarPaso("CAMBIAR CONTRASEÑA", "Re Ingresa nueva contraseña", nueva,"", true , "N");
	}

	public void click_boton_cambiar() {
		String actual = boton.get(0).getText();
		Util.assert_igual("CAMBIAR CONTRASEÑA", "Presiona botón Cambiar Contraseña", actual,"CAMBIAR CONTRASEÑA", true , "N");
		boton.get(0).click();
	}

	public void vp_mensaje() {
		String actual = mensaje.getText();
		//String actual = texto.get(0).getText();
		Util.assert_contiene("CAMBIAR CONTRASEÑA", "Verificación de mensaje de transancción", actual,"Su contraseña se ha actualizado correctamente. ¡Por favor Iniciar sesión!", true , "N");
	}

	public void vp_mensaje_error() {
		String actual = mensaje_error.getText();
		Util.assert_contiene("CAMBIAR CONTRASEÑA", "Verificación de mensaje de transancción", actual,"anteriormente. Intenta con otra", true , "C");
	}
}