package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.support.PageFactory;
import java.time.temporal.ChronoUnit;

public class TiempoSesion {
    public TiempoSesion() {
        if (Util.plataforma.equals("iOS"))
            PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
        else
            PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time=3, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[1]")
    @iOSXCUITFindBy(accessibility = "lblIdleTimeoutHeader")
    MobileElement texto_sesion =null;

    @WithTimeout(time=3, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.TextView")
    @iOSXCUITFindBy(accessibility = "lblIdleTimeAccept")
    MobileElement boton_aceptar =null;

    void click_boton_Aceptar() {
        String actual = boton_aceptar.getText();
        Util.assert_igual("SESI�N",  "Bot�n Aceptar", actual, "Aceptar",true, "N");
        boton_aceptar.click();
    }

    public void Aceptar()
    {
        try {
            String mensaje = texto_sesion.getText();

            if (mensaje.equals("El tiempo de la sesi�n ha concluido.")){
                this.click_boton_Aceptar();
            }
        }
        catch (Exception e) {
            Reporte.agregarPaso("SESI�N", "Verificaci�n de popup de Tiempo de Sesi�n", "No concluy� el tiempo en la sesi�n anterior", "",false, "N");
        }
    }
}
