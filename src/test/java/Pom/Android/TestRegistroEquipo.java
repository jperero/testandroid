package Pom.Android;

public class TestRegistroEquipo{

	public void registroTest()
	{
		String txt_etiqueta1 = null;
		String txt_etiqueta2 = null;

		RegistroEquipo registro =  new RegistroEquipo();
		txt_etiqueta1 = registro.get_etiquetaRegistrar();

		if (txt_etiqueta1.equals("REGISTRAR DISPOSITIVO")) {
			registro.vp_etiqueta_deseas();
			registro.click_check();
			registro.click_boton_registrar();

			txt_etiqueta2 = registro.get_etiquetaPopupEliminar();
			if (txt_etiqueta2 != null) {
				registro.click_boton_ContinuarEliminar();
				registro.vp_etiqueta_equipos();
				registro.selecciona_equipo();
				registro.vp_etiqueta_popupeliminar();
				registro.click_boton_popupEliminar();
				registro.vp_mensaje_eliminado();
				registro.click_boton_ContinuarELiminado();
			}

			OTP otp = new OTP();
			otp.genera_valida_Otp();
			otp.vp_etiqueta_msg();

			registro.vp_etiqueta_alias();
			registro.ingresar_alias();
			registro.click_boton_ContinuarAlias();
			registro.vp_mensaje_registro();

			registro.selecciona_metodo();
			registro.vp_etiqueta_Touch();
			registro.click_boton_Activar();
			registro.vp_mensaje_metodo();
			registro.click_boton_ContinuarMetodo();
			registro.vp_mensaje_error();
		}
	}

	public void registroNoGraciasTest()
	{
		RegistroEquipo registro =  new RegistroEquipo();
		registro.click_boton_NoGracias();
	}

}