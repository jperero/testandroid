/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   CLAVE VIRTUAL - OPCIONES:
                            - DESBLOQUEO M�TODO DE INGRESO
                            - DESBLOQUEO SOFTOKEN
                            - ELIMINACI�N DE SOFTOKEN
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   AGO 13/2021
    Fecha Modificaci�n  :   N/A
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Validaci�n Clave Virtual No Bloqueada
        2. Validaci�n Clave Virtual Desbloqueo
        3. Validaci�n Base Conocimiento (Preguntas / Respuestas) - Fallido (3 intentos)
 */


package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.Iterator;
import java.util.List;

public class ClaveVirtualOpciones {
    public ClaveVirtualOpciones() {
        if (Util.plataforma.equals("iOS"))
            PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
        else
            PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    AndroidElement edita = null;

    List<String> datosBaseConocimiento = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_preguntas_transaccional.txt");
    Iterator<String> itrBaseConocimiento = datosBaseConocimiento.iterator();
    String[] campos = null;
    int linea = 0;

    public String textoFlujo = "";
    String respuestaBaseConocimiento = "";
    String mensajePopUp = "�Deseas eliminar por completo este Dipositivo?\n\nAl realizar esta acci�n eliminar�s definitivamente tu Clave Virtual en este equipo.\n\n";

    static String dato;

    public void setTipoDispositivoSeguridad(String tipoDispositivoSeguridad) {
        dato = tipoDispositivoSeguridad;
    }

    public String getTipoDispositivoSeguridad() {
        return texto.get(12).getText().substring(17);
    }

    public void setBaseConocimiento(String pregunta) {
        while (itrBaseConocimiento.hasNext()) {
            campos = itrBaseConocimiento.next().split("\t");
            linea = linea + 1; //0Sec 1Pregunta_transaccional 2Respuesta_transaccional
            if (linea == 1)
                continue;
            if (pregunta.equals(campos[1])) {
                respuestaBaseConocimiento = campos[2];
                break;
            }
        }
    }

    public String getBaseConocimiento() {
        return respuestaBaseConocimiento;
    }

    public String lb_texto_opcion(int idx) {
        String esperado = "";
        switch (idx) {
            case 9:
                esperado = "Desbloquear Ingreso";
                break;
            case 10:
                esperado = "Desbloqueo de tu Clave Virtual";
                break;
            case 11:
                esperado = "Eliminar";
                break;
        }
        return esperado;
    }

    public void vp_etiqueta_cabecera(String esperado) {
        String actual = texto.get(0).getText();
        Util.assert_igual(textoFlujo, "Verificaci�n Cabecera", actual, esperado, true, "N");
    }

    public void vp_buscar_opciondesbloqueo() {
        if (!texto.get(12).getText().contains("Desbloqueo de tu ")) {
            Util.assert_igual(textoFlujo, "Validaci�n - No posee dispositivo de seguridad", "NO POSEE DISPOSITIVO SEGURIDAD", "NO POSEE DISPOSITIVO SEGURIDAD", true, "N");
        }
    }

    public void click_boton_Opcion(int idx) {
        String tipoDispositivoSeguridad = "";
        String actual = texto.get(idx).getText();
        if (idx == 12) {
            tipoDispositivoSeguridad = getTipoDispositivoSeguridad();
            setTipoDispositivoSeguridad(tipoDispositivoSeguridad);
        }
        if (idx != 12) {
            Util.assert_igual(textoFlujo, "Presiona bot�n " + actual, actual, lb_texto_opcion(idx), true, "N");
        } else {
            Util.assert_igual(textoFlujo, "Presiona bot�n " + actual, actual, "Desbloqueo de tu " + tipoDispositivoSeguridad, true, "N");
        }
        texto.get(idx).click();// 9: DESBLOQUEAR INGRESO
        //10: DESBLOQUEO DE TU CLAVE VIRTUAL
        //11: ELIMINAR
        //12: DESBLOQUEO DE TU: CLAVE VIRTUAL | CLAVE24 | CLAVE DIGITAL (DESDE AJUSTES)
    }

    public void mensajeValidacion() {
        String esperado = "Tu " + dato + " no se encuentra bloqueada";
        try {
            Util.assert_igual(textoFlujo, "Mensaje Validaci�n", Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
    }

    public void mensajeValidacion(String esperado) {
        try {
            switch (esperado) {
                case "�Desbloqueo Exitoso!":
                    Thread.sleep(2000);
                    Util.assert_contiene(textoFlujo, "Mensaje Validaci�n" + esperado, Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + esperado + "')]")).getText(), esperado, true, "N");
                    Thread.sleep(6000);
                    break;
                case "Tu Clave Virtual se ha desbloqueado":
                    Thread.sleep(5000);
                    Util.assert_igual(textoFlujo, "Mensaje Validaci�n", Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
                    Thread.sleep(7000);
                    break;
                case "Dispositivo Seguridad":
                    esperado = "Tu " + dato + " se ha desbloqueado";
                    Thread.sleep(5000);
                    Util.assert_igual(textoFlujo, "Mensaje Validaci�n", Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
                    Thread.sleep(7000);
                    break;
                default:
                    Util.assert_igual(textoFlujo, "Mensaje Validaci�n", Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
                    Thread.sleep(5000);
                    break;
            }
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
    }

    public void mensaje_popup_Error(int idx) {
        if (idx < 2) {
            Util.assert_igual(textoFlujo, "Repuesta Err�nea", texto.get(0).getText(), "Has ingresado una respuesta err�nea. Por favor, intenta nuevamente", true, "N");
            texto.get(1).click();
        } else {
            Util.assert_igual(textoFlujo, "Supera N�mero de Repuesta Err�nea", texto.get(0).getText(), "Superaste los intentos en responder la pregunta.", true, "N");
            texto.get(2).click();
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void mensaje_popup_Eliminar(int idx) {
        String actual = texto.get(0).getText();
        Util.assert_igual(textoFlujo, "Mensaje Confirmaci�n Eliminaci�n", actual, mensajePopUp, false, "N");
        if (idx == 1) {
            Util.assert_igual(textoFlujo, "Confirmaci�n Eliminaci�n", texto.get(idx).getText(), "Cancelar", true, "N");
        } else {
            Util.assert_igual(textoFlujo, "Confirmaci�n Eliminaci�n", texto.get(idx).getText(), "Eliminar", true, "N");
        }
        texto.get(idx).click();
    }

    public void mensaje_popup_Aviso() {
        String actual = texto.get(0).getText();
        Util.assert_igual(textoFlujo, "Aviso Confirmaci�n Eliminaci�n", actual, "Has eliminado correctamente tu Clave Virtual. Por favor, vuelve a iniciar sesi�n", true, "N");
        texto.get(1).click();
    }

    public void pregunta_transaccional() {
        String actual = texto.get(2).getText();
        setBaseConocimiento(actual.replaceAll("[�?\"]", ""));
        Reporte.agregarPaso(textoFlujo, "Valida Base Conocimiento - Pregunta", actual, "", true, "N");
        edita.click();
        edita.clear();
        edita.sendKeys(getBaseConocimiento());
        Reporte.agregarPaso(textoFlujo, "Valida Base Conocimiento - Respuesta", getBaseConocimiento(), "", true, "N");
        texto.get(5).click();
    }

    public void ingreso_respuesta(String respuesta) {
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        edita.click();
        edita.clear();
        edita.sendKeys(respuesta);
        texto.get(5).click(); //5 Bot�n Aceptar
    }

    public void click_boton_Cambio_Pregunta() {
        String actual = boton.get(0).getText();
        Util.assert_igual(textoFlujo, "Presiona bot�n Cambiar de Pregunta", actual, "CAMBIAR DE PREGUNTA", true, "N");
        boton.get(0).click();//0: CAMBIAR DE PREGUNTA
    }
}
