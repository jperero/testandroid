package Pom.Android;

import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.pagefactory.iOSXCUITFindBy;
import org.openqa.selenium.support.PageFactory;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class ScreenPreliminar
{
    public ScreenPreliminar() {
        if (Util.plataforma.equals("iOS"))
            PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
        else
            PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(id = "android:id/content")
    MobileElement pantallaPreliminar=null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> opcPreliminar=null;
    // Saludo --> 0 | Clave Virtual --> 1 | QuickView --> 2 | M�s Opciones --> 3 | Version --> 4

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @iOSXCUITFindBy(accessibility = "richGreetingMessage")
    MobileElement saludo = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    @iOSXCUITFindBy(accessibility = "btnLogin")
    MobileElement boton = null;

    String txt_clavevirtual="Clave Virtual";

    public void vp_etiqueta_saludo()
    {
        String txt_saludoGeneral = "Bienvenido";
        String txt_saludoPersonalizado1 = "Buen";
        String txt_saludoPersonalizado2 = "�Hola";
        String actual = null;

        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (Util.plataforma.equals("iOS"))
            actual = saludo.getText();
        else
            actual = opcPreliminar.get(0).getText();

        Util.assert_contiene("PANTALLA PRELIMINAR", "Verificaci�n de Saludo", actual, txt_saludoGeneral, txt_saludoPersonalizado1, txt_saludoPersonalizado2,true , "N");
    }

    public void vp_etiqueta_version() {
        String txt_version = Util.prop.getProperty("version_app"); //Obtiene la versi�n de la app del archivo de Configuraciones
        String actual = opcPreliminar.get(5).getText();
        Util.assert_igual("PANTALLA PRELIMINAR", "Verificaci�n de versi�n", actual, txt_version, false, "N");
    }

    public void click_boton_ClaveVirtual()
    {
        // PANTALLA PRELIMINAR - VERIFICA CLAVE VIRTUAL
        String actual = opcPreliminar.get(1).getText();
        Util.assert_igual("PANTALLA PRELIMINAR", "Verificaci�n de Opci�n de Clave Virtual", actual, txt_clavevirtual, true , "N");
        opcPreliminar.get(1).click();
        PreClaveVirtual preClaveVirtual = new PreClaveVirtual();
        preClaveVirtual.verificarActivacionCV();
    }

    public void opcionesPreliminar() {
        this.vp_etiqueta_saludo();
        this.vp_etiqueta_version();
        this.click_boton_ClaveVirtual();
    }

    public void click_boton_ingresar()
    {
        //String actual = opcPreliminar.get(4).getText();
        String actual = boton.getText();
        Util.assert_igual("PANTALLA PRELIMINAR", "Presiona opci�n Ingresar", actual ,"Ingresar", false, "N");
        boton.click();
    }
    public void click_boton_mas_opciones(){
        String actual = opcPreliminar.get(3).getText();
        //Util.assert_igual("PANTALLA PRELIMINAR", "Opci�n M�s Opciones", actual ,"M�s Opciones", true);
        opcPreliminar.get(3).click();
    }

}
