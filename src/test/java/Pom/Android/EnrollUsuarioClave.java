package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class EnrollUsuarioClave {
    public EnrollUsuarioClave() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> editor = null;

    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> imagen = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.Button[@text='CONTINUAR']")
    AndroidElement boton_Continuar = null;

    @WithTimeout(time = 15, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Ya cuentas con usuario y contrase�a para acceder a 24m�vil'" +
            "or @text='Lo sentimos int�ntalo m�s tarde'" +
            "or @text='Lo sentimos no pudimos establecer conexi�n, int�ntalo m�s tarde.'" +
            "or @text='El usuario no se encuentra disponible. Int�ntalo nuevamente.'" +
            "or @text='El n�mero de identificaci�n ingresado es inv�lido'" +
            "or @text='C�digo dactilar ingresado es inv�lido']")
    AndroidElement mensajeErrorRojo = null;

    List<String> personaEnroll = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_enroll_-_datos.txt");
    String[] datoEnroll;
    List<String> mensajeError = Util.getCamposDataPool(System.getProperty("user.dir") + "/archivos/dp_mensaje_-_error.txt");
    String[] datoMensajeError;
    ScreenPreliminar preliminar = new ScreenPreliminar();
    PreMasOpciones masOpciones = new PreMasOpciones();

    //METODOS GET / SET - SELECCIONAR USUARIO A REALIZAR ENROLL
    public void setSeleccionPersonaEnroll(int seleccionPersonaEnroll) {
        datoEnroll = personaEnroll.get(seleccionPersonaEnroll).split("\t");
    }

    public String[] getSeleccionPersonaEnroll() {
        return datoEnroll;
    }

    //METODOS GET / SET - SELECCIONAR MENSAJE DE ERROR
    public void setSeleccionMensajeError(int seleccionMensajeError) {
        datoMensajeError = mensajeError.get(seleccionMensajeError).split("\t");
    }

    public String[] getSeleccionMensajeError() {
        return datoMensajeError;
    }

    public void acceso_preliminar_mas_opciones() {
        preliminar.vp_etiqueta_saludo();
        preliminar.click_boton_mas_opciones();
        masOpciones.ingresarRegistrate();
    }

    public void acceso_preliminar_login() {
        preliminar.vp_etiqueta_saludo();
        preliminar.click_boton_ingresar();
    }

    public void vp_etiqueta_no_tienes_usuario() {
        String actual = texto.get(4).getText();
        Util.assert_igual("CREAR MI USUARIO (ENROLL)", "Verificaci�n Etiqueta - Registro", actual, "�No tienes usuario?", false, "N");
    }

    public void click_no_tienes_usuario() {
        String actual = texto.get(5).getText();
        Util.assert_igual("CREAR MI USUARIO (ENROLL)", "Verificaci�n Etiqueta - Registro", actual, "Reg�strate", true, "N");
        texto.get(5).click();
    }

    public void vp_etiqueta_creacion_usuario() {
        String actual = texto.get(0).getText();
        Util.assert_igual("CREAR MI USUARIO (ENROLL)", "Verificaci�n Etiqueta - Registro", actual, "REGISTRO", false, "N");
    }

    public void click_check() {
        Reporte.agregarPaso("CREAR MI USUARIO (ENROLL)", "Presiona check de T�rminos y Condiciones", "", "REGISTRO", true, "N");
        imagen.get(2).click();
    }
    public void click_boton_continuar() {
/*        int idx = Math.toIntExact(boton.stream().count());
        idx = (idx == 1) ? 0 : (idx - 1);
        String actual = boton.get(idx).getText();
        Util.assert_igual("CREAR MI USUARIO (ENROLL)", "Presiona bot�n Continuar", actual, "CONTINUAR", true, "N");
        boton.get(idx).click();//0: CANCELAR  1: CONTINUAR*/

        Util.assert_igual("CREAR MI USUARIO (ENROLL)", "Presiona bot�n Continuar", boton_Continuar.getText(), "CONTINUAR", true, "N");
        boton_Continuar.click();
    }


    public void vp_etiqueta_ingresar_identificacion() {
        String actual = texto.get(2).getText();
        Util.assert_igual("CREAR MI USUARIO (ENROLL)", "Verificaci�n Etiqueta - Ingresa tu Identificaci�n", actual, "Ingresa tu identificaci�n", false, "N");
    }

    public void click_boton_cedula() {
        String actual = texto.get(4).getText();
        Util.assert_igual("CREAR MI USUARIO (ENROLL)", "Presiona bot�n C�dula", actual, "C�dula", true, "N");
        texto.get(4).click();//4: CEDULA  5: PASAPORTE
    }

    public void click_boton_pasaporte() {
        String actual = texto.get(5).getText();
        Util.assert_igual("CREAR MI USUARIO (ENROLL)", "Presiona bot�n C�dula", actual, "Pasaporte", true, "N");
        texto.get(5).click();//4: CEDULA  5: PASAPORTE
    }

    public void vp_ingreso_tipo_documento_identidad() {
        String actual = editor.get(0).getText();
        Util.assert_igual("CREAR MI USUARIO (ENROLL)", "Verificaci�n Etiqueta - Ingresa tu n�mero de identificaci�n", actual, "Ingresa tu n�mero de identificaci�n", false, "N");
    }

    public void ingreso_dato_documento_identidad(String datoDocIndentidad) {
        //editor.get(0).setValue(getSeleccionPersonaEnroll()[1]);
        //Reporte.agregarPaso("CREAR MI USUARIO (ENROLL)", "Dato de Documento de Identidad", getSeleccionPersonaEnroll()[1], "", true, "N");
        editor.get(0).setValue(datoDocIndentidad);
        Reporte.agregarPaso("CREAR MI USUARIO (ENROLL)", "Dato de Documento de Identidad", datoDocIndentidad, "", true, "N");

    }

/*    public void ingreso_dato_documento_identidad() {
        editor.get(0).setValue(getSeleccionPersonaEnroll()[1]);
        Reporte.agregarPaso("CREAR MI USUARIO (ENROLL)", "Dato de Documento de Identidad", getSeleccionPersonaEnroll()[1], "", true, "N");
    }*/

    public void vp_ingreso_codigo_dactilar() {
        String actual = editor.get(1).getText();
        Util.assert_igual("CREAR MI USUARIO (ENROLL)", "Verificaci�n Etiqueta - Ingresa tu c�digo dactilar", actual, "Ingresa tu c�digo dactilar", false, "N");
    }

    /*public void ingreso_codigo_dactilar() {
        editor.get(1).setValue(getSeleccionPersonaEnroll()[2]);
        Reporte.agregarPaso("CREAR MI USUARIO (ENROLL)", "Dato de C�digo Dactilar", getSeleccionPersonaEnroll()[2], "", true, "N");
    }*/

    public void ingreso_codigo_dactilar(String codigoDactilar) {
        //editor.get(1).setValue(getSeleccionPersonaEnroll()[2]);
        //Reporte.agregarPaso("CREAR MI USUARIO (ENROLL)", "Dato de C�digo Dactilar", getSeleccionPersonaEnroll()[2], "", true, "N");
        editor.get(1).setValue(codigoDactilar);
        Reporte.agregarPaso("CREAR MI USUARIO (ENROLL)", "Dato de C�digo Dactilar", codigoDactilar, "", true, "N");
    }

    public void vp_etiqueta_crea_usuario() {
        String actual = texto.get(2).getText();
        Util.assert_igual("CREAR MI USUARIO (ENROLL)", "Verificaci�n Etiqueta - Crea tu nuevo usuario", actual, "Crea tu nuevo usuario", false, "N");
    }

    public void vp_ingreso_dato_usuario() {
        String actual = editor.get(0).getText();
        Util.assert_igual("CREAR MI USUARIO (ENROLL)", "Verificaci�n Etiqueta - Nombre de usuario", actual, "Nombre de usuario", false, "N");
    }

    public void ingreso_dato_usuario(String Usuario) {
        //editor.get(0).setValue(getSeleccionPersonaEnroll()[5]);
        //Reporte.agregarPaso("CREAR MI USUARIO (ENROLL)", "Dato de Usuario", getSeleccionPersonaEnroll()[5], "", true, "N");
        editor.get(0).setValue(Usuario);
        Reporte.agregarPaso("CREAR MI USUARIO (ENROLL)", "Dato de Usuario", Usuario, "", true, "N");
    }

    public void vp_ingreso_dato_contrasenia() {
        String actual = editor.get(1).getText();
        Util.assert_igual("CREAR MI USUARIO (ENROLL)", "Verificaci�n Etiqueta - Contrase�a", actual, "Contrase�a", false, "N");
    }

    public void ingreso_dato_contrasenia(String Contrasenia) {
        //editor.get(1).setValue(getSeleccionPersonaEnroll()[6]);
        //Reporte.agregarPaso("CREAR MI USUARIO (ENROLL)", "Dato de Contrase�a", getSeleccionPersonaEnroll()[6], "", true, "N");
        editor.get(1).setValue(Contrasenia);
        Reporte.agregarPaso("CREAR MI USUARIO (ENROLL)", "Dato de Contrase�a", Contrasenia, "", true, "N");

    }

    public void vp_ingreso_dato_repite_contrasenia() {
        String actual = editor.get(2).getText();
        Util.assert_igual("CREAR MI USUARIO (ENROLL)", "Verificaci�n Etiqueta - Repite tu contrase�a", actual, "Repite tu contrase�a", false, "N");
    }

    public void ingreso_dato_repite_contrasenia(String Contrasenia) {
        //editor.get(2).setValue(getSeleccionPersonaEnroll()[6]);
        //Reporte.agregarPaso("CREAR MI USUARIO (ENROLL)", "Dato de Re-Contrase�a", getSeleccionPersonaEnroll()[6], "", true, "N");
        editor.get(2).setValue(Contrasenia);
        Reporte.agregarPaso("CREAR MI USUARIO (ENROLL)", "Dato de Re-Contrase�a", Contrasenia, "", true, "N");
    }

    public void click_boton_ingresar() {
        String actual = boton.get(0).getText();
        Util.assert_igual("CREAR MI USUARIO (ENROLL)", "Presiona bot�n Ingresar", actual, "INGRESAR", true, "N");
        boton.get(0).click();//0: INGRESAR
    }

    public void mensaje_exito(String Usuario) {
        //String actual = texto.get(1).getText();
        //Util.assert_igual("CREAR MI USUARIO (ENROLL)", "Verificaci�n Etiqueta - �Perfecto! ya puedes usar tu usuario " + getSeleccionPersonaEnroll()[5], actual, "�Perfecto! ya puedes usar tu usuario " + getSeleccionPersonaEnroll()[5], true, "N");
        String actual = texto.get(1).getText();
        Util.assert_igual("CREAR MI USUARIO (ENROLL)", "Verificaci�n Etiqueta - �Perfecto! ya puedes usar tu usuario " + Usuario, actual, "�Perfecto! ya puedes usar tu usuario " + Usuario, true, "N");
    }

    public void mensaje_error() {
        String actual = mensajeErrorRojo.findElement(By.xpath("//*[@text]")).getText();
        Util.assert_igual("CREAR MI USUARIO (ENROLL)", "Mensaje Error: " + getSeleccionMensajeError()[1], actual, getSeleccionMensajeError()[1], true, "N");
    }
}
