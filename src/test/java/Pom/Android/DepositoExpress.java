package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class DepositoExpress {
    public DepositoExpress(){PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);}

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='�Dep�sito Express ha sido activado!']")
    static AndroidElement mensaje_exito = null;

    @WithTimeout(time = 15, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='Lo sentimos, servicio no disponible por el momento. Int�ntalo mas tarde']")
    static AndroidElement mensaje_error = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> imagen = null;

    public void vp_etiqueta_activacion(){
        String actual = texto.get(0).getText();
        Util.assert_igual("ACTIVAR DEPOSITO EXPRESS", "Verificaci�n de etiqueta de Activaci�n", actual,"ACTIVAR SERVICIO", true, "N");
    }
    public void click_boton_Activar(){
        String actual = boton.get(0).getText();
        Util.assert_igual("ACTIVAR DEPOSITO EXPRESS", "Presiona bot�n Activar", actual, "ACTIVAR", true, "N");
        boton.get(0).click();//0: ACTIVAR  1: NO GRACIAS
    }
    public void click_boton_NoGracias(){
        String actual = boton.get(1).getText();
        Util.assert_igual("ACTIVAR DEPOSITO EXPRESS", "Presiona bot�n No Gracias", actual, "NO GRACIAS", true, "N");
        boton.get(1).click();//0: ACTIVAR  1: NO GRACIAS
    }
    public void mensaje_exitoso(){
        try {
            Util.assert_igual("ACTIVAR DEPOSITO EXPRESS", "Mensaje Activaci�n - Exitosa", mensaje_exito.findElement(By.xpath("//*[@text]")).getText(), "�Dep�sito Express ha sido activado!", true, "N");
            Thread.sleep(5000);
        }catch (Exception e){
            System.out.println("No se mostr� mensaje: "+ mensaje_exito.findElement(By.xpath("//*[@text]")).getText() );
        }
    }
    public void mensaje_error(){
        Util.assert_igual("ACTIVAR DEPOSITO EXPRESS", "Mensaje Error - Lista Negra / Segmento y Subsegmento", mensaje_error.findElement(By.xpath("//*[@text]")).getText(), "Lo sentimos, servicio no disponible por el momento. Int�ntalo mas tarde", true, "N");
    }
    public void vp_etiqueta_deposito_express(){
        String actual = texto.get(0).getText();
        Util.assert_igual("ACTIVAR DEPOSITO EXPRESS", "Verificaci�n de etiqueta de Dep�sito Express (Consulta)", actual,"DEP�SITO EXPRESS", true, "N");
    }
    public void click_check(){
        Reporte.agregarPaso("ACTIVAR DEPOSITO EXPRESS", "Presiona check de T�rminos y Condiciones", "", "ACTIVAR", true, "N");
        imagen.get(1).click();
    }
    public void vp_mensaje_registro_equipo(){
        String actual = texto.get(1).getText();
        Util.assert_igual("ACTIVAR DEPOSITO EXPRESS", "Validaci�n Registro Equipo", actual,"Para transaccionar debes registrar tu dispositivo m�vil", true, "N");
    }
    public void vp_mensaje_dispositivo_seguridad(){
        String actual = texto.get(1).getText();
        Util.assert_igual("ACTIVAR DEPOSITO EXPRESS", "Validaci�n Dispositivo Seguridad", actual,"Para realizar esta transacci�n debes activar tu Clave Virtual", true, "N");
    }
    public void vp_mensaje_sin_producto_cuentas(){
        String actual = texto.get(1).getText();
        Util.assert_igual("ACTIVAR DEPOSITO EXPRESS", "Validaci�n Producto Cuentas", actual,"No posees cuenta para realizar esta transacci�n", true, "N");
    }
    public void vp_mensaje_sin_medios_envio(){
        String actual = texto.get(2).getText();
        Util.assert_igual("ACTIVAR DEPOSITO EXPRESS", "Validaci�n Medios Envio", actual,"No tienes registrado ning�n n�mero celular o correo electr�nico", true, "N");
    }
    public void click_boton_Registrar() {
        String actual = boton.get(0).getText();
        Util.assert_igual("ACTIVAR DEPOSITO EXPRESS", "Presiona bot�n Registrar", actual, "REGISTRAR", true, "N");
        boton.get(0).click();//0: REGISTRAR
    }
    public void click_boton_Activar_Clave_Virtual() {
        String actual = boton.get(0).getText();
        Util.assert_igual("ACTIVAR DEPOSITO EXPRESS", "Presiona bot�n Registrar", actual, "ACTIVAR CLAVE VIRTUAL", true, "N");
        //boton.get(0).click();//0: ACTIVAR CLAVE VIRTUAL
    }
    public void click_boton_Cancelar() {
        String actual = texto.get(1).getText();
        Util.assert_igual("ACTIVAR DEPOSITO EXPRESS", "Presiona bot�n Cancelar", actual, "CANCELAR", true, "N");
        texto.get(1).click();
    }
    public void return_posicion_consolidada(){
        String actual = texto.get(0).getText();
        Util.assert_igual("ACTIVAR DEPOSITO EXPRESS", "Verificaci�n de etiqueta de Mis Productos", actual,"Mis productos", true, "N");
    }
}
