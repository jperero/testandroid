/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   QUICKPAY - RETIRO SIN TARJETA (PROPIO | TERCEROS)
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   ENE 25/2021
    Fecha Modificaci�n  :   FEB 03/2021 (REFACTORING - CARGA DE DATOS TERCEROS)
                            JUN 21/2021 (REFACTORING - CAMBIO EN METODOLOGIA DE QA)
                            JUL 09/2021 (REFACTORING - QUICKPAY PROPIO)
                            JUL 13/2021 (REFACTORING - QUICKPAY PROPIO VALIDACION MONTO DATAPOOL)
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Registro Dispositivo Activo
        2. Dispositivo Seguridad Activo
        3. Producto Cuentas Activo
        4. Servicio de QuickPay Activo
        5. Validaci�n de Monto (PROPIO | TERCEROS):
            5.1. PROPIAS:
                 5.1.1. Valor m�ltiplo de $10.00
                 5.1.2. Valor monto m�nimo de $10.00
                 5.1.3. Valor monto m�ximo de $1000.00
                 5.1.4. Valor excede saldo disponible
            5.2. TERCEROS:
                 5.2.1. Valor monto m�nimo de $1.00
                 5.2.2. Valor monto m�ximo de $1000.00
                 5.2.3. Valor excede saldo disponible
        6. Validaci�n de error de d�bito (Disminuir el saldo disponible durante la confirmaci�n)

        Subflujos:
        7. Matriculaci�n de Beneficiario (TERCEROS)
        8. Buscar d�nde retirar
 */
package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class RetiroSinTarjetaQP {
    public RetiroSinTarjetaQP() {
        if (Util.plataforma.equals("iOS"))
            PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
        else
            PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    private List<AndroidElement> texto = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.Button[@text='CONTINUAR']")
    AndroidElement boton_Continuar = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.TextView[@text='CONFIRMAR']")
    AndroidElement boton_Confirmar = null;

<<<<<<< HEAD
=======

>>>>>>> 79cf8155e547aa5f206f1e186fb6dedcb03f3e6f
    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    static List<AndroidElement> datos = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> botonImg = null;

    @WithTimeout(time = 10, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.TextView[contains(@text,'iTransacci�n exitosa! N�mero de referencia:')]")
    AndroidElement mensaje_exito_verde = null;

    int saldoDisponible = 0;
    TecladoNumerico tecladoDatos = new TecladoNumerico();

    public void vp_etiqueta_cabecera() {
        String actual = texto.get(0).getText();
        Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Verificaci�n Cabecera", actual, "RETIRO SIN TARJETA", false, "N");
    }

    public void mensajeValidacion(String validacion, String esperado) {
        try {
            Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Validaci�n " + validacion, Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
    }

    public void click_boton_Cancelar() {
        String actual = texto.get(1).getText();
        Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Presiona bot�n Cancelar", actual, "Cancelar", true, "N");
        texto.get(1).click();//1: Cancelar 2: Activar
    }

    public void click_boton_Retirar() {
        String actual = texto.get(1).getText();
        Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Presiona bot�n Retirar", actual, "RETIRAR", true, "N");
        texto.get(1).click();
    }

    public void vp_etiqueta_cabecera_retiro_de_efectivo() {
        String actual = texto.get(0).getText();
        Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Verificaci�n Cabecera", actual, "RETIRO DE EFECTIVO", false, "N");
    }

    public void click_boton_Yo_hare_el_retiro() {
        String actual = texto.get(2).getText();
        Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Presiona bot�n Yo har� el retiro", actual, "Yo har� el retiro", true, "N");
        texto.get(2).click();
    }

    public void click_boton_Alguien_mas_hara_el_retiro() {
        String actual = texto.get(3).getText();
        Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Presiona bot�n Yo har� el retiro", actual, "Alguien m�s har� el retiro", true, "N");
        texto.get(3).click();
    }

    public void vp_etiqueta_cabecera_beneficiario() {
        String actual = texto.get(0).getText();
        Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Verificaci�n Cabecera", actual, "BENEFICIARIOS", false, "N");
    }

    public void seleccionar_beneficiario_favorito() {
        String esperado = "Beneficiarios Favoritos(";
        Util.assert_contiene("QUICKPAY - RETIRO SIN TARJETA", "Selecci�n Beneficiario Favorito", Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + esperado + "')]")).getText(), esperado, true, "N");
        texto.get(2).click();
    }

    public void seleccionar_beneficiario_no_favorito() {
        String esperado = "Todos los Beneficiarios(";
        Util.assert_contiene("QUICKPAY - RETIRO SIN TARJETA", "Selecci�n Beneficiario No Favorito", Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + esperado + "')]")).getText(), esperado, true, "N");
        texto.get(5).click();
    }

    public void vp_etiqueta_cabecera_monto() {
        String actual = texto.get(0).getText();
        Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Verificaci�n Cabecera", actual, "MONTO", false, "N");
    }

    public void seleccionar_monto() {
        String actual = texto.get(9).getText();
        Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Seleccionar Monto", actual, "10.00", false, "N");
        texto.get(9).click();
    }

    public void click_boton_Continuar() {
        Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Presiona bot�n Continuar", boton_Continuar.getText(), "CONTINUAR", true, "N");
        boton_Continuar.click();
    }

    public void click_boton_Confirmar() {
        Util.driver.hideKeyboard();
        String actual = boton_Confirmar.getText();
        Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Presiona bot�n Confirmar", actual, "CONFIRMAR", true, "N");
        boton_Confirmar.click();
    }

    public void vp_etiqueta_cabecera_confirmacion() {
        String actual = texto.get(0).getText();
        Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Verificaci�n Cabecera", actual, "CONFIRMACI�N", false, "N");
    }

    public void click_boton_Retirar_Monto() {
        String actual = boton.get(1).getText();
        Util.assert_contiene("QUICKPAY - RETIRO SIN TARJETA", "Presiona bot�n Retirar", actual, "RETIRAR", true, "N");
        boton.get(1).click();
    }

    public void vp_etiqueta_cabecera_clave() {
        String actual = texto.get(0).getText();
        Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Verificaci�n Cabecera", actual, "CLAVE DE RETIRO DE EFECTIVO", false, "N");
    }

    public void click_boton_Aceptar() {
        String actual = boton.get(0).getText();
        Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Presiona bot�n Aceptar", actual, "ACEPTAR", true, "N");
        boton.get(0).click();
    }

    public void ingreso_monto(String monto) {
        TecladoNumerico tecladoNumerico = new TecladoNumerico();
        tecladoNumerico.IngresarValor(2, monto, true);
    }

    public void ingreso_monto(String monto, String escenario) {
        if (escenario.equals("Valor excede al saldo disponible")) {
            saldoDisponible = (int) ((Double.parseDouble(texto.get(4).getText().substring(0, (texto.get(4).getText().length() - 4)).replaceAll(",", "")) + Double.parseDouble(monto)) * 100);
            monto = (String.valueOf(saldoDisponible).replaceAll(",", ""));
        }
        TecladoNumerico tecladoNumerico = new TecladoNumerico();
        tecladoNumerico.IngresarValor(2, monto, true);
    }

    public void borra_monto(String monto) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        for (int i = 0; i < monto.length(); i++) {
            botonImg.get(3).click();
        }
    }

    public void borra_monto(String monto, String escenario) {
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (escenario.equals("Valor excede al saldo disponible")) {
            saldoDisponible = (int) ((Double.parseDouble(texto.get(4).getText().substring(0, (texto.get(4).getText().length() - 4)).replaceAll(",", "")) + Double.parseDouble(monto)) * 100);
            monto = (String.valueOf(saldoDisponible).replaceAll(",", ""));
        }
        for (int i = 0; i < monto.length(); i++) {
            botonImg.get(3).click();
        }
    }

    public void click_boton_Matricular_beneficiario() {
        String actual = boton.get(1).getText();
        Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Presiona bot�n Matricular Beneficiario", actual, "MATRICULAR BENEFICIARIO", true, "N");
        boton.get(1).click();
    }

    public void vp_etiqueta_cabecera_contacto() {
        String actual = texto.get(0).getText();
        Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Verificaci�n Cabecera", actual, "CONTACTO", false, "N");
    }

    public void ingreso_dato(String cedula, String celular) {
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        datos.get(0).click();
        datos.get(0).clear();
        datos.get(0).sendKeys(cedula);

        datos.get(1).click();
        datos.get(1).clear();
        datos.get(1).sendKeys(celular);
    }

    public void vp_etiqueta_cabecera_detalles() {
        String actual = texto.get(0).getText();
        Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Verificaci�n Cabecera", actual, "DETALLES", false, "N");
    }

    public void selecciona_favorito(){
        String actual = texto.get(11).getText();
        Reporte.agregarPaso("RETIRO SIN TARJETA","Seleccionar Favorito",actual,"Favorito",true, "N");
        botonImg.get(2).click();
    }

    public void selecciona_favorito() {
        String actual = texto.get(11).getText();
        Reporte.agregarPaso("RETIRO SIN TARJETA", "Seleccionar Favorito", actual, "Favorito", true, "N");
        botonImg.get(2).click();
    }

    public void ingreso_alias(String alias) {
        datos.get(0).click();
        datos.get(0).clear();
        datos.get(0).sendKeys(alias);
    }

    public void mensaje_exito() {
        String txt_expected = "iTransacci�n exitosa! N�mero de referencia:";
        Reporte.agregarPaso("RETIRO SIN TARJETA", "Mensaje Resultado", txt_expected, "", false, "N");
        Util.assert_contiene("RETIRO SIN TARJETA", "Mensaje Resultado", mensaje_exito_verde.getText(), txt_expected, true, "N");
    }

    public void mensaje_exito(String esperado){
        try {
            Thread.sleep(5000);
            Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Mensaje Resultado", Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
    }

    public void mensaje_error(String validacion, String esperado) {
        if (!validacion.equals("Monto M�nimo")) {
            Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Validaci�n " + validacion, Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
            if (Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText().equals("Tu monto ingresado debe ser m�ltiplo de 10")) {
                boton.get(0).click();
            }
        } else {
            Util.assert_contiene("QUICKPAY - RETIRO SIN TARJETA", "Validaci�n " + validacion, Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + esperado + "')]")).getText(), esperado, true, "N");

    public void mensaje_exito(String esperado) {
        try {
            Thread.sleep(10000);
            Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Mensaje Resultado", Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
    }

    public void mensaje_error(String validacion, String esperado) {
        try {
            if (!validacion.equals("Monto M�nimo")) {
                Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA", "Validaci�n " + validacion, Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
                if (Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText().equals("Tu monto ingresado debe ser m�ltiplo de 10")) {
                    boton.get(0).click();
                }
            } else {
                Util.assert_contiene("QUICKPAY - RETIRO SIN TARJETA", "Validaci�n " + validacion, Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + esperado + "')]")).getText(), esperado, true, "N");
            }
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
    }
}