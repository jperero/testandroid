package Pom.Android;

import Auxiliar.*;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

public class MatriculaCta {
	public MatriculaCta() {
		PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
	}

	@WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(className = "android.widget.Button")
	List<AndroidElement> boton = null;

	@WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(className = "android.widget.ImageView")
	List<AndroidElement> imagen = null;

	@WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(className = "android.widget.TextView")
	List<AndroidElement> texto = null;

	@WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(className = "android.widget.EditText")
	List<AndroidElement> txtedit = null;

	@WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(className = "android.widget.TextView")
	List<AndroidElement> combo_tipo_cta = null;

	@WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
	MobileElement msg_confirmacion = null;

	@WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView")
	MobileElement identif = null;

	@WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(xpath = "//android.widget.Button[@text='CONTINUAR']")
	static AndroidElement btn_continuar = null;

	String txt_expected = null;

	String tipo_cta = null;
	public String getTipo_cta() {
		return tipo_cta;
	}

	public void setTipo_cta(String tipo_cta) {
		this.tipo_cta = tipo_cta;
	}
	//++++
	private MobileElement tc_banco = null;

	private String getTC_banco_desc() {
		return tc_banco.getText();
	}

	private MobileElement getTC_banco() {
		return this.tc_banco;
	}
	private void setTC_banco(MobileElement bco) {
		this.tc_banco = bco;
	}

	public void vp_etiqueta_numCta(String banco) {
		String actual = null;
		if (banco.equals("Bolivariano")) {
			actual = texto.get(13).getText();
		}
		else {
			actual = texto.get(4).getText();
		}

		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de etiqueta", actual,"NÚMERO DE CUENTA", false , "N");
	}

	public void vp_etiqueta_numCtaBenef(){
		String actual = texto.get(0).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de etiqueta", actual,"Ingresa el Número de Cuenta del Beneficiario", false , "N");
	}

	public void vp_etiqueta_tipoCta() {
		String actual = texto.get(1).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de etiqueta", actual, "Tipo de Cuenta", true , "N");
	}

	public void seleccionaTipoCtaDestino(String banco, String tipoCtaDestino) {
		//******COMBO TIPO DE CUENTA******
		imagen.get(0).click();
		Reporte.agregarPaso("MATRICULACIÓN DE CUENTA", "Verificación de combo", "ABRIR COMBO TIPO CUENTA",  "", true, "N");

		String actual = null;
		int idx = 0;
		if (banco.equals("Bolivariano")) {
			actual = texto.get(13).getText();

			switch (tipoCtaDestino) { //AHORROS //CORRIENTE // Cancelar
				case "AHORROS": idx=14;  tipo_cta = "CUENTA DE "; break;
				case "CORRIENTE": idx=15; tipo_cta = "CUENTA "; break;
			}
		}
		else {
			actual = texto.get(4).getText();

			switch (tipoCtaDestino) { //AHORROS //CORRIENTE // Cancelar
				case "AHORROS": idx=5;  tipo_cta = "CUENTA DE "; break;
				case "CORRIENTE":  idx=6; tipo_cta = "CUENTA "; break;
			}
		}
		//String actual = texto.get(4).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de etiqueta", actual,  "Tipo de Cuenta", false, "N");

		this.setTipo_cta(tipo_cta + tipoCtaDestino);
		//************

		actual = texto.get(idx).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Selecciona Tipo de Cuenta", actual, tipoCtaDestino, true , "N");
		combo_tipo_cta.get(idx).click();
	}

	//+++++
	public void click_boton_Continuar()  {

		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Presiona botón Continuar", btn_continuar.findElement(By.xpath("//*[@text]")).getText(), "CONTINUAR", true, "N");
		btn_continuar.click();

		/*
		String actual = boton.get(1).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Presiona botón Continuar", actual, "CONTINUAR", true , "N");
		boton.get(1).click();
	 	*/
	}

	public void vp_etiqueta_detalle(String banco) {
		int idx = 15;		//Otros Bancos
		if (banco.equals("Bolivariano")) {
			idx = 11;        //Bolivariano
		}
		String actual = texto.get(idx).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de etiqueta", actual, "DETALLES", true, "N" );
	}

	public void vp_etiqueta_titular() {
		String actual = texto.get(2).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de etiqueta", actual, "TITULAR", false , "N");
	}

	public void vp_etiqueta_nombTitular() {
		String actual = texto.get(0).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de etiqueta", actual, "Ingresa el Nombre del Titular de la cuenta", false , "N");
	}

	public void ingresoTitular(String titular) {
		String titular_= null;
		if (titular.length()>49)
			titular_ = titular.substring(0,49);
		else
			titular_ = titular;

		txtedit.get(0).sendKeys(titular_); //idx 0 para BB 1 para otros bancos}
		Util.driver.pressKey(new KeyEvent(AndroidKey.ENTER));
		Reporte.agregarPaso("MATRICULACIÓN DE CUENTA", "Ingresa Titular", titular_, "", false , "N");
	}
	//+++
	public void seleccionaIdentificacion(String tipoid) {
		String actual = texto.get(2).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de etiqueta", actual, "Tipo de Identificación", false , "N");

		imagen.get(1).click(); //combo

		txt_expected = tipoid;
		int idx = 0;
		switch (tipoid) {
			case "C": txt_expected = "Cédula"; idx=1;  break;
			case "P": txt_expected = "Pasaporte"; idx=2;  break;
			case "R": txt_expected = "RUC"; idx=3;  break;
		}

		actual = texto.get(idx).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Selecciona Tipo de Identificación", actual, txt_expected, true , "N");
		texto.get(idx).click();
	}

	public void vp_etiqueta_numIdentif() {
		String actual = texto.get(4).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de etiqueta", actual, "Número de Identificación", false , "N");
	}

	public void vp_dato_identif(String identificacion) {
		String actual = identif.getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de Identificación ingresada", actual, identificacion, false , "N");
	}

	public void vp_etiqueta_detNumCta() {
		String actual = texto.get(2).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de etiqueta", actual, "Número de Cuenta", false , "N");
	}

	public void vp_dato_detnumCta(String numcuenta) {
		String actual = texto.get(3).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de Cuenta ingresada", actual, numcuenta, false , "N");
	}

	public void vp_etiqueta_detTipoCta() {
		String actual = texto.get(4).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de etiqueta", actual, "Tipo de Cuenta", false , "N");
	}

	public void vp_dato_detTipoCta(String tipocta) {
		String actual = texto.get(5).getText().toUpperCase();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de Tipo de Cuenta ingresada", actual, tipocta, false , "N");
	}

	public void vp_etiqueta_detTitular(){
		String actual = texto.get(6).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de etiqueta", actual, "Titular", false , "N");
	}

	public void vp_dato_detTitular(String titular) {
		String actual = texto.get(7).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de Titular ingresado", actual, titular, false, "N");
	}

	public void vp_etiqueta_favorito() {
		String actual = texto.get(9).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de etiqueta", actual, "Agregar como Favorito?", false, "N");
	}

	//+++++
	public void click_img_Favorito(String Fav) {
		Boolean fav = false;
		if (Fav.equals("1"))
			fav = true;

		if (fav)
		{
			imagen.get(1).click(); //1 Favorito 0 Informacion
			Reporte.agregarPaso("MATRICULACIÓN DE CUENTA ", "Ingresa Marca Favorito", "*", "", true, "N");
		}
	}

	public void vp_etiqueta_alias()  {
		String actual = texto.get(11).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de etiqueta", actual, "Alias", false, "N");
	}

	public void ingresoAlias(int idx, String alias) {
		String alias_= null;
		if (alias.length()>14)
			alias_ = alias.substring(0,14);
		else
			alias_ = alias;

		txtedit.get(idx).clear();
		txtedit.get(idx).sendKeys(alias_); //idx 0 para BB 1 para otros bancos}
		//Util.driver.pressKey(new KeyEvent(AndroidKey.BACK));
		Reporte.agregarPaso("MATRICULACIÓN DE CUENTA ", "Ingresa Alias", alias_, "", true, "N");
	}

	public void click_boton_Confirmar() {
		String actual = boton.get(0).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Presiona botón Confirmar", actual, "CONFIRMAR", false, "N");
		boton.get(0).click();
	}

	public void vp_etiqueta_msg() {
		txt_expected = "ˇMatriculación Exitosa!";
		String actual = msg_confirmacion.getText().toUpperCase();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de Mensaje", actual, txt_expected.toUpperCase(), true, "N");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException exception) {
			exception.printStackTrace();
		}
	}
	//++++
	public void buscaBanco(String banco) {
		String actual = texto.get(0).getText();
		Util.assert_igual("MATRICULACIÓN DE CUENTA", "Verificación de etiqueta", actual, "BUSCAR BANCO", true, "N");

		txtedit.get(0).sendKeys(banco);
		Reporte.agregarPaso("MATRICULACIÓN DE CUENTA ", "Busca Banco", banco, "", true, "N");
	}
	//++++
	public void seleccionaBanco(String banco) {
		this.buscaBanco(banco);

		this.setTC_banco(null);
		this.setTC_banco(texto.get(1));

		Reporte.agregarPaso("MATRICULACIÓN DE CUENTA ", "Selecciona Banco", this.getTC_banco_desc(), "", true, "N");
		this.getTC_banco().click();
	}
}
