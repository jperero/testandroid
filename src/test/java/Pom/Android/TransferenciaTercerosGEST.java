package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static io.appium.java_client.touch.WaitOptions.waitOptions;

public class TransferenciaTercerosGEST {
    public TransferenciaTercerosGEST() {
        if (Util.plataforma.equals("iOS"))
            PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
        else
            PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> imagen = null;

    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> editor = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.Button[@text='CONTINUAR']")
    AndroidElement boton_Continuar = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.Button[@text='CANCELAR']")
    AndroidElement boton_Cancelar = null;


    public String caso = "";
    int saldoDisponible = 0;
    TecladoNumerico tecladoDatos = new TecladoNumerico();

    public void gestualTouchScreenScroll() {
        TouchAction acciones = new TouchAction(Util.driver)
                .press(PointOption.point(540, 2147))
                .waitAction(waitOptions(Duration.ofMillis(10000)))
                .moveTo(PointOption.point(540, 752))
                .release()
                .perform();
    }

    public void io_validar_datos(String datoDescripcion) {
        editor.get(0).setValue(datoDescripcion);
        Reporte.agregarPaso(caso, "Ingreso de Dato para B�squeda / Motivo", datoDescripcion, "", true, "N");
    }

    public void vp_etiqueta_cabecera(String esperado) {
        String actual = texto.get(0).getText();
        Util.assert_igual(caso, "Verificaci�n Cabecera", actual, esperado, false, "N");
    }

    public void vp_popover_cabecera(String esperado) {
        try {
            Util.assert_igual(caso, "Verificaci�n Cabecera PopOver", Util.driver.findElement(By.xpath("//android.view.ViewGroup/android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, false, "N");
        } catch (Exception e) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
    }

    public void vp_popover_mensaje(String esperado) {
        try {
            Util.assert_igual(caso, "Verificaci�n Cabecera PopOver", Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
            //Util.assert_igual(caso, "Verificaci�n Cabecera PopOver", Util.driver.findElement(By.xpath("//android.view.ViewGroup/android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
            System.out.println("ELEMENTO ESPERADO: "+esperado);
            System.out.println("ELEMENTO ENCONTRADO: "+Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText());
        } catch (Exception e) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
    }

    public void click_boton_comprobante(String esperado) {
        try {
            Util.assert_igual(caso, "Presiona bot�n " + esperado, Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
            Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).click();
        } catch (Exception e) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
    }

    public void seleccion_tipo_transferencia(String opcion) {
        int idx = 0;
        if (opcion.equals("Terceros"))
            opcion = "Bolivariano";
        switch (opcion) {
            case "Propias":
                idx = 1;//2
                break;
            case "Bolivariano"://Terceros
                idx = 3;//4
                break;
            case "Otros Bancos":
                idx = 5;//6
                break;
        }
        String actual = texto.get(idx).getText();
        Util.assert_igual(caso, "Presiona bot�n Cuentas " + opcion, actual, "Cuentas " + opcion, true, "N");
        texto.get(idx).click();
    }

    public void buscar_ordenante() {
        texto.get(1).click();
    }

    public void seleccion_ordenante(String ordenante) {
        ordenante = "XXXX" + ordenante;
        boolean flag = true;
        while (flag) {
            try {
                if (Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + ordenante + "')]")).isDisplayed()) {
                    Util.assert_contiene(caso, "Selecci�n de ordenante", Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + ordenante + "')]")).getText(), ordenante, true, "N");
                    flag = false;
                }
            } catch (Exception NoSuchElementException) {
                gestualTouchScreenScroll();
            }
        }
        Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + ordenante + "')]")).click();
    }

    /*
    public void seleccion_ordenante(String ordenante) {
        ordenante = "-" + ordenante;
        try {
            Util.assert_contiene(caso, "Selecci�n de ordenante", Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + ordenante + "')]")).getText(), ordenante, true, "N");
            Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + ordenante + "')]")).click();
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + ordenante);
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    */
//    public void swiptToBottom()
//    {
//        Dimension dim = Util.driver.manage().window().getSize();
//        int height = dim.getHeight();
//        int width = dim.getWidth();
//        int x = width/2;
//        int top_y = (int)(0.80);
//        int bottom_y = (int)(0.20);
//        System.out.println("Coordinates :" + x + " "+ top_y + " "+ bottom_y);
//        TouchAction ts = new TouchAction(Util.driver);
//        ts.press(x,top_y).moveTo(x, bottom_y).release().perform();
//    }

/*    public void seleccion_ordenante() {
        while (Util.driver.findElements(By.id("255")).size() == 0) {
            size = Util.driver.manage().window().getSize();
            int starty = (int) (size.height * 0.80);
            int endy = (int) (size.height * 0.20);
            int startx = size.width / 2;
            System.out.println("starty = " + starty + " ,endy = " + endy + " , startx = " + startx);

            Util.driver.swipe(startx, starty, startx, endy, 3000);
            Thread.sleep(2000);
            Util.driver.swipe(startx, endy, startx, starty, 3000);
            Thread.sleep(2000);
        }
    }*/

/*    public void scrollUp() throws Exception {
// Get the size of screen.
        Dimension size = Util.driver.manage().window().getSize();
// Find swipe start and end point from screen�s with and height.
// Find start y point which is at bottom side of screen.
        int starty = (int) (size.height * 0.80);
// Find end y point which is at top side of screen.
        int endy = (int) (size.height * 0.20);
// Find horizontal point where you wants to swipe. It is in middle of
// screen width.
        int startx = size.width / 2;

        // Swipe from Bottom to Top.
        Util.driver.swipe(startx, starty, startx, endy, 3000);
        Thread.sleep(2000);
    }*/

    public void seleccion_beneficiario(String beneficiario) {
        try {
            Util.assert_contiene(caso, "Selecci�n de beneficiario", Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + beneficiario + "')]")).getText(), beneficiario, true, "N");
            System.out.println("BENEFICIARIO: " + Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + beneficiario + "')]")).getText());
            Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + beneficiario + "')]")).click();
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + beneficiario);
        }
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void ingreso_monto(String monto) {
        TecladoNumerico tecladoNumerico = new TecladoNumerico();
        tecladoNumerico.IngresarValor(0, monto, true);
    }

    public void ingreso_monto(String monto, String escenario) {
        if (escenario.equals("Valor excede al saldo disponible")) {
            saldoDisponible = (int) ((Double.parseDouble(texto.get(4).getText().substring(0, (texto.get(4).getText().length() - 4)).replaceAll(",", "")) + Double.parseDouble(monto)) * 100);
            monto = (String.valueOf(saldoDisponible).replaceAll(",", ""));
        }
        TecladoNumerico tecladoNumerico = new TecladoNumerico();
        tecladoNumerico.IngresarValor(2, monto, true);
    }

    public void borra_monto(String monto, String escenario) {
        try {
            Thread.sleep(6000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (escenario.equals("Valor excede al saldo disponible")) {
            saldoDisponible = (int) ((Double.parseDouble(texto.get(4).getText().substring(0, (texto.get(4).getText().length() - 4)).replaceAll(",", "")) + Double.parseDouble(monto)) * 100);
            monto = (String.valueOf(saldoDisponible).replaceAll(",", ""));
        }
        for (int i = 0; i < monto.length(); i++) {
            imagen.get(2).click();
        }
    }

    public void espera_transfiriendo(int Millis) {
        try {
            Thread.sleep(Millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void mensaje_validacion(String validacion, String esperado) {
        try {
            Util.assert_igual(caso, "Validaci�n " + validacion, Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
    }

    public void mensaje_error(String esperado) {
        try {
            if (esperado.equals("Valor excede al saldo disponible")) {
                Util.assert_contiene(caso, "Validaci�n " + esperado, Util.driver.findElement(By.xpath("//android.widget.TextView[contains(@text,'" + esperado + "')]")).getText(), esperado, true, "N");
            } else {
                Thread.sleep(5000);
                Util.assert_igual(caso, "Validaci�n " + esperado, Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
            }
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void click_boton_Transferir() {
        String esperado = "TRANSFERIR";
        try {
            Util.assert_contiene(caso, "Presiona bot�n Transferir", Util.driver.findElement(By.xpath("//android.widget.Button[contains(@text,'" + esperado + "')]")).getText(), esperado, true, "N");
            boton.get(0).click();
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
    }


    public void click_boton_Cambiar() {
        Util.assert_igual(caso, "Presiona bot�n Cambiar", boton.get(1).getText(), "CAMBIAR", true, "N");
        boton.get(1).click();//Bot�n Cambiar
    }

    public void click_boton_Continuar() {
        Util.assert_igual(caso, "Presiona bot�n Continuar", boton_Continuar.getText(), "CONTINUAR", true, "N");
        boton_Continuar.click();
    }

    public void click_boton_Cancelar() {
        Util.assert_igual(caso, "Presiona bot�n Cancelar", boton_Cancelar.getText(), "CANCELAR", true, "N");
        boton_Cancelar.click();
    }


    public void click_boton(String lb_Boton) {
        String esperado = lb_Boton;
        try {
            Util.assert_contiene(caso, "Presiona bot�n " + lb_Boton, Util.driver.findElement(By.xpath("//android.widget.Button[contains(@text,'" + lb_Boton + "')]")).getText(), esperado, true, "N");
            //Util.assert_igual(caso, "Presiona bot�n " + lb_Boton, Util.driver.findElement(By.xpath("//android.widget.Button[contains(@text,'" + lb_Boton + "')]")).getText(), esperado, true, "N");
            Util.driver.findElement(By.xpath("//android.widget.Button[contains(@text,'" + lb_Boton + "')]")).click();
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
    }

/*    public static WebElement findElementWithProductId(productId) {
        return @FindBy (xpath='//a[text()='productId'])
                }*/
}
