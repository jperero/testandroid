package Pom.Android;

import Auxiliar.Util;

public class TestLogin {

    public Boolean registroEquipo = true;
    public Boolean activoClave = false;
    public Boolean flag_ipinternacional = false;

    public void previoLogin(String seclogin)
    {
        ModoDebug modo = new ModoDebug();
        modo.modoDebug();

        ActualizacionApp actualiza = new ActualizacionApp();
        actualiza.loginActApp();

        ScreenPreliminar preliminar = new ScreenPreliminar();
        preliminar.vp_etiqueta_saludo();
        //preliminar.vp_etiqueta_version();
        preliminar.click_boton_ingresar();

        PreLoginMetodo preMetodo = new PreLoginMetodo();
        preMetodo.cargaDatosLogin(seclogin);
        preMetodo.verifica_metodo_acceso();

        TiempoSesion tiempo = new TiempoSesion();
        tiempo.Aceptar();
    }

    public void continuaLogin()
    {
        TestRegistroEquipo registro = new TestRegistroEquipo();
        if (registroEquipo) {
            registro.registroTest();
        } else {
            registro.registroNoGraciasTest();
        }

        CampActivaClaveVirtual activaClaveVirtual = new CampActivaClaveVirtual();
        if (activoClave) {
            activaClaveVirtual.click_boton_Activar();
        } else {
            activaClaveVirtual.ahoraNo();
        }

        MisProductos misProductos = new MisProductos();
        misProductos.vp_etiqueta_msg();
        misProductos.vp_etiqueta_misproductos();
        this.flag_ipinternacional = misProductos.flag_ipinternacional;
    }

    public void loginTest(String seclogin) {
        this.previoLogin(seclogin);

        LoginIngresar ingreso = new LoginIngresar();
        ingreso.loginIngresar();

        if (ingreso.flag_pssw_obligatorio) {
            Util.assert_igual("LOGIN", "Mensaje", "CORRER SCRIPT DE CAMBIO DE CONTRASEŅA - RunLoginCambioContrasena-cambioContrasenaObligatorio", "", true, "N");
        }

        if (ingreso.flag_pssw_preaviso) {
            Util.assert_igual("LOGIN", "Mensaje", "CORRER SCRIPT DE CAMBIO DE CONTRASEŅA - RunLoginCambioContrasena-cambioContrasenaObligatorio", "", true, "N");
        }

        if (ingreso.flag_pssw_bloqueo) {
            Util.assert_igual("LOGIN", "Mensaje", "CORRER SCRIPT DE CAMBIO DE CONTRASEŅA - RunLoginCambioContrasena", "", true, "N");
        }

        this.continuaLogin();

        if (this.flag_ipinternacional) {
            Util.assert_igual("LOGIN", "Mensaje", "CORRER SCRIPT DE IP INTERNACIONAL - RunRegistroIPInternacional", "", true, "N");
        }
    }

    public void irAProblemasIngresarTest() {
        ModoDebug modo = new ModoDebug();
        modo.modoDebug();

        ActualizacionApp actualiza = new ActualizacionApp();
        actualiza.loginActApp();

        ScreenPreliminar preliminar = new ScreenPreliminar();
        preliminar.vp_etiqueta_saludo();
        preliminar.click_boton_ingresar();

        PreLoginMetodo preMetodo = new PreLoginMetodo();
        preMetodo.verifica_metodo_acceso();

        TiempoSesion tiempo = new TiempoSesion();
        tiempo.Aceptar();
    }
}