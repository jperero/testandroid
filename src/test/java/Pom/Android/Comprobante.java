package Pom.Android;

import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Comprobante {
    public Comprobante() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time = 30, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath ="//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView")
    MobileElement masopciones = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath ="hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.widget.TextView")
    MobileElement nueva = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath ="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.view.ViewGroup[3]/android.view.ViewGroup/android.widget.TextView")
    MobileElement compartir = null;

    public void vp_etiqueta_Confirmacion() {
        String actual = texto.get(0).getText();
        Util.assert_igual("COMPROBANTE", "Verificación de etiqueta", actual, "CONFIRMACIÓN", true, "N");
    }

    public void vp_etiqueta_Exitosa() {
        String actual = texto.get(2).getText();
        Util.assert_igual("COMPROBANTE", "Verificación de etiqueta", actual, "ˇTransacción exitosa!", false, "N");
    }

    public void vp_dato_Monto(String monto) {
        String actual = texto.get(3).getText();
        Util.assert_igual("COMPROBANTE", "Verificación de Monto", actual, monto, false, "N");
    }

    public void vp_dato_ctaDestino(String cuenta) {
        String actual = texto.get(5).getText();
        Util.assert_igual("COMPROBANTE", "Verificación de etiqueta", actual, "Cuenta Destino", false, "N");

        String[] cta = texto.get(6).getText().trim().split("-"); // Tomar los 3 digitos de la cuenta seleccionada
        Util.assert_igual("COMPROBANTE", "Verificación de Cuenta Destino", cta[1].trim(), cuenta, false, "N");
    }
    //7 es banco destino

    public void vp_dato_tcDestino(String alias) {
        String actual = texto.get(5).getText();
        Util.assert_igual("COMPROBANTE", "Verificación de etiqueta", actual, "Tarjeta destino", false, "N");

        String[] tc = texto.get(6).getText().trim().split("-"); // Tomar los 3 digitos de la cuenta seleccionada
        Util.assert_contiene("COMPROBANTE", "Verificación de Tarjeta Destino", tc[0].trim(), alias, false, "N");
    }

    public void vp_dato_ctaOrigen(String cuenta) {
        String actual = texto.get(8).getText();
        Util.assert_igual("COMPROBANTE", "Verificación de etiqueta", actual, "Cuenta Origen", false, "N");

        String[] cta = texto.get(9).getText().split("-"); // Tomar los 3 digitos de la cuenta seleccionada
        Util.assert_igual("COMPROBANTE", "Verificación de Cuenta Origen", cta[1].trim(), cuenta, false, "N");
    }
    //10 es banco origen

    public void click_boton_Continuar() {
        String actual = boton.get(2).getText();
        Util.assert_igual("COMPROBANTE", "Botón Continuar", actual, "CONTINUAR", true, "N");
        boton.get(2).click();
    }

    public void click_boton_MasOpciones() {
        String actual = masopciones.getText();
        Util.assert_igual("COMPROBANTE", "Más Opciones", actual, "MÁS OPCIONES", false, "N");
        masopciones.click();
    }

    public void click_boton_NuevaTransf() {
        try {
            Thread.sleep(1500);
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
        String actual = nueva.getText();
        Util.assert_igual("COMPROBANTE", "Nueva transferencia", actual, "Nueva transferencia", true, "N");
        nueva.click();
    }

    public void click_boton_NuevoPago() {
        String actual = nueva.getText();
        Util.assert_igual("COMPROBANTE", "Nueva transferencia", actual, "Nuevo pago", true, "N");
        nueva.click();
    }
}
