package Pom.Android;

import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class PreClaveVirtualMetodo {
    public PreClaveVirtualMetodo() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time=30, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    MobileElement tecladoPIN = null;

    @WithTimeout(time=30, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> Cancelar = null;

    String[] datos = null;

    @Test
    public void cargaDatosLogin() {
        datos = Util.getCamposDataPool(System.getProperty("user.dir")+"/archivos/dp_login.txt",Util.prop.getProperty("sec_login"));
        if (datos == null)
            System.out.println("setUp==>No hay datos en datapool");
        else
        {
            Util.setDataCliente(datos);
            //Util.logbm.info("setUp==>Datos Leidos de datapool");
        }
    }

    @Test
    public void ingresarMetodoPIN() {
        this.cargaDatosLogin();
        if (datos[9].equals("1")){
           for (int i=1;i<=6;i++) {
             tecladoPIN.click();
           }
        }else{
            Cancelar.get(1).click();
        }
    }

    @Test
    public void ingresarMetodoTOUCHID(){
        //Util.logbm.warning("==>" + " M�todo TOUCHID");
    }
}
