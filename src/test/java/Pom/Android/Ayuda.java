/*
    Instituci�n         :   BANCO BOLIVARIANO, C.A.
    Funcionalidad       :   AYUDA - QUICKPAY RETIRO SIN TARJETA (PROPIO)
    Tester              :   RONALD RODRIGUEZ CASTRO
    Fecha Creaci�n      :   SEPT 28/2021
    Aplicaci�n          :   24m�vil

    Criterios de Aceptaci�n:
        1. Registro Dispositivo Activo
        2. Dispositivo Seguridad Activo
        3. Producto Cuentas Activo
        4. Servicio de QuickPay Activo
        5. Validaci�n de Monto (PROPIO):
            5.1. PROPIAS:
                 5.1.1. Valor m�ltiplo de $10.00
                 5.1.2. Valor monto m�nimo de $10.00
                 5.1.3. Valor monto m�ximo de $1000.00
                 5.1.4. Valor excede saldo disponible
 */
package Pom.Android;

import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.By;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class Ayuda {
    public Ayuda() {
        if (Util.plataforma.equals("iOS"))
            PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
        else
            PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    public String caso = "";

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> radioboton = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.Button[@text='BLOQUEAR TARJETA']")
    AndroidElement boton_Bloquear_Tarjeta = null;

    public void vp_etiqueta_cabecera(String esperado) {
        String actual = texto.get(0).getText();
        Util.assert_igual(caso, "Verificaci�n Cabecera", actual, esperado, false, "N");
    }

    public void click_boton_Opcion(int idx, String esperado) {
        String actual = texto.get(idx).getText();
        //int index = texto.indexOf(esperado);
        //System.out.println("INDICE: "+ index);
        for (AndroidElement x:texto) {
            System.out.println("TEXTO: "+x.getText()+" ID: "+x.getId());
            
        }
        Util.assert_igual(caso, "Presiona bot�n <b>\"" + actual + "\"</b>", actual, esperado, true, "N");
        texto.get(idx).click();
    }
    public void click_boton_Opcion(String esperado){
        try{
            Util.assert_igual(caso, "Presiona bot�n <b>\"" + esperado + "\"</b>", Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
            Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).click();
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
    }


    public void seleccionar_motivo_bloqueo(int idx){
        radioboton.get(idx).click();//4 Reportar como Perdida
                                    //5 Reportar como Robada
    }

    public void click_boton_Bloquear_Tarjeta() {
        Util.assert_igual(caso, "Presiona bot�n Bloquear Tarjeta", boton_Bloquear_Tarjeta.getText(), "BLOQUEAR TARJETA", true, "N");
        boton_Bloquear_Tarjeta.click();
    }

    public void mensaje_exito(String esperado) {
        try {
            Thread.sleep(5000);
            Util.assert_igual(caso, "Mensaje Resultado", Util.driver.findElement(By.xpath("//android.widget.TextView[@text='" + esperado + "']")).getText(), esperado, true, "N");
        } catch (Exception NoSuchElementException) {
            System.out.println("No se encontr� el elemento buscado: " + esperado);
        }
    }

}
