package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.hamcrest.CoreMatchers;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class Transferencia {
    public Transferencia() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time = 20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView[1]")
    MobileElement confirma_monto = null;

    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.widget.TextView[1]")
    MobileElement confirma_cta_orig = null;

    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup[1]/android.widget.TextView[2]")
    MobileElement confirma_tipo_orig = null;

    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.widget.TextView[1]")
    MobileElement confirma_cta_dest = null;

    @WithTimeout(time = 5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.widget.TextView[2]")
    MobileElement confirma_tipo_dest = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
    MobileElement mensaje_exitoso = null;

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> txtedit = null;

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> via = null;

    @WithTimeout(time=6, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
    MobileElement mensaje_error = null;

    @WithTimeout(time=3, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.widget.TextView")
    MobileElement mensaje_loader = null;

    private MobileElement cta_tipo = null;
    private MobileElement cta = null;
    public String getCta_nombre() {
        return cta.getText();
    }

    public MobileElement getCta_tipo() {
        return cta_tipo;
    }
    public void setCta_tipo(MobileElement cta_tipo) {
        this.cta_tipo = cta_tipo;
    }

    public MobileElement getSelectedCta() {
        return cta;
    }

    public void setCta(MobileElement cta) { this.cta = cta;}

    public void vp_etiqueta_CtaDestino() {
        String actual = texto.get(0).getText();
        Util.assert_igual("TRANSFERENCIA", "Verificación de etiqueta", actual, "CUENTA DESTINO", true, "N");
    }

    public void vp_etiqueta_CtaOrigen() {
        String actual = texto.get(0).getText();
        Util.assert_igual("TRANSFERENCIA", "Verificación de etiqueta", actual, "CUENTA ORIGEN", true, "N");
    }

    public void vp_etiqueta_MontoTransferir() {
        String actual = texto.get(0).getText();
        Util.assert_igual("TRANSFERENCIA", "Verificación de etiqueta", actual, "MONTO A TRANSFERIR", true, "N");
    }

    public void click_boton_Cambiar() {
        String actual = boton.get(1).getText();
        Util.assert_igual("TRANSFERENCIA", "Presiona botón Cambiar", actual, "CAMBIAR", false, "N");
        boton.get(1).click();
    }

    public void click_boton_Continuar() {
        String actual = boton.get(2).getText();
        Util.assert_igual("TRANSFERENCIA", "Presiona botón Continuar", actual, "CONTINUAR", false, "N");
        boton.get(2).click();
    }

    public void vp_etiqueta_Confirmacion() {
        String actual = texto.get(0).getText();
        Util.assert_igual("TRANSFERENCIA", "Verificación de etiqueta", actual, "CONFIRMACIÓN", true, "N");
    }

    public void vp_dato_ConfirmaMonto(String monto_ingresado) {
        String[] actual = confirma_monto.getText().split(" ");//Separar USD del monto
        String esperado = monto_ingresado.replace(",",""); //Quitar la coma de decimal
        Util.assert_igual("TRANSFERENCIA", "Verificación de Monto ingresado", actual[0], esperado, false, "N");
    }

    public void vp_dato_ConfirmaCtaOrigen(String ctaorigen_ingresado) {
        String[] actual = confirma_cta_orig.getText().split("-"); // Tomar los 3 digitos de la cuenta seleccionada
        Util.assert_igual("TRANSFERENCIA", "Verificación de Cuenta Origen seleccionada", actual[1], ctaorigen_ingresado, false, "N");
    }

    public void vp_dato_ConfirmaTipoCtaOrigen(String tipoctaorigen_ingresado) {
        String actual = confirma_tipo_orig.getText();
        Util.assert_igual("TRANSFERENCIA", "Verificación de Tipo de Cuenta Origen seleccionado", actual, tipoctaorigen_ingresado, false, "N");
    }

    public void vp_dato_ConfirmaCtaDestino(String ctadestino_ingresado) {
        String[] actual = confirma_cta_dest.getText().split("-");
        Util.assert_igual("TRANSFERENCIA", "Verificación de Cuenta Destino seleccionada", actual[1], ctadestino_ingresado, false, "N");
    }

    public void vp_dato_ConfirmaTipoCtaDestino(String tipoctadestino_ingresado) {
        String actual = confirma_tipo_dest.getText();
        Util.assert_igual("TRANSFERENCIA", "Verificación de Tipo de Cuenta Destino seleccionado", actual, tipoctadestino_ingresado, false, "N");
    }

    public String getConfirmaCtaDestino() {
        String[] conf_ctadest = confirma_cta_dest.getText().split("-");
        return conf_ctadest[1];
    }

    public String getConfirmaTipoCtaDestino() {
        return confirma_tipo_dest.getText();
    }

    public void click_boton_Confirmar() {
        String actual = boton.get(1).getText();
        Util.assert_contiene("TRANSFERENCIA", "Verificación de Tipo de Cuenta Destino seleccionado", actual, "TRANSFERIR", false, "N");
        boton.get(1).click();
    }

    public void vp_mensaje_Confirmacion() throws IOException {
        String actual = mensaje_exitoso.getText();
        Util.assert_contiene("TRANSFERENCIA", "Verificación de Mensaje", actual, "Transacción exitosa", true, "N");
    }

    public void setSelectedCta(int idx, String tipo) //Indice de la cuenta, Tipo de Cuenta Propia, Terceros
    {
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        this.setCta_tipo(null);
        this.setCta(null);

        int cont = 2;//Datos de cuentas empiezan en índice 2

        if (tipo.equals("P")) //Si la cuenta es propia
        {
            if (idx>1)
                cont =  cont + 4*(idx-1); //Titular 2 6 10 Saldo TipoCta SaldoGirar
        }
        else{ //Si la cuenta es de terceros
            if (idx>1)
                cont = cont*idx + 1; //Titular TipoCta
        }
System.out.println("idx:"+idx);
System.out.println("cont:"+cont);
        this.setCta(texto.get(cont));//Titular
        if (tipo.equals("P")) //Si la cuenta es propia
            this.setCta_tipo(texto.get(2 + cont));//Tipo de Cuenta
        else
            this.setCta_tipo(texto.get(1 + cont));//Tipo de Cuenta
    }

    public String getSelectedNumCta(String tipo) //Obtiene número de cuenta
    {
        if (!tipo.equals("O"))
        {
            String[] nombre_cta = null;
            nombre_cta = this.getCta_nombre().split("-");
            return nombre_cta[1];
        }
        else {
            String nombre_cta = null;
            nombre_cta = this.getCta_nombre();
            return nombre_cta;
        }
    }

    public String getSelectedTipoCta()
    {
        return this.getCta_tipo().getText().trim();
    }

    public int numCtasDestino(){//Obtiene número de cuenta
        //Obtiene la cantidad de cuentas destino
        String sec_ctas = texto.get(1).getText().trim();
        int idx1 = sec_ctas.indexOf("(");
        int idx2 = sec_ctas.indexOf(")");
        return Integer.valueOf(sec_ctas.substring(idx1+1, idx2));
    }

    public void seleccionaCta(String tipocta, int ordencta){
        //ordencta = ordencta + 1; // indice indica la posición de la cuenta
        if (ordencta>6) //Para hacer scroll
            Util.driver.pressKey(new KeyEvent(AndroidKey.PAGE_DOWN));
        this.setSelectedCta(ordencta,tipocta); //cta_nombre   cta_tipo  cta
    }

    public void buscaCuenta(String alias) {
        txtedit.get(0).sendKeys(alias); // son 14 caracteres de alias
        Util.driver.pressKey(new KeyEvent(AndroidKey.ENTER));
        Reporte.agregarPaso("TRANSFERENCIA", "Busca Cuenta", alias, "", true, "N");
    }

    public void click_boton_via_Otros() {
        via.get(2).click();
        Reporte.agregarPaso("TRANSFERENCIA", "Presiona vía de la Transferencia", "Transferir a Otros Bancos", "", true, "N");
    }

    public void click_boton_via_Inmediato() {
        via.get(4).click();
        Reporte.agregarPaso("TRANSFERENCIA", "Presiona vía de la Transferencia", "Transferir a Otros Bancos Pago Inmediato", "", true, "N");
    }

    public void vp_etiqueta_msg()
    {
        try {
            String actual = mensaje_error.getText();
            Util.assert_igual("TRANSFERENCIA", "Verificación de Mensaje", actual, "CONFIRMACIÓN", true, "N");
        }
        catch (Exception e) {
            System.out.println("SIN MENSAJE \n" );
        }
    }

    public void vp_verifica_loader(String msgesperado)
    {
        Boolean flag = true;
        int cont = 0;
        while (flag) {
            try {
                String actual = mensaje_loader.getText();
                cont++;
                if (cont == 1) {
                    Reporte.agregarPaso("TRANSFERENCIA", "Loader", msgesperado, "",true, "N");
                }
                if (!actual.equals(msgesperado)) {
                    flag = false;
                    System.out.println("0");
                }
            } catch (Exception e) {
                flag = false;
                System.out.println("1");
                this.vp_etiqueta_msg();
            }
        }
    }
}