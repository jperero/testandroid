package Pom.Android;

import Auxiliar.*;
import java.io.IOException;
import java.time.temporal.ChronoUnit;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class TestLoginBloquearUser {
	public TestLoginBloquearUser() {
		PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
	}

	@WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.EditText")
	MobileElement user = null;

	@WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.EditText")
	MobileElement pssw = null;

	@WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.Button")
	MobileElement btn_ingreso = null;

	@WithTimeout(time=15, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
	MobileElement msg_invalid1 = null;

	@WithTimeout(time=15, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
	MobileElement msg_invalid2 = null;

	@WithTimeout(time=15, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
	MobileElement msg_invalid3 = null;

	String txt_expected = null;
	String[] datos = null;

	@Before
	public void setUp() {
		System.out.println("setUp");
		 datos = Util.getCamposDataPool(System.getProperty("user.dir")+"/archivos/dp_login.txt",Util.prop.getProperty("sec_login"));
		if (datos == null)
			System.out.println("setUp==>No hay datos en datapool");
		else
		{
			Util.setDataCliente(datos);
			//Util.logbm.info("setUp==>Datos cargados de datapool");
		}
	}

	@Test
	public void loginBloquearUserTest() throws InterruptedException, IOException
	{
		//Usuario
		user.sendKeys(datos[1]);

		//Contrase�a inv�lida 1
		pssw.sendKeys("abc");
		Util.driver.pressKey(new KeyEvent(AndroidKey.BACK));
		//Ingresar
		txt_expected = "INGRESAR";
		assertThat(btn_ingreso.getText(), is(txt_expected));
		//Util.logbm.info("==>" + txt_expected);
		btn_ingreso.click();
		//Mensaje
		Util.CapturarImagen();
		assertThat(msg_invalid1.getText(), either(containsString("Usuario y/o contrase�a incorrecta")).or(containsString("Por tu seguridad tu contrase�a se encuentra bloqueada")).or(containsString("Estas bloqueado. Recupera tu contrase�a en Problemas al Ingresar")));
		//Util.logbm.info("==>" + msg_invalid1.getText());
		
		//Contrase�a inv�lida 2
		pssw.sendKeys("abc");
		Util.driver.pressKey(new KeyEvent(AndroidKey.BACK));
		//Ingresar
		txt_expected = "INGRESAR";
		assertThat(btn_ingreso.getText(), is(txt_expected));
		//Util.logbm.info("==>" + txt_expected);
		btn_ingreso.click();
		//Mensaje
		Util.CapturarImagen();
		assertThat(msg_invalid2.getText(), either(containsString("Usuario y/o contrase�a incorrecta")).or(containsString("Por tu seguridad tu contrase�a se encuentra bloqueada")).or(containsString("Estas bloqueado. Recupera tu contrase�a en Problemas al Ingresar")));
		//Util.logbm.info("==>" + msg_invalid2.getText());
		
		//Contrase�a inv�lida 3
		pssw.sendKeys("abc");
		Util.driver.pressKey(new KeyEvent(AndroidKey.BACK));
		//Ingresar
		txt_expected = "INGRESAR";
		assertThat(btn_ingreso.getText(), is(txt_expected));
		//Util.logbm.info("==>" + txt_expected);
		btn_ingreso.click();
		//Mensaje
		Util.CapturarImagen();
		assertThat(msg_invalid3.getText(), either(containsString("Usuario y/o contrase�a incorrecta")).or(containsString("Por tu seguridad tu contrase�a se encuentra bloqueada")).or(containsString("Estas bloqueado. Recupera tu contrase�a en Problemas al Ingresar")));
		//Util.logbm.info("==>" + msg_invalid3.getText());
	}

	@After
	public void tearDown() {
		//Util.driver.quit();
	}
}