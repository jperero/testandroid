package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;
import java.time.temporal.ChronoUnit;

import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ActualizacionApp {
	public ActualizacionApp() {
		if (Util.plataforma.equals("iOS"))
			PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
		else
			PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
	}

	@WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(className = "android.widget.TextView")
	List<AndroidElement> texto = null;

	@WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[3]/android.widget.Button[1]") //64bits
	MobileElement btn_cancela_act =null;

	public void loginActApp()
	{
		try {
			String act= Util.prop.getProperty("verifica_actualizacion");

			if (act.equals("N"))
				return;

			String mensaje = texto.get(6).getText(); //para 64 bits

			if (mensaje.equals("Actualiza la aplicación por favor")) {
				String actual = btn_cancela_act.getText();
				Util.assert_igual("ACTUALIZACIÓN APP", "Verificación de popup de mensaje Optativo", actual, "Cancelar", true, "N");
				btn_cancela_act.click();
			}
		}
		catch (Exception e) {
			Reporte.agregarPaso("ACTUALIZACIÓN APP", "Verificación de versión", "La versión está actualizada", "", false, "N");
		}
	}
}