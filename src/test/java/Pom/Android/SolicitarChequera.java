package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;
import java.time.temporal.ChronoUnit;
import java.util.Iterator;
import java.util.List;
import static Auxiliar.Util.verificarTipoDocumento;

public class SolicitarChequera {
    public SolicitarChequera() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> msj_advertencia = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> listaCuentasCorrientes = null;

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.EditText")
    List<AndroidElement> txtedit = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy (xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup[2]/android.widget.ImageView")
    AndroidElement imgTipoDocumentoIdentidad = null;

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy (xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
    AndroidElement msj_error_rojo = null;

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy (xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
    AndroidElement msj_exito_verde = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    public void validaciones()
    {
        ValidacionesCascada validarCascada = new ValidacionesCascada();

        validarCascada.validarMensajeAdvertencia(msj_advertencia.get(1).getText());
    }

    public void vp_etiqueta_tipochequera()
    {
        String actual = texto.get(1).getText();
        Util.assert_igual("SOLICITUD DE CHEQUERA", "Verificaci�n de etiqueta", actual,"Elige el tipo de chequera que deseas", false, "N");
    }

    public void seleccionarTipoChequera () {
        String [] tipoChequera = {"Elige el tipo de chequera que deseas",
                                  "100 CHEQUES CON TALON",
                                  "100 CHEQUES SIN TALON",
                                  "50 CHEQUES CON TALON",
                                  "50 CHEQUES SIN TALON"};

        for (int i=0; i<texto.size(); i++){
            if(texto.get(i+2).getText().equals(tipoChequera[4])) {
                Reporte.agregarPaso("SOLICITUD DE CHEQUERA", "Selecciona Tipo de Chequera", texto.get(i + 2).getText(), "", true, "N");
                texto.get(i+2).click();
                break;
            }
        }
    }

    public void vp_etiqueta_cuentacte()
    {
        String actual = texto.get(0).getText();
        Util.assert_igual("SOLICITUD DE CHEQUERA","Verificaci�n de etiqueta",actual,"SELECCIONAR CUENTA",true, "N");
    }

    public void seleccionarCuentaCTE () {
        int j=0;
        String lbl_txtTipoCuenta = "Cuenta Corriente";

        for (int i=0; i<texto.size(); i++){
            if (texto.get(i).getText().equals(lbl_txtTipoCuenta)){
                j=i;
                break;
            }
        }
        Reporte.agregarPaso("SOLICITUD DE CHEQUERA","Selecciona Cuenta Corriente",listaCuentasCorrientes.get(j-1).getText(),"",false, "N");
        texto.get(j).click();
    }

    public void vp_etiqueta_oficina()
    {
        String actual = texto.get(0).getText();
        Util.assert_igual("SOLICITUD DE CHEQUERA","Verificaci�n de etiqueta", actual, "OFICINA PARA RETIRAR",true, "N");
    }

    public void seleccionarOficina(String dato) {
        int j=0;
        for (int i=0;i<texto.size(); i++){
            if(texto.get(i).getText().equals(dato)){
                j=i;
                break;
            }
        }
        Util.assert_igual("SOLICITUD DE CHEQUERA","Selecciona Oficina de Retiro Chequera",texto.get(j).getText(),dato,false, "N");
        texto.get(j).click();
    }

    public void vp_etiqueta_quienretira()
    {
        String actual = texto.get(1).getText();
        Util.assert_igual("SOLICITUD DE CHEQUERA","Verificaci�n de etiqueta", actual, "�Qui�n recoger� la chequera?",true, "N");
    }

    public void seleccionar_yoire()
    {
        String actual =texto.get(2).getText();
        Util.assert_igual("SOLICITUD DE CHEQUERA","Selecciona opci�n Yo ir� por la chequera",actual,"Yo ir� por la chequera",false, "N");
        texto.get(2).click();
    }

    public void seleccionar_alguienmasira()
    {
        String actual =texto.get(3).getText();
        Util.assert_igual("SOLICITUD DE CHEQUERA","Selecciona opci�n Alguien m�s ir� por la chequera",actual,"Alguien m�s ir� por la chequera",false, "N");
        texto.get(3).click();
    }

    public Boolean validar_mensaje_cupo(){
        Boolean flagContinua= true;
        try{
            String actual = msj_error_rojo.getText();
            Util.assert_igual("SOLICITUD DE CHEQUERA","Validaci�n de cupo", actual,"Valor excede el cupo disponible",true, "N");
            boton.get(0).click(); // 0-->Bot�n Cancelar
            if (texto.get(0).getText().equals("MIS PRODUCTOS")){
                Salir saliendoAPP = new Salir();
                saliendoAPP.salirAPP();
                flagContinua=false;
            }
        }catch(Exception e){
            /*if (texto.get(0).getText().equals("PERSONA AUTORIZADA")) {
                seleccionaPersonaAutorizada();            }*/
        }
        return flagContinua;
    }

    public void vp_etiqueta_personautoriza()
    {
        String actual = texto.get(0).getText();
        Util.assert_igual("SOLICITUD DE CHEQUERA","Verificaci�n de etiqueta", actual,"PERSONA AUTORIZADA",true, "N");
    }

    public void cargaDatosPersonaAutorizada() {
        String datoTipoDocumento = null;
        String nombrePersonaAutorizada = null;
        String numDocumentoIdentidad = null;

        List<String> personaAutorizada = Util.getCamposDataPool(System.getProperty("user.dir")+"/archivos/dp_solcheq_persona_autorizada.txt");
        Iterator<String> itr = personaAutorizada.iterator();
        String[] campos = null;
        int linea = 0;
        boolean result=false;

        //result = verificarTipoDocumento(Util.getDataCliente()[3],result);
        if (verificarTipoDocumento(Util.getDataCliente()[3],result)){
            datoTipoDocumento= "C�dula";
        }else{
            datoTipoDocumento= "Pasaporte";
        }

        //Recorre archivo: dp_solcheq_persona_autorizada.txt
        while (itr.hasNext()) {
            campos = itr.next().split("\t");
            linea = linea + 1;
            if (linea == 1) //0-->tipo_doc	1-->num_doc    2-->cuenta   3-->nombre_autorizado
                continue;
            if (datoTipoDocumento.equals(campos[0])) {
                numDocumentoIdentidad =   campos[1];
                nombrePersonaAutorizada =   campos[2];
            }
        }
        //Ingresa el Nombre de la Persona Autorizada
        txtedit.get(0).sendKeys(nombrePersonaAutorizada);
        Util.driver.pressKey(new KeyEvent(AndroidKey.ENTER));

        //Selecciona el Tipo de Documento
        imgTipoDocumentoIdentidad.click();//Combo Tipo Documento - Comportamiento at�pico ya que realiza foco en bot�n BACK de la APP
        imgTipoDocumentoIdentidad.click();//Combo Tipo Documento

        for (int j=0; j<texto.size(); j++){ //Recorre combo de lista para seleccionar tipo documento C�dula | Pasaporte | Cancelar
            if (datoTipoDocumento.equals(texto.get(j).getText())){
                texto.get(j).click();
                break;
            }
        }

        //Ingresa el N�mero de Documento de Identidad
        TecladoNumerico tecladoNumerico = new TecladoNumerico();
        switch (datoTipoDocumento) {
            case "C�dula":
                tecladoNumerico.IngresarValor(1,numDocumentoIdentidad,false);
                break;
            case "Pasaporte":
                txtedit.get(1).sendKeys(numDocumentoIdentidad);
                Util.driver.pressKey(new KeyEvent(AndroidKey.ENTER));
                break;
        }

        //Datos de Persona Autorizada
        String cadenaPersonaAutorizada = "</b><br>"+"Persona Autorizada:<br><b>" + nombrePersonaAutorizada + "</b><br>" +
                "Tipo Documento:<br><b>" + datoTipoDocumento + "</b><br>" +
                "N�mero Documento de Identidad:<br><b>" + numDocumentoIdentidad + "</b>";
        Reporte.agregarPaso("SOLICITUD DE CHEQUERA","Ingresa Datos de Persona Autorizada",cadenaPersonaAutorizada,"",true, "N");
    }

    public void click_boton_continuar()
    {
        String actual = boton.get(1).getText();
        Util.assert_igual("SOLICITUD DE CHEQUERA","Presiona bot�n Continuar",actual,"CONTINUAR",false, "N");
        boton.get(1).click();
    }

    public void validar_mensaje_cedula()
    {
        try {//Validaci�n de C�dula
            String actual = msj_error_rojo.getText();
            Util.assert_igual("SOLICITUD DE CHEQUERA", "Validaci�n de D�gito Verificador de C�dula", actual, "El n�mero ingresado no es v�lido", true, "N");
            boton.get(0).click(); // 0-->Bot�n Cancelar
            if (texto.get(0).getText().equals("MIS PRODUCTOS")) {
                Salir saliendoAPP = new Salir();
                saliendoAPP.salirAPP();
            }
        } catch (Exception e) {

        }
    }

    public void vp_etiqueta_confirmacion()
    {
        String actual = texto.get(0).getText();
        Util.assert_igual("SOLICITUD DE CHEQUERA","Verificaci�n de etiqueta",actual,"CONFIRMACI�N",true, "N");
    }

    public void click_boton_solicitar() {
        String actual =boton.get(1).getText();
        Util.assert_igual("SOLICITUD DE CHEQUERA","Presiona el bot�n Solicitar Chequera", actual, "SOLICITAR CHEQUERA",false, "N");
        boton.get(1).click();
    }

    public void validar_mensaje_transaccion() {
        String actual = texto.get(0).getText();//msj_exito_verde.findElement(By.xpath("//*[@text]")).getText();
        System.out.println(actual);
        Util.assert_contiene("SOLICITUD DE CHEQUERA","Verificaci�n de mensaje de transacci�n",actual,"Transacci�n exitosa",true, "N");
    }

}
