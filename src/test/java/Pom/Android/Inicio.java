package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.chrome.ChromeDriver;
import java.net.MalformedURLException;
import java.util.concurrent.TimeUnit;

public class Inicio {

    public String plataforma = "Android";
    public String secDevice = "0";

    public Inicio(String clase)
    {
        //Preparar Log, matriz de pruebas
        try {
            String nclase = clase + ".class";
            //Util.logbm = Logger.getLogger(nclase);
            Util.nombreArchivoLog = clase + "_";
            //Util.generaFileLog();
            //Util.setMatrix_hoja(hoja);
            Reporte.setNombreArchivo(clase);

        } catch (SecurityException e) {
            e.printStackTrace();
            //Util.logbm.warning(e.getMessage());
        }
    }

    private void iniciaReporte(){
        Reporte.setNombreReporte("");
        Reporte.setEntorno("");

    }

    /*INICIAR APPs*/
    public void startApp() {
        try
        {
            this.iniciaReporte();

            if (plataforma.equals("iOS"))
                Util.iniciaAppiOs(this.secDevice);
            else
                Util.iniciaApp(this.secDevice);

            Reporte.agregarPaso("INICIO DE LA APP", "Verificación de Splash", "Cargando Splash", "", true , "N");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    public void startBrowser() {
        System.setProperty("webdriver.chrome.driver", "E://Automatizacion//TestBO//driver//chromedriver.exe");
        Util.webdriver = new ChromeDriver();
        Util.webdriver.manage().window().maximize();
        JavascriptExecutor js;
        String url ="http://internal-aa6b053ca1b4c11ea94d90eaeb2d65b9-985227668.us-east-1.elb.amazonaws.com:9081/limsp-ui/";
                //"internal-aa6b053ca1b4c11ea94d90eaeb2d65b9-985227668.us-east-1.elb.amazonaws.com:9081/limsp-ui/";
        Util.webdriver.get(url);
        Util.webdriver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
    }

}
