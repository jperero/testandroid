package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;
import java.time.temporal.ChronoUnit;

public class TestTransferenciaTercero {
    public TestTransferenciaTercero() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.view.ViewGroup[2]/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView[1]")
    MobileElement monto = null;

    private String _alias = null;
    public String getAlias() {
        return _alias;
    }
    public void setAlias(String alias) {
        _alias = alias;
    }

    private int _ordencta = 0;
    public int getOrdencta() {return _ordencta;}
    public void setOrdencta(int ordencta) {
        this._ordencta = ordencta;
    }

    private String _monto = "0";
    public String getMonto() {return _monto;}

    public void setMonto(String monto) {
        this._monto = monto;
    }

    private boolean _favorito = false;
    public boolean isFavorito() {return _favorito;}

    public void setFavorito(boolean favorito) {
        this._favorito = favorito;
    }

    private String _tipotransf = null;
    public String getTipotransf() {return _tipotransf;}
    public void setTipotransf(String tipotransf) {
        _tipotransf = tipotransf;
    }

    public void TransferenciaTerceroTest() {
        String sel_ctaorig = null;
        String sel_tipoctaorig = null;
        String sel_ctadest = null;
        String sel_tipoctadest = null;

        Transferencia transf = new Transferencia();

        //*Escojo Cuenta Destino//
        transf.vp_etiqueta_CtaDestino();
        transf.buscaCuenta(this.getAlias());
        //transf.seleccionaCta(this.getTipotransf(), 1); //Se selecciona la primera cuenta de la lista
        transf.seleccionaCta(this.getTipotransf(), 1); //Se selecciona la primera cuenta de la lista
        sel_ctadest = transf.getSelectedNumCta(this.getTipotransf());
        sel_tipoctadest = transf.getSelectedTipoCta();
        transf.getSelectedCta().click();

        if (this.getTipotransf().equals("O"))
        {
            transf.click_boton_via_Otros();
        }
        //Ingreso Monto
        transf.vp_etiqueta_MontoTransferir();
        transf.click_boton_Cambiar();

        //*Escojo  Cuenta Origen//
        transf.vp_etiqueta_CtaOrigen();
        int numctas = transf.numCtasDestino();

        if (this.getOrdencta() <= numctas) {
            System.out.println("getOrdencta:" + this.getOrdencta());
            System.out.println("numctas:" + numctas);
            transf.seleccionaCta("P", this.getOrdencta());
            sel_ctaorig = transf.getSelectedNumCta("P");
            sel_tipoctaorig = transf.getSelectedTipoCta();
            transf.getSelectedCta().click();
            if(this.getOrdencta()+1>numctas)
                this.setOrdencta(0);//Si ya se recorrieron todas la ctas origen
            else
                this.setOrdencta(this.getOrdencta()+1);
        }

        TecladoNumerico teclado = new TecladoNumerico();
        teclado.IngresarValor(2, this.getMonto(), true);
        String monto_ingresado = monto.getText();
        Reporte.agregarPaso("TECLADO", "Ingresa Monto", monto_ingresado,"", true, "N");

        transf.click_boton_Continuar();
        //Thread.sleep(2000);

        //**DATOS DE ENTRADA VS DATOS DE CONFIRMACION
        transf.vp_etiqueta_Confirmacion();
        //Monto a Transferir
        transf.vp_dato_ConfirmaMonto(monto_ingresado);
        //Cta origen
        transf.vp_dato_ConfirmaCtaOrigen(sel_ctaorig);
        transf.vp_dato_ConfirmaTipoCtaOrigen(sel_tipoctaorig);
        //Cta destino
        if (this.getTipotransf().equals("O"))
        {
            sel_ctadest = transf.getConfirmaCtaDestino();
            sel_tipoctadest = transf.getConfirmaTipoCtaDestino();
        }
        else
        {
            transf.vp_dato_ConfirmaCtaDestino(sel_ctadest);
            transf.vp_dato_ConfirmaTipoCtaDestino(sel_tipoctadest);
        }

        //Confirmar
        transf.click_boton_Confirmar();

        //SI NO ES FAVORITO
        if (!this.isFavorito())
        {
            OTP otp = new OTP();
            otp.genera_valida_Otp();
            //otp.vp_verifica_loader("Estamos transfiriendo tu dinero");
        }

        transf.vp_verifica_loader("Estamos transfiriendo tu dinero");

        //COMPROBANTE
        Comprobante comp = new Comprobante();
        comp.vp_etiqueta_Confirmacion();
        comp.vp_etiqueta_Exitosa();
        comp.vp_dato_Monto(monto_ingresado);
        comp.vp_dato_ctaDestino(sel_ctadest);
        comp.vp_dato_ctaOrigen(sel_ctaorig);
        comp.click_boton_MasOpciones();
        comp.click_boton_NuevaTransf();
    }
}
