package Pom.Android;

import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class RegistroIPInternacional {
    public RegistroIPInternacional(){PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);}

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.view.ViewGroup")
    List<AndroidElement> botonG = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> imagen = null;

    boolean flag = true;

    public boolean vp_cabecera_registroIP(){
        String cabecera = texto.get(0).getText();
        if(!cabecera.equals("REGISTRO")) {
            flag = false;
        }
        return flag;
    }

    public void vp_etiqueta_viaje(){
        String actual = texto.get(1).getText();
        Util.assert_igual("REGISTRO IP INTERNACIONAL", "Verificaci�n de etiqueta de fuera de viaje", actual,"Sabemos que te encuentras de viaje", true, "N");
    }
    public void click_boton_Aceptar(){
        String actual = boton.get(0).getText();
        Util.assert_igual("REGISTRO IP INTERNACIONAL", "Presiona bot�n Aceptar", actual, "ACEPTAR", true, "N");
        boton.get(0).click();//0: ACEPTAR  1: NO GRACIAS
    }
    public void click_boton_NoGracias(){
        String actual = boton.get(1).getText();
        Util.assert_igual("REGISTRO IP INTERNACIONAL", "Presiona bot�n No Gracias", actual, "NO GRACIAS", true, "N");
        boton.get(1).click();//0: ACEPTAR  1: NO GRACIAS
    }
}
