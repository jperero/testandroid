package Pom.Android;

import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class MisProductos {
    public MisProductos() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time=5, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView")
    MobileElement mensaje_error = null;

    public Boolean flag_ipinternacional = false;

    public void vp_etiqueta_msg()
    {
        try {
            String actual = mensaje_error.getText();
            System.out.println("MisProductos->"+actual);
            Util.assert_igual("LOGIN", "Verificación de acceso al canal", actual, "MIS PRODUCTOS", true, "N");
        }
        catch (Exception e) {
            //Util.logbm.info("LOGIN SIN MENSAJE DE ERROR \n" );
        }
    }

    public void vp_etiqueta_misproductos()
    {
        String actual = texto.get(0).getText();
        if(actual.equals("REGISTRO"))
            flag_ipinternacional =  true;
        else
            Util.assert_igual("LOGIN", "Verificación de título en Posición Consolidada", actual, "Mis productos", true, "N");
    }


}
