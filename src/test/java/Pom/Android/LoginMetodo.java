package Pom.Android;

import Auxiliar.*;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.junit.Test;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class LoginMetodo {
    public LoginMetodo() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time=30, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.ImageView")
    List<AndroidElement> imagen = null;

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView[1]")
    MobileElement txt_metodo = null;

    @WithTimeout(time=15, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "//android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.TextView")
    MobileElement btn_cancelar = null;

    String txt_expected = null;

    public void metodo_TouchID() throws IOException
    {
        try {
            System.out.println(imagen.size());
            imagen.get(6).getText(); //hay 6 imagenes en la pantalla preliminar con toudhId 7
            txt_expected = "Touch ID";
            assertThat(txt_metodo.getText(), is(txt_expected));
            //Util.logbm.info("==>" + txt_expected);
            Util.CapturarImagen();
            this.click_boton_Cancelar();
        }
        catch (Exception e)
        {
            //Util.logbm.info("NO EXISTE METODO DE ACCESO CONFIGURADO " + e.getMessage());
        }
    }

    void click_boton_Cancelar()
    {
        txt_expected = "CANCELAR";
        assertThat(btn_cancelar.getText(), is(txt_expected));
        //Util.logbm.warning("==>" + btn_cancelar.getText());
        btn_cancelar.click();
    }
}
