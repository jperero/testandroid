package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class PreLoginMetodo {
    public PreLoginMetodo() {
        if (Util.plataforma.equals("iOS"))
            PageFactory.initElements(new AppiumFieldDecorator(Util.driveriOS), this);
        else
            PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    MobileElement tecladoPIN = null;

    @WithTimeout(time=2, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> btn_cancelar = null;

    @WithTimeout(time=1, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(id = "com.samsung.android.biometrics.app.setting:id/sem_fingerprint_bg_cancel_button")
    MobileElement btn_cancelar_android = null;

    @WithTimeout(time=10, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    List<AndroidElement> texto = null;

    @WithTimeout(time=2, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.widget.TextView[1]")
    MobileElement nombre_metodo = null;

    @WithTimeout(time=2, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[2]/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.TextView")
    MobileElement btn_cancelar_touch = null;


    String[] datos = null;
    public Boolean ingreso_sin_metodo = false;

    public void cargaDatosLogin(String seclogin) {
        if (seclogin.equals("0"))
            seclogin = Util.prop.getProperty("sec_login");

        datos = Util.getCamposDataPool(System.getProperty("user.dir")+"/archivos/dp_login.txt", seclogin);
        if (datos == null)
            System.out.println("setUp==>No hay datos en datapool");
        else
        {
            Util.setDataCliente(datos);
            System.out.println("setUp==>Datos Leidos de datapool");
        }
    }

    public void ingresarMetodoPIN(String nombreMetodo) {
        if (Util.getDataCliente()[8].equals("1"))
        {
           for (int i=1;i<=6;i++) {
             tecladoPIN.click();
           }
        }
        else {
                btn_cancelar.get(7).click();
                /*LoginIngresar ingreso = new LoginIngresar();
                ingreso.loginIngresar();*/
        }
        Reporte.agregarPaso("METODO DE ACCESO", "Verificaci�n de M�todo", nombreMetodo,"", false, "N");
        this.ingreso_sin_metodo=false;
    }

    void click_boton_Cancelar_touch() {
        String actual = btn_cancelar_touch.getText();
        Util.assert_igual("METODO DE ACCESO", "Presiona el bot�n Cancelar ", actual, "CANCELAR", true, "N");
        btn_cancelar_touch.click();
    }

    public void ingresarMetodoTOUCHID(String nombreMetodo) {
        this.click_boton_Cancelar_touch();
        Reporte.agregarPaso("METODO DE ACCESO", "Verificaci�n de M�todo de acceso", nombreMetodo,"", false, "N");
        this.ingreso_sin_metodo=false;
    }

    void click_boton_Cancelar_touch_Nativo() {
        try {
            String mensaje = btn_cancelar_android.getText();
            if (mensaje.equals("CANCELAR")) {
                btn_cancelar_android.click();
            }
        }
        catch (Exception e) {
            System.out.println("No existe TouchId nativo");
        }
    }

    public void verifica_metodo_acceso()
    {
        this.click_boton_Cancelar_touch_Nativo();
        try {
            String nombreMetodo = nombre_metodo.getText();//texto.get(6).getText();
            System.out.println("NOMBRE METODO: "+nombre_metodo.getText());

            switch (nombreMetodo) { //Encuentra el texto del popup del m�todo
                case "Ingresa Clave de Acceso":
                    this.ingresarMetodoPIN(nombreMetodo);
                    break;
                case "Touch ID":
                    this.ingresarMetodoTOUCHID(nombreMetodo);
                    break;
                default:// si no hay m�todo y pasa a la pantalla login pero sale el popup de Tiempo de sesi�n
                    Reporte.agregarPaso("METODO DE ACCESO", "Verificaci�n de M�todo", "Sin M�todo de acceso configurado","", false, "N");
                    this.ingreso_sin_metodo=true;
                    break;
            }
        } catch (Exception e) { // pas� a pantalla login
            Reporte.agregarPaso("METODO DE ACCESO", "Verificaci�n de M�todo", "Sin M�todo de acceso configurado","", false, "N");
            this.ingreso_sin_metodo=true;
        }
    }
}
