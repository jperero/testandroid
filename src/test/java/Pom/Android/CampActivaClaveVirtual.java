package Pom.Android;

import Auxiliar.Reporte;
import Auxiliar.Util;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class CampActivaClaveVirtual {
    public CampActivaClaveVirtual() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time=3, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup[1]/android.view.ViewGroup/android.view.ViewGroup[1]/android.widget.TextView[2]")
    MobileElement popup_cv_msg = null;

    @WithTimeout(time=20, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    List<AndroidElement> boton = null;

    public void vp_etiqueta_camp()
    {
        String actual = popup_cv_msg.getText();
        Util.assert_contiene("CAMPA�A ACTIVA CLAVE VIRTUAL", "Verificaci�n del popup de campa�a Activa Clave Virtual", actual, "Comienza a generar tus claves para autorizar todas tus transacciones", true, "N");
    }

    public void click_boton_Activar()
    {
        String actual = boton.get(0).getText();
        Util.assert_igual("CAMPA�A ACTIVA CLAVE VIRTUAL", "Click en bot�n Activar", actual, "Activar",false, "N");
        boton.get(0).click();
    }

    public void click_boton_AhoraNo()
    {
        String actual = boton.get(1).getText();
        Util.assert_igual("CAMPA�A ACTIVA CLAVE VIRTUAL", "Click en bot�n Ahora No", actual, "Ahora no",false, "N");
        boton.get(1).click();
    }

    public void ahoraNo()
    {
        try{
            this.vp_etiqueta_camp();
            this.click_boton_AhoraNo();
        }
        catch(Exception e)
        {
            Reporte.agregarPaso("CAMPA�A ACTIVA CLAVE VIRTUAL", "Verificaci�n de Campa�a", "No se muestra Popup", "", false, "N");
        }

    }

    public void activarClave()
    {
        try{
            this.vp_etiqueta_camp();
            this.click_boton_Activar();
        }
        catch(Exception e)
        {
            Reporte.agregarPaso("CAMPA�A ACTIVA CLAVE VIRTUAL", "Verificaci�n de Campa�a", "No se muestra Popup", "", false, "N");
        }
    }
}
