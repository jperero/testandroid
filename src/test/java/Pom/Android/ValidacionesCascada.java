package Pom.Android;

import Auxiliar.Util;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.WithTimeout;
import org.openqa.selenium.support.PageFactory;

import java.time.temporal.ChronoUnit;
import java.util.List;

public class ValidacionesCascada {
    public ValidacionesCascada() {
        PageFactory.initElements(new AppiumFieldDecorator(Util.driver), this);
    }

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.Button")
    private List<AndroidElement> btn_advertencia = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    private List<AndroidElement> msj_noctacte = null;

    @WithTimeout(time = 25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    private List<AndroidElement> btn_aceptar = null;

    @WithTimeout(time=25, chronoUnit = ChronoUnit.SECONDS)
    @AndroidFindBy(className = "android.widget.TextView")
    private List<AndroidElement> cab_titulo = null;

    String mensajeAdvertencia = null;
    private String[] txtMsjNoCtaCte = {"Necesitas una cuenta corriente para poder solicitar una chequera",
                                       "Tu dispositivo de seguridad se encuentra bloqueado definitivamente.",
                                       "No tienes activo el servicio de QuickPay",
                                       "Lo sentimos, servicio no disponible por el momento. Int�ntalo mas tarde"};

    static int i = 0;
    static boolean numeroDocumentoConLetras = false;
    Salir masSalir = new Salir();

    public void validarMensajeAdvertencia(String mensaje) {
        String txt_titulo = null;
        int opcionMenu=0;
        switch (mensaje) {
            case "Para transaccionar debes registrar tu dispositivo m�vil":
                mensajeAdvertencia = "Para transaccionar debes registrar tu dispositivo m�vil";
                Util.assert_igual(cab_titulo.get(0).getText(),"Validaci�n - No posee Registro Equipo",mensaje,mensajeAdvertencia,true, "N");

                txt_titulo=cab_titulo.get(0).getText();
                switch (txt_titulo){
                    case "SOLICITAR CHEQUERA":
                        btn_advertencia.get(1).click();
                        opcionMenu=9;
                        break;
                    case "RETIRO SIN TARJETA":
                        btn_advertencia.get(0).click();
                        opcionMenu=5;
                        break;
                }
                //Realiza Registro de Equipo y Retoma el flujo selecciando Opci�n M�S:
                //- Retiro Sin Tarjeta (Item: 5)
                //- Solicitar Chequera (Item: 9)
                TestRegistroEquipo registro = new TestRegistroEquipo();
                registro.registroTest();

                Menu opcMenu = new Menu();
                opcMenu.presionaMenu(opcionMenu);
                break;
            case "Para realizar esta transacci�n debes activar tu Clave Virtual":
                mensajeAdvertencia = "Para realizar esta transacci�n debes activar tu Clave Virtual";
                Util.assert_igual(cab_titulo.get(0).getText(),"Validaci�n - No posee Dispositivo Seguridad",mensaje,mensajeAdvertencia,true, "N");

                //24/11/2020 12:36 Se debe enganchar con el flujo de Activaci�n de Clave Virtual, cuando este terminado
                txt_titulo=cab_titulo.get(0).getText();
                switch (txt_titulo){
                    case "SOLICITAR CHEQUERA":
                        btn_advertencia.get(1).click();
                        break;
                    case "RETIRO SIN TARJETA":
                        btn_advertencia.get(0).click();
                        break;
                }
                break;
            case "Debes registrar tu dispositivo m�vil para configurar tu cuenta predeterminada":
                mensajeAdvertencia = "Debes registrar tu dispositivo m�vil para configurar tu cuenta predeterminada";
                Util.assert_igual(cab_titulo.get(0).getText(),"Validaci�n - Registro de Equipo",mensaje,mensajeAdvertencia,true, "N");
                btn_aceptar.get(2).click();
                break;
            case "Debes tener cuentas para realizar esta configuraci�n":
                mensajeAdvertencia = "Debes tener cuentas para realizar esta configuraci�n";
                Util.assert_igual(cab_titulo.get(0).getText(),"Validaci�n - Producto Cuentas",mensaje,mensajeAdvertencia,true, "N");
                btn_aceptar.get(2).click();
                break;
            case "Para configurar tu cuenta predeterminada debes crear tu nuevo usuario en 24online":
                mensajeAdvertencia = "Para configurar tu cuenta predeterminada debes crear tu nuevo usuario en 24online";
                Util.assert_igual(cab_titulo.get(0).getText(),"Validaci�n - Usuario Migrado",mensaje,mensajeAdvertencia,true, "N");
                btn_aceptar.get(2).click();
                break;
            case "Tu cuenta est� configurada como predeterminada":
                mensajeAdvertencia = "Tu cuenta est� configurada como predeterminada";
                Util.assert_igual(cab_titulo.get(0).getText(),"Validaci�n - Cuenta �nica",mensaje,mensajeAdvertencia,true, "N");
                btn_aceptar.get(2).click();
                break;
            case "No posees cuenta para realizar esta transacci�n":
                mensajeAdvertencia = "No posees cuenta para realizar esta transacci�n";
                Util.assert_igual(cab_titulo.get(0).getText(),"Validaci�n - No posee Producto Cuenta ",mensaje,mensajeAdvertencia,true, "N");
                masSalir.massalirAPP();
                break;
            case "Lo sentimos intentelo mas tarde":
                mensajeAdvertencia = "Lo sentimos intentelo mas tarde";
                Util.assert_igual(cab_titulo.get(0).getText(),"Validaci�n - Caida del Servicio",mensaje,mensajeAdvertencia,true, "N");
                masSalir.massalirAPP();
                break;
            default:
                System.out.println("==> CUMPLE CON TODAS LAS VALIDACIONES PRELIMINARES");
        }
    }

    public String validarMensajeAdvertencia(String mensaje, String txt_expected)  {
        switch (mensaje) {
            //Mensaje de Validaci�n Producto Cuentas Corrientes (SI/NO)
            case "Necesitas una cuenta corriente para poder solicitar una chequera":
                txt_expected = "MIS PRODUCTOS";
                Util.assert_igual("SOLICITUD DE CHEQUERA","Validaci�n - No posee Cuenta Corriente",mensaje,txtMsjNoCtaCte[0],true, "N");
                msj_noctacte.get(2).click();
                break;
            //Mensaje de Validaci�n Dispositivo de Seguridad Bloqueado
            case "Tu dispositivo de seguridad se encuentra bloqueado definitivamente.":
                txt_expected = "MIS PRODUCTOS";
                Util.assert_igual("SOLICITUD DE CHEQUERA","Validaci�n - Dispositivo Seguridad Bloqueado",mensaje,txtMsjNoCtaCte[1],true, "N");
                msj_noctacte.get(3).click();
                break;
            //Mensaje de Validaci�n Servicio QUICKPAY (Activo / Inactivo)
            case "No tienes activo el servicio de QuickPay":
                txt_expected = "MIS PRODUCTOS";
                Util.assert_igual("QUICKPAY - RETIRO SIN TARJETA","Validaci�n - Servicio QP (Activo | Inactivo)",mensaje,txtMsjNoCtaCte[2],true, "N");
                i=Util.indiceVerificarTipoDocumento(Util.getDataCliente()[3],numeroDocumentoConLetras);
                if (i==3) {
                    //msj_noctacte.get(2).click();<-- SE DEBE DESCOMENTAR ESTA LINEA CUANDO SE MIGRE QP,ACTUALMENTE VA A CANCELAR ACTIVACION
                    msj_noctacte.get(1).click();
                }else {
                    msj_noctacte.get(1).click();
                }
                break;
            //Mensaje de Validaci�n Lista Negra / Segmento y Subsegmento para Dep�sito Express
            case "Lo sentimos, servicio no disponible por el momento. Int�ntalo mas tarde":
                txt_expected = "MIS PRODUCTOS";
                Util.assert_igual("ACTIVAR DEPOSITO EXPRESS","Validaci�n - Lista Negra / Segmento y Subsegmento",mensaje,txtMsjNoCtaCte[3],true, "N");
        }
        return txt_expected;
    }

    public String validarMensajeEmergente(String mensaje){
        String txt_expected=null;
        switch (mensaje){
            case "�Transacci�n exitosa!":
                txt_expected="�Transacci�n exitosa!";
                break;
            case "CUENTA BLOQUEADA PARA MOVIMIENTOS":
                txt_expected="CUENTA BLOQUEADA PARA MOVIMIENTOS";
                break;
            case "TRANSACCION ERRONEA, VERIFICA EL SALDO DE TU CUENTA":
                txt_expected="TRANSACCION ERRONEA, VERIFICA EL SALDO DE TU CUENTA";
                break;
            case "su saldo disponible no le permite realizar esta transacci�n":
                txt_expected="su saldo disponible no le permite realizar esta transacci�n";
                break;
            case "Has configurado tu cuenta predeterminada con �xito":
                txt_expected="Has configurado tu cuenta predeterminada con �xito";
                break;
            case "El monto m�nimo es $10":
                txt_expected="El monto m�nimo es $10";
                break;
            case "Valor excede al saldo disponible":
                txt_expected="Valor excede al saldo disponible";
                break;
            case "El monto ingresado excede al valor permitido":
                txt_expected="El monto ingresado excede al valor permitido";
                break;
/*            case "�Transacci�n exitosa! N�mero de referencia:":
                txt_expected="�Transacci�n exitosa! N�mero de referencia:";
                break;*/
            default:
                txt_expected = mensaje;
        }
        return txt_expected;
    }
}
