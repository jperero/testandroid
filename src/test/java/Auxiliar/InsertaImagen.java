package Auxiliar;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.util.IOUtils;

public class InsertaImagen {
    public static void toExcel(String archivoimagen)
    {
        try {
            FileInputStream filex = new FileInputStream(Util.prop.getProperty("ruta_matriz"));

            Workbook wb = WorkbookFactory.create(filex);
            Sheet sheet = wb.getSheet(Util.getMatrix_hoja());
            //XSSFWorkbook wb = new XSSFWorkbook();
            //Sheet sheet = wb.createSheet("Anexos");
            //Sheet sheet = wb.getSheet("Anexos");
            if (sheet == null)
                sheet = wb.createSheet(Util.getMatrix_hoja());
            //FileInputStream obtains input bytes from the image file
            InputStream inputStream = new FileInputStream(archivoimagen);
            //Get the contents of an InputStream as a byte[].
            byte[] bytes = IOUtils.toByteArray(inputStream);
            //Adds a picture to the workbook
            int pictureIdx = wb.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
            //close the input stream
            inputStream.close();

            //Returns an object that handles instantiating concrete classes
            CreationHelper helper = wb.getCreationHelper();

            //Creates the top-level drawing patriarch.
            Drawing drawing = sheet.createDrawingPatriarch();

            //Create an anchor that is attached to the worksheet
            //ClientAnchor anchor = helper.createClientAnchor();
            //set top-left corner for the image

            XSSFClientAnchor anchor = new XSSFClientAnchor();
            int anchocol = 4, altorow = 28;
            int row1 = 2;
            int col2 = Util.getColinicial() + anchocol;

            anchor.setRow1(row1);
            anchor.setCol1(Util.getColinicial());
            anchor.setRow2(row1 + altorow);
            anchor.setCol2(col2);

            anchor.setDx1(0);
            anchor.setDx2(0);
            anchor.setDy1(0);
            anchor.setDy2(0);

            Util.setColinicial(col2 + 1);
            //Creates a picture
            Picture pict = drawing.createPicture(anchor, pictureIdx);
            //Reset the image to the original size
            //pict.resize();

            //Write the Excel file
            FileOutputStream fileOut = null;
            fileOut = new FileOutputStream(System.getProperty("user.dir") + Util.prop.getProperty("ruta_matriz"), false);
            wb.write(fileOut);
            fileOut.close();
        }
        catch (Exception e)
        {
            //Util.logbm.warning(e.getMessage());
        }

    }

}
