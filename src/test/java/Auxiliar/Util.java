package Auxiliar;

import com.sun.deploy.security.SelectableSecurityManager;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.touch.TapOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.apache.commons.io.FileUtils;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static java.lang.Integer.parseInt;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class Util {
	public static AndroidDriver<AndroidElement> driver;
	public static IOSDriver<IOSElement> driveriOS;
	public static AndroidDriver<AndroidElement> drvBrowser;
	public static WebDriver webdriver;
	public static Properties prop = null;
	public static WebDriverWait wait = null;
	public static String nombreArchivoLog;
	public static File captura = null;
	private static String[] dataCliente;
	private static String[] dataDispositivo;
	private static String num_ctasmat = null;
	private static int colinicial = 0;
	private static String matrix_hoja = null;

	public static String[] getDataCliente() {
		return dataCliente;
	}
	public static void setDataCliente(String[] datos) {
		Util.dataCliente = datos;
	}

	public static String[] getDataDispositivo() {
		return dataDispositivo;
	}

	public static int getColinicial() {
		return colinicial;
	}

	public static void setColinicial(int colinicial) {
		Util.colinicial = colinicial;
	}
	
	public static String getMatrix_hoja() {
		return matrix_hoja;
	}

	public static String appPackageBrowser = "com.android.chrome";
	public static String appActivityBrowser = "com.google.android.apps.chrome.Main";

	public static String plataforma = "Android";

	public static void iniciaApp(String secDevice) throws MalformedURLException
	{
		InicioConfig();
		if (secDevice.equals("0"))
			secDevice = Util.prop.getProperty("sec_dispositivo_and");

		dataDispositivo = Util.getCamposDataPool(System.getProperty("user.dir")+"/archivos/dp_dispositivos.txt", secDevice);

		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		desiredCapabilities.setCapability("deviceName", dataDispositivo[1]);
		desiredCapabilities.setCapability("udid", dataDispositivo[2]);
		desiredCapabilities.setCapability("platformName", dataDispositivo[3]);
		desiredCapabilities.setCapability("platformVersion", dataDispositivo[4]);
		desiredCapabilities.setCapability("appPackage", dataDispositivo[5]);
		desiredCapabilities.setCapability("appActivity", dataDispositivo[6]);
		desiredCapabilities.setCapability("newCommandTimeout", dataDispositivo[7]);
		desiredCapabilities.setCapability("skipServerInstallation", true);
		
		boolean noreset = false;
		if (dataDispositivo[8].equals("1"))
			noreset = true;

		desiredCapabilities.setCapability("noReset", noreset); //noReset false, resetea la cach�
		URL remoteUrl = new URL("http://localhost:4723/wd/hub");

		Util.driver = new AndroidDriver<AndroidElement>(remoteUrl, desiredCapabilities);

		//int segundos =  Integer.parseInt(dataDispositivo[9]);
		//Util.driver.manage().timeouts().implicitlyWait(segundos, TimeUnit.SECONDS);
		//Util.wait = new WebDriverWait(Util.driver,segundos); //segundos
		//Util.logbm.log(Level.INFO, "Inicia App");
	}

	public static void iniciaAppiOs(String secDevice) throws MalformedURLException
	{
		InicioConfig();
		if (secDevice.equals("0"))
			secDevice = Util.prop.getProperty("sec_dispositivo_ios");

		dataDispositivo = Util.getCamposDataPool(System.getProperty("user.dir")+"/archivos/dp_dispositivos.txt", secDevice);

		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		desiredCapabilities.setCapability("deviceName", dataDispositivo[1]);
		desiredCapabilities.setCapability("udid", dataDispositivo[2]);
		desiredCapabilities.setCapability("platformName", dataDispositivo[3]);
		desiredCapabilities.setCapability("platformVersion", dataDispositivo[4]);
		desiredCapabilities.setCapability("bundleId", dataDispositivo[5]);
		desiredCapabilities.setCapability("xcodeOrgId", dataDispositivo[6]);
		desiredCapabilities.setCapability("xcodeSigningId", "iPhone Developer");
		desiredCapabilities.setCapability("automationName", "XCUITest");
		desiredCapabilities.setCapability("skipServerInstallation", true);

		boolean noreset = false;
		if (dataDispositivo[8].equals("1"))
			noreset = true;

		desiredCapabilities.setCapability("noReset", noreset); //noReset false, resetea la cach�

		URL remoteUrl = new URL("http://localhost:4723/wd/hub");
		Util.driveriOS = new IOSDriver<IOSElement>(remoteUrl, desiredCapabilities);
		//Util.driveriOS.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

		Util.plataforma = "iOS";
	}

	public static MobileElement Buscar_xpath(String path)
	{
		return (MobileElement) Util.driver.findElement(By.xpath(path));
	}

	static void InicioConfig() {
		prop = new Properties();
		try {
			prop.load(new FileInputStream(System.getProperty("user.dir")+"/config.properties"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/*public static void generaFileLog() throws SecurityException, IOException
	{
		TimeZone tz = TimeZone.getTimeZone("EST"); // or PST, MID, etc ...
		Date now = new Date();
		DateFormat df = new SimpleDateFormat ("yyyyMMddhhmmss");
		df.setTimeZone(tz);
		String currentTime = df.format(now);

		prop = new Properties();
		try {
			//prop.load(new FileInputStream("E://AutomatizacionPruebas//SCRIPTS//config.properties"));
			prop.load(new FileInputStream(System.getProperty("user.dir")+"/config.properties"));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();	
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//String Archivo = System.getProperty("user.dir") + "/logs/" + nombreArchivoLog + "_" + currentTime + ".log";
		String Directorio = System.getProperty("user.dir") + Util.prop.getProperty("ruta_logs") + "/" ;//"E:/AutomatizacionPruebas/SCRIPTS/Logs/";
		String Archivo = Directorio + nombreArchivoLog + "_" + currentTime + ".log";

		boolean success = (new File(Directorio)).mkdirs();
		if (success) {
			System.out.println("Directories: " + Archivo + " created");
		}

		FileHandler fh = new FileHandler(Archivo);
		logbm.addHandler(fh);
		SimpleFormatter formatter = new SimpleFormatter();  
		fh.setFormatter(formatter);	
	}*/

	public static List<String> getCamposDataPool(String fileName)
	{
		List<String> lines = Collections.emptyList();
		try
		{
			lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return lines;
	}

	public static String[] getCamposDataPool(String fileName, String numregistro )
	{
		List<String> lines = Collections.emptyList();
		String[] campos = null;
		try
		{
			lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
			Iterator<String> itr = lines.iterator();

			while (itr.hasNext())
			{
				campos = itr.next().split("\t");
				if (campos[0].equals(numregistro))
				{
					break;					
				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return campos;
	}

	public static String[] getCamposDataPool(String fileName, String datocompara, int idx)
	{
		List<String> lines = Collections.emptyList();
		String[] campos = null;
		try
		{
			lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
			Iterator<String> itr = lines.iterator();

			while (itr.hasNext())
			{
				campos = itr.next().split("\t");
				if (campos[idx].equals(datocompara))
				{
					break;
				}
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return campos;
	}

	public static void TocarPantalla(int x, int y)
	{
		//Tap en la coordenada
		(new TouchAction(driver))
		.tap(TapOptions.tapOptions().withPosition(PointOption.point(x, y)))
		.waitAction()
		.perform();
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("tap " + x + "," + y);
	}

	public static void TocarPantalla(int x1, int y1, int x2, int y2)
	{
		(new TouchAction(driver))
		.press(PointOption.point(x1, y1))
		.moveTo(PointOption.point(x2, y2))
		.release()
		.waitAction()
		.perform();
		System.out.println("swipe de " + x1 + "," + y1 + " a " + x2 + "," + y2);
	}

	public static void RefrescarBusq()
	{
		//Tap en cuadro de b�squeda especifica
		//TocarPantalla(145,225);
		Dimension windowSize = Util.driver.manage().window().getSize();
        TocarPantalla(150,(int)(windowSize.getHeight()*0.17));
		Util.driver.pressKey(new KeyEvent(AndroidKey.BACK));
		System.out.println("<");
	}

	public static void CapturarImagen()
	{
		TimeZone tz = TimeZone.getTimeZone("EST"); // or PST, MID, etc ...
		Date now = new Date();
		DateFormat df = new SimpleDateFormat ("yyyyMMddHHmmss");
		df.setTimeZone(tz);
		String currentTime = df.format(now);

		if (Util.plataforma.equals("iOS"))
			Util.captura  = ((TakesScreenshot)Util.driveriOS).getScreenshotAs(OutputType.FILE);
		else
			Util.captura  = ((TakesScreenshot)Util.driver).getScreenshotAs(OutputType.FILE);
	}

	public static Boolean verificarTipoDocumento(String numeroDocumentoIdentidad, Boolean result){
		for (char ch : numeroDocumentoIdentidad.toCharArray()) {
			if (!Character.isDigit(ch)) {
				result = true;
				break;
			}
		}
		return result;
	}
	public static int indiceVerificarTipoDocumento(String numeroDocumentoIdentidad, Boolean result){
		int i = 0;
		result = verificarTipoDocumento(numeroDocumentoIdentidad, result);
		if (result){
			//Cliente con Pasaporte
			//Solicitud de Chequera: Selecciona - Alguien irá por la chequera
			//QP - Retiro Sin Tarjeta: Activa Servicio QP
			//QP - Retiro Sin Tarjeta: Selecciona - Alguien más hará el retiro
			i=3;
		}else{
			if ((parseInt(numeroDocumentoIdentidad) % 2) == 0){
				//Cliente con Cédula PAR
				//Solicitud de Chequera: Selecciona - Yo iré por la chequera
				//QP - Retiro Sin Tarjeta: No Activa Servicio QP
				//QP - Retiro Sin Tarjeta: Selecciona - Yo haré el retiro
				i=2;
			}else{
				//Cliente con Cédula IMPAR
				//Solicitud de Chequera: Selecciona - Alguien irá por la chequera
				//QP - Retiro Sin Tarjeta: Activa Servicio QP
				//QP - Retiro Sin Tarjeta: Selecciona - Alguien más hará el retiro
				i=3;
			}
		}
		return i;
	}
	public static String tipoVerificarTipoDocumento(String numeroDocumentoIdentidad, String tipoDocumento,Boolean result){
		result = verificarTipoDocumento(numeroDocumentoIdentidad, result);
		if (result){
			tipoDocumento="P"; // Cliente con Pasaporte, el Autorizado será con Pasaporte
		}else {
			if ((parseInt(numeroDocumentoIdentidad) % 2) == 0){
				tipoDocumento = "C";// Cliente con Cédula PAR, el Autorizado será con Cédula
			} else {
				tipoDocumento = "P";// Cliente con Cédula IMPAR, el Autorizado será con Pasaporte
			}
		}
		return tipoDocumento;
	}

	public static String tipoVerificarTipoDocumentoRapido(String numeroDocumentoIdentidad, String tipoDocumento,Boolean result){
		if (result){
			tipoDocumento="P"; // Cliente con Pasaporte, el Autorizado será con Pasaporte
		}else {
			if ((parseInt(numeroDocumentoIdentidad) % 2) == 0){
				tipoDocumento = "C";// Cliente con Cédula PAR, el Autorizado será con Cédula
			} else {
				tipoDocumento = "P";// Cliente con Cédula IMPAR, el Autorizado será con Pasaporte
			}
		}
		return tipoDocumento;
	}


	public static void assert_igual(String caso, String detalle, String actual, String esperado, Boolean captura, String categoria)
	{
		try
		{
			assertThat(actual, is(esperado));
			Reporte.agregarPaso(caso, detalle, actual, esperado, captura, categoria);
		}
		catch (AssertionError e) {
			Reporte.agregarPaso(caso, detalle, e.getMessage(), esperado, captura, 1, categoria);
			Util.fallo(e.getMessage());
		}
	}

	public static void assert_contiene(String caso, String detalle, String actual, String esperado, Boolean captura, String categoria)
	{
		try
		{
			assertThat(actual, CoreMatchers.containsString(esperado));
			Reporte.agregarPaso(caso, detalle, actual, esperado, captura, categoria);
		}
		catch (AssertionError e) {
			Reporte.agregarPaso(caso, detalle, e.getMessage(), esperado, captura, 1, categoria);
			Util.fallo(e.getMessage());
		}
	}

	public static void assert_contiene(String caso, String detalle, String actual, String esperado1, String esperado2, Boolean captura, String categoria)
	{
		try
		{
			assertThat(actual, either(containsString(esperado1)).or(containsString(esperado2)));
			Reporte.agregarPaso(caso, detalle, actual, "", captura, categoria);
		}
		catch (AssertionError e) {
			Reporte.agregarPaso(caso, detalle, e.getMessage(), "", captura, 1, categoria);
			Util.fallo(e.getMessage());
		}
	}

	public static void assert_contiene(String caso, String detalle, String actual, String esperado1, String esperado2, String esperado3, Boolean captura, String categoria)
	{
		try
		{
			assertThat(actual, either(containsString(esperado1)).or(containsString(esperado2)).or(containsString(esperado3)));
			Reporte.agregarPaso(caso, detalle, actual, "", captura, categoria);
		}
		catch (AssertionError e) {
			Reporte.agregarPaso(caso, detalle, e.getMessage(), "", captura, 1, categoria);
			Util.fallo(e.getMessage());
		}
	}

    public static void assert_noigual(String caso, String detalle, String actual, String esperado, Boolean captura, String categoria)
    {
        try
        {
            assertThat(actual, is(not(esperado)));
            Reporte.agregarPaso(caso, detalle, actual, esperado, captura, categoria);
        }
        catch (AssertionError e) {
            Reporte.agregarPaso(caso, detalle, e.getMessage(), esperado, captura, 1, categoria);
			Util.fallo(e.getMessage());
        }
    }

    static void fallo(String msgerror)
	{
		System.out.println(msgerror);
		Assert.fail();

	}
}
