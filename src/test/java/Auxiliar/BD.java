package Auxiliar;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class BD {
	Connection dbconn= null;
	
	public BD() 
	{
		try
		{
			//Connect to the database
			//Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			dbconn = DriverManager.getConnection("jdbc:sqlserver://172.16.23.44:2668;databaseName=db_sms;user=user_testing;password=P@ssw0rd;");
			System.out.println("Connected to the database");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public String ConsultarOTP(String identificacion) throws InterruptedException
	{
		String otp = null;
		ResultSet rs = null;
		Statement sqlStatement = null;
		try
		{
			//declare the statement object
			sqlStatement = dbconn.createStatement();
			//declare the result set    
			
			//Build the query string, making sure to use column aliases
			String queryString="Select pr_valor ";
			queryString+="From db_sms.dbo.sm_par_req_recibidos_retir ";
			queryString+="Where pr_requerimiento in (Select	Top 1 rr_requerimiento ";
			queryString+="From db_sms.dbo.sm_req_recibidos_retir ";
			queryString+="Where rr_login = Isnull('"+ identificacion +"', rr_login) ";
			queryString+="And rr_servicio In('QOTPI', 'OTPIN') ";
			queryString+="And rr_canal = 'WAP' ";
			queryString+="Order By rr_requerimiento Desc) ";
			queryString+="and pr_parametro = 'MATRICULA' ";

			//print the query string to the screen
			//System.out.println("\nQuery string:\n");
			//System.out.println(queryString);

			//execute the query
			rs=sqlStatement.executeQuery(queryString);
			//System.out.println(queryString);
			while (rs.next()) {
				otp = rs.getString("pr_valor");
				if (otp!= null)
					System.out.println("OTP:"+rs.getString("pr_valor"));
				else
					System.out.println("No se encontr� OTP");
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		    if (rs != null) {
		        try {
		            rs.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (sqlStatement != null) {
		        try {
		        	sqlStatement.close();
		        } catch (SQLException e) { /* ignored */}
		    }
		    if (dbconn != null) {
		        try {
		        	dbconn.close();
		        	System.out.println("Conexi�n cerrada");
		        } catch (SQLException e) { /* ignored */}
		    }
		}
		return otp;
	}
}