Sec	Escenario	Valor	Validacion
1	Múltiplo de 10	1100	Tu monto ingresado debe ser múltiplo de 10
2	Monto Mínimo	10	El monto mínimo es
3	Monto Máximo	200000	El monto ingresado excede al valor permitido
4	Saldo Disponible	50	Valor excede al saldo disponible
5	Saldo Disponible	1000	Valor válido