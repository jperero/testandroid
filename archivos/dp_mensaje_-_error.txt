cod_error	mensaje_error
1	El número de identificación ingresado es inválido
2	Código dactilar ingresado es inválido
3	Ya cuentas con usuario y contraseña para acceder a 24móvil
4	Lo sentimos no pudimos establecer conexión, inténtalo más tarde.
5	Lo sentimos inténtalo más tarde
6	El usuario no se encuentra disponible. Inténtalo nuevamente.